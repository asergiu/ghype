#ifndef CCLIENTINFO_H
#define CCLIENTINFO_H

#include <QObject>
#include <QList>

#include <QXmppVCardIq.h>
#include <QXmppPresence.h>
#include <QQmlListProperty>

#include "cpp/message-items/_message.h"
#include "cpp/message-items/_messagemodel.h"
#include "cpp/contact/cprofileinfo.h"

class CMessage;
class CFileTransfer;
class CCommunicationItem;
class QXmppMessage;
class CContact : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString jid READ getJid NOTIFY sJidChanged())
    Q_PROPERTY(CProfileInfo* profile READ getProfile CONSTANT)
    Q_PROPERTY(bool composing READ composing NOTIFY sComposingChanged())
    Q_PROPERTY(MType type READ getType NOTIFY sPresenceChanged())
    Q_PROPERTY(MAvailableStatusType availableType READ getAvailableType NOTIFY sPresenceChanged())
    Q_PROPERTY(bool hasUnreadMessages READ hasUnreadMessages WRITE setHasUnreadMessages NOTIFY sHasUnreadMessagesChanged())
    Q_PROPERTY(MessageModel* messagesModel READ getMessagesModel NOTIFY sMessagesModelChanged)
    Q_PROPERTY(int msgHistoryChunkSize READ getMsgHistoryChunkSize)
    Q_PROPERTY(bool canRequestHistoryLogs READ canRequestHistoryLogs)
    Q_ENUMS(MType)
    Q_ENUMS(MAvailableStatusType)

public:
    CContact(QObject *parent=0);
    CContact(QString jid, QObject *parent=0);
    ~CContact();

    //>>status type
    /* *** ! BE AWARE ! ask clau if you want to modify *** */
    enum MType /* *** the same enum as in QXmppPresence (redefined here such that the enum can be used in QML) *** */
    {
        Error = 0,
        Available,
        Unavailable,
        Subscribe,
        Subscribed,
        Unsubscribe,
        Unsubscribed,
        Probe
    };
    QStringList m_types;
    //<<status type

    //>>status available type
    /* *** ! BE AWARE ! ask clau if you want to modify *** */
    enum MAvailableStatusType /* *** the same enum as in QXmppPresence (redefined here such that the enum can be used in QML) *** */
    {
        Online=0,
        Away,
        XA,
        DND,
        Chat
    };
    QStringList m_availableStatusTypes;
    //<<status available type

    /* *** Q INVOKABLE *** */
    Q_INVOKABLE QStringList getTypes(){
        return m_types;
    }
    Q_INVOKABLE QStringList getAvailableTypes(){
        return m_availableStatusTypes;
    }

    /* *** QML read *** */
    QString imagePath();
    QString xmlInfoPath();
    QString getJid();
    Q_INVOKABLE QString getJidUsername();
    CProfileInfo* getProfile();
    bool composing();
    MType getType();
    MAvailableStatusType getAvailableType();
    bool hasUnreadMessages();
    MessageModel* getMessagesModel();

    /* *** QML write *** */
    void setImagePath(QString imagePath);
    void setXmlInfoPath(QString xmlPath);
    void setJid(QString jid);
    void setPresence(QXmppPresence presence);
    void setHasUnreadMessages(bool hasUnreadMessages);

    /* *** functions *** */
    void addMessage(CCommunicationItem *message, bool emitSignal = true, bool toBack = true);
    void markMessage(QString id, int mark);
    void clearMessages();
    bool abortFileTransfer(QString uid);
    CFileTransfer* findFileTransfer(QString uid);
    CMessage* findMessage(QString id);
    void setVCard(QXmppVCardIq vCard, bool saveOnDisk);
    void setIsComposing(bool composing);

    int getMsgHistoryChunkStart(){return mMsgHistoryChunkStart;}
    int getMsgHistoryChunkSize(){return mMsgHistoryChunkSize;}

    bool canRequestHistoryLogs(){return m_requestHistoryLogs;}
    void setCanRequestHistoryLogs(bool req){m_requestHistoryLogs = req;}

    Q_INVOKABLE bool saveToFile();
    bool readFromFile();

signals:
    void sJidChanged();
    void sPresenceChanged();
    void sMessagesModelChanged();
    void sHasUnreadMessagesChanged();
    void sComposingChanged();
    void sFinishedFileTransfer(QString fileSid, QString contactJid);

private:
    QString m_imagePath;
    QString m_xmlInfoPath;
    QString m_jid;
    bool m_isComposing;
    QXmppPresence m_presence;
    int mMsgHistoryChunkStart;
    int mMsgHistoryChunkSize;
    MessageModel *m_messagesModel;
    bool m_hasUnreadMessages;
    CProfileInfo *m_profileInfo;
    bool m_requestHistoryLogs;/* *** this boolean has the role to enable/disable history requests for a specific contact.
                                For the future versions this solution must be rethink if history requests must be enable even if all history logs have been received*** */
};

#endif // CCLIENTINFO_H
