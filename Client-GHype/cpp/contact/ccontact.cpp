#include "ccontact.h"

//#include <ghype-protocol/cfilelog.h>
//#include <ghype-protocol/ghypefilerequest.h>
#include <QStandardPaths>
#include <QDir>

#include "../message-items/ccommunicationitem.h"
#include "../message-items/cmessage.h"
#include "../message-items/cfiletransfer.h"


#include <qdebug.h>

CContact::CContact(QObject *parent) :
    QObject(parent)
{
    m_types << "Error" << "Available" << "Unavailable" << "Subscribe" << "Subscribed" << "Unsubscribe" << "Unsubscribed" << "Probe";
    m_availableStatusTypes << "Online" << "Away" << "XA" << "DND" << "Chat";
    m_hasUnreadMessages = false;
    m_isComposing = false;
    mMsgHistoryChunkStart = 0;
    mMsgHistoryChunkSize = 50;
    m_messagesModel = new MessageModel(this);
    m_profileInfo = new CProfileInfo(m_jid, this);
    m_requestHistoryLogs = true;
}

CContact::CContact(QString jid, QObject *parent) :
    CContact(parent)
{
    m_jid = jid;
    m_profileInfo->setJid(m_jid);
    m_presence.setType(QXmppPresence::Unavailable);
}

CContact::~CContact()
{
    delete m_messagesModel;
    m_messagesModel = NULL;

    delete m_profileInfo;
    m_profileInfo = NULL;
}

void CContact::addMessage(CCommunicationItem *message, bool emitSignal, bool toBack)
{
    if(message->itemType() == CCommunicationItem::MESSAGE){
        if( ((CMessage*)message)->marker() == QXmppMessage::Displayed){
            m_hasUnreadMessages = true;
            emit sHasUnreadMessagesChanged();
        }
    }

    if(message->itemType() == CCommunicationItem::FILE_TRANSFER)
    {
        if(((CFileTransfer*)message)->direction() == CFileTransfer::IncomingDirection)
        {
            m_hasUnreadMessages = true;
            emit sHasUnreadMessagesChanged();
        }
    }

    mMsgHistoryChunkStart++;

    if(toBack) m_messagesModel->addMessageBack(message);
    else m_messagesModel->addMessageFront(message);

    if(emitSignal) emit sMessagesModelChanged();
}

void CContact::markMessage(QString id, int marker)
{
    m_messagesModel->markMessage(id, marker);
    emit sMessagesModelChanged();
}

void CContact::setVCard(QXmppVCardIq vCard, bool saveOnDisk){
    m_profileInfo->setVCard(vCard);
    if(saveOnDisk) {
        saveToFile();
    }
}

void CContact::clearMessages()
{
    m_messagesModel->clearMessages();
}

bool CContact::abortFileTransfer(QString uid)
{
    return m_messagesModel->abortFileTransfer(uid);
}

CFileTransfer* CContact::findFileTransfer(QString uid)
{
    return m_messagesModel->findFileTransfer(uid);
}

CMessage* CContact::findMessage(QString id)
{
    return m_messagesModel->findMessage(id);
}


bool CContact::saveToFile()
{
    QString appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/";
    QString contactDirPath = appDataPath + getJidUsername() + "/";

    QDir dir, contactDir;
    if(!dir.exists(appDataPath))
    {
        bool ret = dir.mkdir(appDataPath);
        if(!ret) {qDebug()<<Q_FUNC_INFO<<" cannot create directory: " << appDataPath; return false;}
    }

    if(!contactDir.exists(contactDirPath))
    {
        bool ret = contactDir.mkdir(contactDirPath);
        if(!ret) {qDebug()<<Q_FUNC_INFO<<" cannot create sub-directory: " << contactDirPath; return false;}
    }

    QFile file(contactDirPath + "profile.xml");
    file.resize(0);
    bool ret = file.open(QIODevice::ReadWrite);
    if(!ret) {qDebug()<<Q_FUNC_INFO<<" cannot open file: " << contactDirPath + "profile.xml"; return false;}

    QString avatarPath = contactDirPath + "avatar.jpg";;
    m_profileInfo->createAndSaveAvatarImage(avatarPath);

    QXmlStreamWriter stream(&file);
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    stream.writeStartElement("profile");
    stream.writeAttribute("jid", m_jid);
    stream.writeTextElement("nickname", m_profileInfo->nickname());
    stream.writeTextElement("firstname", m_profileInfo->firstName());
    stream.writeTextElement("lastname", m_profileInfo->lastName());
    stream.writeTextElement("email", m_profileInfo->email());
    stream.writeTextElement("birthdate", m_profileInfo->birthday().toString("dd/MM/yyyy"));
    stream.writeTextElement("imagename", m_profileInfo->imagePath());
    stream.writeTextElement("description", m_profileInfo->description());
    stream.writeTextElement("country", m_profileInfo->country());
    stream.writeTextElement("city", m_profileInfo->city());
    stream.writeTextElement("telephone", m_profileInfo->telephone());
    stream.writeTextElement("webpage", m_profileInfo->webPage());
    stream.writeEndElement(); // profile
    stream.writeEndDocument();

    file.close();

    return true;
}

bool CContact::readFromFile()
{
    QString appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/";
    QString contactDirPath = appDataPath + getJidUsername() + "/";

    QDir dir, contactDir;
    if(!dir.exists(appDataPath))
    {
        qDebug()<<Q_FUNC_INFO<<" directory does not exist: " << appDataPath;
        return false;
    }

    if(!contactDir.exists(contactDirPath))
    {
        qDebug()<<Q_FUNC_INFO<<" directory does not exist: " << contactDirPath;
        return false;
    }

    QFile file(contactDirPath + "profile.xml");
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<Q_FUNC_INFO<<" cannot read file: " << contactDirPath + "profile.xml";
        return false;
    }

    QXmlStreamReader stream(&file);
    stream.readNext();
    while (!stream.atEnd()){
        if(stream.isStartElement()){
            QStringRef name = stream.name();
            QString value;
            if(name == "profile")
            {//PROFILE TAG
            }
            else if(name == "nickname")
            {
                value = stream.readElementText();
                m_profileInfo->setNickname(value);
            }
            else if(name == "firstname")
            {
                value = stream.readElementText();
                m_profileInfo->setFirstname(value);
            }
            else if(name == "lastname")
            {
                value = stream.readElementText();
                m_profileInfo->setLastName(value);
            }
            else if(name == "email")
            {
                value = stream.readElementText();
                m_profileInfo->setEmail(value);
            }
            else if(name == "birthdate")
            {
                value = stream.readElementText();
                QDate date;
                date = QDate::fromString(value, "dd/MM/yyyy");
                m_profileInfo->setBirthdate(date);
            }
            else if(name == "imagename")
            {
                value = stream.readElementText();
                m_profileInfo->setImagePath(value);
            }
            else if(name == "description")
            {
                value = stream.readElementText();
                m_profileInfo->setDescription(value);
            }
            else if(name == "country")
            {
                value = stream.readElementText();
                m_profileInfo->setCountry(value);
            }
            else if(name == "city")
            {
                value = stream.readElementText();
                m_profileInfo->setCity(value);
            }
            else if(name == "telephone")
            {
                value = stream.readElementText();
                m_profileInfo->setTelephone(value);
            }
            else if(name == "webpage")
            {
                value = stream.readElementText();
                m_profileInfo->setWebpage(value);
            }
        }
        stream.readNext();
    }
    if (stream.hasError()) {
        qDebug()<<Q_FUNC_INFO<<"Error! An error has occured while parsing the xml file "<< contactDirPath << "profile.xml. Error: "<<stream.error()<<" "<<stream.errorString();
    }

    return true;
}

/* *** Q QML PROPERTY READ *** */
QString CContact::imagePath()
{
    return m_imagePath;
}
QString CContact::xmlInfoPath()
{
    return m_xmlInfoPath;
}
QString CContact::getJid()
{
    return m_jid;
}

QString CContact::getJidUsername()
{
    int i = m_jid.indexOf('@');
    QString jidUsername = m_jid.mid(0, i);
    return jidUsername;
}

bool CContact::composing()
{
    return m_isComposing;
}

CContact::MType CContact::getType()
{
    return static_cast<MType>(m_presence.type());
}
CContact::MAvailableStatusType CContact::getAvailableType()
{
    return static_cast<MAvailableStatusType>(m_presence.availableStatusType());
}

bool CContact::hasUnreadMessages()
{
    return m_hasUnreadMessages;
}

CProfileInfo* CContact::getProfile()
{
    return m_profileInfo;
}

MessageModel * CContact::getMessagesModel()
{
    return m_messagesModel;
}

void CContact::setIsComposing(bool composing)
{
    m_isComposing = composing;
    emit sComposingChanged();
}

/* *** Q QML PROPERTY WRITE *** */
void CContact::setImagePath(QString imagePath)
{
    m_imagePath = imagePath;
}
void CContact::setXmlInfoPath(QString xmlPath)
{
    m_xmlInfoPath = xmlPath;
}
void CContact::setJid(QString jid)
{
    m_jid = jid; /* *** don't emit signal jidChanged *** */;
}

void CContact::setPresence(QXmppPresence presence)
{
    m_presence = presence;
    emit sPresenceChanged();
}
void CContact::setHasUnreadMessages(bool hasUnreadMessages)
{
    m_hasUnreadMessages = hasUnreadMessages;
    m_messagesModel->acknoledgeMessages();
    emit sHasUnreadMessagesChanged();
}

