#ifndef CGROUP
#define CGROUP

#include <QObject>
#include <QQmlListProperty>

class CContact;
class CGroup : public QObject{

    Q_OBJECT
public:
    CGroup();
    CGroup(const QString &name, QObject *parent=0);
    ~CGroup();

    Q_PROPERTY(QString groupName READ getName CONSTANT)
    Q_PROPERTY(QQmlListProperty<CContact> contacts READ getContacts /*NOTIFY sContactsChanged*/ CONSTANT)

    QString getName();
    QQmlListProperty<CContact> getContacts();

    void removeContact(CContact *contact);
    void addContact(CContact *contact);
    void setContacts(QList<CContact*> contacts);

signals:
//    void sNameChanged();
//    void sContactsChanged();
private:
    QString m_groupName;
    QList<CContact*> m_contacts;
};

#endif // CGROUP

