#include "cgroup.h"
#include <qdebug.h>

CGroup::CGroup()
{

}

CGroup::CGroup(const QString &name, QObject *parent):QObject(parent)
{
    m_groupName = name;
}

CGroup::~CGroup()
{

}

QString CGroup::getName()
{
    return m_groupName;
}

QQmlListProperty<CContact> CGroup::getContacts()
{
    return QQmlListProperty<CContact>(this, m_contacts);
}

void CGroup::removeContact(CContact *contact)
{
    int ret = m_contacts.removeAll(contact);
    if(ret<0) qDebug()<<Q_FUNC_INFO<<" Warning! 0 contacts removed from group.";
}

void CGroup::addContact(CContact *contact)
{
    if(!m_contacts.contains(contact))
        m_contacts.append(contact);
}

void CGroup::setContacts(QList<CContact *> contacts){m_contacts = contacts;}
