#ifndef CPROFILEINFO_H
#define CPROFILEINFO_H

#include <QObject>
#include <QVariant>
#include <QSettings>
#include <QStringList>
#include "QXmppVCardIq.h"

class CProfileInfo: public QObject{
    Q_OBJECT

    Q_PROPERTY(QString nickname READ nickname WRITE setNickname NOTIFY nicknameChanged)
    Q_PROPERTY(QString firstName READ firstName WRITE setFirstname NOTIFY firstNameChanged)
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY lastNameChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(QDate birthday READ birthday WRITE setBirthdate NOTIFY birthdayChanged)
    Q_PROPERTY(QString imagePath READ imagePath WRITE setImagePath NOTIFY imagePathChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString country READ country WRITE setCountry NOTIFY countryChanged)
    Q_PROPERTY(QString city READ city WRITE setCity NOTIFY cityChanged)
    Q_PROPERTY(QString telephone READ telephone WRITE setTelephone NOTIFY telephoneChanged)
    Q_PROPERTY(QString webPage READ webPage WRITE setWebpage NOTIFY webPageChanged)
    Q_PROPERTY(QString fullName READ fullName)

public:
    CProfileInfo(QObject *parent = 0);
    CProfileInfo(const QString &jid, QObject *parent = 0);

    QString nickname() const;
    void setNickname(const QString &nickname);
    QString firstName() const;
    void setFirstname(const QString &firstname);
    QString lastName() const;
    void setLastName(const QString &lastname);
    QString email() const;
    void setEmail(const QString &email);
    QDate birthday() const;
    void setBirthdate(const QDate &birthdate);
    QString imagePath();
    void setImagePath(QString imagePath);
    QString description() const;
    void setDescription(const QString &description);
    QString country() const;
    void setCountry(const QString &country);
    QString city() const;
    void setCity(const QString &city);
    QString telephone() const;
    void setTelephone(const QString &telephone);
    QString webPage();
    void setWebpage(const QString &webpage);
    QString jid() const;
    void setJid(QString jid);

    QString fullName() const;

    QXmppVCardIq getVCard();
    void setVCard(const QXmppVCardIq vCard);

    void createAndSaveAvatarImage(QString avatarPath);

signals:
    void nicknameChanged();
    void firstNameChanged();
    void lastNameChanged();
    void emailChanged();
    void birthdayChanged();
    void imagePathChanged();
    void descriptionChanged();
    void countryChanged();
    void cityChanged();
    void telephoneChanged();
    void webPageChanged();

private:
    QXmppVCardIq m_vCard;
    QString m_imagePath;
    QString m_jid;
};


#endif // CPROFILEINFO_H
