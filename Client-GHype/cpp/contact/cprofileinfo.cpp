#include "cprofileinfo.h"

#include <QDebug>
#include <QBuffer>
#include <QFile>
#include <QImage>
#include <QDir>
#include <QStandardPaths>

CProfileInfo::CProfileInfo(QObject *parent) : QObject(parent)
{
    // Default constructor for qml usage. Don't change anything here, check the second contructor!
}

CProfileInfo::CProfileInfo(const QString &jid, QObject *parent) : QObject(parent)
{
    m_jid = jid;
}

QString CProfileInfo::nickname() const
{
    return m_vCard.nickName();
}

void CProfileInfo::setNickname(const QString &name){
    m_vCard.setNickName(name);
    emit nicknameChanged();
}

QString CProfileInfo::firstName() const
{
    return m_vCard.firstName();
}

void CProfileInfo::setFirstname(const QString &firstname)
{
    m_vCard.setFirstName(firstname);
    emit firstNameChanged();
}

QString CProfileInfo::lastName() const
{
    return m_vCard.lastName();
}

void CProfileInfo::setLastName(const QString &lastname)
{
    m_vCard.setLastName(lastname);
    emit lastNameChanged();
}

QString CProfileInfo::email() const
{
    return m_vCard.email();
}

void CProfileInfo::setEmail(const QString &email)
{
    m_vCard.setEmail(email);
    emit emailChanged();
}

QDate CProfileInfo::birthday() const
{
    return m_vCard.birthday().isNull() ? QDate(1950, 1, 1) : m_vCard.birthday();
}

void CProfileInfo::setBirthdate(const QDate &birthdate)
{
    m_vCard.setBirthday(birthdate);
    emit birthdayChanged();
}

QString CProfileInfo::imagePath()
{
    if(!m_imagePath.isEmpty())
    {
        if(m_imagePath.contains("file:/")){
            return m_imagePath;
        }
        else{
#ifdef Q_OS_WIN
            return "file:///" + m_imagePath;
#endif
#ifdef Q_OS_UNIX
            return "file://" + m_imagePath;
#endif
        }
    }
    else{
        return "";
    }
}

void CProfileInfo::setImagePath(QString imagePath)
{
#ifdef Q_OS_WIN
    imagePath = imagePath.replace("file:///", "");
#endif
#ifdef Q_OS_MACX
    imagePath = imagePath.replace("file://", "");
#endif

    if(QFile::exists(imagePath))
    {
        QByteArray ba;
        QImage fullImg(imagePath);
        const QSize requestedImageSize = QSize(100, 100);
        QImage image = fullImg.scaled(requestedImageSize, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);

        QBuffer buffer(&ba);
        buffer.open(QBuffer::WriteOnly);
        if(image.save(&buffer, "JPG"))
        {
            m_vCard.setPhoto(ba);
            m_imagePath = imagePath;
        }
        else
        {
            qDebug()<<Q_FUNC_INFO<<" Error! Cannot find image : "<<imagePath<<". Path set to empty string";
            m_imagePath = "";
        }
    }
    else
    {
        qDebug()<<Q_FUNC_INFO<<" Warning! Image does not exist";
        m_imagePath = "";
    }

    emit imagePathChanged();
}

QString CProfileInfo::description() const
{
    return m_vCard.description();
}

void CProfileInfo::setDescription(const QString &description)
{
    m_vCard.setDescription(description);
    emit descriptionChanged();
}

QString CProfileInfo::country() const
{
    if(m_vCard.addresses().size()>0)
        return m_vCard.addresses().at(0).country();
    return "";
}

void CProfileInfo::setCountry(const QString &country)
{
    QXmppVCardAddress address;
    address.setCountry(country);
    if(m_vCard.addresses().size()>0)
        address.setLocality(m_vCard.addresses()[0].locality());
    QList<QXmppVCardAddress> addresses;
    addresses.append(address);
    m_vCard.setAddresses(addresses);

    emit countryChanged();
}

QString CProfileInfo::city() const
{
    if(m_vCard.addresses().size()>0)
        return m_vCard.addresses().at(0).locality();

    return "";
}

void CProfileInfo::setCity(const QString &city)
{
    QXmppVCardAddress address;
    address.setLocality(city);
    if(m_vCard.addresses().size()>0)
        address.setCountry(m_vCard.addresses()[0].country());
    QList<QXmppVCardAddress> addresses;
    addresses.append(address);
    m_vCard.setAddresses(addresses);

    emit cityChanged();
}

QString CProfileInfo::telephone() const
{
    if(m_vCard.phones().size()>0)
        return m_vCard.phones().at(0).number();

    return "";
}

void CProfileInfo::setTelephone(const QString &telephone)
{
    QXmppVCardPhone phone;
    phone.setNumber(telephone);
    QList<QXmppVCardPhone> phones;
    phones.append(phone);
    m_vCard.setPhones(phones);

    emit telephoneChanged();
}

QString CProfileInfo::webPage()
{
    return m_vCard.url();
}

void CProfileInfo::setWebpage(const QString &webpage)
{
    m_vCard.setUrl(webpage);
    emit webPageChanged();
}

QString CProfileInfo::jid() const
{
    return m_jid;
}

void CProfileInfo::setJid(QString jid)
{
    m_jid = jid;
}

QString CProfileInfo::fullName() const
{
    if (m_vCard.firstName().isEmpty() && m_vCard.lastName().isEmpty()) {
        if (m_jid.isEmpty()) {
            return "";
        } else {
            int i = m_jid.indexOf('@');
            QString jidUsername = m_jid.mid(0, i);
            return jidUsername;
        }
    } else {
        return m_vCard.firstName() + " " + m_vCard.lastName();
    }
}

void CProfileInfo::setVCard(QXmppVCardIq vCard)
{
    m_vCard = vCard;

    emit nicknameChanged();
    emit firstNameChanged();
    emit lastNameChanged();
    emit emailChanged();
    emit birthdayChanged();
    emit imagePathChanged();
    emit descriptionChanged();
    emit countryChanged();
    emit cityChanged();
    emit telephoneChanged();
    emit webPageChanged();
}

void CProfileInfo::createAndSaveAvatarImage(QString avatarPath)
{
    QByteArray imageArray = m_vCard.photo();
    if(!imageArray.isEmpty())
    {
        QImage avatarImage;
        bool ret = avatarImage.loadFromData(imageArray);
        bool saveOK = avatarImage.save(avatarPath);
        if(saveOK)
        {
            m_imagePath = avatarPath;
        }
        else
        {
            m_imagePath = "";
            qDebug()<<Q_FUNC_INFO<<"Error! Cannot save image file! with path: "<<avatarPath;
        }
    }
    else
    {
        m_imagePath = "";
        qDebug()<<Q_FUNC_INFO<<"Image array is empty!";
    }
    emit imagePathChanged();
}

QXmppVCardIq CProfileInfo::getVCard()
{
    return m_vCard;
}
