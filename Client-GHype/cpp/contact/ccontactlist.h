#ifndef CCONTACTLIST
#define CCONTACTLIST

#include <QList>
#include <QObject>
#include "ccontact.h"

template <class T>
class CContactList : public QList<T>{

public:
    const T &getContactByJid(const QString jid) const{
        int size = this->size();
        for(int i=0;i<size;i++){
            if(this->at(i)->getJid() == jid){
                return this->at(i);
            }
        }

        throw "CContact with jid does not exist in the list";
    }

    bool containsContactJid(const QString jid) const{
        int size = this->size();
        for(int i=0;i<size;i++){
            if(this->at(i)->getJid() == jid){
                return true;
            }
        }
        return false;
    }
    int indexOfJid(const QString jid) const{
        int size = this->size();
        for(int i=0;i<size;i++){
            if(this->at(i)->getJid() == jid){
                return i;
            }
        }
        return -1;
    }
};

#endif // CCONTACTLIST

