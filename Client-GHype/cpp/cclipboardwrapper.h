#ifndef CCLIPBOARDWRAPPER
#define CCLIPBOARDWRAPPER

#include <QObject>

class QClipboard;

class CClipBoardWrapper : public QObject{

    Q_OBJECT

public:
    CClipBoardWrapper(QClipboard *clipboard, QObject *parent = 0);
    ~CClipBoardWrapper();

    Q_INVOKABLE void copyTextToClipboard(QString newText);
    Q_INVOKABLE QString retrieveTextFromClipboard();

private:
    QClipboard *m_clipboard;
};

#endif // CCLIPBOARDWRAPPER

