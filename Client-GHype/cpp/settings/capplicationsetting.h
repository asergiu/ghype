#ifndef CAPPLICATIONSETTING_H
#define CAPPLICATIONSETTING_H

#include <QObject>
#include <QVariant>
#include <QSettings>
#include <QStringList>
#include "QXmppVCardIq.h"

class CProfileInfo;
class CApplicationSetting: public QObject
{
    Q_OBJECT
public:

    static CApplicationSetting& instance();

    Q_INVOKABLE const QStringList countries();
    Q_INVOKABLE const QString applicationFolder();
    Q_INVOKABLE const QString avatarImagePath();
    Q_INVOKABLE const QString profileImagesFolder();
    Q_INVOKABLE const QString dateFormat();

    Q_INVOKABLE CProfileInfo* profileInfo();

    Q_INVOKABLE bool askWhereToSaveFiles();
    Q_INVOKABLE void setAskWhereToSaveFile(bool ask);

    Q_INVOKABLE QString operatingSystem() const;

    /// @todo make a new class utils and move these methods there
    Q_INVOKABLE bool openFolder(QString filePath);
    void removeFileProtocolPrefix(QString &path);
    /// -----------------------------------------------------

signals:
    void profileInfoChanged();

private:
    CApplicationSetting(QObject *parent = 0);
    CApplicationSetting(CApplicationSetting const&); // Don't Implement
    void operator=(CApplicationSetting const&); // Don't implement
    ~CApplicationSetting();

    QSettings* m_applicationSettings;
    QString m_applicationSettingsFile;
    QStringList m_settingsGroups;

    CProfileInfo *m_profileInfo;

    const static QString FILE_TRANSFER_GROUP;
    const static QString PROFILE_GROUP;
    const static QString PROILE_IMAGES_DIR;

};

#endif // CAPPLICATIONSETTING_H
