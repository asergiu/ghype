#include "capplicationsetting.h"
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QByteArray>
#include <QBuffer>
#include <QImage>
#include <QTranslator>
#include <QTextStream>
#include <QFileInfo>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QFileInfo>
#include <QUrl>

#include "../contact/cprofileinfo.h"

const QString CApplicationSetting::PROFILE_GROUP = "Profile";
const QString CApplicationSetting::FILE_TRANSFER_GROUP = "File-Transfer";
const QString CApplicationSetting::PROILE_IMAGES_DIR = "images/";

CApplicationSetting &CApplicationSetting::instance()
{
    static CApplicationSetting instance; // Guaranteed to be destroyed.
    return instance;
}

const QStringList CApplicationSetting::countries()
{
    QStringList countryList;

    const QString fileName = ":/countries.txt";

    QFile inputFile(fileName);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            countryList<<tr(line.toUtf8().constData());
        }
        inputFile.close();
    }

    return countryList;
}

const QString CApplicationSetting::applicationFolder()
{
    QString appLoc = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    return appLoc;
}

const QString CApplicationSetting::avatarImagePath()
{
    QString folder= profileImagesFolder();
    QString avatarImagePath = folder + "avatar-image.jpg";
    return avatarImagePath;
}

const QString CApplicationSetting::profileImagesFolder(){
    QString folderName = applicationFolder()+"/"+PROILE_IMAGES_DIR;
    QDir folder = QDir(folderName);

    bool ok = true;
    if(!folder.exists())
        ok &= folder.mkdir(folderName);

    return !ok ? "" : folderName;
}

const QString CApplicationSetting::dateFormat()
{
    return m_applicationSettings->value("date-format").toString();
}

CProfileInfo *CApplicationSetting::profileInfo()
{
    return m_profileInfo;
}

bool CApplicationSetting::openFolder(QString filePath)
{
    removeFileProtocolPrefix(filePath);

    QFile file(filePath);
    QDir directory = QFileInfo(file).absoluteDir();
    QString fileProtocol;
#ifdef Q_OS_WIN
    fileProtocol = "file:///";
#else
    fileProtocol = "file://";
#endif
    QUrl url = QUrl(fileProtocol + directory.absolutePath());
    return QDesktopServices::openUrl(url);
}

void CApplicationSetting::removeFileProtocolPrefix(QString &path)
{
#ifdef Q_OS_UNIX
    const QString fileProtocolPrefix = "file://";
#endif
#ifdef Q_OS_WIN
    const QString fileProtocolPrefix = "file:///";
#endif

    path.replace(fileProtocolPrefix, "");
}

bool CApplicationSetting::askWhereToSaveFiles()
{
    bool ask = false;
    m_applicationSettings->beginGroup(FILE_TRANSFER_GROUP);
    ask = m_applicationSettings->value("askWhereToSave", QVariant()).toBool();
    m_applicationSettings->endGroup();

    return ask;
}

void CApplicationSetting::setAskWhereToSaveFile(bool ask)
{
    m_applicationSettings->beginGroup(FILE_TRANSFER_GROUP);
    m_applicationSettings->setValue("askWhereToSave", ask);

    m_applicationSettings->endGroup();
}

QString CApplicationSetting::operatingSystem() const
{
#ifdef Q_OS_MAC
    return "Q_OS_MAC";
#endif
#ifdef Q_OS_WIN
    return "Q_OS_WIN";
#endif
}


CApplicationSetting::CApplicationSetting(QObject *parent):QObject(parent)
{
    m_applicationSettingsFile = applicationFolder()+"/settings.ini";
    m_applicationSettings = new QSettings(m_applicationSettingsFile, QSettings::NativeFormat, this);

    if(m_applicationSettings->value("date-format", QVariant()) == QVariant()){
        m_applicationSettings->setValue("date-format", "dd/MM/yyyy");
    }
}

CApplicationSetting::~CApplicationSetting()
{}
