#ifndef CCLIENT_H
#define CCLIENT_H

#include <QMap>
#include <QList>
#include <QHash>
#include <QQmlListProperty>
#include <QQmlPropertyMap>
#include <QObject>
#include <QQueue>
#include <QTimer>
#include <QMap>
#include <QPair>
#include <QXmppClient.h>
#include <QXmppVCardIq.h>
#include <QXmppStanza.h>
#include <QQmlListProperty>
#include "../contact/ccontactlist.h"

class CContact;
class CMessage;
class CFileTransfer;
class CGroup;
class QXmppTransferJob;
class QXmppVCardManager;
class QXmppRosterManager;
class QXmppTransferManager;
class QXmppMucManager;
class GHypeMessageLogs;
class GHypeFileTransferItem;
class GHypeFileLogs;
class GHypeServerTime;
class GHypeFileAck;
class GHypeFileAbort;
class GHypeJidValidation;

class CClient : public QXmppClient
{
    Q_OBJECT

public:
    CClient(QObject *parent = 0);
    ~CClient();

    /* *** QML properties *** */
    Q_PROPERTY(State clientState READ getClientState NOTIFY sClientStateChanged)
    Q_PROPERTY(CContact* user READ getUser NOTIFY sUserChanged)
    Q_PROPERTY(QQmlListProperty<CContact> searchedContacts READ getSearchedContacts NOTIFY sSearchedContactsChanged)
    Q_PROPERTY(QVariantMap groups READ getGroups NOTIFY sGroupsChanged)
    Q_PROPERTY(QQmlListProperty<GHypeFileTransferItem> fileTransferHistory READ fileTransferHistory NOTIFY sFileTransferHistoryChanged)
    Q_PROPERTY(int MAX_CHARACTERS_PER_MESSAGE READ getMaxCharactersPerMessage)

    Q_ENUMS(State)
    Q_ENUMS(Error)

    /* *** Q_INVOKABLE functions *** */
    Q_INVOKABLE void saveCredentials(bool connected);
    Q_INVOKABLE bool loadCredentials(bool autoAuth);
    Q_INVOKABLE void authenticate(QString username, QString password, bool saveInfo);
    Q_INVOKABLE void signout();
    Q_INVOKABLE void sendMessageText(QString messageText, CContact *interlocutor);
    Q_INVOKABLE void sendMessageComposing(bool composing, CContact *interlocutor);
    Q_INVOKABLE void setAvailableStatus(int availableStatus);
    Q_INVOKABLE void getChatHistoryByDate(const QDateTime& dateFrom, const QDateTime& dateTo, const QString& withBareJid);
    Q_INVOKABLE void getChatHistoryByChunk(CContact* contact);
    Q_INVOKABLE void createConferenceRoom(QString name="");
    Q_INVOKABLE void sendFile(CContact *interlocutor, QString localFilePath, QString description = QString());
    Q_INVOKABLE void requestFileTransferLogs(const QDateTime &startDate = QDateTime(), const QDateTime &endDate = QDateTime(), const QString &interlocutor = QString());
    Q_INVOKABLE void requestJidServerValidation(QString jid);
    Q_INVOKABLE void subscribe(QString jid);
    Q_INVOKABLE void acceptSubscription(QString jid);
    Q_INVOKABLE void denySubscription(QString jid);
    Q_INVOKABLE void removeSubscription(QString jid);
    Q_INVOKABLE void clearTransferLogs();
    Q_INVOKABLE QQmlListProperty<GHypeFileTransferItem> fileTransferHistory();
    Q_INVOKABLE void updateClientInfo();
    Q_INVOKABLE void requestFileLog(CFileTransfer* fileTransfer);

    /* *** functions *** */
    State getClientState();
    CContact* getUser();
    QQmlListProperty<CContact> getSearchedContacts();
    QVariantMap getGroups();
    void requestServerUTC();
    void calculateOffsetFromServerUTC();
    void setInterlocutor(CContact *interlocutor);
    static QString formatJid(QString jid);
    int getMaxCharactersPerMessage(){return MAX_CHARACTERS_PER_MESSAGE;}

public slots:
    void onConnected();
    void onDisconnected();
    void onErrorReceived(QXmppClient::Error error);
    void onGHypeMessageLogsReceived(const GHypeMessageLogs &ghypeMessageLogs);
    void onGHypeServerTimeReceived(const GHypeServerTime &ghypeServerTime);

    /// messages
    void onMessageReceived(const QXmppMessage &message);
    void onPresenceReceived (const QXmppPresence &presence);

    /// friend list related slots
    void onRosterReceived();
    void onClientVCardReceived(const QXmppVCardIq &vCard);
    void onSubscriptionRecevived(QString jid);
    void onRequestVCard();
    void onGHypeJidValidationReceived(const GHypeJidValidation &ghypeJidValidation);

    /// file transfer related slots
    void onFileReceived(QXmppTransferJob *job);
    void onFileSuccesfullyRecieved();
    void onFileTransferTerminated();
    void onGHypeFileLogsReceived(const GHypeFileLogs &ghypeFileLogs);
    void onGhypeFileAckReceived(const GHypeFileAck & fileAck);
    void onGHypeFileAbortReceived(const GHypeFileAbort &fileAbort);

    //ack
    void onAckTimerTriggered();

signals:
    void sClientStateChanged(QXmppClient::Error error = QXmppClient::NoError);
    void sInterlocutorChanged();
    void sSearchedContactsChanged();
    void sGroupsChanged();
    void sFriendRequestReceived(QString jid, QString text);
    void sUserChanged();
    void ghypeFileLogsReceived();
    void sFileTransferHistoryChanged();
    void sCredentialsExist(QString user, QString pass, bool check);
    void sJidValidationReceived(QString username, QString message);
    void sMessageLogsReceived();

private:
    /* *** properties *** */
    CContactList<CContact*> m_friendsInfo;
    CContactList<CContact*> m_searchedContacts;
    QList<GHypeFileTransferItem*> m_transferItems;
    QMap<QString, QVariant> m_groups;
    QString s_username, s_password;
    QQueue<CFileTransfer*> m_sendFilesQueue;
    bool m_fileSending;
    const int MAX_CHARACTERS_PER_MESSAGE;

    CContact *m_user;
    CContact *m_interlocutor;
    QXmppConfiguration mXmppConfiguration;
    QString m_vCardsPath;

    ///////// QXMPP managers
    QXmppRosterManager *m_rosterManager;
    QXmppTransferManager *m_transferManager;
    QXmppVCardManager *m_vCardManager;
    QXmppMucManager *m_mucManager;

    QString m_domain;

    //ACK Stanza
    QTimer *m_ackTimer;
    QMap<QString, QPair<QXmppStanza*,int> > m_ackMap;

    //requesting vcard periodically
    QTimer m_requestVCardTimer;
    int m_requestContactIndex;

    //server UTC
    QDateTime m_serverUTC;
    int m_diff;

    /* *** functions *** */
    void clearFriendsList();
    void addContact(CContact *contact);
    void removeContact(QString jid);
    void clearACK();
    bool sendStanzaPacket(QXmppStanza *packet, const bool isACK = false);
    qint64 getSimpleCryptKey(QString username);
};

#endif // CCLIENT_H
