#include "cclient.h"
#include "../contact/ccontact.h"
#include "../contact/ccontactlist.h"
#include "../message-items/cmessage.h"
#include "../simplecrypt.h"
#include "../contact/cgroup.h"
#include "../message-items/cfiletransfer.h"
#include<QDomElement>
#include<QXmppVCardIq.h>
#include<QXmppMessage.h>
#include<QXmppVCardManager.h>
#include<QXmppRosterManager.h>
#include<QXmppTransferManager.h>
#include<QXmppMucManager.h>
#include<QXmppStanza.h>
#include<QApplication>
#include<QMutex>
#include<ghype-protocol/ghypemessagelogs.h>
#include<ghype-protocol/ghypeservertime.h>
#include<ghype-protocol/cfilelog.h>
#include<ghype-protocol/ghypefilerequest.h>
#include<QXmppUtils.h>
#include<QCryptographicHash>
#include<qfile.h>
#include<QtCore/qmath.h>
#include<QDir>
#include<QDataStream>
#include<QStandardPaths>

#include <QDebug>
#include <QtGlobal>

CClient::CClient(QObject *parent):QXmppClient(parent), MAX_CHARACTERS_PER_MESSAGE(5000)
{
    m_domain = "jabber.tvarita.ro";

    QXmppLogger::getLogger()->setLoggingType(QXmppLogger::StdoutLogging);

    m_rosterManager = NULL;
    m_transferManager = NULL;
    m_vCardManager = NULL;
    m_mucManager = NULL;
    //    m_profileInfo = new CProfileInfo();
    m_user = new CContact(this);
    m_fileSending = false;

    m_interlocutor = new CContact(this);

    bool check;
    check = connect(this, SIGNAL(connected()), this, SLOT(onConnected()));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(messageReceived(QXmppMessage)), this, SLOT(onMessageReceived(QXmppMessage)));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(presenceReceived(QXmppPresence)), this, SLOT(onPresenceReceived(QXmppPresence)));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(ghypeMessageLogsReceived(GHypeMessageLogs)), this, SLOT(onGHypeMessageLogsReceived(GHypeMessageLogs)));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(ghypeServerTimeReceived(GHypeServerTime)), this, SLOT(onGHypeServerTimeReceived(GHypeServerTime)));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(gHypeFileLogsReceived(GHypeFileLogs)), this, SLOT(onGHypeFileLogsReceived(GHypeFileLogs)));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(error(QXmppClient::Error)), this, SLOT(onErrorReceived(QXmppClient::Error)));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(gHypeFileAckReceived(GHypeFileAck)), this, SLOT(onGhypeFileAckReceived(GHypeFileAck)));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(gHypeFileAbortReceived(GHypeFileAbort)), this, SLOT(onGHypeFileAbortReceived(GHypeFileAbort)));
    Q_ASSERT(check);
    check = connect(this, SIGNAL(gHypeJidValidationReceived(GHypeJidValidation)), this, SLOT(onGHypeJidValidationReceived(GHypeJidValidation)));
    Q_ASSERT(check);

    m_mucManager = new QXmppMucManager();
    this->addExtension(m_mucManager);

    /// file transfer manager
    m_transferManager = new QXmppTransferManager();
    /// ?? @todo proxy
    m_transferManager->setProxy(m_domain);
    addExtension(m_transferManager);

    check = connect(m_transferManager, SIGNAL(fileReceived(QXmppTransferJob*)), this, SLOT(onFileReceived(QXmppTransferJob*)));
    Q_ASSERT(check);

    //ACK
    m_ackTimer = new QTimer(this);
    check = connect(m_ackTimer, SIGNAL(timeout()), this, SLOT(onAckTimerTriggered()));

    //request vcard
    m_requestContactIndex = 0;
    check = connect(&m_requestVCardTimer, SIGNAL(timeout()), this, SLOT(onRequestVCard()));
    Q_ASSERT(check);

    m_requestVCardTimer.start(20000);
}

CClient::~CClient()
{
    disconnectFromServer();

    //TODO trebuie sa vedem cum sa facem delete fara sa crape aplicatia
    //clearFriendsList();
}

//===================================================================================
qint64 CClient::getSimpleCryptKey(QString username){
    // -----creating username based key--------
    int asciiVal=0, len;
    qint64 simplecrypt_key = 0;
    QString number;

    if (username.size() >= 6)
        len = 6;
    else
        len = username.size();
    for(int i=0; i < len; i++)
    {
        // Used toLatin1 as toAscii is obsolete as of qt 5.2, provides the same result
        asciiVal = username.at(i).toLatin1();
        number = QString::number(asciiVal);
        // qDebug()<<Q_FUNC_INFO<<"ascii:" << asciiVal << "size:" << number.size() << "key:" << simplecrypt_key <<endl;
        simplecrypt_key = (simplecrypt_key * pow(10,number.size())) + asciiVal;
    }
    return simplecrypt_key;
    //-------------------------------------------
}

bool CClient::loadCredentials(bool autoAuth)
{
    QString user,pass;

    QString appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/";
    QDir appDataDir;
    if(!appDataDir.exists(appDataPath))
    {
        qDebug()<<Q_FUNC_INFO<<" - app data directory does not exist";
        return false;
    }
    QFile file(appDataPath+"user.info");

    if ( file.open(QIODevice::ReadOnly) && file.size())
    {
        QDataStream in(&file);    // read the data serialized from the file
        QString line;

        in >> line;
        //QByteArray line = QByteArray::fromRawData(buf,bufSize);

//        qDebug() << Q_FUNC_INFO << "line:"<< line << endl;

        if(line.contains(',')){
            QList<QString> credentials=line.split(',');
            user = QString(credentials.at(0));

            line = file.readAll();
            qint64 crypt_key = getSimpleCryptKey(user);
            SimpleCrypt crypt(crypt_key);
            pass = crypt.decryptToString(credentials.at(1));

//            qDebug() << Q_FUNC_INFO << credentials.size() <<"Read from file username:" << user << "password:" << pass;
            if (autoAuth){
                authenticate(user,pass,true);
                return true;
            } else {
                emit sCredentialsExist(user,pass,true);
            }
        } else {
            qDebug() << Q_FUNC_INFO << "Error reading from file!Invalid data";

            file.close();
            file.open(QIODevice::WriteOnly);
            file.flush();
        }

    } else {
        qDebug() << Q_FUNC_INFO << "Error reading from file!";

        file.close();
        file.open(QIODevice::WriteOnly);
        file.flush();
    }
    file.close();
    return false;
}

void CClient::saveCredentials(bool connected)
{
    QString appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/";
    QDir appDataDir;
    if(!appDataDir.exists(appDataPath))
    {
        bool ret = appDataDir.mkdir(appDataPath);
        if(!ret) qDebug()<<Q_FUNC_INFO<<" cannot create directory: " << appDataPath;
    }
    QFile file(appDataPath+"user.info");


    if(s_username != "" && s_password != "" && connected){
        // ----Saving the username and the encrypted password to a file-----
        QByteArray pass;
        pass.append(s_password);
        // -----initial hash --------
        /* QCryptographicHash hash(QCryptographicHash::Md5);
        hash.addData(pass);

        QByteArray ba;
        ba = hash.result(); */
        // -----get username based key--------
        qint64 simplecrypt_key = getSimpleCryptKey(s_username);
        // ----- encrypt the md5 has using the above generated key ------
        SimpleCrypt crypt(simplecrypt_key);

        QString res = crypt.encryptToString(pass);

        // ----- save to file ----

        if ( file.open(QIODevice::WriteOnly) )
        {
            file.flush();
            QDataStream stream( &file );
            stream << QString(s_username + ',' + res);
        }

        qDebug()<<Q_FUNC_INFO<<" saving the credentials to file!" << endl
               << "initial" << s_password << endl
               << "encrypted:" << res << endl
               << "decrypted" << crypt.decryptToString(res)<< endl;
    } else {
        if ( file.open(QIODevice::WriteOnly) )
        {
            file.flush();
        }
    }
    file.close();
}

void CClient::authenticate(QString username, QString password, bool saveInfo)
{
    mXmppConfiguration.setHost(m_domain);
    mXmppConfiguration.setDomain(m_domain);
    mXmppConfiguration.setPort(5222);
    mXmppConfiguration.setUser(username);
    mXmppConfiguration.setPassword(password);
    connectToServer(mXmppConfiguration);

    emit sClientStateChanged();

    // ----------------------------------------------------------
    if(saveInfo){
        s_username = username;
        s_password = password;
    } else {
        s_username = "";
        s_password = "";
    }
    // ----------------------------------------------------------

    m_user->setJid(username + "@" + m_domain);
}
//===================================================================================

void CClient::signout()
{
    this->disconnectFromServer();
    m_groups.clear();
    for(int i = 0; i < m_friendsInfo.size(); i++)
        m_friendsInfo[i]->deleteLater();
    m_friendsInfo.clear();

    m_requestVCardTimer.stop();

    clearACK();
}

void CClient::sendMessageText(QString messageText, CContact *interlocutor)
{
    if(messageText.trimmed().isEmpty())
        return;

    if(messageText.length() > MAX_CHARACTERS_PER_MESSAGE){
        messageText.chop(messageText.length() - MAX_CHARACTERS_PER_MESSAGE);
    }

    CMessage *message = new CMessage(this);
    message->setFrom(m_user->getJid());
    message->setTo(interlocutor->getJid());
    message->setBody(messageText);
    message->setMarkable(true);
    message->setMarker(QXmppMessage::NoMarker);

    message->setStamp(QDateTime::currentDateTimeUtc().addSecs(-m_diff));
    message->generateId();
    interlocutor->addMessage(message);

    qDebug()<<Q_FUNC_INFO<<" send message id: " << message->id() << " from: " << message->from()<< " to: "<<message->to()<< " content: "<<message->body();

    bool ret = sendStanzaPacket(message);
    if(!ret) qDebug()<<Q_FUNC_INFO<<": sendStanzaPacket failed;";
}

void CClient::sendMessageComposing(bool composing, CContact *interlocutor)
{
    CMessage message;
    message.setFrom(m_user->getJid());
    message.setTo(interlocutor->getJid());
    message.setMarkable(false);
    message.setMarker(QXmppMessage::NoMarker);
    if(composing)message.setState(CMessage::Composing);
    else message.setState(CMessage::Paused);

    bool ret = sendPacket(message);
    if(!ret) qDebug()<<Q_FUNC_INFO<<": sendPacket failed;";
}

void CClient::setAvailableStatus(int availableStatus)
{
    if(availableStatus < 0) return;
    QXmppPresence presence = this->clientPresence();
    presence.setAvailableStatusType(((QXmppPresence::AvailableStatusType)availableStatus));
    this->setClientPresence(presence);
}

void CClient::getChatHistoryByDate(const QDateTime& dateFrom, const QDateTime& dateTo, const QString& withBareJid)
{   
    GHypeMessageLogs *ghypeMessageLogs = new GHypeMessageLogs(m_user->getJid(), dateFrom, dateTo, withBareJid);
    sendStanzaPacket(ghypeMessageLogs);
}

void CClient::getChatHistoryByChunk(CContact *contact)
{
    if(contact->canRequestHistoryLogs())/* *** see ccontact.h for details(why to use this if) *** */
    {
        qDebug()<<Q_FUNC_INFO<<" request chat history for contact: "<<contact->getJid();
        GHypeMessageLogs *ghypeMessageLogs = new GHypeMessageLogs(m_user->getJid(), contact->getMsgHistoryChunkStart(), contact->getMsgHistoryChunkSize(), contact->getJid());
        sendStanzaPacket(ghypeMessageLogs);
    }
}

void CClient::requestServerUTC()
{
    GHypeServerTime *ghypeServerTime = new GHypeServerTime();
    ghypeServerTime->setFrom(m_user->getJid());
    sendStanzaPacket(ghypeServerTime);
}

void CClient::createConferenceRoom(QString name)
{
    //TODO is in progress
    if(name=="") name = QString::number(QDateTime::currentMSecsSinceEpoch());

    QXmppMucRoom* room = m_mucManager->addRoom("room1@" + m_domain);
    room->setNickName("ClauNickname");
    room->join();
}

void CClient::onConnected()
{
    qDebug()<<"cclient.cpp - onConnected: client succcesfully authenticated " << state();
    /// friend list managers
    m_vCardManager = &vCardManager();
    m_rosterManager = &rosterManager();

    bool check = false;
    check = connect(m_vCardManager, SIGNAL(vCardReceived(QXmppVCardIq)), this, SLOT(onClientVCardReceived(QXmppVCardIq)), (Qt::ConnectionType)(Qt::UniqueConnection|Qt::AutoConnection));
    check = connect(m_rosterManager, SIGNAL(subscriptionReceived(QString)), this, SLOT(onSubscriptionRecevived(QString)), (Qt::ConnectionType)(Qt::UniqueConnection|Qt::AutoConnection));
    check = connect(m_rosterManager, SIGNAL(rosterReceived()), this, SLOT(onRosterReceived()), (Qt::ConnectionType)(Qt::UniqueConnection|Qt::AutoConnection));

    emit sClientStateChanged();

    m_ackTimer->start(10000);

    //request UTC time from server
    requestServerUTC();
}

void CClient::onDisconnected()
{
    qDebug()<<Q_FUNC_INFO<<" user disconnected!";
    clearACK();

    sClientStateChanged();
}

void CClient::onErrorReceived(QXmppClient::Error error)
{
    qDebug()<<Q_FUNC_INFO<<" ERROR CODE Received: " << error; //error 3(KeepAliveError) when username or password incorrect

    emit sClientStateChanged(error);

    clearACK();
}

void CClient::onMessageReceived(const QXmppMessage &message)
{
    qDebug()<<Q_FUNC_INFO<<" from: "<<message.from() << " message: " <<message.body()<<" id: "<<message.id()<<" marker: "<<(int)message.marker()<<" is markable: "<<message.isMarkable();
    QString jid_from = CClient::formatJid(message.from());
    QString jid_to = CClient::formatJid(message.to());

    if(message.type() == QXmppMessage::Error){
        qDebug() << Q_FUNC_INFO << " message error. The recipient may not exist!";
        m_ackMap.remove(message.id());
        return;
    }

    if(jid_to == m_user->getJid() && m_friendsInfo.containsContactJid(jid_from))
    {
        if(message.state() == QXmppMessage::Composing)
        {
            m_friendsInfo.getContactByJid(jid_from)->setIsComposing(true);
        }
        else if(message.state() == QXmppMessage::Paused)
        {
            m_friendsInfo.getContactByJid(jid_from)->setIsComposing(false);
        }
        else if(message.marker() == QXmppMessage::Acknowledged && message.state() == QXmppMessage::Inactive)
        {
            /* *** receiving ack message from a contact that removed the receiver contact *** */
            m_ackMap.remove(message.id());
        }
        else if(message.marker() == QXmppMessage::Acknowledged || message.marker() == QXmppMessage::Received)
        {
            m_friendsInfo.getContactByJid(jid_from)->markMessage(message.id(), (int)message.marker());
            m_ackMap.remove(message.id());
        }
        else if(!message.body().trimmed().isEmpty()) /* *** received the actual message from interlocutor + send ack *** */
        {
            CMessage *newMessage = new CMessage(message, this);
            newMessage->setMarkable(true);
            newMessage->setMarker(QXmppMessage::Displayed);
            m_friendsInfo.getContactByJid(jid_from)->addMessage(newMessage);

            CMessage *ackMessage = new CMessage(this);
            ackMessage->setFrom(message.to());
            ackMessage->setTo(message.from());
            ackMessage->setMarker(QXmppMessage::Acknowledged);
            ackMessage->setId(message.id());

            bool ret = sendStanzaPacket(ackMessage, true);
            if(!ret) qDebug()<<Q_FUNC_INFO<<": sendStanzaPacket failed;";

            delete ackMessage;
        }
    }
    else{
        /* *** fuck off" branch (adica iesi acas) *** */
        CMessage *ackMessage = new CMessage(this);
        ackMessage->setFrom(message.to());
        ackMessage->setTo(message.from());
        ackMessage->setMarker(QXmppMessage::Acknowledged);
        ackMessage->setId(message.id());
        ackMessage->setState(QXmppMessage::Inactive);

        bool ret = sendStanzaPacket(ackMessage, true);
        if(!ret) qDebug()<<Q_FUNC_INFO<<": sendStanzaPacket failed;";

        delete ackMessage;
    }
}

void CClient::onPresenceReceived(const QXmppPresence &presence)
{
    QString bareJid = CClient::formatJid(presence.from());

    if(m_friendsInfo.containsContactJid(bareJid))
    {
        CContact *contact = m_friendsInfo.getContactByJid(bareJid);

        // NOT a very good solution !
        if(presence.type() == QXmppPresence::Subscribe || presence.type() == QXmppPresence::Subscribed){
            contact->setPresence(QXmppPresence::Available);

            if(presence.type() == QXmppPresence::Subscribe)
                m_rosterManager->acceptSubscription(bareJid);
                m_rosterManager->subscribe(bareJid);
        }
        else{
            contact->setPresence(presence);
        }

        CGroup *onlineGroup = m_groups["Online"].value<CGroup*>();
        if(presence.type() == QXmppPresence::Available)
        {
            onlineGroup->addContact(contact);
        }
        else if(presence.type() == QXmppPresence::Unavailable || presence.type() == QXmppPresence::Unsubscribe || presence.type() == QXmppPresence::Unsubscribed){
            onlineGroup->removeContact(contact);
        }
        emit sGroupsChanged();
    }
}

void CClient::addContact(CContact *contact)
{
    m_friendsInfo.append(contact);

    CGroup *groupAll;
    if(!m_groups.contains("All"))
    {
        groupAll = new CGroup("All", this);
        QVariant variantGroupAll;
        variantGroupAll.setValue(groupAll);
        m_groups.insert("All", variantGroupAll);
    }
    else
    {
        groupAll = m_groups["All"].value<CGroup*>();
    }

    groupAll->addContact(contact);

    if(!m_groups.contains("Online"))
    {
        CGroup *groupOnline = new CGroup("Online", this);
        QVariant variantGroupOnline;
        variantGroupOnline.setValue(groupOnline);
        m_groups.insert("Online", variantGroupOnline);
    }

    QList<QString> groupList = m_rosterManager->getRosterEntry(contact->getJid()).groups().toList();

    foreach (QString groupName, groupList)
    {
        CGroup *contactGroup;
        if(m_groups.contains(groupName))
        {
            contactGroup = m_groups[groupName].value<CGroup*>();
        }
        else
        {
            contactGroup = new CGroup(groupName, this);
            QVariant v;
            v.setValue(contactGroup);
            m_groups.insert(groupName, v);
        }

        contactGroup->addContact(contact);
    }

    emit sGroupsChanged();
}

void CClient::removeContact(QString jid)
{
    int index = m_friendsInfo.indexOfJid(jid);

    if(index != -1)
    {
        CContact *contact = m_friendsInfo[index];
        QList<QString> keys = m_groups.keys();
        for(int i=0;i<keys.size();i++)
        {
            if(m_groups.contains(keys.at(i)))
                m_groups[keys.at(i)].value<CGroup*>()->removeContact(contact);
        }

        m_friendsInfo[index]->deleteLater();
        m_friendsInfo.removeAt(index);

        emit sGroupsChanged();
    }
}

void CClient::onGHypeMessageLogsReceived(const GHypeMessageLogs &ghypeMessageLogs)
{
    if(m_ackMap.contains(ghypeMessageLogs.id()))/* *** check if we expect the incoming message, else drop *** */
    {
        QList<QXmppStanza*> messages = ghypeMessageLogs.getMessages();
        QPair<QXmppStanza*, int> pair = m_ackMap.value(ghypeMessageLogs.id());
        QString withBareJid = ((GHypeMessageLogs*)pair.first)->getWithBareJid();

        CContact *contact = m_friendsInfo.getContactByJid(withBareJid);
        if(messages.size() == 0)
        {/* *** block future request if all history logs for a contact was received; see ccontact.h for details *** */
            contact->setCanRequestHistoryLogs(false);
        }
        else
        {
            int size = messages.size();
            bool search = true;
            for(int i = size - 1; i >= 0; --i)
            {
                QXmppMessage *a = dynamic_cast<QXmppMessage*>(messages.at(i));
                CFileLog *b = dynamic_cast<CFileLog*>(messages.at(i));
                CCommunicationItem *log;
                if(a != NULL)
                {
                    log = new CMessage((*a), this);

                    if(search){
                        if(a->id() != ""){
                            search = contact->findMessage(a->id()) != NULL;
                            if(search) continue;
                        }
                    }
                }
                if(b != NULL){
                    CFileTransfer::TransferDirection direction;
                    if(withBareJid == b->from())
                    {
                        direction = CFileTransfer::IncomingDirection;
                    }
                    else
                    {
                        direction = CFileTransfer::OutgoingDirection;
                    }
                    log = new CFileTransfer(withBareJid, b->filename(), b->uid(), b->stamp(), 0, "", direction);
                    ((CFileTransfer*)log)->setStatus(CFileTransfer::HistoryState);

                    if(search){
                        if(b->uid() != ""){
                            search = contact->findFileTransfer(b->uid()) != NULL;
                            if(search) continue;
                        }
                    }
                }

                if(log == NULL) continue;

                if(i == 0) contact->addMessage(log, true, false);//add message FRONT
                else contact->addMessage(log, false, false);
            }
        }

        emit sMessageLogsReceived();

        delete pair.first;
        m_ackMap.remove(ghypeMessageLogs.id());
    }
}

void CClient::requestFileLog(CFileTransfer *fileTransfer)
{
    GHypeFileRequest *logFileRequest = new GHypeFileRequest(fileTransfer->uid(), m_user->getJid());
    sendStanzaPacket(logFileRequest, true);
}

void CClient::onGHypeServerTimeReceived(const GHypeServerTime &ghypeServerTime)
{
    if(m_ackMap.contains(ghypeServerTime.id())){
        m_serverUTC = ghypeServerTime.getServerTime();
        m_diff = m_serverUTC.secsTo(QDateTime::currentDateTimeUtc());
        QPair<QXmppStanza*, int> pair = m_ackMap.value(ghypeServerTime.id());
        delete pair.first;
        m_ackMap.remove(ghypeServerTime.id());
    }
}

void CClient::onAckTimerTriggered()
{
    int size = m_ackMap.size();
    if(size>0)
    {
        QList<QString> keys = m_ackMap.keys();
        for(int i=0;i<keys.size();i++)
        {
            if(m_ackMap[keys.at(i)].second == 0)
            {
                m_ackMap.remove(keys.at(i));
            }
            else if(m_ackMap[keys.at(i)].second < 4)
            {
                bool ret = sendPacket(*m_ackMap[keys.at(i)].first);
                if(!ret) qDebug()<<Q_FUNC_INFO<<": sendPacket(QXmppStanza) failed;";
                m_ackMap[keys.at(i)].second--;
            }
            else
            {
                m_ackMap[keys.at(i)].second--;
            }
        }
    }
}

bool CClient::sendStanzaPacket(QXmppStanza *packet, const bool isACK)
{
    bool ret = sendPacket(*packet);
    if(!ret) return false;

    if(!isACK)
    {
        m_ackMap.insert(packet->id(), QPair<QXmppStanza*, int>(packet, 4));
    }

    return true;
}

void CClient::updateClientInfo()
{
    QXmppVCardIq vCardIq = m_user->getProfile()->getVCard();
    m_vCardManager->setClientVCard(vCardIq);
}

void CClient::clearACK()
{
    m_ackTimer->stop();
    m_ackMap.clear();
}

QString CClient::formatJid(QString jid)
{
    int i = jid.indexOf('/');
    QString formattedJid = jid.mid(0, i);
    return formattedJid;
}

//Q_PROPERTY READ
QXmppClient::State CClient::getClientState()
{
    return state();
}

CContact* CClient::getUser()
{
    return m_user;
}

QQmlListProperty<CContact> CClient::getSearchedContacts()
{
    return QQmlListProperty<CContact>(this, m_searchedContacts);
}

QVariantMap CClient::getGroups()
{
    return m_groups;
}

//Q_PROPERTY WRITE
void CClient::setInterlocutor(CContact *interlocutor)
{
    m_interlocutor = interlocutor;
    emit sInterlocutorChanged();
}
