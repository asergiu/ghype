#include "cclient.h"
#include <QXmppTransferManager.h>
#include <QDebug>
#include <QApplication>
#include <QtGlobal>

#include "../message-items/cfiletransfer.h"
#include "../contact/ccontact.h"
#include "../message-items/cmessage.h"
#include "ghype-protocol/ghypefilelogs.h"
#include "ghype-protocol/ghypefileack.h"
#include "ghype-protocol/ghypefileabort.h"
#include <QFileInfo>


void CClient::sendFile(CContact *interlocutor, QString localFilePath, QString description)
{
    /// test if client knows how to send files
    QXmppTransferManager* ext = findExtension<QXmppTransferManager>();
    if(ext)
    {
        //        CApplicationSetting::instance().removeFileProtocolPrefix(localFilePath);
#ifdef Q_OS_WIN
        localFilePath = localFilePath.replace("file:///", "");
#endif
#ifdef Q_OS_UNIX
        localFilePath = localFilePath.replace("file://", "");
#endif
        QFileInfo fileInfo(localFilePath);
        QString errorMessage;

        /// max transfered file size 200 MB
        const int maxSize = 200000000;
        if(fileInfo.absoluteFilePath().indexOf(QRegExp("[#`%^{}]")) != - 1)
        {
            errorMessage = "Error: cannot send files that contains special characters!";
        }
        else
        {
            if(fileInfo.size() == 0)
            {
                errorMessage = "Error: cannot send empty files!";
            }
            else if(fileInfo.size() > maxSize)
            {
                errorMessage = "Error: file is too large";
            }
        }

        if(!errorMessage.isEmpty())
        {
            CMessage *message = new CMessage(this);
            message->setFrom(interlocutor->getJid());
            message->setTo(m_user->getJid());
            message->setBody(errorMessage);
            message->setStamp(QDateTime::currentDateTimeUtc());
            message->setMarkable(true);
            message->setMarker(QXmppMessage::NoMarker);
            message->generateId();

            interlocutor->addMessage(message);

            return;
        }

        QFile file(localFilePath);
        CFileTransfer* fileTransfer = new CFileTransfer(interlocutor->getJid(), file.fileName(), "", QDateTime::currentDateTime(), file.size(), description, CFileTransfer::OutgoingDirection, this);

        //this should be uncomment if one file at a time sending should be enabled
//        if(!m_fileSending){
            QXmppTransferJob* transferJob = m_transferManager->sendFile(fileTransfer->friendJid(), localFilePath, fileTransfer->fileDescription());
            m_fileSending = true;
            fileTransfer->startFileTransfer(transferJob);

            bool check = connect(fileTransfer, SIGNAL(sTransferTerminated()), this, SLOT(onFileTransferTerminated()));
            Q_ASSERT(check);
//        }

        interlocutor->addMessage(fileTransfer);
//        m_sendFilesQueue.enqueue(fileTransfer);
    }
    else
    {
        qDebug()<<"Client doesn`t know how to send files";
    }
}

void CClient::requestFileTransferLogs(const QDateTime &startDate, const QDateTime &endDate, const QString &interlocutor)
{
    sendStanzaPacket(new GHypeFileLogs(interlocutor, startDate, endDate, ""));
}

void CClient::onFileReceived(QXmppTransferJob *job)
{
    /// test if client knows how to send files
    QXmppTransferManager* ext = findExtension<QXmppTransferManager>();

    if(ext) {
        QString fromJid = job->jid();
        int pos = fromJid.indexOf('/');
        if (pos > 0) fromJid = fromJid.remove(pos, fromJid.size() - pos);

        CContact *senderContact = NULL;
        if (m_friendsInfo.containsContactJid(fromJid)){
            senderContact = m_friendsInfo.getContactByJid(fromJid);
        } else {
            qDebug() << Q_FUNC_INFO << " received file transfer from " << fromJid << ", but this jid is not in our contact list";
            return;
        }

        QString jobUid = job->fileUID();

        CFileTransfer* fileTransfer = senderContact->findFileTransfer(jobUid);;
        if(fileTransfer == NULL)
        {
            fileTransfer = new CFileTransfer(job, this);
            if (senderContact)
            {
                connect(fileTransfer, SIGNAL(sFileSuccessfullyReceived()), this, SLOT(onFileSuccesfullyRecieved()));
                senderContact->addMessage(fileTransfer);
                qDebug() << Q_FUNC_INFO << "file offer received from " << fromJid;
            }
        }
        else
        {
            connect(fileTransfer, SIGNAL(sFileSuccessfullyReceived()), this, SLOT(onFileSuccesfullyRecieved()));

            fileTransfer->startFileTransfer(job);
            fileTransfer->accept(job->fileName());
        }
    } else {
        qDebug()<<"Client doesn`t know how to send files";
    }

}

void CClient::onFileSuccesfullyRecieved()
{
    CFileTransfer* signalSender = (CFileTransfer*)sender();
    QString to = signalSender->friendJid();
    QString filename = signalSender->filename();
    QString sid = signalSender->sid();

    QString from = m_user->getJid();
    GHypeFileAck *ack = new GHypeFileAck(from, to, sid, filename);
    sendStanzaPacket(ack, true);
}

void CClient::onFileTransferTerminated()
{
    CFileTransfer* signalSender = (CFileTransfer*)sender();
    if (signalSender->error() == CFileTransfer::AbortError && signalSender->direction() == CFileTransfer::OutgoingDirection) {
        QString from = m_user->getJid();
        QString to = signalSender->friendJid();
        QString uid = signalSender->uid();
        QString filename = signalSender->filename();
        GHypeFileAbort *fileAbort = new GHypeFileAbort(from, to, uid, filename);
        sendStanzaPacket(fileAbort, true);
    }

    if (!m_sendFilesQueue.isEmpty())
    {
        CFileTransfer* dequeuedElement = m_sendFilesQueue.dequeue();

        if(signalSender->sid() == dequeuedElement->sid())
        {
            qDebug()<<"DROP FIRST PACKET";
            if(m_sendFilesQueue.isEmpty()) return;
            dequeuedElement = m_sendFilesQueue.dequeue();

            qDebug()<<" B queue size: "<< m_sendFilesQueue.size();
        }

        qDebug()<<" SID send dequee element: "<< dequeuedElement->sid();
        QXmppTransferJob* transferJob = m_transferManager->sendFile(dequeuedElement->friendJid(), dequeuedElement->filename(), dequeuedElement->fileDescription());
        m_fileSending = true;
        dequeuedElement->startFileTransfer(transferJob);

        bool check = connect(dequeuedElement, SIGNAL(sTransferTerminated()), this, SLOT(onFileTransferTerminated()));
        Q_ASSERT(check);
    } else {
        m_fileSending = false;
    }
}

void CClient::clearTransferLogs()
{
    for(int i = 0; i < m_transferItems.size(); i++){
        m_transferItems[i]->deleteLater();
    }
    m_transferItems.clear();
    emit sFileTransferHistoryChanged();
}

QQmlListProperty<GHypeFileTransferItem> CClient::fileTransferHistory()
{
    return QQmlListProperty<GHypeFileTransferItem>(this, m_transferItems);
}

void CClient::onGHypeFileLogsReceived(const GHypeFileLogs &ghypeFileLogs)
{
    if(m_ackMap.contains(ghypeFileLogs.id()))/* *** check if we expect the incoming message, else drop *** */
    {
        clearTransferLogs();
        m_transferItems = ghypeFileLogs.fileTransfers();

        emit sFileTransferHistoryChanged();

//        m_ackMutex.lock();
        delete m_ackMap.value(ghypeFileLogs.id()).first;
        m_ackMap.remove(ghypeFileLogs.id());
//        m_ackMutex.unlock();
    }
}

void CClient::onGhypeFileAckReceived(const GHypeFileAck &fileAck)
{
    if (fileAck.uid() == "" || fileAck.filename() == "") {
        qDebug() << "invalid file ack received";
        return;
    }

    CMessage* msg = new CMessage();

    msg->setFrom(fileAck.from());
    msg->setMarkable(true);
    msg->setMarker(QXmppMessage::Acknowledged);

    QString friendJid = formatJid(fileAck.from());
    if(m_friendsInfo.containsContactJid(friendJid)) {
        CContact* contact = m_friendsInfo.getContactByJid(friendJid);
        msg->setBody(contact->getProfile()->fullName() + " downloaded the file " + fileAck.filename());
        contact->addMessage(msg);
    } else {
        qDebug()<<" friend list does not contain user from which you received the file";
    }
}

void CClient::onGHypeFileAbortReceived(const GHypeFileAbort &fileAbort)
{
    QString friendJid = formatJid(fileAbort.from());
    CContact *contact = NULL;
    if (m_friendsInfo.containsContactJid(friendJid)) {
        contact = m_friendsInfo.getContactByJid(friendJid);
    }
    if (contact) {
        bool ret = contact->abortFileTransfer(fileAbort.uid());
        qDebug() << Q_FUNC_INFO << "aborted file transfer with uid: " << fileAbort.uid() << " : " << ret;
    } else {
        qDebug() << Q_FUNC_INFO << "abort file transfer: contact " << friendJid << " NOT found";
    }
}
