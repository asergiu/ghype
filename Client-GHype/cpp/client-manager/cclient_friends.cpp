#include "cclient.h"

#include <QDir>
#include <QBuffer>
#include <QPair>
#include <QImageReader>
#include <QCoreApplication>
#include <QXmppVCardManager.h>
#include <QXmppRosterManager.h>

#include "../contact/ccontact.h"
#include "../message-items/ccommunicationitem.h"
#include "../contact/cprofileinfo.h"
#include "../contact/cgroup.h"
#include <ghype-protocol/ghypejidvalidation.h>

/**
 * @brief CClient::onRosterReceived
 *  This slot is called when the signal rosterReceived() from the roster manager class is emmited.
 *  The signal is emitted when the Roster IQ is received after a successful connection.
 *  Steps:
 *  - get client`s roster and vCard manager and connect the signals from the manages to the
 *          appropriate slots
 *  - get all the friends jid`s
 *  - for each friend jid, request the client`s vCard
 */
void CClient::onRosterReceived()
{
    clearFriendsList();

    QStringList friendsJids = m_rosterManager->getRosterBareJids();

    if(!m_user->readFromFile())
    {
        qDebug()<<Q_FUNC_INFO << "CANNOT READ m_user from file, REQUEST VCARD";
        m_vCardManager->requestVCard(m_user->getJid());
    }

    if(friendsJids.empty()){
        qDebug()<<Q_FUNC_INFO<<": friends list is empty";
        return;
    }

    for(int i = 0; i < friendsJids.size(); i++)
    {
        CContact *contact = new CContact(friendsJids[i]);
        addContact(contact);

        if(!contact->readFromFile())
        {
            qDebug() << Q_FUNC_INFO << "CANNOT READ FILE, REQUEST VCARD";
            m_vCardManager->requestVCard(friendsJids[i]);
        }
    }
}

void CClient::onRequestVCard()
{
    if(m_friendsInfo.size()>0)
    {
        if(m_requestContactIndex >= m_friendsInfo.size())
        {
            m_requestContactIndex = 0;
        }

        QString jid = m_friendsInfo[m_requestContactIndex]->getJid();
        m_vCardManager->requestVCard(jid);
        m_requestContactIndex++;
    }
}


void CClient::onGHypeJidValidationReceived(const GHypeJidValidation &ghypeJidValidation)
{
    if(!m_ackMap.contains(ghypeJidValidation.id()))
        return;

    delete m_ackMap.value(ghypeJidValidation.id()).first;
    m_ackMap.remove(ghypeJidValidation.id());

    QPair<QString, bool> validationData = ghypeJidValidation.getValidationData();

    qDebug()<<"got user validation "<<validationData;
    QString jid = validationData.first;
    QString username = QString(jid).remove("@" + m_domain);
    if(!validationData.second) {
        qDebug() << "User " << jid <<" was not validated";
        emit sJidValidationReceived(username, "is not valid.");
        return;
    }


    if(!m_friendsInfo.containsContactJid(jid)){
        m_rosterManager->subscribe(jid);
        CContact *newContact = new CContact(jid, this);
        QXmppPresence presence = m_rosterManager->getPresence( CClient::formatJid(jid), "QXmpp");
        newContact->setPresence(presence);
        addContact(newContact);

        m_vCardManager->requestVCard(jid);

        emit sJidValidationReceived(username, "was added to your contact list.");
    } else {
        qDebug() << Q_FUNC_INFO << "Already send friend request to "<<jid;
    }
}

/**
 * @brief CClient::onClientVCardReceived
 * This slot is the handler for vCardReceived() signal of the vCard manager, that is emitted
 *  when the requested vCard is received after calling the requestVCard() function on the vCard manager.
 * @param vCard - the received vCard
 */
void CClient::onClientVCardReceived(const QXmppVCardIq &vCard)
{
    QString bareJid = vCard.from();
    CContact* info;

    qDebug()<<"Client vcard received: "<< bareJid;

    if(bareJid == m_user->getJid()){
        /// current client vCard
        //        setProfileInfo(new CProfileInfo(vCard));
        //        CApplicationSetting::instance().setProfileInfo(m_profileInfo);
        info = m_user;
        info->setVCard(vCard, true);
    }
    else{
        try{
            info = m_friendsInfo.getContactByJid(bareJid);
            info->setVCard(vCard, true);
        }
        catch(...){
            qDebug()<<Q_FUNC_INFO<< " CContact with jid: "<<bareJid<<" does not exist in the list of friends";
            QCoreApplication::quit();
            return;
        }
    }

    QXmppPresence presence = m_rosterManager->getPresence(bareJid, "QXmpp");
    info->setPresence(presence);

    CGroup *onlineGroup = m_groups["Online"].value<CGroup*>();
    if(presence.type() == QXmppPresence::Available && bareJid != m_user->getJid())
    {
        onlineGroup->addContact(info);
    }
    else{
        onlineGroup->removeContact(info);
    }

    emit sGroupsChanged();
}

void CClient::subscribe(QString jid)
{

    if (!jid.contains(m_domain)) {
        jid.append("@" + m_domain);
    }

    QString username = QString(jid).remove("@" + m_domain);
    if (m_friendsInfo.containsContactJid(jid)) {
        emit sJidValidationReceived(username, "is already in your contact list.");
    } else if (jid ==  m_user->getJid()) {
        emit sJidValidationReceived(username, "is you.");
    } else {
        requestJidServerValidation(jid);
    }

    //    if(!m_friendsInfo.containsContactJid(jid)){
    //        m_rosterManager->subscribe(jid);
    //        CContact *newContact = new CContact(jid, this);
    //        QXmppPresence presence = m_rosterManager->getPresence( CClient::formatJid(jid), "QXmpp");
    //        newContact->setPresence(presence);
    //        addContact(newContact);

    //        m_vCardManager->requestVCard(jid);
    //    }else{
    //        qDebug() << Q_FUNC_INFO << "Already send friend request to "<<jid;
    //    }
}

void CClient::acceptSubscription(QString jid)
{
    m_rosterManager->acceptSubscription(jid);
    m_rosterManager->subscribe(jid);
    CContact *newContact = new CContact(jid, this);
    QXmppPresence presence = m_rosterManager->getPresence( CClient::formatJid(jid), "QXmpp");
    newContact->setPresence(presence);
    addContact(newContact);

    m_vCardManager->requestVCard(jid);
}

void CClient::denySubscription(QString jid)
{
    m_rosterManager->refuseSubscription(jid, "I don`t want to be your friend");
}
void CClient::removeSubscription(QString jid)
{
    removeContact(jid);

    m_rosterManager->removeItem(jid);
}

void CClient::onSubscriptionRecevived(QString jid)
{
    if(m_friendsInfo.containsContactJid(jid)){
        m_rosterManager->acceptSubscription(jid);
    }
    else{
        emit sFriendRequestReceived(jid, QString());
    }
}

void CClient::clearFriendsList()
{
    m_friendsInfo.clear();
    m_groups.clear();
    emit sGroupsChanged();
}

void CClient::requestJidServerValidation(QString jid){
    qDebug()<<"request jid validation for "<<jid;
    sendStanzaPacket(new GHypeJidValidation(m_user->getJid(), jid));
}
