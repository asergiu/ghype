#include "cclipboardwrapper.h"

#include <QMimeData>
#include <QClipboard>
#include <qdebug.h>

CClipBoardWrapper::CClipBoardWrapper(QClipboard *clipboard, QObject *parent) : QObject(parent)
{
    m_clipboard = clipboard;
}


CClipBoardWrapper::~CClipBoardWrapper()
{
}

void CClipBoardWrapper::copyTextToClipboard(QString newText)
{
    qDebug()<<"COPY: "<<newText;

    m_clipboard->setText(newText);
}

QString CClipBoardWrapper::retrieveTextFromClipboard()
{
    QString oldText;
    const QMimeData *mimeData = m_clipboard->mimeData();

    if(mimeData->hasText()) oldText = mimeData->text();

    qDebug()<<"PASTE: " <<oldText;

    return oldText;
}
