#include "_messagemodel.h"

#include "_message.h"
#include "cfiletransfer.h"
#include "cmessage.h"
#include <qdebug.h>

MessageModel::MessageModel(QObject *parent) : QAbstractListModel(parent)
{
}

MessageModel::~MessageModel()
{
    int size = m_messages.size();
    for(int i=0;i<size;i++)
    {
        delete m_messages[i];
    }
}

void MessageModel::addMessageBack(CCommunicationItem *message)
{
    int count = rowCount();
    message->setParent(this);
    beginInsertRows(QModelIndex(), count, count);

    m_messages.push_back(message);
    endInsertRows();
}

void MessageModel::addMessageFront(CCommunicationItem *message)
{
    message->setParent(this);
    beginInsertRows(QModelIndex(), 0, 0);
    m_messages.push_front(message);
    endInsertRows();
}

void MessageModel::deleteMessage(int index){
    beginRemoveRows(QModelIndex(), index, index);
    m_messages.removeAt(index);
    endRemoveRows();
}

bool MessageModel::abortFileTransfer(QString uid)
{
    int msgLength = m_messages.length();
    for (int i = msgLength - 1; i >= 0; --i) {
        if (m_messages[i]->itemType() == CCommunicationItem::FILE_TRANSFER) {
            CFileTransfer *fileTransfer = (CFileTransfer*)m_messages[i];
            if (fileTransfer->status() == CFileTransfer::FinishedState) {
                return true;
            } else if (fileTransfer->uid() == uid) {
                fileTransfer->abort();
                return true;
            }
        }
    }
    return false;
}

CFileTransfer* MessageModel::findFileTransfer(QString uid)
{
    int size = m_messages.length();
    for(int i = (size-1);i>=0;i--)
    {
        if (m_messages[i]->itemType() == CCommunicationItem::FILE_TRANSFER)
        {
            CFileTransfer *fileTransfer = (CFileTransfer*)m_messages[i];
            if(fileTransfer->uid() == uid)
            {
                return fileTransfer;
            }
        }
    }
    return NULL;
}

CMessage* MessageModel::findMessage(QString id)
{
    int size = m_messages.length();
    for(int i = (size-1);i>=0;i--)
    {
        if (m_messages[i]->itemType() == CCommunicationItem::MESSAGE)
        {
            CMessage *message = (CMessage*)m_messages[i];
            if(message->id() == id)
            {
                return message;
            }
        }
    }
    return NULL;
}

int MessageModel::rowCount(const QModelIndex &parent) const{
    Q_UNUSED(parent);
    int count = m_messages.count();
    return count;
}

QVariant MessageModel::data(const QModelIndex &index, int role) const{
    if (index.row() < 0 || index.row() >= m_messages.count()){
        return QVariant();
    }
    if(index.row()>=m_messages.size()) return QVariant();
    CCommunicationItem *message = m_messages.at(index.row());
    QVariant variant;
    switch (role) {
    case ItemType:
        variant = message->itemType();
        break;
    case Msg_Body:
        variant = ((CMessage*)message)->body();
        break;
    case Msg_From:
        variant = ((CMessage*)message)->from();
        break;
    case Msg_Marker:
        variant = (int)((CMessage*)message)->marker();
        break;
    case Msg_Time:{

        QDateTime stamp = ((CMessage*)message)->stamp().toLocalTime();
        QDate date = stamp.date();
        QDate currentDate = QDateTime::currentDateTimeUtc().toLocalTime().date(); // current date expressed in system clock


        int year = date.year();
        int month = date.month();
        int day = date.day();
        QString time;

        if(year != currentDate.year())
        {
            time = stamp.toString("yyyy MMMM dd / h:mm ap");
        }
        else if(month != currentDate.month())
        {
            time = stamp.toString("MMMM dd / h:mm ap");
        }
        else if(day != currentDate.day())
        {
            time = stamp.toString("ddd dd/ h:mm ap");
        }
        else
        {
            time = stamp.toString("h:mm ap");
        }

        variant = QVariant(time);
        break;
        //        variant = ((CMessage*)message)->stamp().toLocalTime().toString("ddd MMMM / h:mm ap");
    }
        break;
    case File_Transfer_Object:
        variant.setValue((CFileTransfer*)message);
        break;
    default:
        variant = QVariant();
        break;
    }
    return variant;
}

QHash<int, QByteArray> MessageModel::roleNames() const{
    QHash<int, QByteArray> roles;

    roles[ItemType] = "itemType";
    roles[Msg_Body] = "msg_body";
    roles[Msg_From] = "msg_from";
    roles[Msg_Marker] = "msg_marker";
    roles[Msg_Time] = "msg_time";
    roles[File_Transfer_Object] = "file_transfer_object";
    return roles;
}

Qt::ItemFlags MessageModel::flags(const QModelIndex &index){
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool MessageModel::setData(const QModelIndex &index, const QVariant &value, int role){
    if (index.row() < 0 || index.row() >= m_messages.count()){
        return false;
    }
    bool succesfull = false;
    if(index.row()>=m_messages.size())return false;
    CCommunicationItem *message = m_messages.at(index.row());
    switch (role) {
    case ItemType:
        message->setItemType(value.toInt());
        succesfull = true;
        break;
    case Msg_Body:
        ((Message*)message)->setBody(value.toString());
        succesfull = true;
        break;
    case Msg_From:
        ((Message*)message)->setFrom(value.toString());
        succesfull = true;
        break;
    case Msg_Marker:
        ((Message*)message)->setMarker((Message::Marker)value.toInt());
        succesfull = true;
        break;
    case Msg_Time:
        //message stamp should not be set
        //        ((Message*)message)->setStamp();
        succesfull = true;
        break;
        //    case File_Transfer_Object:
        //        succesfull = true;
        //        break;
    default:
        succesfull = false;
        break;
    }
    if(succesfull)
        emit dataChanged(index, index);
    return succesfull;
}

void MessageModel::markMessage(QString id, int marker)
{
    for(int i=0;i<m_messages.size();i++)
    {
        if(m_messages.at(i)->itemType() == CCommunicationItem::MESSAGE)
        {
            CMessage *message = ((CMessage*)m_messages.at(i));

            if( message->id() == id )
            {
                //                message->setMarker( (QXmppMessage::Marker)marker );
                setData(QAbstractListModel::index(i), marker, Msg_Marker);

                break;
            }
        }
    }
}

void MessageModel::acknoledgeMessages()
{
    int i=0;
    while(i<m_messages.size()
          && m_messages.at(i)->itemType() == CCommunicationItem::MESSAGE
          && ((Message*)m_messages.at(i))->marker() == QXmppMessage::Displayed )
    {
        ((Message*)m_messages.at(i))->setMarker(QXmppMessage::Acknowledged);
        i++;
    }
}

void MessageModel::clearMessages()
{
    m_messages.clear();
}
