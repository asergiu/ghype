#ifndef CMESSAGE
#define CMESSAGE

#include <QObject>
#include <QDateTime>

#include "QXmppMessage.h"
#include "ccommunicationitem.h"

class CMessage : public CCommunicationItem, public QXmppMessage{
    Q_OBJECT

public:
    CMessage(QObject *parent = 0);
    CMessage(const QXmppMessage &message, QObject *parent = 0);
    ~CMessage();

    Q_PROPERTY(QString body READ body WRITE setBody NOTIFY bodyChanged)
    Q_PROPERTY(QString from READ from NOTIFY sFromChanged)
    Q_PROPERTY(QString time READ getTime NOTIFY sTimeChanged)
    Q_PROPERTY(int marker READ getMarker NOTIFY sMarkerChanged)

    /* *** Q PROPERTY functions *** */
    QString getTime();
    int getMarker();

    /* *** functions *** */
    void generateId(){
        generateAndSetNextId();
    }

signals:
    void bodyChanged();
    void sFromChanged(QString s);
    void sTimeChanged();
    void sMarkerChanged();
};

#endif // CMESSAGE

