#ifndef CCOMMUNICATIONITEM_H
#define CCOMMUNICATIONITEM_H

#include <QObject>

class CCommunicationItem : public QObject
{
    Q_OBJECT
    Q_ENUMS(ItemType)
    Q_PROPERTY(ItemType itemType READ itemType WRITE setItemType NOTIFY sItemTypeChanged)

public:
    typedef enum{
        MESSAGE = 0,
        FILE_TRANSFER,
        MAX_ITEM_TYPES
    }ItemType;

    explicit CCommunicationItem(QObject *parent = 0);
    virtual ~CCommunicationItem();

    Q_INVOKABLE ItemType itemType(){return m_type;}
    Q_INVOKABLE void setItemType(int type){m_type = (ItemType)type;}

signals:
    void sItemTypeChanged(ItemType type);

protected:
    ItemType m_type;
};

#endif // CCOMMUNICATIONITEM_H
