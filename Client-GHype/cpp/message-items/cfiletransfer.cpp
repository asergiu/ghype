#include "cfiletransfer.h"
#include <QDir>
#include <QDebug>
#include <QStandardPaths>
#include <QFileInfo>
#include <QFileInfoList>

CFileTransfer::CFileTransfer(QObject *parent): CCommunicationItem(parent){
    setItemType(FILE_TRANSFER);
}

CFileTransfer::CFileTransfer(QString interlocutorJid, QString filename, QString uid, QDateTime stamp, int fileSize, QString description, TransferDirection direction, QObject *parent) : CCommunicationItem(parent){
    setItemType(FILE_TRANSFER);

    m_localFilePath = "";

    m_transferJob = NULL;
    m_filename = filename;
    m_uid = uid;
    m_time = stamp;
    m_fileSize = fileSize;
    m_description = description;
    m_direction = direction;
    m_progress = 0;
    m_friendJid = interlocutorJid;
    m_status = OfferState;
    m_sid = "";
}

CFileTransfer::CFileTransfer(QXmppTransferJob* transferJob, QObject *parent):
    CCommunicationItem(parent)
{
    startFileTransfer(transferJob);
}

CFileTransfer::~CFileTransfer()
{
}

void CFileTransfer::abort()
{
    m_transferTimer.stop();
    if(m_transferJob != NULL){
        m_transferJob->abort();
        m_transferJob->deleteLater();
    }
}

void CFileTransfer::accept(QString localFilePath, QString dirPath)
{
    m_transferTimer.stop();
#ifdef Q_OS_WIN
    dirPath = dirPath.replace("file:///", "");
#endif
#ifdef Q_OS_UNIX
    dirPath = dirPath.replace("file://", "");
#endif

    if(dirPath.isEmpty())
        dirPath = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);

    if(!dirPath.endsWith("/"))
        dirPath = dirPath.append("/");

#ifdef Q_OS_WIN
    localFilePath = localFilePath.replace("file:///", "");
#endif
#ifdef Q_OS_UNIX
    localFilePath = localFilePath.replace("file://", "");
#endif

    localFilePath = computeSaveFilename(dirPath, localFilePath);

    m_localFilePath = dirPath + localFilePath;
    qDebug()<<Q_FUNC_INFO<<" local file path: "<<m_localFilePath;
    m_transferJob->accept(m_localFilePath);
}

void CFileTransfer::startFileTransfer(QXmppTransferJob *transferJob)
{
    setItemType(FILE_TRANSFER);
    m_localFilePath = "";

    m_transferJob = transferJob;
    m_filename = m_transferJob->fileName();
    m_uid = m_transferJob->fileUID();
    m_time = m_transferJob->fileDate();
    m_fileSize = m_transferJob->fileSize();
    m_direction = (TransferDirection)m_transferJob->direction();
    m_status = (TransferStatus)m_transferJob->state();
    m_progress = 0;
    m_sid = m_transferJob->sid();
    m_friendJid = m_transferJob->jid();
    int pos = m_friendJid.indexOf('/');
    if (pos > 0) m_friendJid = m_friendJid.remove(pos, m_friendJid.size() - pos);

    bool check = true;
    check = connect(m_transferJob, SIGNAL(error(QXmppTransferJob::Error)), this, SLOT(onError(QXmppTransferJob::Error)));
    Q_ASSERT(check);
    check = connect(m_transferJob, SIGNAL(progress(qint64,qint64)), this, SLOT(onProgressChanged(qint64,qint64)));
    Q_ASSERT(check);
    check = connect(m_transferJob, SIGNAL(finished()), this, SLOT(onFinished()));
    Q_ASSERT(check);
    check = connect(m_transferJob, SIGNAL(finished()), this, SIGNAL(sTransferTerminated()));
    Q_ASSERT(check);
    check = connect(m_transferJob, SIGNAL(stateChanged(QXmppTransferJob::State)), this, SLOT(onStateChanged(QXmppTransferJob::State)));
    Q_ASSERT(check);

    if (transferJob->direction() == QXmppTransferJob::IncomingDirection) {
        m_transferTimer.setInterval(60 * 1000);
        m_transferTimer.setSingleShot(true);

        check = connect(&m_transferTimer, SIGNAL(timeout()), this, SLOT(onTimerTimeout()));
        Q_ASSERT(check);

        m_transferTimer.start();
    }
}

double CFileTransfer::progress(){
    return m_progress;
}

void CFileTransfer::setProgress(double progress)
{
    m_progress = progress;
    emit sProgressChanged(m_progress);
}

QString CFileTransfer::localFilePath()
{
    return m_localFilePath;
}

QString CFileTransfer::localFileFolder()
{
    QFileInfo fileInfo(m_localFilePath);
    if(!fileInfo.exists())
        return "";

    return fileInfo.absoluteDir().absolutePath();
}

QString CFileTransfer::filename()
{
    return m_filename;
}

QString CFileTransfer::friendJid()
{
    return m_friendJid;
}

int CFileTransfer::fileSize()
{
    return m_fileSize;
}

QString CFileTransfer::stateString(QXmppTransferJob::State s)
{
    switch (s) {
    case    QXmppTransferJob::OfferState:
        return "Offer";
    case QXmppTransferJob::TransferState:
        return "Transfer";
    case QXmppTransferJob::FinishedState:
        return "Finished";

    default:
        return "";
    }
}

QString CFileTransfer::computeSaveFilename(QString dirName, QString requestedFilename)
{
    QDir dir(dirName);
    if(!dir.exists())
        return QString();

    if(!dir.exists(requestedFilename))
        return requestedFilename;

    QString baseName;
    QString extension;
    int pointIndex = requestedFilename.lastIndexOf('.');
    if(pointIndex != -1){
        baseName = requestedFilename;
        baseName.truncate(pointIndex);
        extension = requestedFilename.right(requestedFilename.length() - pointIndex);
    }else
        baseName = requestedFilename;

    QStringList nameFilters;
    nameFilters<<requestedFilename<<baseName+"-*"+extension;

    QFileInfoList fileNames = dir.entryInfoList(nameFilters, QDir::Files, QDir::Name);
    if(!fileNames.size())
        return requestedFilename;

    int indexMinus;
    int max = -1;
    int saveIndex = -1;
    for(int i = 0; i < fileNames.size(); i++){
        QString lastFileName = fileNames[i].fileName();
        indexMinus = lastFileName.lastIndexOf('-');
        if(indexMinus == -1){
            saveIndex = 0;
            continue;
        }
        lastFileName = lastFileName.right(lastFileName.length() - indexMinus - 1);
        lastFileName.replace(extension, "");
        saveIndex = lastFileName.toInt();
        if(saveIndex > max)
            max = saveIndex;
    }

    max++;
    return baseName+"-"+QString::number(max)+extension;
}

QString CFileTransfer::state()
{
    //    QXmppTransferJob::State s = m_transferJob->state();
    switch (m_status) {
    case    OfferState:
        return "Offer";
    case TransferState:
        return "Transfer";
    case FinishedState:
        return "Finished";

    default:
        return "";
    }
}

QString CFileTransfer::sid()
{
    return m_sid;
}

QString CFileTransfer::uid() const
{
    return m_uid;
}

QString CFileTransfer::getTime()
{
    QDateTime stamp = m_time.toLocalTime();
    QDate date = stamp.date();
    QDate currentDate = QDateTime::currentDateTimeUtc().toLocalTime().date(); // current date expressed in system clock

    int year = date.year();
    int month = date.month();
    int day = date.day();
    QString time;

    if(year != currentDate.year())
    {
        time = stamp.toString("yyyy MMMM dd / h:mm ap");
    }
    else if(month != currentDate.month())
    {
        time = stamp.toString("MMMM dd / h:mm ap");
    }
    else if(day != currentDate.day())
    {
        time = stamp.toString("ddd dd/ h:mm ap");
    }
    else
    {
        time = stamp.toString("h:mm ap");
    }

    return time;
}

void CFileTransfer::onError(QXmppTransferJob::Error error)
{
    qDebug()<<Q_FUNC_INFO<<"File Transfer Error is "<<error;
    m_error = (TransferError)error;

    emit sErrorChanged(m_error);
}

void CFileTransfer::onProgressChanged(qint64 done, qint64 total)
{
    setProgress(double(done)/double(total));
}

void CFileTransfer::onStateChanged(QXmppTransferJob::State state)
{
    m_status = (TransferStatus)state;
    emit sStatusChanged(m_status);

    QString s = stateString(state);
    emit sStateChanged(s);
}

void CFileTransfer::onFinished()
{
    qDebug()<<Q_FUNC_INFO<<" Job finished; delete transfer job object";

    QXmppTransferJob::Error e = m_transferJob->error();
    m_error = (TransferError)e;
    qDebug()<<Q_FUNC_INFO<<" error is "<<e;
    emit sErrorChanged(m_error);
    if(e == QXmppTransferJob::NoError && m_transferJob->direction() == QXmppTransferJob::IncomingDirection){
        qDebug()<<Q_FUNC_INFO<<" File received no error: !!!";
        emit sFileSuccessfullyReceived();
    }
    else{
        qDebug()<<Q_FUNC_INFO<<" error: "<<e;
    }
    m_transferTimer.stop();
    m_transferJob->deleteLater();
}

void CFileTransfer::onTimerTimeout()
{
    if(!m_transferJob)
        return;
    QXmppTransferJob::State state = m_transferJob->state() ;
    if(state ==  QXmppTransferJob::OfferState || state ==  QXmppTransferJob::StartState){
        m_transferJob->abort();
        m_transferJob->deleteLater();
    }
}
