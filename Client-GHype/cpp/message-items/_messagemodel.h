#ifndef _MESSAGEMODEL
#define _MESSAGEMODEL

#include <QAbstractListModel>
#include <QMutex>
#include <QDeclarativeEngine>

#include "_message.h"
#include "ccommunicationitem.h"
#include "cfiletransfer.h"
#include "cmessage.h"

class MessageModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum MessageRoles{
        ItemType =0,
        Msg_Body,
        Msg_From,
        Msg_Marker,
        Msg_Time,
//        Message_Object,
        File_Transfer_Object
    };

    MessageModel(QObject *parent=0);
    ~MessageModel();

    void addMessageBack(CCommunicationItem *message);
    void addMessageFront(CCommunicationItem *message);
    void deleteMessage(int index);
    bool abortFileTransfer(QString uid);
    CFileTransfer* findFileTransfer(QString uid);
    CMessage* findMessage(QString id);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    void markMessage(QString id, int marker);
    void acknoledgeMessages();
    void clearMessages();

protected:
    QHash<int, QByteArray> roleNames() const;
    Qt::ItemFlags flags(const QModelIndex & index);
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

private:
    QList<CCommunicationItem*> m_messages;
};

#endif // _MESSAGEMODEL

