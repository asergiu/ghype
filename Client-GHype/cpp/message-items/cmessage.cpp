#include "cmessage.h"

CMessage::CMessage(QObject *parent):CCommunicationItem(parent),QXmppMessage()
{
    setItemType(MESSAGE);
}

CMessage::CMessage(const QXmppMessage &message, QObject *parent):CCommunicationItem(parent), QXmppMessage(message)
{
    setItemType(MESSAGE);
}

CMessage::~CMessage()
{

}

QString CMessage::getTime()
{
//    QDateTime time = stamp().toLocalTime();
//    QDate date = time.date();
//    QDate currentDate = QDate::currentDate();

//    int year = date.year();
//    int month = date.month();
//    int day = date.day();

//    if(year != currentDate.year())
//    {
//        return stamp().toLocalTime().toString("yyyy MMMM dd / h:mm ap");
//    }
//    else if(month != currentDate.month())
//    {
//        return stamp().toLocalTime().toString("MMMM dd / h:mm ap");
//    }
//    else if(day != currentDate.day())
//    {
//        return stamp().toLocalTime().toString("ddd / h:mm ap");
//    }
//    else
//    {
        return stamp().toLocalTime().toString("h:mm ap");
//    }
}

int CMessage::getMarker()
{
    return (int)marker();
}
