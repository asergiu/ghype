#include "_message.h"

Message::Message(const QXmppMessage &message, QObject *parent):CCommunicationItem(parent), QXmppMessage(message)
{
    setItemType(MESSAGE);
}
