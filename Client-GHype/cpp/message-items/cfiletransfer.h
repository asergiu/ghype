#ifndef CFILETRANSFER_H
#define CFILETRANSFER_H

#include "ccommunicationitem.h"
#include <QXmppTransferManager.h>
#include <QTimer>

class CFileTransfer: public CCommunicationItem
{
    Q_OBJECT

    Q_ENUMS(TransferStatus TransferError TransferDirection)

    Q_PROPERTY(double progress READ progress WRITE setProgress NOTIFY sProgressChanged)
    Q_PROPERTY(TransferStatus status READ status WRITE setStatus NOTIFY sStatusChanged)
    Q_PROPERTY(TransferError error READ error WRITE setError NOTIFY sErrorChanged)
    Q_PROPERTY(TransferDirection direction READ direction WRITE setDirection NOTIFY sDirectionChanged)
    Q_PROPERTY(QString friendJid READ friendJid NOTIFY sFriendJidChanged)
    Q_PROPERTY(QString state READ state NOTIFY sStateChanged)
    Q_PROPERTY(QString fileName READ filename NOTIFY sFileNameChanged)
    Q_PROPERTY(int fileSize READ fileSize)
    Q_PROPERTY(QString time READ getTime NOTIFY sTimeChanged)

public:
    typedef enum{
        NoError = QXmppTransferJob::NoError,
        AbortError = QXmppTransferJob::AbortError,
        FileAccessError = QXmppTransferJob::FileAccessError,
        FileCorruptError = QXmppTransferJob::FileCorruptError,
        ProtocolError = QXmppTransferJob::ProtocolError
    } TransferError;

    typedef enum{
        OfferState = QXmppTransferJob::OfferState,
        StartState = QXmppTransferJob::StartState,
        TransferState = QXmppTransferJob::TransferState,
        FinishedState = QXmppTransferJob::FinishedState,
        HistoryState
    } TransferStatus;

    typedef enum{
        IncomingDirection = QXmppTransferJob::IncomingDirection,
        OutgoingDirection = QXmppTransferJob::OutgoingDirection
    }TransferDirection;

    CFileTransfer(QObject *parent = NULL);
    CFileTransfer(QString interlocutorJid, QString filename, QString uid, QDateTime stamp, int fileSize, QString description, TransferDirection direction, QObject *parent = NULL);
    CFileTransfer(QXmppTransferJob* transferJob, QObject* parent = NULL);
    ~CFileTransfer();

    Q_INVOKABLE void abort();
    Q_INVOKABLE void accept(QString localFilePath, QString dirPath = "");

    double progress();
    void setProgress(double progress);
    void startFileTransfer(QXmppTransferJob* transferJob);

    Q_INVOKABLE QString localFilePath();
    QString localFileFolder();
    QString filename();
    int fileSize();

    QString fileDescription(){return m_description;}
    QString friendJid();
    QString state();
    QString sid();
    Q_INVOKABLE QString uid() const;

    TransferStatus status(){return m_status;}
    TransferError error(){return m_error;}
    TransferDirection direction(){return m_direction;}

    QString getTime();

    void setStatus(TransferStatus status){m_status = status;}
    void setError(TransferError error){m_error = error;}
    void setDirection(TransferDirection direction) {m_direction = direction;}

public slots:
    void onError(QXmppTransferJob::Error error);
    void onProgressChanged(qint64 done, qint64 total);
    void onStateChanged(QXmppTransferJob::State state);
    void onFinished();
    void onTimerTimeout();

signals:
    void sStateChanged(QString state);
    void sFriendJidChanged(QString jid);
    void sProgressChanged(double progress);
    void sStatusChanged(TransferStatus status);
    void sErrorChanged(TransferError error);
    void sDirectionChanged(TransferDirection direction);
    void sFileNameChanged(QString filename);
    void sFileSuccessfullyReceived();
    void sTransferTerminated();
    void sTimeChanged();

private:
    QXmppTransferJob* m_transferJob;
    double m_progress;
    QString m_localFilePath;

    QString m_friendJid;
    QString m_filename;
    QString m_sid;
    QString m_uid;
    QDateTime m_time;
    int m_fileSize;
    QString m_description;

    TransferError m_error;
    TransferStatus m_status;
    TransferDirection m_direction;

    QTimer m_transferTimer;

    /// private methods
    QString stateString(QXmppTransferJob::State s);
    QString computeSaveFilename(QString dir, QString requestedFilename);
};

#endif // CFILETRANSFER_H
