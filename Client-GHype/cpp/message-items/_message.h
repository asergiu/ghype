#ifndef _MESSAGE
#define _MESSAGE

#include <QObject>
#include "QXmppMessage.h"
#include "ccommunicationitem.h"

class Message : public CCommunicationItem, public QXmppMessage{

    Q_OBJECT

public:
    Message(const QXmppMessage &message, QObject *parent = 0);

};

#endif // _MESSAGE

