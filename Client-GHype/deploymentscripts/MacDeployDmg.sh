DEPLOY_DIR=$1
EXE_NAME=$2
BUNDLE_NAME=$EXE_NAME.app
EXE_DIR="$DEPLOY_DIR/$BUNDLE_NAME/Contents/MacOS"
BIN_FILE="$EXE_DIR/$EXE_NAME"

#for P in `otool -L $BIN_FILE | awk '{print $1}'`
#do
#    if [[ $P == *: ]] # skip first line
#    then
#        continue
#    fi
#    LIB_DIR="/Users/work/qt_simulations/deploy/lib/"
#    if [[ ! $P == /* ]] # then it's not an absolute path
#    then
#        NEW_PATH="$LIB_DIR$P"
#        install_name_tool -change $P $NEW_PATH $BIN_FILE
#        P=$NEW_PATH
#        for P1 in `otool -L $P | awk '{print $1}'`
#        do
#            if [[ $P1 == *: ]]
#            then
#                continue
#            fi
#            then
#                NEW_PATH="$LIB_DIR$P1"
#                install_name_tool -change $P1 $NEW_PATH $P
#            fi
#        done
#    fi
#    if [[ "$P" == *//* ]]
#    then
#        PSLASH=$(echo $P | sed 's,//,/,g')
#        install_name_tool -change $P $PSLASH $BIN_FILE
#    fi
#done

QT_DIR=/Users/admin/Qt/5.4/clang_64
for F in `find $QT_DIR/lib $QT_DIR/plugins $QT_DIR/qml  -perm 755 -type f`
do
    for P in `otool -L $F | awk '{print $1}'`
        do
            if [[ "$P" == *//* ]]
            then
                PSLASH=$(echo $P | sed 's,//,/,g')
                install_name_tool -change $P $PSLASH $F
            fi
         done
done

# copy additional files needed for deployment
cp -r $QT_DIR/qml/QtQuick $EXE_DIR
cp -r $QT_DIR/qml/QtQuick.2 $EXE_DIR
cp -r $QT_DIR/qml/QtMultimedia $EXE_DIR
cp -r $QT_DIR/qml/QtGraphicalEffects $EXE_DIR
cp -r $QT_DIR/qml/QtWebKit $EXE_DIR
cp -r $QT_DIR/qml/Qt $EXE_DIR
#cp -r $DEPLOY_DIR/config $EXE_DIR
#cp $DEPLOY_DIR/config.xml $EXE_DIR
#cp $DEPLOY_DIR/frames.xml $EXE_DIR

#copy themes
#RESOURCES_DIR="$DEPLOY_DIR/$BUNDLE_NAME/Contents/Resources/"
#cp -av $DEPLOY_DIR/themes    $RESOURCES_DIR
#cp -av $DEPLOY_DIR/themes-SM $RESOURCES_DIR

#copy qca framework and qca plugins
#echo path is `pwd`
#QCA_DIR=/Users/work/qca-2.1.0/build
#rsync -avl $QCA_DIR/lib/qca.framework "$DEPLOY_DIR/$BUNDLE_NAME/Contents/Frameworks/"
#echo $?
##echo "copy qca command: cp -av $QCA_DIR/lib/qca.framework  "$DEPLOY_DIR/$BUNDLE_NAME/Contents/Frameworks/""
#rsync -avl $QCA_DIR/lib/qca/crypto "$DEPLOY_DIR/$BUNDLE_NAME/Contents/PlugIns/"
##echo "copy plugins command: cp -av $QCA_DIR/lib/qca.framework  "$DEPLOY_DIR/$BUNDLE_NAME/Contents/#Frameworks/""


QXMPP_DIR=/Users/work/ghype/Client-QXmpp/src
echo deploy dir is $DEPLOY_DIR
echo bundle name is $BUNDLE_NAME
#echo ceva "$DEPLOY_DIR/$BUNDLE_NAME/Contents/Frameworks/"
#echo ceva $DEPLOY_DIR/$BUNDLE_NAME/Contents/Frameworks/
rsync -av $QXMPP_DIR/libqxmpp*.dylib "$DEPLOY_DIR/$BUNDLE_NAME/Contents/Frameworks/"


#install_name_tool -change $QT_DIR/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore "$DEPLOY_DIR/$BUNDLE_NAME/Contents/Frameworks/qca.framework/Versions/2.1.0/qca"

install_name_tool -change $QT_DIR/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore


# copy plugins directory in deploy directory otherwise macdeployqt will not find it
cp -r $QT_DIR/plugins $DEPLOY_DIR
cd $DEPLOY_DIR
$QT_DIR/bin/macdeployqt $BUNDLE_NAME -dmg -verbose=3

#cp /usr/lib/libstdc++.6.dylib /usr/lib/libSystem.B.dylib .
#cp -R /System/Library/Frameworks/OpenGL.framework/Versions/A/OpenGL /System/Library/Frameworks/AGL.framework/Versions/A/AGL .
