ROOT_DIR = .
equals(TEMPLATE, lib): DESTDIR = $$ROOT_DIR/deploy/lib
equals(TEMPLATE, app): DESTDIR = $$ROOT_DIR/deploy
OBJECTS_DIR = obj
MOC_DIR = gen
RCC_DIR = gen

CONFIG += c++11

#CONFIG += console

win32: RC_FILE += FoureoIcon.rc
macx: ICON = resources/foureologo.icns

TARGET = Foureo

TEMPLATE = app

QT += qml quick widgets xml declarative

# Default rules for deployment.
include(deployment.pri)

#define XMPP_HOME >>
win32{
    XMPP_HOME = "../Client-QXmpp/src"
}

unix:!macx{
    XMPP_HOME = "/usr/local"
}

macx{
    XMPP_HOME = "/Users/work/ghype/Client-QXmpp/src"
    QMAKE_POST_LINK = "sh deploymentscripts/MacDeployDmg.sh deploy Foureo"
}
#<<

#INCLUDEPATH >>
win32|macx{
    INCLUDEPATH += $$XMPP_HOME/client
    INCLUDEPATH += $$XMPP_HOME/SERVER
    INCLUDEPATH += $$XMPP_HOME/base

    LIBS += -L$$XMPP_HOME
}

unix:!macx{
    INCLUDEPATH += $$XMPP_HOME/include/qxmpp
    LIBS += -L$$XMPP_HOME/lib/
}
#<<

#LIBS >>
win32{
    CONFIG (release, debug|release): LIBS += -lqxmpp0
    else: LIBS += -lqxmpp_d0
}

unix|macx{
    CONFIG (release, debug|release): LIBS += -lqxmpp
    else: LIBS += -lqxmpp_d
}
#<<

SOURCES += main.cpp \
    cpp/client-manager/cclient.cpp \
    cpp/client-manager/cclient_friends.cpp \
    cpp/client-manager/cclient_fileTransfer.cpp \
    cpp/message-items/cfiletransfer.cpp \
    cpp/message-items/ccommunicationitem.cpp \
    cpp/message-items/cmessage.cpp \
    cpp/contact/ccontact.cpp \
    cpp/settings/capplicationsetting.cpp \
    cpp/contact/cprofileinfo.cpp \
    cpp/contact/cgroup.cpp \
    cpp/simplecrypt.cpp \
    cpp/message-items/_message.cpp \
    cpp/message-items/_messagemodel.cpp \
    cpp/message-items/_filetransfer.cpp \
    cpp/cclipboardwrapper.cpp

RESOURCES += \
    GHype.qrc

HEADERS += \
    cpp/client-manager/cclient.h \
    cpp/message-items/cmessage.h \
    cpp/message-items/cfiletransfer.h \
    cpp/message-items/ccommunicationitem.h \
    cpp/contact/ccontactlist.h \
    cpp/contact/ccontact.h \
    cpp/settings/capplicationsetting.h \
    cpp/contact/cprofileinfo.h \
    cpp/contact/cgroup.h \
    cpp/simplecrypt.h \
    cpp/message-items/_message.h \
    cpp/message-items/_messagemodel.h \
    cpp/message-items/_filetransfer.h \
    cpp/cclipboardwrapper.h

OTHER_FILES += \
    countries.txt

DISTFILES += \
    qml/MainView.qml \
    qml/Start.qml \
    qml/Login.qml \
    user.info \
    qml/components/FriendRequestDialog.qml \
    qml/components/MComboBox.qml \
    qml/components/Chat-components/ChatDialog.qml \
    qml/components/MListView.qml \
    qml/components/MessageDelegate.qml \
    qml/components/AddUserDialog.qml \
    qml/components/FileTransferDelegate.qml \
    qml/components/EditProfileDataInput.qml \
    qml/components/GHypeCalendar.qml \
    qml/settingsviews/AdvancedSettings.qml \
    qml/settingsviews/PreferencesSettings.qml \
    qml/settingsviews/PrivacySettings.qml \
    qml/settingsviews/ProfileSettings.qml \
    qml/settingsviews/SettingsDialog.qml \
    qml/ProfileInfoView.qml \
    qml/components/Chat-components/Emoticons.qml



