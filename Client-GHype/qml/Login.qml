import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.3
import CClient 1.0
import "components"

Image{
    anchors.fill: parent
    fillMode: Image.PreserveAspectCrop
    source: "qrc:/images/resources/login_background.png"

    property int windowGlobalPosX
    property int windowGlobalPosY

    onVisibleChanged: {
        statusTimer.stop()
    }

    function updateCredentials(saved_username, saved_password, signed_check){
        username.text = saved_username
        password.text = saved_password
        keep_signed.checked = signed_check
    }

    function changeStatus(statusMessage, type){
        status.text = statusMessage
        status.color = type==="error"?"red":"black"


        statusDisappearAnimation.start()
        statusTimer.stop()

        if(type==="error"){
            statusTimer.repeat = false;
            statusTimer.interval = 3000;
            password.text = "";
        } else {
            statusTimer.repeat = true;
            statusTimer.interval = 500;
        }

        statusTimer.restart();
        statusAppearAnimation.start();
        statusTimer.statusType = type;
    }

    function validateLoginInput(){
        var username_val = username.text;
        var password_val = password.text;

        if(username_val == "" || password_val == ""){
            changeStatus(qsTr("Empty username or password!"), "error")
            return;
        }

        if (keep_signed.checked)
            client.authenticate(username_val, password_val,true);
        else
            client.authenticate(username_val, password_val,false);
    }

    Rectangle{
        width: effect_line.width - 20 /* *** -10 adjust this value if you want to position the logo closer or further to the welcome text *** */
        height: 100
        anchors.bottom: effect_line.top
        anchors.horizontalCenter: parent.horizontalCenter
        color: "transparent"

        Text{
            id: title1
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 22
            width: paintedWidth
            text: qsTr("Welcome to")
            font.family: "Roboto Regular"
            font.pointSize: 18
            color: "#3e4e63"
        }

        Image {
            id: logo
            width: 90
            height: 50
            fillMode: Image.PreserveAspectFit
            anchors.left: title1.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 22
            source: "qrc:/images/resources/foureologo100x68.png"
            antialiasing: true
            smooth: true
        }
    }

    Rectangle{
        id: effect_line
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: title2.top
        anchors.bottomMargin: 20
        width: 240
        height: 1
        color: "#979797"
    }

    Text{
        id: title2
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: username_holder.top
        anchors.bottomMargin: 45
        width: 280
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        text: qsTr("Cross platform app messaging with friends all over the world")
        color: "#3e4e63"
        font.family: "Roboto Regular"
        font.pointSize: 12
    }

    Rectangle{
        id: mid
        anchors.centerIn: parent
        width: 1
        height: 1
        color: "transparent"
    }

    Rectangle{
        id: username_holder
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: mid.top
        anchors.bottomMargin: 5
        width: 250
        height: 50
        border.width: 1
        border.color: "#dee2e5"
        radius: 4

        Image{
            id: username_icon
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: 20
            width: 50
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/resources/login_username.png"
        }

        TextField{
            id: username
            anchors.left: username_icon.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.margins: 2
            font.pointSize: 12
            focus: true
            style: TextFieldStyle {
                    textColor: "#666666"
                    background: Rectangle {
                        border.width: 0
                    }
                   }

            Keys.onReturnPressed: {
                validateLoginInput()
            }
            Keys.onEnterPressed: Keys.onReturnPressed(event)
        }
    }

    Rectangle{
        id: password_holder
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: mid.bottom
        anchors.topMargin: 5
        height: 50
        width: 250
        border.width: 1
        border.color: "#dee2e5"
        radius: 4

        Image{
            id: password_icon
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: 20
            width: 50
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/resources/login_password.png"
        }

        TextField{
            id: password
            anchors.left: password_icon.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.margins: 2
            font.pointSize: 12
            echoMode: TextInput.Password
            style: TextFieldStyle {
                    textColor: "#666666"
                    background: Rectangle {
                        border.width: 0
                    }
                }

            Keys.onReturnPressed: {
                validateLoginInput()
            }
            Keys.onEnterPressed: Keys.onReturnPressed(event)
        }
    }

    Button{
        id: login_button
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: password_holder.bottom
        anchors.topMargin: 10
        width: 250
        height: 50
        text: qsTr("Sign in")
        style: ButtonStyle {
            label: Text{
                text: login_button.text
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family: "Roboto Regular"
                color: "white"
                font.pointSize: 16
            }
                background: Rectangle {
                    border.width: 0
                    radius: 4
                    color: control.pressed? "#318002" : "#5bb923"
                }
            }
        onClicked: {
            validateLoginInput()
        }
    }

    CheckBox {
        id: keep_signed
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: login_button.bottom
        anchors.topMargin: 20
        text: qsTr("Sign in automatically")
        style: CheckBoxStyle {
            label: Text{
                text: keep_signed.text
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family: "Roboto Regular"
                color: "#3e4e63"
                font.pointSize: 10
            }
            indicator: Rectangle {
                    implicitWidth: 18
                    implicitHeight: 19
                    border.width: 0
                    color: "transparent"

                    Image {
                        visible: control.checked
                        fillMode: Image.PreserveAspectFit
                        anchors.fill: parent
                        source: "qrc:/images/resources/common_checkbox_checked.png"
                    }

                    Image{
                        visible: !control.checked
                        fillMode: Image.PreserveAspectFit
                        anchors.fill: parent
                        source: "qrc:/images/resources/common_checkbox_unchecked.png"
                    }
            }
        }

        onCheckedChanged: {
            if(!checked){
                client.saveCredentials(false);
            }
        }
    }

    Text{
        id: forget_pass
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: keep_signed.bottom
        anchors.topMargin: 20
        color: "#707070"
        font.family: "Roboto Regular"
        font.pointSize: 10
        text: qsTr("Forgot your password?")
        visible: false
    }

    Text {
        id: status
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: forget_pass.bottom
        anchors.topMargin: 40
        font.pointSize: 10
        opacity: 0
    }

    Timer{
        id: statusTimer
        property string statusType
        onTriggered: {
            if(statusType == "error"){
                statusDisappearAnimation.start()
            }
            else{
                if(status.text.indexOf("...")!== -1){
                    status.text = status.text.replace("   ","")
                    status.text = status.text.replace("...","")
                }
                else{
                    status.text = " " + status.text
                    status.text += "."
                }
            }
        }
    }

    SequentialAnimation {
        id: statusAppearAnimation
        running: false
        NumberAnimation { target: status; property: "opacity"; to: 1.0; duration: 300}
    }

    SequentialAnimation {
        id: statusDisappearAnimation
        running: false
        NumberAnimation { target: status; property: "opacity"; to: 0.0; duration: 300}
    }

    Connections{
        id: clientLoginConnections
        target: client

        onSCredentialsExist:{
            updateCredentials(user,pass,check)
        }
    }
}
