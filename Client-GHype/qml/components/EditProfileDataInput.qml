import QtQuick 2.0

Rectangle{

    color: "transparent"

    property int labelWidth: 90
    property int labelHeight: 30
    property int spacing: 5
    property alias inputValidator: inputValue.validator
    property alias label: inputLabel.text
    property alias textMaxLength: inputValue.maximumLength
    property alias value: inputValue.text
    property bool editable: true
    property alias textColor: inputLabel.color

    height: labelHeight

    onFocusChanged: {
        if (focus) {
            inputValue.focus = focus
        }
    }

    Text{
        id: inputLabel
        text: qsTr("Label")
        width: labelWidth
        height: labelHeight
        verticalAlignment: Text.AlignVCenter
        font.pointSize: 9
        font.family: "Roboto Regular"
        color: "#7d7d7f"
    }

    Rectangle{
        anchors.left: inputLabel.right
        anchors.leftMargin: spacing
        anchors.right: parent.right
        height: labelHeight
        border.width: 1
        border.color: "#c5ccd2"
        radius: 5
        clip: true

        TextInput{
            id: inputValue
            visible: editable
            anchors.fill: parent
            anchors.margins: 5
            maximumLength: 30
            verticalAlignment: TextInput.AlignVCenter
            validator: RegExpValidator{}
            font.family: "Roboto Regular"
            font.pointSize: 9
        }


        Text {
            id: inputText
            visible: !editable
            anchors.fill: parent
            anchors.margins: 5
            verticalAlignment: TextInput.AlignVCenter
            text: value
        }
    }

}
