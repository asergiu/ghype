import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.3
import QtQuick.Controls.Private 1.0


Rectangle{
    id: combobox_root
    color: "transparent"

    property alias model : box.model
    property string color1: "#87d44d"
    property string color2: "#76798f"
    property string color3: "#318002"
    property string textColor: "#white"
    property string textFont: "Roboto Bold"
    property int textSizePoint: 8
    property int currentIndex: box.currentIndex
    property alias currentText: box.currentText

    ComboBox {
        id:box
        width: combobox_root.width
        height: combobox_root.height
        activeFocusOnPress: true

        style: ComboBoxStyle{
            background: Rectangle{
                width: box.width
                height: box.height
                color: "transparent"

                Rectangle{
                    id: first_part
                    anchors.left: parent.left
                    anchors.right: second_part.left
                    height: parent.height
                    color: color1
                    radius: 4

                    Rectangle{
                        anchors.right: parent.right
                        width: first_part.radius
                        height: parent.height
                        color: color1
                    }

                    Text{
                        anchors.centerIn: parent
                        text: control.currentText
                        font.family: textFont
                        font.pointSize: textSizePoint
                        color: combobox_root.textColor
                    }
                }

                Rectangle{
                    id: second_part
                    anchors.right: parent.right
                    width: 20
                    height: parent.height
                    color: color2
                    radius: 4

                    Rectangle{
                        anchors.left: parent.left
                        width: second_part.radius
                        height: parent.height
                        color: color2
                    }

                    Image{
                        width: parent.width/1.8
                        height: parent.height/1.8
                        anchors.centerIn: parent
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/images/resources/common_arrow_down.png"
                    }
                }
            }

            label: Text {
                color: "transparent"
                text: control.currentText
            }

            // drop-down customization here
//            property Component __dropDownStyle: MenuStyle {
//                __maxPopupHeight: 600
//                __menuItemType: "comboboxitem"

//                frame: Rectangle {              // background
//                    color: "#ffffff"
//                }

//                itemDelegate.label:             // an item text
//                    Text {
//                        verticalAlignment: Text.AlignVCenter
//                        horizontalAlignment: Text.AlignHCenter
//                        font.pointSize: 8
//                        width: 100
//                        height: 29
//                        font.family: textFont
//                        color: styleData.selected ? combobox_root.textColor : "black"
//                        text: styleData.text
//                    }

//                itemDelegate.background:
//                    Rectangle{
//                        width: 100
//                        height: 28
//                        color: "transparent"

//                        Rectangle{
//                            id: element
//                            width: parent.width
//                            height: 30
//                            radius: 3
//                            color: styleData.selected ? combobox_root.color3 : combobox_root.color1
//                        }

//                        Rectangle{
//                            id: gap
//                            width: parent.width
//                            height: 1
//                            anchors.top: element.bottom
//                            color: "#76798f"
//                        }
//                    }

//                __scrollerStyle: ScrollViewStyle { }
//            }
        }
    }
}


