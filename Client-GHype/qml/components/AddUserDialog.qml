import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Dialog{
    id: dialog
    title: qsTr("Add user")

    signal addClient(string jid)

    function jidValidationReceived(username, message) {
        var statusText = "User " + username + " " + message;
        contentItem.setStatusText(statusText);
    }

    onVisibleChanged: {
        if(visible) {
            root.isValidationInProgress = false;
            userNameTA.text = statusText.text = "";
            userNameTA.focus = true;
        }
    }

    contentItem: Rectangle {
        id: root
        color: "#979ef8"
        implicitWidth: 500
        implicitHeight: 100

        property bool isValidationInProgress: false

        function requestUserAdd() {
            if (root.isValidationInProgress)
                return;
            var username = userNameTA.text.trim();
            if (username != "") {
                statusText.text = qsTr("Adding user " + username + " to your contact list...");
                root.isValidationInProgress = true;
                addClient(username);
            }
        }

        function setStatusText(text) {
            statusText.text = text;
            root.isValidationInProgress = false;
        }

        Column{

            anchors.centerIn: parent
            spacing: 10

            Row{

                spacing: 5

                Text{
                    text: qsTr("Username")
                    width: paintedWidth + 10
                    height: userNameTA.height
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: 13
                    color: "white"
                }

                TextArea{
                    id: userNameTA
                    width: 200
                    height: 30
                    horizontalAlignment: TextEdit.AlignHCenter
                    verticalAlignment: TextEdit.AlignVCenter

                    style: TextAreaStyle {
                        textColor: "#333"
                        selectionColor: "steelblue"
                        selectedTextColor: "#eee"
                        backgroundColor: "#eee"
                    }
                    Keys.onReturnPressed: root.requestUserAdd();
                    Keys.onEnterPressed: Keys.onReturnPressed(event);
                }


                Button{
                    width: 40
                    height: 40
                    anchors.verticalCenter: userNameTA.verticalCenter
                    onClicked: root.requestUserAdd()
                    style: ButtonStyle{
                        background: Rectangle{
                            width: control.width
                            height: control.height
                            color: "transparent"
                            radius: 5

                            Image{
                                anchors.fill: parent
                                source: control.hovered ? "qrc:/images/resources/add_contact_hovered.png" : "qrc:/images/resources/add_contact.png"
                                fillMode: Image.PreserveAspectFit
                            }
                        }
                    }
                }

                Button{
                    width: 40
                    height: 40
                    anchors.verticalCenter: userNameTA.verticalCenter
                    onClicked: dialog.close()
                    style: ButtonStyle{
                        background: Rectangle{
                            width: control.width
                            height: control.height
                            color: "transparent"
                            radius: 5

                            Image{
                                anchors.fill: parent
                                source: control.hovered ? "qrc:/images/resources/cancel_hovered.png" : "qrc:/images/resources/cancel.png"
                                fillMode: Image.PreserveAspectFit
                            }
                        }
                    }
                }
            }

            Text {
                id: statusText
                height: 20
                width: parent.width
                font.pointSize: 11
                color: "white"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }
}
