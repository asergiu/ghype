import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.0
//import QtGraphicalEffects 1.0
import CFileTransfer 1.0

Rectangle {
    id: root

    property int buttonSize: 25
    property CFileTransfer fileTransfer
    property bool alignRight

    color: "red"

    onAlignRightChanged: {
        if (alignRight) {
            //align elements to right
        } else {
            //align elements to left
        }
    }

    FileDialog{
        id: selectDownloadFolder
        selectMultiple: false
        selectExisting: false
        selectFolder: true
        visible: false
        title: "Select folder"

        onAccepted: {
            fileTransfer.accept(fileTransfer.fileName, selectDownloadFolder.fileUrl)
        }
    }

    function saveFile(){
        var askWhereToSave = appSettings.askWhereToSaveFiles();
        if(askWhereToSave === false)
            fileTransfer.accept(fileTransfer.fileName)
        else{
            selectDownloadFolder.open()
        }
    }

    function computeStatusText(status){
        var statusText = "";
        if(status === CFileTransfer.OfferState){
            statusText = qsTr("File transfer offered");
        }

        if(status === CFileTransfer.StartState){
            statusText = qsTr("File transfer started")
        }

        if(status === CFileTransfer.TransferState){
            statusText = qsTr("Transfering")
        }

        if(status === CFileTransfer.FinishedState){
            var error = fileTransfer.error

            switch(error){
            case CFileTransfer.AbortError:
                statusText = qsTr("File transfer aborted.");
                break;
            case CFileTransfer.FileAccessError:
                statusText = qsTr("Error: unable to access the file.");
                break;

            case CFileTransfer.FileCorruptError:
                statusText = qsTr("Error: file is corrupted");
                break;

            case CFileTransfer.ProtocolError:
                statusText = qsTr("Error: protocol error");
                break;
            case CFileTransfer.NoError:
                if(fileTransfer.direction === CFileTransfer.IncomingDirection)
//                    statusText = qsTr("File successfully downloaded to ") + fileTransfer.localFilePath();
                    statusText = qsTr("File downloaded")
                else
//                    statusText = qsTr("File successfully uploaded.");
                    statusText = qsTr("File uploaded.");
                break;
            default:
//                statusText = qsTr("File successfully uploaded.");
                statusText = qsTr("File uploaded.");
                break;
            }
        }
        return statusText;
    }

    Row{
        id: row
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 20

        spacing: 5

        Column{
            id: column
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width - buttonsRow.width

            spacing: 5

            Text{
                id: text
                text: fileTransfer.fileName
                height: paintedHeight
                width: root.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }

            Item{
                width: parent.width
                height: 15

                ProgressBar{
                    id: progressBar
                    anchors.fill: parent

                    value: (fileTransfer.status === CFileTransfer.HistoryState && fileTransfer.direction === CFileTransfer.OutgoingDirection)? 1.0 : fileTransfer.progress
                    minimumValue: 0
                    maximumValue: 1.0

                    style: ProgressBarStyle {
                        background: Rectangle {
                            radius: progressBar.width
                            color: "white"
                            border.color: "white"
                            border.width: 1
                        }
                        progress: Rectangle {
                            radius: progressBar.width
                            color: "#87d54e"
                            border.color: "white"
                        }
                    }
                }
            }

            Row{
                spacing: 30

                Text{
                    id:timeText
                    text : fileTransfer.time
                    width: paintedWidth
                    height: paintedHeight
                }

                Text{
                    id: statusText
                    text: (fileTransfer.status === CFileTransfer.HistoryState && fileTransfer.direction === CFileTransfer.OutgoingDirection)? qsTr("File transfered"): computeStatusText(fileTransfer.status)
                    width: root.width
                    height: paintedHeight
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
            }
        }

        Row{
            id: buttonsRow
            spacing: 2
            anchors.verticalCenter: parent.verticalCenter

            Button{
                id: denyButton

                property string icon: "qrc:/images/resources/cancel.png"
                property string hoveredIcon:  "qrc:/images/resources/cancel_hovered.png"

                visible: (fileTransfer.status === CFileTransfer.FinishedState) || (fileTransfer.status === CFileTransfer.HistoryState /*&& fileTransfer.direction === CFileTransfer.OutgoingDirection*/)? false:true
                width: buttonSize
                height: buttonSize
                //                text: "No"
                onClicked: fileTransfer.abort()
                style: buttonStyle

            }

            Button{
                id: acceptButton

                property string icon: "qrc:/icons/download-icon.png"
                property string hoveredIcon: "qrc:/icons/download-icon.png"

                visible: fileTransfer.direction === CFileTransfer.IncomingDirection && (fileTransfer.status === CFileTransfer.OfferState || fileTransfer.status === CFileTransfer.HistoryState)

                width: buttonSize
                height: buttonSize
                //                text: "Ok"
                onClicked: {
                    if(fileTransfer.status === CFileTransfer.HistoryState){
                        client.requestFileLog(fileTransfer)
                    }
                    else{
                        saveFile()
                    }
                }
                style: buttonStyle
            }

            ToolButton{
                id: openContainingFolder
                iconSource: "qrc:/icons/open-folder-icon.png"
                visible:  fileTransfer.direction === CFileTransfer.IncomingDirection && fileTransfer.status === CFileTransfer.FinishedState
                onClicked: {
                    var opened = appSettings.openFolder(fileTransfer.localFilePath())
                }
            }
        }
    }

    Component{
        id: buttonStyle

        ButtonStyle{
            background: Item{
                width: 40
                height: 40

                Image{
                    anchors.fill: parent
                    source: !control.hovered ? control.icon : control.hoveredIcon
                }
            }
        }
    }
}
