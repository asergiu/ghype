import QtQuick 2.0
import QtQuick.Window 2.2

Window{
    id: listview_root
    flags: Qt.FramelessWindowHint
    width: 200 // no sublist view
    height: delegateHeight*model.count
    color: appSettings.operatingSystem() === "Q_OS_MAC" ? "transparent" : "white"

    FocusScope {
        id: focusScope
        focus: true
        anchors.fill: parent
        onActiveFocusChanged: {
            if(!activeFocus){
               listview_root.hide()
            }
        }
    }

    property int arrowX: 0
    property int arrowY: 0
    property int currentIndex: -1
    property int sublistIndex: -1
    property string generalIndex: "-1"
    property alias model: listview.model

    property bool positionedToLeft: false
    readonly property int delegateHeight: 52

    function display(pos, size){

        var maxPosX = Screen.desktopAvailableWidth

        var listPos = Qt.point(0, 0);
        var arrowPos = Qt.point(0, 0);

        /// if we can position the list below the clicked button
        if(pos.x + size.width + listview_root.width < maxPosX){
            /// position the list to the right
            listPos = Qt.point(pos.x + size.width, pos.y);
            positionedToLeft = false
            listView_holder.x = listView_holder.parent.x + arrow.width / 2
            arrow.x = -arrow.width / 2 + listView_holder.border.width
            arrow.y = delegateHeight*0.25
        }
        else{
            /// position the list to the left
            listPos = Qt.point(pos.x - listview_root.width, pos.y)
            positionedToLeft = true
            listView_holder.x = 0;
            arrow.x = listView_holder.width - arrow.width / 2 - listView_holder.border.width
            arrow.y = delegateHeight*0.25
        }

        listview_root.x = listPos.x
        listview_root.y = listPos.y

        show();
    }

    Rectangle {
        id: listView_holder
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: listview_root.width - 5
        color: "#22c064"
        radius: 5
        border.color: "white"
        border.width: 1

        Rectangle{
            id: arrow
            //x: listview_root.arrowX == 0 ? listview_root.arrowX - width/2 : listview_root.arrowX
            x: -width/2
            y: delegateHeight*0.25
            width:10
            height:10
            rotation: 45
            color: "#22c064"
            border.color: parent.border.color
            border.width: parent.border.width
        }

        ListView {
            id: listview
            anchors.fill:parent
            model: listview_root.model
            delegate:dropdownDelegate
            interactive: false
        }

        Component{
            id: dropdownDelegate

            Item{
                width: parent.width
                height: listView_holder.height/listview_root.model.count
                Rectangle{
                    id: list_item
                    anchors.fill: parent
                    color: "#22c064"
                    anchors.leftMargin: 1
                    anchors.rightMargin: 1
                    anchors.topMargin: index==0 ? 1:0
                    anchors.bottomMargin: index==listview_root.model.count-1 ? 1:0
                    radius: index==0 || index==listview_root.model.count-1 ? listView_holder.radius : 0

                    Rectangle{ //covers the bottom/top radius for the first/last element (when highlighted)
                        height: list_item.radius
                        color: list_item.color
                        y: index==0 ? parent.y+parent.height : parent.y
                        anchors.left: list_item.left
                        anchors.right: list_item.right
                    }

                    Rectangle{
                        id: separator
                        color: "#21b25c"
                        visible: index==listview_root.model.count-1 ? false: true
                        height: 2
                        width: parent.width
                        anchors.bottom: parent.bottom
                    }
                    Image{
                        id: left_symbol
                        anchors.verticalCenter: parent.verticalCenter
                        x: parent.x + 17
                        fillMode: Image.PreserveAspectFit
                        source: icon
                    }
                    Text{
                        anchors.verticalCenter: parent.verticalCenter
                        x: left_symbol.width==0 ?
                               left_symbol.x + left_symbol.width
                             :
                               left_symbol.x + left_symbol.width + 10

                        text: name
                        color: "white"
                        font.pixelSize: 12
                        font.family: "Roboto Bold"
                    }
                    Image{
                        id: right_symbol
                        anchors.verticalCenter: parent.verticalCenter
                        x: parent.x+parent.width-20-width
                        fillMode: Image.PreserveAspectFit
                        source: symbol
                    }

                    MouseArea{
                        id: mouse_area
                        anchors.fill:parent
                        hoverEnabled: true
                        onClicked:{
                            listview_root.generalIndex = index;

                        }
                        onContainsMouseChanged: {
                            listview_root.currentIndex = index;
                            if(containsMouse){
                                sublistview.visible=true
                                list_item.color="#84c82c"
                            }
                            else{
                                sublistview.visible=false
                                list_item.color="#22c064";
                            }
                        }
                    }
                    Rectangle{
                        id: sublist_root
                        height: (list_item.height - 2) * sublist.count+100
                        anchors.top: parent.top
                        x: positionedToLeft ? parent.x - parent.width/2 : parent.x + parent.width - 1

                        ListView{
                            id: sublistview
                            model: sublist
                            delegate:sublistDelegate
                            anchors.fill:parent
                            visible: false
                        }
                        Component{
                            id: sublistDelegate
                            Item{
                                width: list_item.width/2
                                height: listview_root.currentIndex==model.count-1 ?
                                            list_item.height
                                        :
                                            list_item.height - 2
                                Rectangle{
                                    id: sublist_item
                                    anchors.fill:parent
                                    color: "#84c82c"
                                    Text{
                                        anchors.verticalCenter: parent.verticalCenter
                                        x: 14
                                        text: name
                                        color: "white"
                                        font.pixelSize: 12
                                        font.family: "Roboto Bold"
                                    }
                                    MouseArea{
                                        id: sublist_mouse_area
                                        anchors.fill:parent
                                        hoverEnabled: true
                                        onClicked:{
                                            listview_root.sublistIndex = index;
                                            listview_root.generalIndex = listview_root.currentIndex+"-"+index;
                                        }

                                        onContainsMouseChanged: {
                                            listview_root.sublistIndex = -1;
                                            if(containsMouse) {
                                                sublistview.visible=true
                                                list_item.color="#84c82c"
                                                sublist_item.color="#90CE41"
                                            }
                                            else{
                                                sublistview.visible=false
                                                list_item.color="#22c064"
                                                sublist_item.color="#84c82c"
                                            }
                                        }
                                    }//mouse area
                                }//sublist_item

                            }//item

                        }//component sublistDelegate
                    }//sublist_root

                } //list_item

            }//item

        }//component dropdownDelegate

    }//listview_root
}
