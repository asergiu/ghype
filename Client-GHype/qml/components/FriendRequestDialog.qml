import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Dialog{
    id: friendReqDialog
    title: "New friend request"
    width: 500
    height: 150
    contentItem: Rectangle{
        color: "#979ef8"

        Column{
            anchors.centerIn: parent

            Text{
                id: dialogText
                text: qsTr("You received a new friend request from <b>")+friendReqDialog.id+"</b>"
                width: paintedWidth + 10
                height: paintedHeight + 10
                anchors.horizontalCenter: parent.horizontalCenter
                color: "white"
            }

            Row{
                anchors.right: dialogText.right
                spacing: 10

                Button{
                    id: accept
                    width: 40
                    height: 40

                    onClicked: {
                        client.acceptSubscription(friendReqDialog.id)
                        friendReqDialog.close()
                    }

                    style: ButtonStyle{
                        background: Rectangle{
                            width: control.width
                            height: control.height
                            color: "transparent"
                            radius: 5

                            Image{
                                anchors.fill: parent
                                source: control.hovered ? "qrc:/images/resources/add_contact_hovered.png" : "qrc:/images/resources/add_contact.png"
                                fillMode: Image.PreserveAspectFit
                            }
                        }
                    }
                }

                Button{
                    id: deny
                    width: 40
                    height: 40

                    onClicked: {
                        client.denySubscription(friendReqDialog.id)
                        friendReqDialog.close()
                    }

                    style: ButtonStyle{
                        background: Rectangle{
                            width: control.width
                            height: control.height
                            color: "transparent"
                            radius: 5

                            Image{
                                anchors.fill: parent
                                source: control.hovered ? "qrc:/images/resources/cancel_hovered.png" : "qrc:/images/resources/cancel.png"
                                fillMode: Image.PreserveAspectFit
                            }
                        }
                    }
                }
            }
        }
    }

    property string id: ""

    function openDialog(jid){
        friendReqDialog.id = jid
        friendReqDialog.open()
    }
}

