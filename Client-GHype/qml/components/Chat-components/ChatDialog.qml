import QtQuick 2.3;
import QtQuick.Dialogs 1.2;
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import CClient 1.0;
import CContact 1.0;
import CMessage 1.0;
import CCommunicationItem 1.0
import CFileTransfer 1.0
import MessageModel 1.0
import "../../components" as Components

Window{
    id: window
    visible: false
    width: 370
    height: 470
    minimumWidth: 300
    minimumHeight: 200
    title: getContactFullName()
    modality: Qt.NonModal

    property var contact
    property int index
    property var callbackFunction
    property variant win
    property var profileInfoWin: null

    signal sDialogDestroyed(var sJid)

    onVisibleChanged: {
        if(!visible){
            sDialogDestroyed.connect(callbackFunction)
            sDialogDestroyed(contact.jid)
            destroy()
        } else {
            if (messagesList.count <= contact.msgHistoryChunkSize) {
                client.getChatHistoryByChunk(contact);
            }
        }
    }

    function getContactFullName() {
        var defaultContactName = ""
        if (typeof contact === "undefined") {
            return defaultContactName;
        } else {
            if (contact.profile.firstName === "" && contact.profile.lastName === ""){
                if (typeof contact.getJidUsername() === "undefined") {
                    return defaultContactName;
                } else {
                    return contact.getJidUsername();
                }
            } else {
                return contact.profile.firstName + " " + contact.profile.lastName;
            }
        }
    }

    Connections{
        target: client
        ignoreUnknownSignals: true
        onSMessageLogsReceived: {
            messagesList.requestHistory = false;
            if (!contact.canRequestHistoryLogs) {
                loadMoreBtn.visible = false;
            }
        }
    }

    MouseArea{/* *** mouse area used to detect wheter the user entered with the mouse inside chat dialog bounds *** */
        anchors.fill: parent
        hoverEnabled: true
        onEntered: {
            contact.hasUnreadMessages = false
        }
    }

    Rectangle{
        anchors.fill: parent
        color: '#3e51b5'

        Rectangle{
            id: first_part
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: parent.height * (70/100)
            anchors.margins: 1
            color: "#eef1f6"

            Rectangle{
                id: interlocutor_rectangle
                anchors.left: parent.left
                anchors.top: parent.top
                width: parent.width * 0.5
                height:40
                color: "#979df7"
                onWidthChanged: {
                    profile_name.text=text_computing()
                }

                function text_computing(){
                    var text
                    if (typeof contact==="undefined"){
                        text= ""
                    }
                    else{
                        if (contact.profile.firstName === "" && contact.profile.firstName === "") {
                            text = contact.getJidUsername();
                        } else {
                            text = contact.profile.firstName + " " + contact.profile.lastName
                        }
                    }
                    if(typeof contact==="undefined" || typeof contact.getJidUsername() === "undefined"){
                        return ""
                    }
                    else if (profile_name.contentWidth>(interlocutor_rectangle.width-contact_icon.width-contact_icon.anchors.leftMargin-profile_name.anchors.leftMargin)-6){
                        //  TO DO !! Merge doar la size textului de acum (pointSize 10)
                        var res = text.substring(0,(20*(interlocutor_rectangle.width-contact_icon.width-contact_icon.anchors.leftMargin-profile_name.anchors.leftMargin)/125)-3)
                        res=res.concat("...")
                        return res
                    }
                    if (profile_name.contentWidth<(interlocutor_rectangle.width-contact_icon.width-contact_icon.anchors.leftMargin-profile_name.anchors.leftMargin)-3)
                    {
                        return text
                    }
                    if (interlocutor_rectangle.width<window.minimumWidth*20/100) {
                        var aux
                        if(contact.profile.firstName === ""&&contact.profile.lastName===""){
                            aux=contact.getJidUsername();
                        }
                        else{
                            aux=contact.profile.lastName
                        }
                        profile_name.text=aux.substring(0,1)}
                }

                Image{
                    id: contact_icon
                    anchors {
                        left: parent.left; leftMargin: 5;
                        verticalCenter: parent.verticalCenter;
                    }
                    source: typeof contact==="undefined" || contact.profile.imagePath === "" ? "qrc:/images/resources/common_contact_70x70.png" : contact.profile.imagePath
                    width: 30
                    height: 30
                    fillMode: Image.PreserveAspectCrop
                    antialiasing: true
                    smooth: true

                    Image {
                        id: contact_icon_mask
                        anchors.fill: parent
                        source: "qrc:/images/resources/contact_icon_purple_mask.png"
                        antialiasing: true
                        smooth: true
                    }
                }

                Text{
                    id: profile_name
                    anchors.left: contact_icon.right
                    anchors.leftMargin: 5
                    anchors.verticalCenter: contact_icon.verticalCenter
                    text: getContactFullName();
                    font { family: "Roboto Regular"; pointSize: 10; }
                    color: "#ffffff"
                    onTextChanged: {
                        var aux
                        if (contentWidth>(interlocutor_rectangle.width-contact_icon.width-contact_icon.anchors.leftMargin-profile_name.anchors.leftMargin))
                        {
                            //  TO DO !! Merge doar la size textului de acum
                            var res = text.substring(0,(20*(interlocutor_rectangle.width-contact_icon.width-contact_icon.anchors.leftMargin-profile_name.anchors.leftMargin)/125)-3)
                            res=res.concat("...")
                            profile_name.text=res
                        }
                        else if (interlocutor_rectangle.width<window.minimumWidth*20/100) {


                            if(contact.profile.firstName === ""&&contact.profile.lastName===""){
                                aux=contact.getJidUsername();
                            }
                            else{
                                aux=contact.profile.lastName
                            }

                            profile_name.text=aux.substring(0,1)}
                    }
                }

                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        if(profileInfoWin !== null){
                            profileInfoWin.show()
                            profileInfoWin.raise()
                        }else{
                            var component = Qt.createComponent("qrc:/qml/ProfileInfoView.qml");
                            if(component.status == Component.Ready ){
                                profileInfoWin = component.createObject(window);
                                profileInfoWin.show();
                            }
                        }
                    }
                }
            }

            Text {
                id: loadMoreBtn
                text: "Load Earlier Messages"
                anchors { top: interlocutor_rectangle.bottom; topMargin: 3; horizontalCenter: parent.horizontalCenter; }
                visible: false
                color: "blue"

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        messagesList.requestHistory = true;
                    }
                    onEntered: {
                        cursorShape = Qt.PointingHandCursor;
                    }
                    onExited: {
                        cursorShape = Qt.ArrowCursor;
                    }
                }
            }

            ScrollView {
                id: messagesScrollView
                anchors { fill: parent; topMargin: 60; bottomMargin: 20; leftMargin: 10; }
                flickableItem.boundsBehavior: Flickable.StopAtBounds

                property bool positionViewAtEnd: true
                property real lastContentHeight: NaN

                function positionAtEnd() {
//                    console.log("position at end: content y: ", flickableItem.contentY, "; content height: ", flickableItem.contentHeight, "; viewport height: ", viewport.height, "; ratio: ", flickableItem.visibleArea.heightRatio)
                    if (flickableItem.contentHeight > viewport.height) {
                        flickableItem.contentY = flickableItem.contentHeight
                        flickableItem.returnToBounds();
                    }
                }

                function isAtBottom() {
                    var bottomThreshold = 1 - flickableItem.visibleArea.heightRatio;
                    var bottomVisibleArea = flickableItem.visibleArea.yPosition + flickableItem.visibleArea.heightRatio;
                    return bottomVisibleArea > bottomThreshold;
                }

                flickableItem.onContentHeightChanged: {
//                    console.log("content height changed: ", flickableItem.contentHeight, "count is: ", messagesList.count)
                    if (positionViewAtEnd && flickableItem.contentHeight > 0) {
                        positionAtEnd();
                        positionViewAtEnd = false;
                    }
                    if (!isNaN(lastContentHeight)) {
//                        console.log("last content height: ", lastContentHeight, "; current content height: ", flickableItem.contentHeight)
                        flickableItem.contentY = flickableItem.contentHeight - lastContentHeight;
                    }
                }

                flickableItem.onContentYChanged: {
//                    console.log("content y changed: ", flickableItem.contentY, " viewport height: ", viewport.height)
                    if (flickableItem.contentY == 0 && contact.canRequestHistoryLogs) {
                        loadMoreBtn.visible = true;
                    } else {
                        loadMoreBtn.visible = false;
                    }
                    // hack for scrollview jumping after first scroll after first request history
                    var scrollSize = appSettings.operatingSystem() === "Q_OS_WIN" ? 20 : 40;
                    if (!isNaN(lastContentHeight)) {
                        var localLastContentHeight = lastContentHeight;
                        if (localLastContentHeight == flickableItem.contentY + viewport.height + scrollSize) {
                            lastContentHeight = NaN;
                            flickableItem.contentY = flickableItem.contentHeight - localLastContentHeight - scrollSize;
                        } else if (localLastContentHeight == flickableItem.contentY + viewport.height - scrollSize) {
                            lastContentHeight = NaN;
                            flickableItem.contentY = flickableItem.contentHeight - localLastContentHeight + scrollSize;
                        }
                    }
                }

                Column {
                    width: messagesScrollView.width - 25

                    Repeater {
                        id: messagesList
                        width: parent.width
                        model: contact ? contact.messagesModel: []
                        delegate: delegate

                        property bool requestHistory

                        onRequestHistoryChanged: {
//                            console.log("request history changed to: ", requestHistory)
                            if (requestHistory) {
                                client.getChatHistoryByChunk(contact);
                                messagesScrollView.lastContentHeight = messagesScrollView.flickableItem.contentHeight;
                            }
                        }

                        onCountChanged: {
//                            console.log("count changed: ", count, "; content height: ", messagesScrollView.flickableItem.contentHeight, "; request history: ", requestHistory)
                            if (messagesScrollView.flickableItem.contentHeight == 0 || (messagesScrollView.flickableItem.contentHeight > 0 && messagesScrollView.isAtBottom())) {
                                messagesScrollView.positionViewAtEnd = true;
                            }
                            if (!requestHistory) {
                                messagesScrollView.lastContentHeight = NaN;
                            }
                        }
                    }
                }
            }

            Component{
                id: delegate

                Item{
                    id: itemRow
                    width: parent.width
                    height: itemType === CCommunicationItem.MESSAGE? (messageLoader.item.height + 10) : messageLoader.item.height

                    onWidthChanged: {
                        if(messageLoader.status == Loader.Ready){
                            if(loader.item){
                                if(itemType === CCommunicationItem.MESSAGE){
                                    messageLoader.item.maxWidth = messagesList.width / 1.2
                                }
                                else{
                                    messageLoader.item.width = messagesList.width * 0.75
                                }
                            }
                        }
                    }

                    Loader {
                        id: messageLoader
                        source: itemType === CCommunicationItem.FILE_TRANSFER ? "qrc:/qml/components/FileTransferDelegate.qml" : "qrc:/qml/components/MessageDelegate.qml"
                        onLoaded: {

                            var alignRight;
                            if(itemType === CCommunicationItem.FILE_TRANSFER) // file transfer delegate
                            {
//                                messageLoader.item.width = window.width*0.75
                                messageLoader.item.width = messagesList.width * 0.75
                                messageLoader.item.height = 70
                                messageLoader.item.color = "transparent"
                                messageLoader.item.fileTransfer = file_transfer_object
                                alignRight = (messageLoader.item.fileTransfer.direction !== CFileTransfer.OutgoingDirection)
                                messageLoader.item.alignRight = alignRight

                                if(alignRight) {
                                    anchors.right = itemRow.right
                                }
                                else{
                                    anchors.left = itemRow.left
                                }
                            }
                            else // message delegate
                            {
                                messageLoader.item.maxWidth = messagesList.width / 1.2
                                alignRight = msg_from !== client.user.jid
                                messageLoader.item.alignRight = alignRight

                                if(alignRight) {
                                    anchors.right = itemRow.right
                                } else {
                                    anchors.left = itemRow.left
                                }
                            }
                        }
                    }
                }
            }

            Text{
                id: interlocutor_action
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 5
                text: getContactFullName() + qsTr(" is typing")
                font.family: "Roboto Light"
                font.pointSize: 8
                color: "#bcc2d2"
                visible: contact && contact.type!==CContact.Unavailable? contact.composing: false
            }
        }

        Rectangle{
            id: second_part
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: first_part.bottom
            anchors.margins: 1

            Rectangle{
                id: chatTFLayout
                anchors.left: parent.left
                anchors.leftMargin: 30
                anchors.top: parent.top
                anchors.topMargin: 15
                anchors.right: controls.left
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 15
                color: "transparent"

                TextArea {
                    id: chatTF
                    anchors { fill: parent; }
                    font.pointSize: 10
                    font.family: "Roboto Light"
                    wrapMode: Text.WrapAnywhere
                    clip: true
                    selectByMouse: true
                    flickableItem.boundsBehavior: Flickable.StopAtBounds

                    property int maximumLength: client.MAX_CHARACTERS_PER_MESSAGE
                    property string previousText

                    onTextChanged: {
                        if (length > maximumLength) {
                            var etcText = "..";
                            if (!previousText) {
                                cursorPosition = maximumLength;
                                text = text.slice(0, maximumLength - etcText.length) + etcText;
                                cursorPosition = maximumLength;
                            } else {
                                if (previousText.length != length - 1) {
                                    if (cursorPosition >= length) {
                                        text = text.slice(0, maximumLength - etcText.length) + etcText;
                                        cursorPosition = maximumLength;
                                    }
                                }
                                cursorPosition = cursorPosition - 1;
                                var prevCursorPos = cursorPosition;
                                text = previousText;
                                cursorPosition = prevCursorPos;
                            }
                        }
                        previousText = text;
                    }

                    Keys.onPressed: {
                        //send composing message
                        if(!pauseComposingTimer.running)
                            client.sendMessageComposing(true, contact)

                        pauseComposingTimer.restart()
                    }

                    Keys.onReturnPressed: {
                        var str = chatTF.text.replace(/</g, "?")
                        str = emoticonHolder.replaceEmoticons(str)

                        client.sendMessageText(str, contact)
                        chatTF.text = ""
                    }
                    Keys.onEnterPressed: Keys.onReturnPressed(event)

                    Menu{
                        id: contextMenu
                        MenuItem{
                            text: qsTr("Copy")
                            onTriggered: {
                                clipboard.copyTextToClipboard(chatTF.text)
                            }
                        }
                        MenuItem{
                            text: qsTr("Paste")
                            onTriggered: {
                                chatTF.text = emoticonHolder.replaceRichText(clipboard.retrieveTextFromClipboard())
                            }
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.RightButton
                        hoverEnabled: true

                        onEntered: {cursorShape = Qt.IBeamCursor}

                        onExited: {cursorShape = Qt.ArrowCursor}

                        onClicked: {
                            if (mouse.button == Qt.RightButton){
                                contextMenu.popup()
                            }
                        }
                    }
                }
            }

            Rectangle{
                id: controls
                width: 60
                height: parent.height
                anchors.right: parent.right
                color: "transparent"

                property size btnSize: Qt.size(40, 40)

                Button{
                    id:attach
                    width: controls.btnSize.width
                    height: controls.btnSize.height
                    anchors.centerIn: parent
                    style: ButtonStyle {
                        background: Image {
                            width: controls.btnSize.width
                            height: controls.btnSize.height
                            source: "qrc:/images/resources/common_attach_icon.png"

                            Rectangle{
                                anchors.fill: parent
                                radius: 50
                                color: control.pressed? "grey" : "transparent"
                                opacity: 0.6
                            }
                        }
                    }
                    onClicked: {
                        fileDialog.visible = true;
                    }
                }
            }

            FileDialog {
                id: fileDialog
                title: "Attach"
                onAccepted: {
                    client.sendFile(contact, fileDialog.fileUrl, "")
                    visible = false

                    }
                    onRejected: {
                        visible = false
                    }
                }

                Timer {
                    id: pauseComposingTimer
                    interval: 2000; running: false; repeat: false
                    onTriggered:{
                        //send pause message
                        client.sendMessageComposing(false, contact)
                    }
                }
            }

            Emoticons{
                id: emoticonHolder
                width:200
                height:120
                anchors.bottom: emoticonButton.top
                anchors.left: emoticonButton.right
                visible:  false

                onEmoticonClicked: {
                    var cursorPos = chatTF.cursorPosition;
                    var crtText = chatTF.text;
                    chatTF.text = [crtText.slice(0, cursorPos), symbol, crtText.slice(cursorPos)].join('')
                    chatTF.focus = true
                    chatTF.cursorPosition = cursorPos + symbol.length;
                    emoticonHolder.visible = false
                }
            }

            Button{
                id:emoticonButton
                width: 20
                height: 20
                anchors {
                    top: first_part.bottom
                    topMargin: 15
                    left: first_part.left
                    leftMargin: 5
                }
                style: ButtonStyle {
                    background: Image {
                        width: controls.btnSize.width
                        height: controls.btnSize.height
                        source: "qrc:/images/resources/emoticons/smile_mc.png"
                    }
                }
                onClicked: {
                    if(emoticonHolder.visible == false)
                        emoticonHolder.visible= true
                    else
                        emoticonHolder.visible= false
                }
            }
        }
}
