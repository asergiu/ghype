import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.3

Rectangle {
    id: emoticonRoot

    function replaceEmoticons(text){

        /* *** !regex usage + escape characters for: '@', ':', '(' ,')', '|' *** */
        text=text.replace(/\:\@\)/g,"<img src=qrc:/images/resources/emoticons/angry_mo.png>")
        text=text.replace(/\:\@/g,"<img src=qrc:/images/resources/emoticons/angry_mc.png>")
        text=text.replace(/\:xx/g,"<img src=qrc:/images/resources/emoticons/inlove_mo.png>")
        text=text.replace(/\:x/g,"<img src=qrc:/images/resources/emoticons/inlove_mc.png>")
        text=text.replace(/\:\(o/g,"<img src=qrc:/images/resources/emoticons/sad_mo.png>")
        text=text.replace(/\:\(\(/g,"<img src=qrc:/images/resources/emoticons/tears.png>")
        text=text.replace(/\:\(/g,"<img src=qrc:/images/resources/emoticons/sad_mc.png>")
        text=text.replace(/\:\|/g,"<img src=qrc:/images/resources/emoticons/shocked.png>")
        text=text.replace(/\:\)\)/g,'<img src=qrc:/images/resources/emoticons/smile_mo.png>')
        text=text.replace(/\:\)/g,'<img src=qrc:/images/resources/emoticons/smile_mc.png>')
        text=text.replace(/\:\o/g,"<img src=qrc:/images/resources/emoticons/surprized.png>")
        return text
    }

    function replaceRichText(text){
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/angry_mo\.png\>/g, ":@)")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/angry_mc\.png\>/g, ":@")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/inlove_mo\.png\>/g, ":xx")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/inlove_mc\.png\>/g, ":x")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/sad_mo\.png\>/g, ":(o")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/tears\.png\>/g, ":((")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/sad_mc\.png\>/g, ":(")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/shocked\.png\>/g, ":|")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/smile_mo\.png\>/g, ":))")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/smile_mc\.png\>/g, ":)")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/surprized\.png\>/g, ":o")
        return text
    }

    signal emoticonClicked(var symbol)

    Component{
        id: emoticonDelegate
        Item{
            id: item
            width: emoticonGrid.cellWidth
            height: emoticonGrid.cellHeight

            Column{
                anchors.centerIn: parent
                Button{
                    id: btn
                    style: ButtonStyle {
                        background: Image {
                            width: emoticonGrid.cellWidth
                            height: emoticonGrid.cellHeight
                            source: image
                        }
                    }
                    opacity: hovered? 1 : 0.7
                    onClicked:emoticonClicked(symbol)
                }
            }
        }
    }

    FocusScope{
        id: focusScope
        anchors.fill: parent

        onActiveFocusChanged: {

            if(activeFocus === false)
                emoticonRoot.visible = false
        }
    }

    GridView{
        id: emoticonGrid
        anchors.fill:parent
        model: emoticonModel
        delegate: emoticonDelegate
        cellWidth: 40
        cellHeight: 40
        clip:true
        flow:GridView.FlowLeftToRight
    }

    onVisibleChanged: {
        if(visible === true)
            focusScope.focus = true
    }

    ListModel{
        id: emoticonModel
        ListElement{
            symbol: ":@)"
            image: "qrc:/images/resources/emoticons/angry_mo.png"
        }
        ListElement{
            symbol: ":@"
            image: "qrc:/images/resources/emoticons/angry_mc.png"
        }
        ListElement{
            symbol: ":xx"
            image: "qrc:/images/resources/emoticons/inlove_mo.png"
        }
        ListElement{
            symbol: ":x"
            image: "qrc:/images/resources/emoticons/inlove_mc.png"
        }
        ListElement{
            symbol: ":(o"
            image: "qrc:/images/resources/emoticons/sad_mo.png"
        }
        ListElement{
            symbol: ":(("
            image: "qrc:/images/resources/emoticons/tears.png"
        }
        ListElement{
            symbol: ":("
            image: "qrc:/images/resources/emoticons/sad_mc.png"
        }
        ListElement{
            symbol: ":|"
            image: "qrc:/images/resources/emoticons/shocked.png"
        }
        ListElement{
            symbol: ":))"
            image: "qrc:/images/resources/emoticons/smile_mo.png"
        }
        ListElement{
            symbol: ":)"
            image: "qrc:/images/resources/emoticons/smile_mc.png"
        }
        ListElement{
            symbol: ":o"
            image: "qrc:/images/resources/emoticons/surprized.png"
        }
    }
}

