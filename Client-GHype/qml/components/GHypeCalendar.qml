import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.2

Calendar{
    id: calendar

    visible: false
    visibleYear: 2015
    focus: true

    signal newDateSelected(string dateString)

    function show(x, y){
        calendar.x = x
        calendar.y = y
        visible = true;
    }

    function hide(){
        calendar.x = 0;
        calendar.y = 0;
    }

    style: CalendarStyle {
        gridVisible: false
        navigationBar:Rectangle {
            color: "#f9f9f9"
            height: 25 * 2

            Rectangle {
                color: Qt.rgba(1, 1, 1, 0.6)
                height: 1
                width: parent.width
            }

            Rectangle {
                anchors.bottom: parent.bottom
                height: 1
                width: parent.width
                color: "#ddd"
            }
            ToolButton {
                id: previousMonth
                width: parent.height
                height: width
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                iconSource: "qrc:/icons/left-angle-arrow.png"
                onClicked: control.showPreviousMonth()
            }

            Row{
                spacing: 20
                anchors.centerIn: parent
                ComboBox{
                    height: 20
                    width: 75
                    model: [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN","JUL", "AUG", "SEP", "OCT", "NOV", "DEC" ];
                    currentIndex: control.visibleMonth

                    onCurrentIndexChanged:  {
                        calendar.visibleMonth = currentIndex
                    }

                }

                SpinBox{
                    id: yearSpinBox
                    maximumValue: new Date().getFullYear();
                    minimumValue: 1900
                    value: control.visibleYear

                    onValueChanged: {
                        calendar.visibleYear = yearSpinBox.value
                    }
                }
            }

            ToolButton {
                id: nextMonth
                width: parent.height
                height: width
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                iconSource: "qrc:/icons/right-angle-arrow.png"
                onClicked: control.showNextMonth()
            }
        }

        dayDelegate: Rectangle {
            gradient: Gradient {
                GradientStop {
                    position: 0.00
                    color: styleData.selected ? "#111" : (styleData.visibleMonth && styleData.valid ? "#444" : "#666");
                }
                GradientStop {
                    position: 1.00
                    color: styleData.selected ? "#444" : (styleData.visibleMonth && styleData.valid ? "#111" : "#666");
                }
                GradientStop {
                    position: 1.00
                    color: styleData.selected ? "#777" : (styleData.visibleMonth && styleData.valid ? "#111" : "#666");
                }
            }

            Label {
                text: styleData.date.getDate()
                anchors.centerIn: parent
                color: styleData.valid ? "white" : "grey"
            }

            Rectangle {
                width: parent.width
                height: 1
                color: "#555"
                anchors.bottom: parent.bottom
            }

            Rectangle {
                width: 1
                height: parent.height
                color: "#555"
                anchors.right: parent.right
            }
        }
    }

    onClicked: {
        var dateString = Qt.formatDate(date, "dd/MM/yyyy");
        newDateSelected(dateString)
        visible = false
    }

}
