import QtQuick 2.4
import QtQuick.Controls 1.3
import CMessage 1.0

Item {
    id: msgHolder
    height: textRect.height + timeText.height + timeText.anchors.topMargin
    width: maxWidth

    property int maxWidth
    property string originalText
    property bool alignRight

    function replaceRichText(text){
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/angry_mo\.png\>/g, ":@)")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/angry_mc\.png\>/g, ":@")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/inlove_mo\.png\>/g, ":xx")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/inlove_mc\.png\>/g, ":x")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/sad_mo\.png\>/g, ":(o")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/tears\.png\>/g, ":((")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/sad_mc\.png\>/g, ":(")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/shocked\.png\>/g, ":|")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/smile_mo\.png\>/g, ":))")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/smile_mc\.png\>/g, ":)")
        text=text.replace(/\<img src\=qrc\:\/images\/resources\/emoticons\/surprized\.png\>/g, ":o")
        return text
    }

    onAlignRightChanged: {
        if (alignRight) {
            timeText.anchors.right = textRect.anchors.right = msgHolder.right;
        } else {
            timeText.anchors.left = textRect.anchors.left = msgHolder.left;
        }
    }

    Rectangle {
        id: textRect
        width: messageText.width + 10
        height: messageText.height + 10
        color: alignRight ? "#B6D2E3" : "#D7EBF7"
        radius: 5

        Text {
            id: hiddenText
            text: msg_body
            textFormat: TextEdit.RichText
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            font.family: "Roboto Light"
            visible: false
        }

        /* ***Workaround: draw TextInput over Text(invisible) element in order to enable copying text. The Text component text cannot pe coppied *** */
        TextEdit {
            id: messageText
            width: hiddenText.width > maxWidth - 10 ? maxWidth - 10 : hiddenText.width
            anchors { centerIn: parent; }
            text: msg_body
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            color: msg_marker === 0 ? "red" : msg_marker === 1 ? "blue" : "black"
            font.family: "Roboto Light"
            selectByMouse: true
            readOnly: true
            textFormat: TextEdit.AutoText
            onTextChanged: originalText = msgHolder.replaceRichText(msg_body)

            function copySelectedText(){
                var str = messageText.selectedText
                var k=0
                for(var i=0;i<str.length;i++){
                    if(str.charCodeAt(i) === 65532){// 65532 is the value displayed in js of the undefined character/emoticon
                        k+=2
                    }
                }

                str = originalText.slice(messageText.selectionStart, messageText.selectionEnd + k)

                return str
            }

            function copyAllText(){
                return originalText
            }

            Menu{
                id: contextMenu
                MenuItem{
                    text: qsTr("Copy Selection")
                    onTriggered: clipboard.copyTextToClipboard(messageText.copySelectedText())
                }
                MenuItem{
                    text: qsTr("Copy All")
                    onTriggered: clipboard.copyTextToClipboard(messageText.copyAllText())
                }
            }

            Keys.onPressed: {
                if(event.modifiers && Qt.ControlModifier) {
                    if(event.key === Qt.Key_C) {
                        event.accepted = true
                        clipboard.copyTextToClipboard(copySelectedText())
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.RightButton
                hoverEnabled: true
                cursorShape: Qt.ArrowCursor

                onEntered: {cursorShape = Qt.IBeamCursor}

                onExited: {cursorShape = Qt.ArrowCursor}

                onClicked: {
                    if (mouse.button == Qt.RightButton){
                        contextMenu.popup()
                    }
                }
            }
        }
    }

    Text {
        id: timeText
        anchors { top: textRect.bottom; topMargin: 5; }
        text: msg_time//"15/06/2015"
    }
}

