import QtQuick 2.2
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.2
import CClient 1.0
import CContact 1.0
import CMessage 1.0
import QtQuick.Window 2.2
import "components"
import "settingsviews"

ApplicationWindow {

    property string appVersion: "1.0.1"

    id: main
    visible: true
    minimumWidth: 330
    minimumHeight: 500
    width: 330
    height: 700
    title: qsTr("Foureo") + " v" + appVersion

    menuBar: MenuBar {
        id: menuBar

        Menu {
            id: menu_profile
            title: qsTr("Foureo")
            visible: false

            MenuItem{
                text: qsTr("Foureo Settings")
                onTriggered:{
                    settingsDialog.open()
                }
            }

            MenuItem {
                text: qsTr("Sign out")
                onTriggered: {
                    loader.source = loginView
                    client.loadCredentials(false)
                    client.signout();
                }
            }
        }

//        Menu{
//            id: menu_history
//            title: qsTr("History")
//            visible: false

//            MenuItem{
//                text: qsTr("File transfer")
//                onTriggered: {
//                    fileTransferHistoryDialog.open()
//                }
//            }
//        }

        Menu {
            id: menu_contacts
            title: qsTr("Contacts")
            visible: false
            MenuItem {
                text: qsTr("Add contact")
                onTriggered: addUserDialog.open()
            }
        }
    }

    readonly property string loginView: "qrc:/qml/Login.qml"
    readonly property string chatRoomView: "qrc:/qml/MainView.qml"

    onXChanged: {
        if(loader.status == Loader.Ready){
            if(loader.item /*&& loader.item.windowGlobalPosX*/){
                loader.item.windowGlobalPosX = x;
            }
        }
    }
    onYChanged: {
        if(loader.status == Loader.Ready){
            if(loader.item /*&& loader.item.windowGlobalPosY*/){
                loader.item.windowGlobalPosY = y;
            }
        }
    }

    function enableMenuBar(val){
        menu_profile.visible = val
        menu_contacts.visible = val
//        menu_history.visible = val
    }

    Component.onCompleted: {

        if(!client.loadCredentials(true))
            loader.source = loginView;
    }

    CClient{
        id: client

        property string errorMsg

        onClientStateChanged:{

            console.log("CLIENT STATE: " + error)

            if(error === CClient.NoError){
                if(client.clientState === CClient.ConnectedState){
                    client.saveCredentials(true)
                    loader.source = chatRoomView
                    main.enableMenuBar(true)

                    if(loader.status == Loader.Ready){
                        if(loader.item /*&& loader.item.windowGlobalPosX*/){
                            loader.item.windowGlobalPosX = x
                            loader.item.windowGlobalPosY = y
                        }
                    }
                }
                else if(client.clientState == CClient.ConnectingState){
                    client.saveCredentials(false);
                    loader.source = loginView;
                    loader.item.changeStatus("Authenticating", "auth");
                    main.enableMenuBar(false)
                }
                else if(client.clientState === CClient.DisconnectedState){
                    client.saveCredentials(false);
                    loader.source = loginView

                    loader.item.changeStatus(errorMsg, "error")
                    errorMsg = ""
                    main.enableMenuBar(false)
                }
            }
            else{
                if(error === CClient.SocketError){
                    errorMsg = "Cannot connect to server!"
                }
                else if(error === CClient.KeepAliveError){
                    errorMsg = "Keep alive error! Contact the administrator!"
                }
                else if(error === CClient.XmppStreamError){
                    errorMsg = "Username and/or password incorrect!"
                }

                loader.item.changeStatus(errorMsg, "error")

                if (addUserDialog.visible)
                    addUserDialog.close()
                main.enableMenuBar(false)
            }
        }

        onSJidValidationReceived: {
            addUserDialog.jidValidationReceived(username, message);
        }
    }

    Connections{
        id: clientConnections
        target: client
        ignoreUnknownSignals: true
        onSFriendRequestReceived:{
            friendReqDialog.openDialog(jid);
        }

        onSFileTransferReqArrived:{
            internalVars.currentFileTransfer = fileTransfer
            fileTransferDialog.open()
        }
    }

    Loader {
        id:loader
        anchors.fill: parent
        visible: source != ""
        focus: true
//        onLoaded: {
//            loader.item.changeStatus("Username and/or password incorrect!", "error");
//        }
    }

    AddUserDialog{
        id: addUserDialog
        onAddClient: {
            client.subscribe(jid)
        }
    }

    FriendRequestDialog{
        id: friendReqDialog
    }

    Connections{
        target: loader.sourceComponent
        onExitProfile:{}
    }

    //////////////////////////////// file transfer provisory

    QtObject{
        id: internalVars
        property var currentFileTransfer;
    }

    FileDialog {
        id: fileDialog
        title: "Save file"

        selectExisting: false
        selectMultiple: false
        selectFolder: true
        onAccepted: {
            visible = false
            fileTransferDialog.close();
            close();
            internalVars.currentFileTransfer.accept(fileUrl + "/"+internalVars.currentFileTransfer.fileName);
        }
        onRejected: {
            visible = false
            fileTransferDialog.close();
            internalVars.currentFileTransfer.abort()
            close();
        }

    }

    Dialog{
        id: settingsDialog
        width: 650
        height: 600
        visible: false

        onVisibleChanged: {
            settingsContentItem.visible = visible
        }

        contentItem: SettingsDialog{
            id: settingsContentItem
            width: 550
            height: 500
            onCloseSettingsDialog: settingsDialog.close()
        }
    }
}
