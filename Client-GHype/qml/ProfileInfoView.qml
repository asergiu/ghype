import QtQuick 2.0
import QtQuick.Window 2.2
import CClient 1.0;
import CContact 1.0;
import CMessage 1.0;
import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

Window {
    title: qsTr("Profile Info")
    width: 500
    height: 500
    maximumHeight: 500
    minimumHeight: 500
    maximumWidth: 500
    minimumWidth: 500
    Image{
        id: profile_img
        width: 100
        height: 100
        anchors.top:parent.top
        anchors.left: parent.left
        anchors.topMargin: 25
        anchors.leftMargin: 25
        fillMode: Image.PreserveAspectCrop
        source: typeof contact==="undefined" || contact.profile.imagePath === "" ? "qrc:/images/resources/common_contact_70x70.png" : contact.profile.imagePath
    }
    TextArea {
        height: 100
        width: 330
        anchors.left: profile_img.right
        anchors.leftMargin: 10
        anchors.top: profile_img.top
        text: contact.profile.description
        font.family: "Roboto Regular"
        font.pointSize: 10
        activeFocusOnPress:false
        style: TextAreaStyle {
            textColor: "#272727"
        }
    }
    Rectangle {
        id:effectLine
        width: 460
        height: 1
        color:"#e0e0e0"
        anchors.top: profile_img.bottom
        anchors.topMargin: 25
        anchors.left:parent.left
        anchors.leftMargin: 20
    }
    Rectangle{
        id: holder_textlabel
        width: 150
        height: 250
        color:"transparent"
        anchors.top: effectLine.bottom
        anchors.left:parent.left
        anchors.topMargin: 10
        anchors.leftMargin: 30
        Text{
            id:firstName_txt
            anchors.top :parent.top
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 15
            text: qsTr("First name:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
        Text{
            id:lastName_txt
            anchors.top :firstName_txt.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: qsTr("Last Name:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
        Text{
            id: nickName_txt
            anchors.top :lastName_txt.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: qsTr("Nickname:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
        Text{
            id:email_txt
            anchors.top :nickName_txt.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: qsTr("Email:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
        Text{
            id : country_txt
            anchors.top :email_txt.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: qsTr("Country:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
        Text{
            id:city_txt
            anchors.top :country_txt.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: qsTr("City:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
        Text{
            id:telephone_txt
            anchors.top :city_txt.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: qsTr("Telephone:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
        Text{
            id:webPage_txt
            anchors.top :telephone_txt.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: qsTr("Web page:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
        Text{
            anchors.top :webPage_txt.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: qsTr("Birthday:")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#717173"
        }
    }
    Rectangle{
        width: 150
        height: 250
        color:"transparent"
        anchors.top:holder_textlabel.top
        anchors.left:holder_textlabel.right
        Text{
            id:firstName_txt2
            anchors.top :parent.top
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 15
            text:  contact.profile.firstName==="undefined" ? "" : contact.profile.firstName
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
        Text{
            id:lastName_txt2
            anchors.top :firstName_txt2.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: contact.profile.lastName==="undefined" ? "" : contact.profile.lastName
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
        Text{
            id: nickName_txt2
            anchors.top :lastName_txt2.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text:  contact.profile.nickname==="undefined" ? "" : contact.profile.nickname
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
        Text{
            id:email_txt2
            anchors.top :nickName_txt2.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text:  contact.profile.email==="undefined" ? "" : contact.profile.email
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
        Text{
            id : country_txt2
            anchors.top :email_txt2.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text:  contact.profile.country==="undefined" ? "" : contact.profile.country
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
        Text{
            id:city_txt2
            anchors.top :country_txt2.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: contact.profile.city==="undefined" ? "" : contact.profile.city
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
        Text{
            id:telephone_txt2
            anchors.top :city_txt2.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: contact.profile.telephone==="undefined" ? "" : contact.profile.telephone
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
        Text{
            id:webPage_txt2
            anchors.top :telephone_txt2.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text:typeof contact.profile.webPage==="undefined" ? "" : contact.profile.webPage
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
        Text{
            anchors.top :webPage_txt2.bottom
            anchors.left : parent.left
            anchors.leftMargin: 20
            anchors.topMargin: 10
            text: contact.profile.birthday==="undefined" ? "" : Qt.formatDate(contact.profile.birthday, "dd/MM/yyyy")
            font.family: "Roboto Regular"
            font.pointSize: 12
            color: "#272727"
        }
    }
}

