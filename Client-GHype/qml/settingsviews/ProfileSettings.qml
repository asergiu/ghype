import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Dialogs 1.2
import CProfileInfo 1.0

import "../components"

Rectangle {
    id: root
    color: "white"
    clip: true

    signal closeDialog()

    function submitValues(){
        client.user.profile.nickname = nicknameInput.value
        client.user.profile.lastName = lnInput.value
        client.user.profile.firstName = fnInput.value
        client.user.profile.email = emailInput.value
        client.user.profile.city = cityInput.value
        client.user.profile.country = countriesList.currentText
        client.user.profile.telephone = phoneInput.value
        client.user.profile.birthday = birthdaySelector.selectedDate
        client.user.profile.imagePath = imageChooserDialog.fileUrl
        client.user.profile.description = descriptionText.text
        client.user.profile.webPage = webPageInput.value

        client.updateClientInfo();
        client.user.saveToFile()
        closeDialog()
    }

    QtObject{
        id: internalVars
        property int formElementSpacing: 5
        property int formLabelWidth: 80
        property int formLabelHeight: 30
    }

    MouseArea{
        id: calendarMouseArea
        visible:  birthdaySelector.visible
        anchors.fill: parent
        onClicked: {
            birthdaySelector.visible = false
        }
    }

    ScrollView {
        anchors {
            left: parent.left; leftMargin: 15;
            right: parent.right;
            top: parent.top; topMargin: 15;
            bottom: submit_holder.top
        }

        Flickable{
            id: settingsFlickable
            anchors { fill: parent; }
            contentHeight: infoColumn.height
            contentWidth: infoColumn.width
            flickableDirection: Flickable.VerticalFlick
            boundsBehavior: Flickable.StopAtBounds

            Column{
                id: infoColumn
                width: root.width - 50
                spacing: 20
                anchors.horizontalCenter: parent.horizontalCenter

                EditProfileDataInput{
                    id: nicknameInput
                    width: parent.width
                    labelHeight: internalVars.formLabelHeight
                    labelWidth: internalVars.formLabelWidth
                    spacing: internalVars.formElementSpacing
                    label: qsTr("Nickname:")
                    value: client.user.profile.nickname
                    KeyNavigation.tab: fnInput
                    focus: true
                }

                EditProfileDataInput{
                    id: fnInput
                    width: parent.width
                    labelHeight: internalVars.formLabelHeight
                    labelWidth: internalVars.formLabelWidth
                    spacing: internalVars.formElementSpacing
                    label: qsTr("First name:")
                    value: client.user.profile.firstName
                    inputValidator: RegExpValidator{
                        regExp: /^[a-zA-Z\s\-]+$/
                    }
                    KeyNavigation.tab: lnInput
                    KeyNavigation.backtab: nicknameInput
                }

                EditProfileDataInput{
                    id: lnInput
                    width: parent.width
                    labelHeight: internalVars.formLabelHeight
                    labelWidth: internalVars.formLabelWidth
                    spacing: internalVars.formElementSpacing
                    label: qsTr("Last name:")
                    value: client.user.profile.lastName
                    inputValidator: RegExpValidator{
                        regExp: /^[a-zA-Z\s]+$/
                    }
                    KeyNavigation.tab: emailInput
                    KeyNavigation.backtab: fnInput
                }

                EditProfileDataInput{
                    id: emailInput
                    width: parent.width
                    labelHeight: internalVars.formLabelHeight
                    labelWidth: internalVars.formLabelWidth
                    spacing: internalVars.formElementSpacing
                    label: qsTr("Email:")
                    value: client.user.profile.email
                    inputValidator: RegExpValidator{}
                    KeyNavigation.tab: countriesList
                    KeyNavigation.backtab: lnInput
                }

                Item{
                    width: parent.width
                    height: internalVars.formLabelHeight

                    Text{
                        id: countryLabel
                        text: "Country:"
                        width: internalVars.formLabelWidth
                        height: internalVars.formLabelHeight
                        verticalAlignment: Text.AlignVCenter
                        font.family: "Roboto Regular"
                        color: "#7d7d7f"
                    }

                    ComboBox{
                        id: countriesList
                        clip: true
                        height: internalVars.formLabelHeight
                        anchors.left: countryLabel.right
                        anchors.right: parent.right
                        anchors.leftMargin: internalVars.formElementSpacing

                        property string country : client.user.profile.country

                        KeyNavigation.tab: cityInput
                        KeyNavigation.backtab: emailInput

                        onCountryChanged: {
                            for(var i=0;i<model.length;i++){
                                if(model[i] === client.user.profile.country){
                                    currentIndex = i;
                                    return;
                                }
                            }
                        }

                        model:[
                        "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antigua & Deps", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan",
                        "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia", "Bosnia Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina", "Burundi",
                        "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Rep", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo", "Congo {Democratic Rep}", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic",
                        "Denmark", "Djibouti", "Dominica", "Dominican Republic",
                        "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia",
                        "Fiji", "Finland", "France",
                        "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana",
                        "Haiti", "Honduras", "Hungary",
                        "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland {Republic}", "Israel", "Italy", "Ivory Coast",
                        "Jamaica", "Japan", "Jordan",
                        "Kazakhstan", "Kenya", "Kiribati", "Korea North", "Korea South", "Kosovo", "Kuwait", "Kyrgyzstan",
                        "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg",
                        "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique"," Myanmar, {Burma}",
                        "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway",
                        "Oman",
                        "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal",
                        "Qatar",
                        "Romania", "Russian Federation", "Rwanda",
                        "St Kitts & Nevis", "St Lucia", "Saint Vincent & the Grenadines", "Samoa", "San Marino", "Sao Tome & Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria",
                        "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu",
                        "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan",
                        "Vanuatu", "Vatican City", "Venezuela", "Vietnam",
                        "Yemen",
                        "Zambia", "Zimbabwe"
                        ]
                    }
                }

                EditProfileDataInput{
                    id: cityInput
                    width: parent.width
                    labelHeight: internalVars.formLabelHeight
                    labelWidth: internalVars.formLabelWidth
                    spacing: internalVars.formElementSpacing
                    label: qsTr("City:")
                    value: client.user.profile.city
                    inputValidator: RegExpValidator{
                        regExp: /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/
                    }
                    KeyNavigation.tab: phoneInput
                    KeyNavigation.backtab: countriesList
                }

                EditProfileDataInput{
                    id: phoneInput
                    width: parent.width
                    labelHeight: internalVars.formLabelHeight
                    labelWidth: internalVars.formLabelWidth
                    spacing: internalVars.formElementSpacing
                    label: qsTr("Telephone:")
                    value: client.user.profile.telephone
                    inputValidator: RegExpValidator{regExp: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/}
                    KeyNavigation.tab: webPageInput
                    KeyNavigation.backtab: cityInput
                }

                EditProfileDataInput{
                    id: webPageInput
                    width: parent.width
                    labelHeight: internalVars.formLabelHeight
                    labelWidth: internalVars.formLabelWidth
                    spacing: internalVars.formElementSpacing
                    label: qsTr("Web page:")
                    value: client.user.profile.webPage
                    KeyNavigation.tab: descriptionText
                    KeyNavigation.backtab: phoneInput
                }

                Item{
                    width: parent.width
                    height: internalVars.formLabelHeight
                    z: 2

                    EditProfileDataInput{
                        id: birthdayInput
                        editable: false
                        anchors.left: parent.left
                        anchors.right: selectDateBtn.left
                        anchors.rightMargin: internalVars.formElementSpacing
                        labelHeight: internalVars.formLabelHeight
                        labelWidth: internalVars.formLabelWidth
                        spacing: internalVars.formElementSpacing
                        label: qsTr("Birthday:")
                    }

                    Calendar{
                        id: birthdaySelector
                        visible: false
                        anchors.bottom: selectDateBtn.top
                        anchors.right: selectDateBtn.left
                        visibleYear: 1980
                        selectedDate: client.user.profile.birthday

                        onSelectedDateChanged: {
                            var dateString = Qt.formatDate(selectedDate, "dd/MM/yyyy");
                            birthdayInput.value = dateString
                            visible = false
                        }

                        style: CalendarStyle {
                            gridVisible: false
                            navigationBar:Rectangle {
                                color: "#f9f9f9"
                                height: 25 * 2

                                Rectangle {
                                    color: Qt.rgba(1, 1, 1, 0.6)
                                    height: 1
                                    width: parent.width
                                }

                                Rectangle {
                                    anchors.bottom: parent.bottom
                                    height: 1
                                    width: parent.width
                                    color: "#ddd"
                                }
                                ToolButton {
                                    id: previousMonth
                                    width: parent.height
                                    height: width
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.left: parent.left
                                    iconSource: "qrc:/icons/left-angle-arrow.png"
                                    onClicked: control.showPreviousMonth()
                                }

                                Row{
                                    spacing: 20
                                    anchors.centerIn: parent
                                    ComboBox{
                                        height: 20
                                        width: 75
                                        model: [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN","JUL", "AUG", "SEP", "OCT", "NOV", "DEC" ];
                                        currentIndex: control.visibleMonth

                                        onCurrentIndexChanged:  {
                                            birthdaySelector.visibleMonth = currentIndex
                                        }

                                    }

                                    SpinBox{
                                        id: yearSpinBox
                                        maximumValue: new Date().getFullYear();
                                        minimumValue: 1900
                                        value: control.visibleYear

                                        onValueChanged: {
                                            birthdaySelector.visibleYear = yearSpinBox.value
                                        }
                                    }
                                }

                                ToolButton {
                                    id: nextMonth
                                    width: parent.height
                                    height: width
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.right: parent.right
                                    iconSource: "qrc:/icons/right-angle-arrow.png"
                                    onClicked: control.showNextMonth()
                                }
                            }

                            dayDelegate: Rectangle {
                                gradient: Gradient {
                                    GradientStop {
                                        position: 0.00
                                        color: styleData.selected ? "#111" : (styleData.visibleMonth && styleData.valid ? "#444" : "#666");
                                    }
                                    GradientStop {
                                        position: 1.00
                                        color: styleData.selected ? "#444" : (styleData.visibleMonth && styleData.valid ? "#111" : "#666");
                                    }
                                    GradientStop {
                                        position: 1.00
                                        color: styleData.selected ? "#777" : (styleData.visibleMonth && styleData.valid ? "#111" : "#666");
                                    }
                                }

                                Label {
                                    text: styleData.date.getDate()
                                    anchors.centerIn: parent
                                    color: styleData.valid ? "white" : "grey"
                                }

                                Rectangle {
                                    width: parent.width
                                    height: 1
                                    color: "#555"
                                    anchors.bottom: parent.bottom
                                }

                                Rectangle {
                                    width: 1
                                    height: parent.height
                                    color: "#555"
                                    anchors.right: parent.right
                                }
                            }
                        }
                    }

                    ToolButton{
                        id: selectDateBtn
                        iconSource: "qrc:/icons/icon-calendar.png"
                        width: internalVars.formLabelHeight
                        height: internalVars.formLabelHeight
                        anchors.right: parent.right
                        onClicked: {
                            birthdaySelector.visible = !birthdaySelector.visible
                        }
                    }
                }

                Rectangle{
                    width: parent.width/1.1
                    height: 1
                    color: "#e0e0e0"
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Rectangle{
                    id: imageRect
                    width: parent.width*0.35
                    height: width
                    color: "transparent"
                    border { width: 1; color: "grey"; }

                    Image{
                        id: avatarImage
                        anchors { fill: parent; margins: 1; }
                        cache: false
                        fillMode: Image.PreserveAspectFit
                        source: client.user.profile.imagePath

                        MouseArea{
                            anchors.fill: parent
                            onClicked:  imageChooserDialog.open();
                        }

                        Rectangle{
                            anchors.right: parent.right
                            anchors.bottom: parent.bottom
                            width: 30
                            height: 33
                            color: "#22c064"

                            Image {
                                anchors.fill: parent
                                anchors.topMargin: 3
                                anchors.rightMargin: 3
                                anchors.bottomMargin: 3
                                anchors.leftMargin: 5
                                fillMode: Image.PreserveAspectFit
                                source: "qrc:/images/resources/common_edit_icon.png"
                            }
                        }
                    }

                    FileDialog {
                        id: imageChooserDialog
                        //visible: false
                        selectFolder: false
                        selectMultiple: false
                        selectExisting: true
                        title: qsTr("Please choose an avatar image")
                        nameFilters: [ "Image files (*.jpg *.png)", "All files (*)" ]
                        onAccepted: {
                            avatarImage.source = imageChooserDialog.fileUrl
                            close();
                        }
                        onRejected: {
                            close()
                        }
                    }
                }

                Column{
                    spacing: 10
                    width: parent.width

                    Text{
                        text: qsTr("Description:")
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        font.pointSize: 9
                        font.family: "Roboto Regular"
                        color: "#7d7d7f"
                        width: paintedWidth
                        height: paintedHeight
                    }

                    TextArea {
                        id: descriptionText
                        width: parent.width
                        height: 100
                        text: client.user.profile.description
                        font.family: "Roboto Regular"
                    }

                    Rectangle {
                        width: parent.width
                        height: 5
                        color: "transparent"
                    }
                }
            }
        }
    }

    Rectangle{
        id: submit_holder
        width: parent.width
        height: internalVars.formLabelHeight + 20
        anchors.bottom: parent.bottom
        opacity: 0.8

        Button{
            id: cancelButton
            width: 70
            height: internalVars.formLabelHeight
            anchors.right: submitButton.left
            anchors.rightMargin: 20
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("Cancel")
            onClicked: closeDialog()
        }

        Button{
            id: submitButton
            width: 70
            height: internalVars.formLabelHeight
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.verticalCenter: parent.verticalCenter
            text: qsTr("Submit")
            onClicked: submitValues()
        }
    }
}
