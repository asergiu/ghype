import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Rectangle {

    signal closeSettingsDialog;

    onVisibleChanged: {
        if(visible){
            loader.source = ""
            loader.source = model.get(0).panelSource
        }
    }

    Component.onCompleted: {        
        list.currentIndex = 0
    }

    Loader {
        id : loader
        height: parent.height
        anchors.left : listHolder.right
        anchors.right : parent.right
    }

    Connections{
        target: loader.item
        onCloseDialog: {
            closeSettingsDialog()
        }
    }

    Rectangle{
        id : listHolder
        height: parent.height
        width: 0.25*parent.width
        color: "#ededee"

        ListView {
            id: list
            interactive: false
            anchors.fill:parent
            model: model
            focus: true
            currentIndex: -1
            boundsBehavior: Flickable.StopAtBounds

            onCurrentIndexChanged:{
                if(list.currentIndex == -1)
                    return;
                loader.source = model.get(list.currentIndex).panelSource
            }

            delegate: Component {
                Item {
                    width: parent.width
                    height: 35
                    Text {
                        anchors.left: parent.left
                        anchors.leftMargin: 20
                        anchors.right: parent.right
                        height: parent.height
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        text:  name
                        font.pointSize: 11
                        color: panelEnabled=="true"? (index === list.currentIndex? "white": "black") : "lightgrey"
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(list.model.get(index).panelEnabled === "true")
                                list.currentIndex = index
                        }
                    }
                }
            }
            highlight: Rectangle {
                color: '#22bf64'
                radius: 4
                Rectangle {
                    anchors.left :parent.right
                    width: 20; height:20
                    color: "#22bf64"
                    transform: Rotation { origin.x:-10; origin.y: -5; angle: 45}
                }
            }
        }
    }
    ListModel {
        id: model
        ListElement {
            name:'Profile Info'
            panelSource:"qrc:/qml/settingsviews/ProfileSettings.qml"
            panelEnabled: "true"
        }
        ListElement {
            name:'Privacy'
            panelSource:"qrc:/qml/settingsviews/PrivacySettings.qml"
            panelEnabled: "false"
        }
        ListElement {
            name:'Preferences'
            panelSource:"qrc:/qml/settingsviews/PreferencesSettings.qml"
            panelEnabled: "false"
        }
        ListElement {
            name:'Advanced'
            panelSource:"qrc:/qml/settingsviews/AdvancedSettings.qml"
            panelEnabled: "false"
        }
    }
}
