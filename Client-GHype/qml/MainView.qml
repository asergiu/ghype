import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.3
import QtQuick.Controls.Private 1.0
import CContact 1.0
import QtQuick.Dialogs 1.2
import CProfileInfo 1.0
import 'components'
import 'components/Chat-components'

Rectangle {
    id: mainView
    anchors.fill: parent
    color: "#3f52b4"

    property var openedDialogs:[]
    property int windowGlobalPosX: 0
    property int windowGlobalPosY: 0

    function openMessageDialog(index){
        listView.model[index].hasUnreadMessages = false

        var windowIsOpened = false
        var openedWindow;
        for(var i=0;i<openedDialogs.length;i++){
            if(listView.model[index].jid === openedDialogs[i].contact.jid){
                openedWindow = openedDialogs[i]
                windowIsOpened = true
                break;
            }
        }

        if(!windowIsOpened){
            var component = Qt.createComponent("qrc:/qml/components/Chat-components/ChatDialog.qml");
            if (component.status === Component.Ready) {
                var dialog = component.createObject(mainView);
                dialog.contact = listView.model[index]
                dialog.callbackFunction = listView.dialogClosed
                dialog.show()

                dialog.index = index

                openedDialogs.push(dialog)
                component.destroy()

                return dialog
            }
            else if(component.status === Component.Error){
                console.log("MainView.qml createComponent error: " + component. errorString())
            }
        }
        else{
            openedWindow.requestActivate()
            openedWindow.show()
            openedWindow.raise()
            return openedWindow
        }
    }

    onWindowGlobalPosXChanged: {
        expand_listView.hide()
        expand_listView.visible = false
    }
    onWindowGlobalPosYChanged: {
        expand_listView.hide()
        expand_listView.visible = false
    }

    Rectangle{
        id: top
        height: 100
        anchors.top: parent.top
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.right: parent.right
        anchors.rightMargin: 2
        color: "#979ef8"

        Image{
            id: profile_icon
            anchors {
                left: parent.left; leftMargin: 20;
                top: parent.top; topMargin: 20;
            }
            fillMode: Image.PreserveAspectCrop
            cache: false
            width: 70
            height: width
            antialiasing: true
            smooth: true
            source: client.user.profile.imagePath === "" ? "qrc:/images/resources/common_contact_70x70.png" : client.user.profile.imagePath

            Image{
                id: profile_icon_mask
                anchors.fill: parent
                source: "qrc:/images/resources/contact_icon_purple_mask.png"
                antialiasing: true
                smooth: true
            }
        }

        Text{
            id: profile_name
            text: {
                var text = client.user.profile.firstName + " " + client.user.profile.lastName
                text = text.trim();
                return !text ? client.user.getJidUsername() : text
            }
            anchors.left: profile_icon.right
            anchors.leftMargin: 20
            anchors.top: parent.top
            anchors.topMargin: 30
            font.bold: true
            font.family: "Roboto Bold"
            font.pointSize: 14
            color: "#fff"
        }

        MComboBox{
            anchors.left: profile_icon.right
            anchors.leftMargin: 20
            anchors.top: profile_name.bottom
            anchors.topMargin: 10
            width: 130
            height: 28

            model: client.user.getAvailableTypes()
            color1: "#87d44d"
            color2: "#76798f"
            color3: "#318002"
            textColor: "#ffffff"
            textFont: "Roboto Bold"
            textSizePoint: 8
            onCurrentIndexChanged: {
                client.setAvailableStatus(currentIndex)
            }
        }
    }

    Rectangle{
        id: top2
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.right: parent.right
        anchors.rightMargin: 2
        anchors.top: top.bottom
        color: "#bcc2d2"
        height: 43

        MComboBox{
            id: groupsCombobox
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.verticalCenter: parent.verticalCenter
            width: 75
            height: 25
            model: myModel
            color1: "#ffd946"
            color2: "#ffd946"
            //            color3: "#ffd946"
            textColor: "#000000"
            textFont: "Roboto Bold"
            textSizePoint: 8
            visible: search.text.length == 0? true:false

            onCurrentTextChanged:{
                if(typeof client.groups[groupsCombobox.currentText] !== 'undefined')
                    listView.model = client.groups[groupsCombobox.currentText].contacts
            }

            onModelChanged: {
                if(typeof client.groups[groupsCombobox.currentText] !== 'undefined')
                    listView.model = client.groups[groupsCombobox.currentText].contacts
            }

            function refreshModel(){
                myModel.clear();
                for (var property in client.groups) {
                    myModel.append({"text": client.groups[property].groupName})
                }

                var crtGroup = client.groups[groupsCombobox.currentText];
                listView.model = crtGroup ? crtGroup.contacts : undefined
            }

            ListModel {
                id: myModel
                ListElement { text: "All" }
            }

            Connections{
                target: client
                onSGroupsChanged:{
                    groupsCombobox.refreshModel()
                }
            }
        }

        Rectangle{
            id: searchfield_holder
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 20
            width: 200
            height: 25
            radius: 20

            TextField{
                id: search
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: search_icon.left
                anchors.leftMargin: 5
                anchors.rightMargin: 2
                anchors.topMargin: 2
                anchors.bottomMargin: 2
                font.pointSize: 10
                placeholderText: qsTr("Search Friends")
                style: TextFieldStyle {
                    textColor: "#546587"
                    background: Rectangle {
                        border.width: 0
                    }
                }
            }

            Image{
                id: search_icon
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                width: 40
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/resources/common_search_icon.png"
            }
        }
    }

    Rectangle{
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.right: parent.right
        anchors.rightMargin: 2
        anchors.top: top2.bottom
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 2
        color: "white"

        ScrollView {
            anchors { fill: parent; }

            ListView {
                id: listView
                anchors.fill: parent
                model: typeof  client.groups[groupsCombobox.currentText] === 'undefined' ? 0 : client.groups[groupsCombobox.currentText].contacts
                delegate: contactDelegate
                currentIndex: -1
                clip: true
                focus: true
                boundsBehavior: Flickable.StopAtBounds

                function dialogClosed(sJid){
                    var index;
                    for(index = 0; index < openedDialogs.length; index++) {
                        if (sJid === openedDialogs[index].contact.jid){
                            openedDialogs[index].destroy()
                            openedDialogs.splice(index, 1);
                        }
                    }
                    for (index = 0; index < listView.model.length; ++index) {
                        if (sJid === listView.model[index].jid) {
                            listView.model[index].hasUnreadMessages = false;
                        }
                    }
                }
            }
        }
    }

    Component {
        id: contactDelegate

        Rectangle{
            id: root_delegate
            width: parent.width
            height: (search.text === "") || (jid.toLowerCase().indexOf(search.text.toLowerCase()) !== -1 || profile.lastName.toLowerCase().indexOf(search.text.toLowerCase()) !== -1 || profile.firstName.toLowerCase().indexOf(search.text.toLowerCase()) !== -1 ) ? 70 : 0
            visible: (search.text === "") || (jid.toLowerCase().indexOf(search.text.toLowerCase()) !== -1 || profile.lastName.toLowerCase().indexOf(search.text.toLowerCase()) !== -1 || profile.firstName.toLowerCase().indexOf(search.text.toLowerCase()) !== -1 )

            Rectangle{
                anchors.fill: parent
                anchors.leftMargin: 5
                anchors.rightMargin: 5
                radius: 2
                color: mouseArea.containsMouse? "#23c065": "white"

                Image{
                    id: contact_icon
                    anchors {
                        left: parent.left; leftMargin: 10;
                        verticalCenter: parent.verticalCenter;
                    }
                    source: profile.imagePath === "" ? "qrc:/images/resources/common_contact_70x70.png" : profile.imagePath
                    width: 45
                    height: width
                    fillMode: Image.PreserveAspectCrop
                    antialiasing: true
                    smooth: true

                    Image {
                        id: contact_icon_mask
                        anchors.fill: parent
                        source: mouseArea.containsMouse ? "qrc:/images/resources/contact_icon_green_mask.png": "qrc:/images/resources/contact_icon_white_mask.png"
                        antialiasing: true
                        smooth: true
                    }
                }

                Text{
                    id: contact_name
                    anchors.left: contact_icon.right
                    anchors.leftMargin: 10
                    y: contact_icon.y
                    text: profile.firstName + " " + profile.lastName
                    onTextChanged: {
                        if (!/[a-zA-Z]+/.test(contact_name.text))
                            contact_name.text = getJidUsername();
                    }
                    font.bold: true
                    font.family: "Roboto Regular"
                    font.pointSize: 10
                    color: type===CContact.Unavailable?"#949494":mouseArea.containsMouse? "#fff" : "#1a1a1a"
                }

                Rectangle{
                    id: contact_unread_notifier
                    width: 12
                    height: 12
                    anchors.right: expand_button.left
                    anchors.rightMargin: 30
                    anchors.verticalCenter: parent.verticalCenter
                    color: "orange"
                    radius: 50
                    visible: hasUnreadMessages? true : false
                    opacity: 0.9
                    onVisibleChanged: {
                        if(visible){
                            appearAnimation.start()
                        }
                        else{
                            appearAnimation.stop()
                            disappearAnimation.stop()
                        }
                    }

                    SequentialAnimation {
                        id: appearAnimation
                        running: false
                        NumberAnimation { target: contact_unread_notifier; property: "opacity"; to: 1.0; duration: 700}

                        onStopped: disappearAnimation.start()
                    }

                    SequentialAnimation {
                        id: disappearAnimation
                        running: false
                        NumberAnimation { target: contact_unread_notifier; property: "opacity"; to: 0.0; duration: 700}
                        onStopped: appearAnimation.start()
                    }
                }

                Text{
                    id: contact_status
                    anchors.left: contact_icon.right
                    anchors.leftMargin: 10
                    anchors.top: contact_name.bottom
                    anchors.topMargin: 10
                    text: {
                        if(type === CContact.Available){
                            return getAvailableTypes()[availableType]
                        }
                        else{
                            return getTypes()[type]
                        }
                    }
                    font.bold: true
                    font.family: "Roboto Light"
                    font.pointSize: 8
                    color: type===CContact.Unavailable?"#949494":mouseArea.containsMouse? "#fff" : "#707070"
                }

                Image{
                    id:contact_status_icon
                    anchors.left: contact_status.right
                    anchors.leftMargin: 10
                    y: contact_status.y
                    height: 12
                    width: 12
                    fillMode: Image.PreserveAspectFit
                    source: {
                        if(type === CContact.Available){
                            if(availableType === CContact.DND){
                                return "qrc:/images/resources/common_busy_icon.png"
                            }
                            else if(availableType === CContact.Away){
                                return "qrc:/images/resources/common_idle_icon.png"
                            }
                            else{
                                return "qrc:/images/resources/common_available_icon.png"
                            }
                        }
                        else{
                            return "qrc:/images/resources/common_offline_icon.png"
                        }
                    }
                }

                Rectangle{
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width/1.05
                    height: 1
                    color: mouseArea.containsMouse? "#23c065": "#e6e6e6"
                }

                Image{
                    id: expand_button
                    width: 30
                    height: 30
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 20
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/resources/common_expand_button.png"

                    MouseArea{
                        anchors.fill: parent
                        onClicked:{
                            listView.currentIndex=index
                            if(expand_listView.visible){
                                expand_listView.hide()
                                expand_listView.visible = false
                                expand_listView.generalIndex="-1"
                            }
                            else{
                                /// map to screen coordinates
                                var windowCoords = mapToItem(null, 0, 0)

                                var startPos = Qt.point(windowCoords.x + main.x, windowCoords.y + main.y)
                                expand_listView.display(startPos, Qt.size(expand_button.width, expand_button.height));
                            }
                        }
                    }
                }

                MouseArea {
                    id: mouseArea
                    anchors.left: parent.left
                    anchors.right:expand_button.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    hoverEnabled: true
                    onClicked: {
                        //                        listView.model[index].hasUnreadMessages = false

                        for(var i=0;i<openedDialogs.length;i++){
                            if(listView.model[index].jid === openedDialogs[i].contact.jid){
                                openedDialogs[i].requestActivate()
                                return;
                            }
                        }
                    }
                    onContainsMouseChanged: {
                        if(containsMouse){
                            listView.currentIndex=index
                        }
                    }

                    onDoubleClicked: {
                        var dialog = openMessageDialog(listView.currentIndex)
//                        connections.target = dialog
                    }
                }
            }
        }
    }

    MListView{
        id: expand_listView
        visible: false
        model: contactListModel
        arrowY: 25
        onGeneralIndexChanged: {
            expand_listView.hide()
            switch(generalIndex){
            case "0":{
                openMessageDialog(listView.currentIndex)
                break;
            }
            case "1":{
                //REMOVE
                for(var i = 0; i < openedDialogs.length; i++){
                    if(listView.model[listView.currentIndex].jid === openedDialogs[i].contact.jid){
                        openedDialogs[i].close();
                    }
                }

                client.removeSubscription(listView.model[listView.currentIndex].jid)
                break;
            }
//            case "2":{
//                console.log("Send")
//                break;
//            }
//            case "2-0":{
//                console.log("Send File")
//                break;
//            }
//            case "2-1":{
//                console.log("Send Contacts")
//                break;
//            }
//            case "3":{
//                console.log("Remove")
//                client.removeSubscription(listView.model[listView.currentIndex].jid)

//                break;
//            }
//            case "4":{
//                console.log("Block")
//                break;
//            }
            }
            expand_listView.generalIndex="-1"

        }
    }

    ListModel{
        id: contactListModel
        ListElement{
            icon: "qrc:/images/resources/listview_message_icon.png"
            name: "Message"
            symbol: ""
            sublist: []
        }
//        ListElement{
//            icon: "qrc:/images/resources/listview_call_icon.png"
//            name: "Call"
//            symbol: ""
//            sublist: []
//        }
//        ListElement{
//            icon: "qrc:/images/resources/listview_send_icon.png"
//            name: "Send"
//            symbol: "qrc:/images/resources/common_arrow_right.png"
//            sublist: [
//                ListElement { name: "Files" },
//                ListElement { name: "Contacts" }]

//        }
        ListElement{
            icon: "qrc:/images/resources/listview_remove_icon.png"
            name: "Remove"
            symbol: ""
            sublist: []
        }
//        ListElement{
//            icon: "qrc:/images/resources/listview_block_icon.png"
//            name: "Block"
//            symbol: ""
//            sublist: []
//        }
    }
}


//          MsgAdvancedListView ListModel
//
//ListModel {
//        id: model
//        ListElement {
//            name:'Profile Info'
//        }
//        ListElement {
//            name:'Privacy'
//        }
//        ListElement {
//            name:'Preferences'
//        }
//        ListElement {
//           name:'Advanced'
//        }
//}



//ListModel{
//    id: testModel2
//    ListElement{
//        icon: "qrc:/resources/common_calendar.png"
//        name: "Calendar"
//        symbol: ""
//        sublist: []
//    }
//    ListElement{
//        icon: "qrc:/resources/common_notes.png"
//        name: "Notes"
//        symbol: ""
//        sublist: []
//    }
//    ListElement{
//        icon: "qrc:/resources/common_external_app.png"
//        name: "External App"
//        symbol: ""
//        sublist: [
//            ListElement { name: "Word" },
//            ListElement { name: "Calc" },
//            ListElement { name: "Excel" },
//            ListElement { name: "Add..." }]

//    }
//}

