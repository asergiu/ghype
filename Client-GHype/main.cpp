#include <QApplication>
#include <QQuickWindow>
#include <QDesktopWidget>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>

#include "cpp/client-manager/cclient.h"
#include "cpp/contact/ccontact.h"
#include "cpp/message-items/cfiletransfer.h"
#include "cpp/message-items/ccommunicationitem.h"
#include "cpp/message-items/cmessage.h"
#include "cpp/settings/capplicationsetting.h"
#include "cpp/contact/cprofileinfo.h"
#include "ghype-protocol/ghypefilelogs.h"
#include "cpp/message-items/_messagemodel.h"
#include "cpp/cclipboardwrapper.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    qmlRegisterType<CClient>("CClient", 1, 0, "CClient");
    qmlRegisterType<CContact>("CContact", 1, 0, "CContact");
    qmlRegisterType<CMessage>("CMessage", 1, 0, "CMessage");
    qmlRegisterType<MessageModel>("MessageModel", 1, 0, "MessageModel");
    qmlRegisterType<CFileTransfer>("CFileTransfer", 1, 0, "CFileTransfer");
    qmlRegisterType<CCommunicationItem>("CCommunicationItem", 1, 0, "CCommunicationItem");
    qmlRegisterType<CProfileInfo>("CProfileInfo", 1, 0, "CProfileInfo");
    qmlRegisterType<GHypeFileTransferItem>("GHypeFileTransferItem", 1, 0, "GHypeFileTransferItem");

    CApplicationSetting *settings = &CApplicationSetting::instance();
    engine.rootContext()->setContextProperty("appSettings", settings);

    CClipBoardWrapper *clipboard = new CClipBoardWrapper(QApplication::clipboard());
    engine.rootContext()->setContextProperty("clipboard", clipboard);

    QUrl url(QStringLiteral("qrc:///qml/Start.qml"));
    engine.load(url);

    return app.exec();
}
