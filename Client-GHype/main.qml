import QtQuick 2.3
import QtQuick.Controls 1.2

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Button{
        width: 100
        height: 100
        text: "Apasa-l"
        anchors.centerIn: parent
        onClicked: {
            controller.sendMessage("Salut din QML v2!");
        }
    }
}
