DROP TABLE IF EXISTS `queue`;

CREATE TABLE `queue` (
  `collection-owner` text NOT NULL,
  `object-sequence` bigint(20) NOT NULL AUTO_INCREMENT,
  `xml` mediumtext,
  `id` varchar(255) NOT NULL,
  `type` varchar(128) NOT NULL,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `time` int(11) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`object-sequence`),
  KEY `collection-owner` (`collection-owner`(255))
) ENGINE=MyISAM AUTO_INCREMENT=690 DEFAULT CHARSET=utf8;

