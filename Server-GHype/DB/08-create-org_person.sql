--
-- Table structure for table `org_person`
--

DROP TABLE IF EXISTS `org_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `active` enum('yes','no') NOT NULL DEFAULT 'no',
  `email` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `confirmation_token` varchar(255) NOT NULL,
  `password_request` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `login_count` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org_person`
--

LOCK TABLES `org_person` WRITE;
/*!40000 ALTER TABLE `org_person` DISABLE KEYS */;
INSERT INTO `org_person` VALUES (1,'Administrator','1','root','$1$Dz2.Qp1.$LbwelxXS01YkzzS6chm490','yes','test@gmail.com','','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `org_person` ENABLE KEYS */;
UNLOCK TABLES;