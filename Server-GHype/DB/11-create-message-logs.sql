DROP TABLE IF EXISTS `message-logs`;

CREATE TABLE `message-logs` (
    `collection-owner` TEXT NOT NULL, KEY(`collection-owner`(255)),
    `object-sequence` BIGINT NOT NULL AUTO_INCREMENT, PRIMARY KEY(`object-sequence`),
    `from` VARCHAR(255),
    `to` VARCHAR(255),
    `time` INT,
    `content` TEXT
) DEFAULT CHARSET=UTF8;
