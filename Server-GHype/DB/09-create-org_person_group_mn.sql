
--
-- Table structure for table `org_person_group_mn`
--

DROP TABLE IF EXISTS `org_person_group_mn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_person_group_mn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_person_id` int(11) NOT NULL,
  `org_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org_person_group_mn`
--

LOCK TABLES `org_person_group_mn` WRITE;
/*!40000 ALTER TABLE `org_person_group_mn` DISABLE KEYS */;
INSERT INTO `org_person_group_mn` VALUES (1,1,1),(2,5,1),(3,100,1),(4,1,2);
/*!40000 ALTER TABLE `org_person_group_mn` ENABLE KEYS */;
UNLOCK TABLES;