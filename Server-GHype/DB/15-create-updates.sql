DROP TABLE IF EXISTS `updates`;

CREATE TABLE `updates` (
  `object-sequence` bigint(20) NOT NULL AUTO_INCREMENT,
  `so` varchar(255) NOT NULL,
  `data` datetime NOT NULL,
  `source_version` bigint(11) DEFAULT NULL,
  `target_version` bigint(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`object-sequence`)
) ENGINE=MyISAM AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;
