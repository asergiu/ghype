-- MySQL dump 10.14  Distrib 5.5.40-MariaDB, for Linux (x86_64)
--
-- Host: 192.168.44.224    Database: bp
-- ------------------------------------------------------
-- Server version	5.1.61

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backup`
--

DROP TABLE IF EXISTS `backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `timestart` datetime NOT NULL,
  `next` enum('daily','weekly','monthly') DEFAULT NULL,
  `action` enum('copy','move') NOT NULL DEFAULT 'copy',
  `period_end_date` datetime DEFAULT NULL,
  `destination` enum('ftp','hdd') NOT NULL DEFAULT 'hdd',
  `parameters` varchar(255) NOT NULL,
  `backup_database` enum('yes','no') NOT NULL DEFAULT 'yes',
  `backup_media` enum('yes','no') NOT NULL DEFAULT 'yes',
  `backup_data` enum('yes','no') NOT NULL DEFAULT 'yes',
  `backup_monitor` enum('yes','no') NOT NULL DEFAULT 'yes',
  `status` enum('not started','running','finished') NOT NULL DEFAULT 'not started',
  `message` varchar(255) DEFAULT NULL,
  `active` enum('yes','no') NOT NULL DEFAULT 'yes',
  `deactivation_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
