--
-- Table structure for table `org_acl_aco_mn`
--

DROP TABLE IF EXISTS `org_acl_aco_mn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_acl_aco_mn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_acl_id` int(11) DEFAULT NULL,
  `org_aco_id` int(11) DEFAULT NULL,
  `access_type` char(16) NOT NULL,
  `crud` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org_acl_aco_mn`
--

LOCK TABLES `org_acl_aco_mn` WRITE;
/*!40000 ALTER TABLE `org_acl_aco_mn` DISABLE KEYS */;
INSERT INTO `org_acl_aco_mn` VALUES (1,1,1,'permit',15);
/*!40000 ALTER TABLE `org_acl_aco_mn` ENABLE KEYS */;
UNLOCK TABLES;