DROP TABLE IF EXISTS `file-transfer`;

CREATE TABLE `file-transfer` (
  `sid` varchar(255) NOT NULL,
  `type` enum('push','pull') NOT NULL DEFAULT 'push',
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `hash` varchar(64) DEFAULT NULL,
  `uniqueid` varchar(64) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` int(11) NOT NULL,
  `filecontent` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` enum('activate','finish','error') DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
