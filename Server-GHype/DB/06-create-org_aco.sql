--
-- Table structure for table `org_aco`
--

DROP TABLE IF EXISTS `org_aco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_aco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` char(128) NOT NULL,
  `crud_mask` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org_aco`
--

LOCK TABLES `org_aco` WRITE;
/*!40000 ALTER TABLE `org_aco` DISABLE KEYS */;
INSERT INTO `org_aco` VALUES (1,NULL,'ACL_ACCESS_ALL',15),(2,NULL,'ACL_ACCESS_SECURITY',15),(3,NULL,'ACL_ACCESS_USERS',15),(4,NULL,'ACL_ACCESS_GROUPS',15),(5,3,'ACL_ACCESS_USERS_GROUPS',6),(6,NULL,'ACCESS_AUDIO_MESSAGES',15);
/*!40000 ALTER TABLE `org_aco` ENABLE KEYS */;
UNLOCK TABLES;