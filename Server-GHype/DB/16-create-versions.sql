DROP TABLE IF EXISTS `versions`;

CREATE TABLE `versions` (
  `collection-owner` varchar(255) NOT NULL,
  `object-sequence` bigint(20) NOT NULL AUTO_INCREMENT,
  `so` varchar(255) NOT NULL,
  `app` varchar(255) NOT NULL,
  `data` datetime NOT NULL,
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`object-sequence`),
  KEY `collection-owner` (`collection-owner`)
) ENGINE=MyISAM AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;
