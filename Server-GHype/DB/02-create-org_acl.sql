--
-- Table structure for table `org_acl`
--

DROP TABLE IF EXISTS `org_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(128) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org_acl`
--

LOCK TABLES `org_acl` WRITE;
/*!40000 ALTER TABLE `org_acl` DISABLE KEYS */;
INSERT INTO `org_acl` VALUES (1,'Administrator','Acces total'),(2,'Operator','Restricted access');
/*!40000 ALTER TABLE `org_acl` ENABLE KEYS */;
UNLOCK TABLES;
