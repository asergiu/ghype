DROP TABLE IF EXISTS `preferences`;

CREATE TABLE `preferences` (
  `collection-owner` varchar(255) NOT NULL,
  `object-sequence` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`object-sequence`),
  KEY `collection-owner` (`collection-owner`)
) ENGINE=MyISAM AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;
