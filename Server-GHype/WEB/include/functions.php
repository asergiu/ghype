<?php

/**
 * 
 *
 * Autoload any classes that are required
 *
 */
 
function __autoload($className) {
	if (file_exists(CLASS_PATH . DIRECTORY_SEPARATOR . $className . '.class.php')) {
		require_once(CLASS_PATH . DIRECTORY_SEPARATOR . $className . '.class.php');
		return true;
	} else {
		/* Error Generation Code Here */
		return false;
	}
}

/**
 * FUNCTION: getSettings()
 *   loads all application settings
 * PARAMETERS:
 *
 * RETURNS:
 * ARRAY, returns an array with all configs vars from the all .ini files from configs directory
 */
function getSettings() {
    $config = array();
    if (file_exists($server_file = CONFIG_PATH . DIRECTORY_SEPARATOR . "server.ini")) {
        $s_content = file($server_file);
        $row_s = explode('=', $s_content[0]);

        if (trim($row_s[0]) == 'server.type')
            $config['server_env'] = trim($row_s[1]);


        $items = glob(CONFIG_PATH . '/*.ini');
        if (count($items)) {
            $is_authenticated = isset($_SESSION) && isset($_SESSION['PERSON_ID']) && $_SESSION['PERSON_ID'] ? true : false;
            $_rights = Object::getSessionValue("RIGHTS", array());
            if ( count($_rights) === 0 && $is_authenticated ){
                $is_authenticated = FALSE;
            }
            foreach ($items as $item) {
                $file = str_replace(CONFIG_PATH . '/', '', $item);
                //ignore files that starts with "login-" is user is logged
                if ($is_authenticated && (preg_match('#^login-#i', $file) === 1))
                    continue;
                //ignore files thats start with "secured-" if user isn't logged
                if (!$is_authenticated && (preg_match('#^secured-#i', $file) === 1))
                    continue;
                if ($file != 'server.ini')
                    $files[] = $file;
                unset($file);
            }
            if (count($files)) {
                sort($files);
                foreach ($files as $file) {
                    if (file_exists(CONFIG_PATH . "/{$file}")) {
                        $app_lines = file(CONFIG_PATH . "/{$file}");

                        if (count($app_lines))
                            foreach ($app_lines as $k => $app_line) {
                                $temp = explode("=", $app_line);

                                $key = isset($temp[0]) ? trim($temp[0]) : '';
                                //$value	= isset($temp[1]) ? trim($temp[1]):'';
                                if (isset($temp[1])) {
                                    for ($i = 2; $i < count($temp); $i++)
                                        $temp[1] .= "=" . $temp[$i];
                                    $value = null;
                                    $pattern = "/\"(.*)\"/";

                                    preg_match($pattern, $temp[1], $matches);
                                    if (isset($matches[1])) {
                                        $value.= trim($matches[1]);
                                    } else {
                                        $temp_values = explode(" ", $temp[1]);
                                        if (count($temp_values)) {
                                            foreach ($temp_values as $temp_value) {
                                                if (defined(trim($temp_value))) {
                                                    $value .= constant(trim($temp_value));
                                                } else
                                                    $value.= trim($temp_value);
                                            }
                                        }
                                    }
                                } else
                                    $value = "";

                                $add_key = true;

                                /* find the development environment, an env must be between [] */
                                $pattern = "/\[(.*)\]/";
                                preg_match($pattern, $key, $matches);

                                if (isset($matches[1])) {
                                    $matche = $matches[1];
                                    if (strpos($matche, ':')) {
                                        $envs = explode(":", $matche);
                                        $obj = new stdClass();
                                        $obj->first = trim($envs[0]);
                                        $obj->second = trim($envs[1]);
                                        $config['extends'][] = $obj;
                                        unset($obj);

                                        $matche = trim($envs[0]);
                                    }
                                    $add_key = false;
                                }

                                if ($add_key && $key && isset($matche)) {
                                    $var = "";
                                    $pattern = "/\((.*)\)/";
                                    preg_match($pattern, $key, $matches);
                                    if (strpos($key, ".")) {
                                        $vars = (explode(".", $key));
                                        if (count($var))
                                            foreach ($vars as $tvar) {
                                                $pattern = "/\((.*)\)/";
                                                preg_match($pattern, $tvar, $matches);
                                                if (isset($matches[1])) {
                                                    $def = (explode("(", $tvar));
                                                    if (count($def)) {
                                                        $var .= "['{$def[0]}'][{$matches[1]}]";
                                                    } else
                                                        $var .= "['{$tvar}']";
                                                } else
                                                    $var .= "['{$tvar}']";
                                            }
                                    }
                                    $var = $var ? $var : "['$key']";
                                    $instr = '$config[$matche]' . $var . ' = $value;';
                                    eval($instr);
                                }
                            }
                        if (isset($matche))
                            unset($matche);
                    }
                }
            }
        }
    }

    if (count($config['extends']))
        foreach ($config['extends'] as $extend) {
            $config[$extend->first] = array_merge_recursive_distinct(isset($config[$extend->second]) ? (array) $config[$extend->second] : array(), isset($config[$extend->first]) ? (array) $config[$extend->first] : array());
        }

    $configs = array_merge(array('env' => $config['server_env']), (array) $config[$config['server_env']]);
    return $configs;
}

/**
 * FUNCTION: array_merge_recursive_distinct ()
 *
 * Merges any number of arrays / parameters recursively, replacing
 * entries with string keys with values from latter arrays.
 * If the entry or the next value to be assigned is an array, then it
 * automagically treats both arguments as an array.
 * Numeric entries are appended, not replaced, but only if they are
 * unique
 *
 * calling: result = array_merge_recursive_distinct(a1, a2, ... aN)
 *
 * RETURNS:
 * ARRAY
**/
function array_merge_recursive_distinct () {
  $arrays = func_get_args();
  $base = array_shift($arrays);
  if(!is_array($base)) $base = empty($base) ? array() : array($base);
  foreach($arrays as $append) {
    if(!is_array($append)) $append = array($append);
    foreach($append as $key => $value) {
      if(!array_key_exists($key, $base) and !is_numeric($key)) {
        $base[$key] = $append[$key];
        continue;
      }
      if(is_array($value) or is_array($base[$key])) {
        $base[$key] = array_merge_recursive_distinct($base[$key], $append[$key]);
      } else if(is_numeric($key)) {
        if(!in_array($value, $base)) $base[] = $value;
      } else {
        $base[$key] = $value;
      }
    }
  }
  return $base;
}
/**
 * Initilize PHP session
 * @return integer session_id
 */
function init_session() {
	if(!isSet($_SESSION)) {
		session_start();
		$session_id = session_id();
		// Session expire in 6 hours
		ini_set('session.gc_maxlifetime', 43200);
		session_cache_expire(480);
	} else {
		$session_id = false;
	}
	return $session_id;
}

/**
 * Close PHP session
 * @param integer session_id
 */
function close_session($session_id) {
	if($session_id != false) session_write_close();
}

/**
 * Close PHP session
 * @param integer session_id
 */
function auto_version($debug, $directory, $url) {
  $result = null;
  $path = str_replace('//', '/', pathinfo(DOCUMENT_ROOT_PATH."/".$directory."/".$url));
  if($debug == 1 || $debug == '1' || $debug == "1" || $debug) {
    if(($_time = @filemtime(DOCUMENT_ROOT_PATH."/".$directory."/".$url)) != false) {
      $ver = '.'.@filemtime(DOCUMENT_ROOT_PATH."/".$directory."/".$url).'.';
      $result = str_replace('//', '/', str_replace(DOCUMENT_ROOT_PATH, '', $path['dirname']).'/'.str_replace('.', $ver, $path['basename']));
    } else $result = $directory."/".$url;
  } else {
    $result= str_replace('//', '/', str_replace(DOCUMENT_ROOT_PATH, '', $path['dirname']).'/'.$path['basename']);
  }
  if (strpos($result, '/')===0) $result = substr($result, 1);
  return $result;
}

/**
 * FUNCTION : create_password
 * Generate password
 *
 * PARAMETERS:
 * @param <INT> $pw_length
 * @param <BOOL> $use_caps
 * @param <BOOL> $use_numeric
 * @param <BOOL> $use_specials
 * @return <STRING>
 */
function create_password($pw_length = 8, $use_caps = true, $use_numeric = true, $use_specials = true) {
	$caps = array();
	$numbers = array();
	$num_specials = 0;
	$reg_length = $pw_length;
	$pws = array();
	for ($ch = 97; $ch <= 122; $ch++) $chars[] = $ch; // create a-z
	if ($use_caps) for ($ca = 65; $ca <= 90; $ca++) $caps[] = $ca; // create A-Z
	if ($use_numeric) for ($nu = 48; $nu <= 57; $nu++) $numbers[] = $nu; // create 0-9
	$all = array_merge($chars, $caps, $numbers);
	if ($use_specials) {
		$reg_length =  ceil($pw_length*0.75);
		$num_specials = $pw_length - $reg_length;
		if ($num_specials > 5) $num_specials = 5;
		for ($si = 33; $si <= 47; $si++) $signs[] = $si;
		$rs_keys = array_rand($signs, $num_specials);
		foreach ($rs_keys as $rs) {
			$pws[] = chr($signs[$rs]);
		}
	}
	$rand_keys = array_rand($all, $reg_length);
	foreach ($rand_keys as $rand) {
		$pw[] = chr($all[$rand]);
	}
	$compl = array_merge($pw, $pws);
	shuffle($compl);
	return implode('', $compl);
}

function echo_memory_usage() {
    $mem_usage = memory_get_usage(true);

    if ($mem_usage < 1024)
        echo $mem_usage." bytes";
    elseif ($mem_usage < 1048576)
        echo round($mem_usage/1024,2)." kilobytes";
    else
        echo round($mem_usage/1048576,2)." megabytes";

    echo "<br/>";
}

/**
 * Funtion: isValidEmail()
 *
 * test if an email is valid or not
 *
 * Returns : BOOL
 */
function isValidEmail($email){
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function isValidMAC( $_mac ) {
    $_result = FALSE;
    if(preg_match('/^[0-9a-fA-F]{2}(?=([:;.]?))(?:\\1[0-9a-fA-F]{2}){5}$/', $_mac)) {
        $_result = TRUE;
    }
    
    return $_result;
}

function formatTimestamp( $_seconds ) {
    $minute = 60;
    $hour   = 60*$minute;
    $day    = 24*$hour;

    $ans["day"]    = floor( $_seconds/$day );
    $ans["hour"]   = floor( ( $_seconds%$day)/$hour );
    $ans["minute"] = floor( ( ( $_seconds%$day)%$hour)/$minute);
    $ans["second"] = floor(((($_seconds%$day)%$hour)%$minute));
    
    $_output = "";
    $_output .= $ans[ "day" ] > 0 ? $ans[ "day" ] . " D " : "";
    $_output .= $ans[ "hour" ] > 0 ? $ans[ "hour" ] . " h " : "";
    $_output .= $ans[ "minute" ] > 0 ? $ans[ "minute" ] . "m " : "";
    $_output .= $ans[ "second" ]."s";
    
    return $_output;
}
?>
