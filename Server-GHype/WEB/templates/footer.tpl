        <script type="text/javascript">
          var words = new Array,
              SITE_PATH = '{$SITEPATH}',
              AXTEN = AXTEN || {},
              socket = false;
        </script>
        
        {foreach from = $js_include_list item = js}
          <script type="text/javascript" src="{$js}"></script>
        {/foreach}

        {if $AUTHENTICATED }
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/salsa20.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/bigint.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/core_1.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/enc-base64.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/md5_1.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/evpkdf.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/cipher-core.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/aes.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/sha1_1.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/sha256.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/hmac.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/pad-nopadding.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/mode-ctr.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/eventemitter.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/otr.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/strophe.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/strophe.vcard.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/strophe.disco.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/strophe.ping.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/moment-with-locales.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/index.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/jed.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/locales.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/templates.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/utils.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/converse.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/require.js'}"></script>
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/application.js'}"></script>

            <script type="text/javascript">
                $(document).ready(function() {
                    {if Object::getSessionValue('PERSON_ID')}
                        AXTEN.init({
                            debug: true,
                            username: '{Object::getSessionValue('PERSON_USERNAME')}',
                            password: '{Object::getSessionValue('PERSON_PASSWORD')}',
                            domain: '{Object::getSessionValue('REALM')}'
                        });
                    {/if}
                });
            </script>
        {else}
            <script type="text/javascript" src="{$SITEPATH}{$CONF.default.debug|auto_version:'':'/javascript/login.js'}"></script>
        {/if}
    </body>
</html>
