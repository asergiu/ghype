<!DOCTYPE HTML>
<html>
    <head>
        <title></title>
        <meta http-equiv="pragma" content="nocache">
        <meta http-equiv="expires" content="-1">
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        {foreach from = $css_include_list item = css}
            <link rel="stylesheet" type="text/css" href="{$SITEPATH}{$css}"/>
        {/foreach}

        {if !$AUTHENTICATED } 
            <link rel="stylesheet" type="text/css" href="{$SITEPATH}{1|auto_version:'':'/css/login.css'}"/>
        {else}    
            <link rel="stylesheet" type="text/css" href="{$SITEPATH}{1|auto_version:'':'/css/bootstrap.min.css'}"/>
            <link rel="stylesheet" type="text/css" href="{$SITEPATH}{1|auto_version:'':'/css/theme.css'}"/>
            <link rel="stylesheet" type="text/css" href="{$SITEPATH}{1|auto_version:'':'/css/converse.css'}"/>
            <link rel="stylesheet" type="text/css" href="{$SITEPATH}{1|auto_version:'':'/css/application.css'}"/>
            <link rel="stylesheet" type="text/css" href="{$SITEPATH}{1|auto_version:'':'/css/responsive.css'}"/>
            
            
        {/if}
    </head>
    <body class="{if !$AUTHENTICATED }login{else}application-skin{/if}" id="page-top" data-spy="scroll" data-target=".navbar-custom">