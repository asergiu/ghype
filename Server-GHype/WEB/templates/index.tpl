{include file="header.tpl"}
<!--Content Start--> 
{if isset($old_browser) && $old_browser}
    {include file='old-browser.tpl'}
{else}
    {if !$AUTHENTICATED}                     
        <div id="login-window" ></div>
    {else}

        <div class="header navbar navbar-inverse ">

            <div class="navbar-inner">
                <div class="header-seperation">
                    <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">
                        <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu" class="">
                                <div class="iconset top-menu-toggle-white"></div>
                            </a>                         
                        </li>
                    </ul>
                    <a href="#" id="home"><img src="images/logo.png" alt="foureo" title="Foureo"/></a>
                    <ul class="nav pull-right notifcation-center">
                        <li class="dropdown" id="header_task_bar"> 
                            <a href="#" class="dropdown-toggle active trigger-sidebar-toggle" data-toggle="">
                                <div class="iconset top-menu-toggle-dark"></div>
                            </a> 
                        </li>
                    </ul>
                </div>

                <div class="header-quick-nav">

                    <div class="pull-left" id="menu-toggle-container">
                        <ul class="nav quick-section">
                            <li class="quicklinks hide" id="sidebar-toggle-2"> <a href="#" class="trigger-sidebar-toggle" >
                                    <div class="iconset top-menu-toggle-dark"></div>
                                </a> 
                            </li>
                        </ul>
                    </div>

                    <div class="pull-right">

                        <ul class="nav quick-section ">

                            <li class="quicklinks"> 
                                <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">
                                    {if Object::getSessionValue('PERSON_LASTNAME') || Object::getSessionValue('PERSON_FIRSTNAME')}
                                        {Object::getSessionValue('PERSON_LASTNAME','LASTNAME')} {Object::getSessionValue('PERSON_FIRSTNAME','FIRSTNAME')} 
                                    {else} 
                                        {*Object::getSessionValue('PERSON_ID','USERNAME')*}
                                        {Object::getSessionValue('USERNAME')}
                                    {/if}
                                    <div class="iconset top-settings-dark " style="display:inline-block"></div>
                                </a>
                                <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                                    <li><a href="javascript:;" id="my-account-trigger">{Language::getWord('MY_ACCOUNT')}</a> </li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:;" data-trigger="logout"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-container row-fluid ">

            {include file='menu.tpl'}
            <div class="footer-widget">
                <div class="progress transparent progress-small no-radius no-margin hide">
                    <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar"></div>
                </div>
                <div class="pull-right">
                    <div class="details-status hide"> <span data-animation-duration="560" data-value="86" class="animate-number"></span>% </div>
                    <a href="javascript:;" data-trigger="logout"><i class="fa fa-power-off"></i></a>
                </div>
            </div>
            <div class="page-content full-height" >
                <div class="clearfix"></div>
                <div class="content sm-gutter" id="page-content" style="padding-right: 0;overflow: hidden">

                </div>            
            </div>
            <div id="loading_bar">
                <img src="images/loading.gif" alt="Loading..."/>
            </div>
        </div>

        {include file="converse_chat.tpl"} 
    {/if}
{/if}
<!--Content End--> 

{include file="footer.tpl"}
{if isset($old_browser) && $old_browser}

{/if}