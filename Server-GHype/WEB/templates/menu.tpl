<div class="page-sidebar" id="main-menu">
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        <ul>
            <li class=""> <a href="javascript:;"><i class="fa fa-lock"></i> <span class="title">{Language::getWord('BUSINESS_ADMIN')}</span> <span class="arrow "></span> </a>
                <ul class="sub-menu">    
                    {if Object::checkAccess("ACL_ACCESS_USERS",  Object::ACL_READ) || Object::checkAccess("ACL_ACCESS_SECURITY",  Object::ACL_READ) || Object::checkAccess("ACL_ACCESS_GROUPS",  Object::ACL_READ)}
                        <li class=""> <a href="javascript:;"> <i class="fa fa-user"></i> <span class="title">{Language::getWord('APPLICATION_ADMIN')}</span> <span class="arrow "></span> </a>
                            <ul class="sub-menu">
                                {if Object::checkAccess("ACL_ACCESS_USERS",  Object::ACL_READ)}
                                    <li class="menu__item"> <a href="javascript:;" data-trigger="User@loadList"><i class="fa fa-list"></i> {Language::getWord('USERS_LIST')} </a> </li>
                                {/if}
                                {if Object::checkAccess("ACL_ACCESS_GROUPS",  Object::ACL_READ)}
                                    <li class="menu__item"> <a href="javascript:;" data-trigger="Group@loadList"><i class="fa fa-group"></i> {Language::getWord('USERS_LIST_GROUPS')} </a> </li>
                                {/if}
                                {if Object::checkAccess("ACL_ACCESS_SECURITY",  Object::ACL_READ)}
                                    <li  class="menu__item"> <a href="javascript:;" data-trigger="ACL@loadList"> <i class="fa fa-lock"></i> <span class="title">{Language::getWord('SECURITY')}</span></a></li>
                                {/if}
                                
                            </ul>

                        </li>
                    {/if}
                </ul>
            </li>
            {if Object::checkAccess("ACL_ACCESS_FILESTRANSFER",  Object::ACL_READ)}
                <li  class="menu__item"> <a href="javascript:;" data-trigger="TransferedFile::showPage"> <i class="fa fa-file"></i> <span class="title">{Language::getWord('FILESTRANSFER')}</span></a></li>
            {/if}
        </ul>
        <div class="clearfix"></div>
    </div>
</div>