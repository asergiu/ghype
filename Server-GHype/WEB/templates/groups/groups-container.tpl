<div class="view-page full-width">
    <div class="row " id="groups-container">
        <div class="col-md-12">
            <div class="grid simple" >
                <div class="grid-title border-title">
                    <h4>{Language::getWord('GROUPS')} <i class="fa fa-plus-circle header-add-btn" data-trigger="Group@viewFormModal"title="{Language::getWord('GROUP_ADD')}"></i></h4>
                    
                </div>
                <div class="grid-body border-body" id="groups-list-container">
                    {include file='groups/groups-list.tpl'}
                </div>
            </div>
        </div>
    </div>
</div>