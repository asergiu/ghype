{Language::getWord('GROUP_USERS')}: 
{if count($users)}
    <ul class="list-inline">
        {foreach from=$users item=user}
            <li><a href="javascript:">{$user['FIRSTNAME']} {$user['LASTNAME']}</a></li>
        {/foreach}
    </ul>
{/if}