<ul class="list-unstyled custom-list">
    {foreach $groups as $group_item}
        <li class="list-item" data-key="{$group_item['HKEY']}">
        <div class="media">
            <div class="media-body">
                <h4 class="media-heading"><span class="trigger-details" data-trigger="Group@toggleDetails">{$group_item['NAME']}</span> <span class="badge {if $group_item['ACTIVE'] === 'yes'}badge-active{/if}"><i class="fa fa-circle"></i> {if $group_item['ACTIVE'] === 'yes'}{Language::getWord('ACTIVE')}{else}{Language::getWord('INACTIVE')}{/if}</span></h4>
                <div class="short-details-container">
                    <i class="fa fa-info"></i> {$group_item['DESCRIPTION']}
                </div>
                <div class="details-container hidden">
                    
                </div>
            </div>
            <a class="pull-right" data-trigger="Group@viewEditModal">{Language::getWord('EDIT')}</a>
            <a class="pull-right" data-trigger="Group@deleteConfirmation">{Language::getWord('DELETE')}</a>
        </div>
    </li>
    {/foreach}
</ul>