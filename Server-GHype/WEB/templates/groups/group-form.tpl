<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-form-group" class="modal fade in" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <br>
                <i class="fa fa-user fa-7x"></i>
                <h4 class="semi-bold" id="myModalLabel">Fill the form with group info.</h4>
                <br>
            </div>
            <div class="modal-body">
                <form id="form-group">
                    <div class="row form-row">
                        <div class="col-md-12">
                            <input type="text" placeholder="{Language::getWord('GROUP_NAME')}" class="form-control" title="{Language::getWord('GROUP_NAME')}" name="group_name" required="required" value="{if isset($group_data)}{$group_data['NAME']}{/if}"/>
                        </div>
                        
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12">
                            <textarea placeholder="{Language::getWord('GROUP_DESCRIPTION')}" class="form-control" title="{Language::getWord('GROUP_DESCRIPTION')}" name="group_description" required="required">{if isset($group_data)}{$group_data['DESCRIPTION']}{/if}</textarea>
                        </div>
                    </div>
                    
                    <div class="row form-row">
                        <div class="col-md-4">
                            <select class="form-control" name="group_status">
                                <option value="active" {if isset($group_data) && $group_data['ACTIVE']=='yes'}selected="selected"{/if}>{Language::getWord('ACTIVE')}</option>
                                <option value="inactive" {if isset($group_data) && $group_data['ACTIVE']=='no'}selected="selected"{/if}>{Language::getWord('INACTIVE')}</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="form_id" value=""/>
                    <input type="hidden" name="item_key" value="{if isset($item_key)}{$item_key}{/if}"/>
                </form>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">{Language::getWord('CLOSE')}</button>
                <button class="btn btn-primary" type="button" data-trigger="Group@save">{Language::getWord('SAVE')}</button>
            </div>
        </div>

    </div>

</div>