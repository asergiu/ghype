{if count($items)}
<ul class="list-unstyled custom-list">
    {foreach $items as $item}
        <li class="list-item" data-key="{$item['HKEY']}">
        <div class="media">
            <div class="media-body">
                <h4 class="media-heading"><span class="trigger-details" data-trigger="ACL@toggleDetails">{$item['NAME']}</span></h4>
                <div class="short-details-container">
                    <i class="fa fa-info"></i> {$item['DESCRIPTION']}
                </div>
                <div class="details-container hidden">
                    
                </div>
            </div>
            <a class="pull-right" data-trigger="ACL@viewEditModal">{Language::getWord('EDIT')}</a>
        </div>
    </li>
    {/foreach}
</ul>
{/if}