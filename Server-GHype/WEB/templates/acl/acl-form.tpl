<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-form-acl" class="modal fade in" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <br>
                <i class="fa fa-lock fa-7x"></i>
                <h4 class="semi-bold" id="myModalLabel">Fill the form with ACL info.</h4>
                <br>
            </div>
            <div class="modal-body">
                <form id="form-acl">
                    <div class="row form-row">
                        <div class="col-md-12">
                            <input type="text" placeholder="{Language::getWord('ACL_NAME')}" class="form-control" title="{Language::getWord('ACL_NAME')}" name="acl_name" required="required" value="{if isset($acl_data)}{$acl_data['NAME']}{/if}"/>
                        </div>
                        
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12">
                            <textarea placeholder="{Language::getWord('ACL_DESCRIPTION')}" class="form-control" title="{Language::getWord('ACL_DESCRIPTION')}" name="acl_description" required="required">{if isset($acl_data)}{$acl_data['DESCRIPTION']}{/if}</textarea>
                        </div>
                    </div>
                    <div class="tabs-container">
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="active"><a data-tab="#1" role="tab" data-toggle="tab">{Language::getWord('PERMISSIVE_RIGHTS')}</a></li>
                          <li><a data-tab="#2" role="tab" data-toggle="tab">{Language::getWord('RESTRICTIVE_RIGHTS')}</a></li>
                          <li><a data-tab="#3" role="tab" data-toggle="tab">{Language::getWord('ACO_USERS')}</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" data-tab="#1">
                                {include file='acl/acl-tree-by-type.tpl' access_type='permit'}
                            </div>
                            <div class="tab-pane" data-tab="#2">
                                {include file='acl/acl-tree-by-type.tpl' access_type='deny'}
                            </div>
                            <div class="tab-pane" data-tab="#3">
                                {if count($users)}
                                    <ul class="list-inline">
                                        {foreach from=$users item=user_item}
                                            <li>
                                                <label><input type="checkbox" {if isset($selected_persons) && in_array($user_item['HKEY'], $selected_persons)}checked="checked"{/if} name="acl_person_{$user_item['HKEY']}"/> {if $user_item['FIRSTNAME'] || $user_item['LASTNAME']} {$user_item['FIRSTNAME']} {$user_item['LASTNAME']}{else}{$user_item['USERNAME']}{/if}</label>
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </div>
                        </div>
                    </div>  
                        
                    <input type="hidden" name="form_id" value=""/>
                    <input type="hidden" name="item_key" value="{if isset($item_key)}{$item_key}{/if}"/>
                </form>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">{Language::getWord('CLOSE')}</button>
                <button class="btn btn-primary" type="button" data-trigger="ACL@save">{Language::getWord('SAVE')}</button>
            </div>
        </div>
    </div>
</div>