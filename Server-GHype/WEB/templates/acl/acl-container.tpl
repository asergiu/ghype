<div class="view-page full-width">
    <div class="row " id="acl-container">
        <div class="col-md-12">
            <div class="grid simple" >
                <div class="grid-title border-title">
                    <h4>{Language::getWord('Security')} <i class="fa fa-plus-circle header-add-btn" data-trigger="ACL@viewFormModal" title="{Language::getWord('ACL_ADD')}"></i></h4>
                </div>
                <div class="grid-body border-body" id="acl-list-container">
                    {include file='acl/acl-list.tpl'}
                </div>
            </div>
        </div>
    </div>
</div>