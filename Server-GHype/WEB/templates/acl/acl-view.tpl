{if count($aco_list)}
    {foreach from=$aco_list item=items key=access_type}
        {if $access_type == 'permit'}{Language::getWord('PERMISSIVE_RIGHTS')}{else}{Language::getWord('RESTRICTIVE_RIGHTS')}{/if}:
        <ul class="list-inline">
            {foreach from=$items item=aco}
                <li>{$aco['ACONAME']} ({$aco['RIGHT']})</li>
            {/foreach}
        </ul>
    {/foreach}
{/if}