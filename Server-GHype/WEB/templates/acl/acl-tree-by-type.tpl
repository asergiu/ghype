{if isset($aco_tree) && count($aco_tree) && isset($access_type)}
    {if !isset($selected_ACOs)}
        {assign var=selected_ACOs value=array()}
    {/if}
    <div class="row form-row">
        <div class="col-md-7"></div>
        <div class="col-md-5"><span style="width:16px;display: inline-block;text-align: center">C</span> <span style="width:13px;display: inline-block;text-align: center">R</span> <span style="width:13px;display: inline-block;text-align: center">U</span> <span style="width:13px;display: inline-block;text-align: center">D</span></div>
    </div>
    <ul class="list-unstyled">
        {foreach from=$aco_tree item=aco_child}
            <li>
                {if isset($selected_ACOs[$aco_child['HKEY']])}
                    {assign var=selected_aco value=$selected_ACOs[$aco_child['HKEY']]}
                    {if ($selected_aco['ACCESS_TYPE'] != $access_type)}
                        {assign var=selected_aco value=''}
                    {/if}
                {else}
                    {assign var=selected_aco value=''}
                {/if}
                <div class="row form-row">
                    <div class="col-md-7">
                        {if count($aco_child['CHILDRENS'])}
                            <i class="fa fa-folder-open"></i>
                        {else}
                            <i class="fa fa-file-o"></i>
                        {/if}
                        {Language::getWord($aco_child['NAME'])}
                    </div>
                    <div class="col-md-5">
                        {assign var=c_enable value=$aco_child['CRUD_MASK']&8}
                        {assign var=r_enable value=$aco_child['CRUD_MASK']&4}
                        {assign var=u_enable value=$aco_child['CRUD_MASK']&2}
                        {assign var=d_enable value=$aco_child['CRUD_MASK']&1}
                        <input type="checkbox" {if !$c_enable}disabled="disable"{/if} name="{$access_type}_c_{$aco_child['HKEY']}" {if  $selected_aco && $selected_aco['CRUD'] & 8} checked="checked"{/if}/>
                        <input type="checkbox" {if !$r_enable}disabled="disable"{/if} name="{$access_type}_r_{$aco_child['HKEY']}" {if  $selected_aco && $selected_aco['CRUD'] & 4} checked="checked"{/if}/>
                        <input type="checkbox" {if !$u_enable}disabled="disable"{/if} name="{$access_type}_u_{$aco_child['HKEY']}" {if  $selected_aco && $selected_aco['CRUD'] & 2} checked="checked"{/if}/>
                        <input type="checkbox" {if !$d_enable}disabled="disable"{/if} name="{$access_type}_d_{$aco_child['HKEY']}" {if  $selected_aco && $selected_aco['CRUD'] & 1} checked="checked"{/if}/>
                    </div>
                </div>
                {if count($aco_child['CHILDRENS'])}
                    {assign var=aco_childrens value=$aco_child['CHILDRENS']}
                    <ul class="list-unstyled">
                    {foreach from=$aco_childrens item=aco_item}
                        {if isset($selected_ACOs[$aco_item['HKEY']])}
                            {assign var=selected_aco value=$selected_ACOs[$aco_item['HKEY']]}
                            {if ($selected_aco['ACCESS_TYPE'] != $access_type)}
                                {assign var=selected_aco value=''}
                            {/if}
                        {else}
                            {assign var=selected_aco value=''}
                        {/if}
                        <div class="row form-row">
                            <div class="col-md-7">&nbsp;&nbsp;&nbsp;<i class="fa fa-file-o"></i> {Language::getWord($aco_item['NAME'])}</div>
                            <div class="col-md-5">
                                {assign var=c_enable value=$aco_item['CRUD_MASK']&8}
                                {assign var=r_enable value=$aco_item['CRUD_MASK']&4}
                                {assign var=u_enable value=$aco_item['CRUD_MASK']&2}
                                {assign var=d_enable value=$aco_item['CRUD_MASK']&1}
                                <input type="checkbox" {if !$c_enable}disabled="disable"{/if} name="{$access_type}_c_{$aco_item['HKEY']}" {if  $selected_aco && $selected_aco['CRUD'] & 8} checked="checked"{/if}/>
                                <input type="checkbox" {if !$r_enable}disabled="disable"{/if} name="{$access_type}_r_{$aco_item['HKEY']}" {if  $selected_aco && $selected_aco['CRUD'] & 4} checked="checked"{/if}/>
                                <input type="checkbox" {if !$u_enable}disabled="disable"{/if} name="{$access_type}_u_{$aco_item['HKEY']}" {if  $selected_aco && $selected_aco['CRUD'] & 2} checked="checked"{/if}/>
                                <input type="checkbox" {if !$d_enable}disabled="disable"{/if} name="{$access_type}_d_{$aco_item['HKEY']}" {if  $selected_aco && $selected_aco['CRUD'] & 1} checked="checked"{/if}/>
                            </div>
                        </div>
                    {/foreach}
                    </ul>
                {/if}
            </li>
        {/foreach}
    </ul>
{/if}