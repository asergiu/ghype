<div class="tabs-container">
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a data-tab="#1" role="tab" data-toggle="tab">General information</a></li>
      <li><a data-tab="#2" role="tab" data-toggle="tab">Activities</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" data-tab="#1">
            <dl class="dl-horizontal">
                <dt>{Language::getWord('USER_USERNAME')}</dt>
                <dd>{if isset($user_data['USERNAME']) && $user_data['USERNAME'] }{$user_data['USERNAME']}{else}-{/if}</dd>
                <dt>{Language::getWord('USER_EMAIL')}</dt>
                <dd>{if isset($user_data['EMAIL']) && $user_data['EMAIL'] }<a title="$user_data['EMAIL']" href="mailto: $user_data['EMAIL']">{$user_data['EMAIL']}</a>{else}-{/if}</dd>
                <dt>{Language::getWord('USER_GROUPS')}</dt>
                <dd>Administrator</dd>
                <dt>{Language::getWord('USER_PHONE')}</dt>
                <dd>{if isset($user_data['PHONE']) && $user_data['PHONE'] }{$user_data['PHONE']}{else}-{/if}</dd>
            </dl>
            <pre>{print_r($user_data)}</pre>
        </div>
      <div class="tab-pane" data-tab="#2">...</div>
    </div>
</div>