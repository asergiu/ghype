<div class="view-page full-width">
    <div class="row " id="users-container">
        <div class="col-md-12">
            <div class="grid simple" >
                <div class="grid-title border-title">
                    <h4>{Language::getWord('USERS')} <i class="fa fa-plus-circle header-add-btn" data-trigger="User@viewFormModal"title="{Language::getWord('USER_ADD')}"></i></h4>
                </div>
                <div class="grid-body border-body" id="users-list-container">
                    {include file='users/users-list.tpl'}
                </div>
            </div>
        </div>
    </div>
</div>