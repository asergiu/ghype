<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-form-user" class="modal fade in" style="display: block;" data-trigger="User@removeModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button" data-trigger="User@removeModal">×</button>
                <br>
                <i class="fa fa-user fa-7x"></i>
                <h4 class="semi-bold" id="myModalLabel">{Language::getWord('USER_FORM_INFO')}</h4>
                <br>
            </div>
            <div class="modal-body">
                <form id="form-user"> 
                    <div class="tabs-container">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a data-tab="#1" role="tab" data-toggle="tab">{Language::getWord('USER_GENERAL_INFO')}</a></li>
                            <li><a data-tab="#2" role="tab" data-toggle="tab">{Language::getWord('USER_ADRESS')}</a></li>
                            <li><a data-tab="#3" role="tab" data-toggle="tab">{Language::getWord('USER_GROUPS')}</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" data-tab="#1">
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_FIRSTNAME')}" class="form-control" title="{Language::getWord('USER_FIRSTNAME')}" name="user_firstname" required="required" value="{if isset($user_data)}{$user_data['N-FAMILY']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_LASTNAME')}" class="form-control" title="{Language::getWord('USER_LASTNAME')}" name="user_lastname" required="required" value="{if isset($user_data)}{$user_data['N-GIVEN']}{/if}">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_MIDLENAME')}" class="form-control" title="{Language::getWord('USER_MIDLENAME')}" name="user_n-middle" value="{if isset($user_data)}{$user_data['N-MIDDLE']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_USERNAME')}" class="form-control" title="{Language::getWord('USER_USERNAME')}" name="username" required="required" value="{if isset($user_data)}{$user_data['USERNAME']}{/if}" {if isset($user_data)}readonly id="username-input"{/if}/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_TITLE')}" class="form-control" title="{Language::getWord('USER_TITLE')}" name="user_title" value="{if isset($user_data)}{$user_data['TITLE']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <!-- <input type="text" placeholder="{Language::getWord('USER_ROLE')}" class="form-control" title="{Language::getWord('USER_ROLE')}" name="user_role" required="required" value="{if isset($user_data)}{$user_data['ROLE']}{/if}"/> -->
                                        <select class="form-control" name="user_role">
                                                <option value="role" {if !isset($selected_role)}selected="selected"{/if}>[Role]</option>
                                            {foreach $roles as $role}
                                                <option value="{$role}"{if isset($selected_role) && $selected_role === $role}selected="selected"{/if}>{$role}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-7">
                                        <input type="text" placeholder="{Language::getWord('USER_NICKNAME')}" class="form-control" title="{Language::getWord('USER_NICKNAME')}" name="user_nickname" required="required" value="{if isset($user_data)}{$user_data['NICKNAME']}{/if}"/>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="{Language::getWord('USER_JABBERID')}" class="form-control" title="{Language::getWord('USER_JABBERID')}" name="user_jabberid" value="{if isset($user_data)}{$user_data['JABBERID']}{/if}"/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-7">
                                        <input type="text" placeholder="{Language::getWord('USER_BDAY')}" class="form-control" title="{Language::getWord('USER_BDAY')}" name="user_bday" required="required" value="{if isset($user_data)}{$user_data['BDAY']}{/if}"/>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="{Language::getWord('USER_TZ')}" class="form-control" title="{Language::getWord('USER_TZ')}" name="user_tz" value="{if isset($user_data)}{$user_data['TZ']}{/if}"/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_ORG-ORGNAME')}" class="form-control" title="{Language::getWord('USER_ORG-ORGNAME')}" name="user_org-orgname" required="required" value="{if isset($user_data)}{$user_data['ORG-ORGNAME']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_ORG-ORGUNIT')}" class="form-control" title="{Language::getWord('USER_ORG-ORGUNIT')}" name="user_org-orgunit" value="{if isset($user_data)}{$user_data['ORG-ORGUNIT']}{/if}"/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_DESC')}" class="form-control" title="{Language::getWord('USER_DESC')}" name="user_desc" required="required" value="{if isset($user_data)}{$user_data['DESC']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_NOTE')}" class="form-control" title="{Language::getWord('USER_NOTE')}" name="user_note" value="{if isset($user_data)}{$user_data['NOTE']}{/if}"/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_EMAIL')}" class="form-control" title="{Language::getWord('USER_EMAIL')}" name="user_email" required="required" value="{if isset($user_data)}{$user_data['EMAIL']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_PHONE')}" class="form-control" title="{Language::getWord('USER_PHONE')}" name="user_phone" value="{if isset($user_data)}{$user_data['TEL']}{/if}"/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="{Language::getWord('USER_URL')}" class="form-control" title="{Language::getWord('USER_URL')}" name="user_url" required="required" value="{if isset($user_data)}{$user_data['URL']}{/if}"/>
                                    </div>
                                </div>
                                <!-- <div class="row form-row">
                                    <div class="col-md-4">
                                        <select class="form-control" name="user_status">
                                            <option value="active" {if isset($user_data) && $user_data['ACTIVE']=='yes'}selected="selected"{/if}>{Language::getWord('ACTIVE')}</option>
                                            <option value="inactive" {if isset($user_data) && $user_data['ACTIVE']=='no'}selected="selected"{/if}>{Language::getWord('INACTIVE')}</option>
                                        </select>
                                    </div>
                                </div> -->
                            </div>

                            <div class="tab-pane" data-tab="#2">
                                <div class="row form-row">
                                    <div class="col-md-5">
                                        <input type="text" placeholder="{Language::getWord('USER_ADR-STREET')}" class="form-control" title="{Language::getWord('USER_ADR-STREET')}" name="user_adr-street" required="required" value="{if isset($user_data)}{$user_data['ADR-STREET']}{/if}"/>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" placeholder="{Language::getWord('USER_ADR-POBOX')}" class="form-control" title="{Language::getWord('USER_ADR-POBOX')}" name="user_adr-pobox" required="required" value="{if isset($user_data)}{$user_data['ADR-POBOX']}{/if}"/>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="{Language::getWord('USER_ADR-EXTADD')}" class="form-control" title="{Language::getWord('USER_ADR-EXTADD')}" name="user_adr-extadd" value="{if isset($user_data)}{$user_data['ADR-EXTADD']}{/if}"/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_ADR-LOCALITY')}" class="form-control" title="{Language::getWord('USER_ADR-LOCALITY')}" name="user_adr-locality" required="required" value="{if isset($user_data)}{$user_data['ADR-LOCALITY']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_ADR-REGION')}" class="form-control" title="{Language::getWord('USER_ADR-REGION')}" name="user_adr-region" value="{if isset($user_data)}{$user_data['ADR-REGION']}{/if}"/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_ADR-PCODE')}" class="form-control" title="{Language::getWord('USER_ADR-PCODE')}" name="user_adr-pcode" required="required" value="{if isset($user_data)}{$user_data['ADR-PCODE']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_ADR-COUNTRY')}" class="form-control" title="{Language::getWord('USER_ADR-COUNTRY')}" name="user_adr-country" value="{if isset($user_data)}{$user_data['ADR-COUNTRY']}{/if}"/>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_GEO-LAT')}" class="form-control" title="{Language::getWord('USER_GEO-LAT')}" name="user_geo-lat" required="required" value="{if isset($user_data)}{$user_data['GEO-LAT']}{/if}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{Language::getWord('USER_GEO-LON')}" class="form-control" title="{Language::getWord('USER_GEO-LON')}" name="user_geo-lon" value="{if isset($user_data)}{$user_data['GEO-LON']}{/if}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" data-tab="#3">
                                {if count($groups)}
                                    <ul class="list-inline">
                                        {foreach from=$groups item=group_item}
                                            <li>
                                                <label><input type="checkbox" {if isset($selected_groups) && in_array($group_item['HKEY'], $selected_groups)}checked="checked"{/if} name="group_{$group_item['HKEY']}"/> {$group_item['NAME']}</label>
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </div>
                            
                        </div>
                    </div>
                    <input type="hidden" name="form_id" value=""/>
                    <input type="hidden" name="item_key" value="{if isset($item_key)}{$item_key}{/if}"/>
                </form>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button" data-trigger="User@removeModal">{Language::getWord('CLOSE')}</button>
                <button class="btn btn-primary" type="button" data-trigger="User@save">{Language::getWord('SAVE')}</button>
            </div>
        </div>

    </div>

</div>