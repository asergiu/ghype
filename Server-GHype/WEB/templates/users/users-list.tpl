{if $page == 0}
    <ul class="list-unstyled custom-list" id="user_list">
{/if}
    {foreach $users as $user_item}
        <li class="list-item" data-key="{$user_item['HKEY']}" style="list-style: none;margin-bottom: 10px;">
        <div class="media">
            <a class="pull-left" href="#">
                <img class="media-object" src="{ROUTING::getRelativePath('images/info-user.png')}" alt="...">
            </a>
            <div class="media-body">
                <h4 class="media-heading"><span class="trigger-details" data-trigger="User@toggleDetails">
                {if $user_item['FIRSTNAME'] || $user_item['LASTNAME']} {$user_item['FIRSTNAME']} {$user_item['LASTNAME']}{else}{$user_item['USERNAME']}{/if}</span> <!-- <span class="badge {if $user_item['ACTIVE'] === 'yes'}badge-active{/if}"><i class="fa fa-circle"></i> {if $user_item['ACTIVE'] === 'yes'}{Language::getWord('ACTIVE')}{else}{Language::getWord('INACTIVE')}{/if}</span> --></h4>
                <div class="short-details-container">
                    <i class="fa fa-group"></i> {if $user_item['GROUPS']}{$user_item['GROUPS']}{else}-{/if}
                    {*(<i class="fa fa-envelope"></i>&nbsp; {$user_item['EMAIL']})*}
                </div>
                <div class="details-container hidden">
                    
                </div>
            </div>
            <a class="pull-right" data-trigger="User@viewEditModal">{Language::getWord('EDIT')}</a>
            <a class="pull-right" data-trigger="User@deleteConfirmation">{Language::getWord('DELETE')}</a>
        </div>
        </li>
        
    {/foreach}
{if $page == 0}
    </ul>
{/if}
