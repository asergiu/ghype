
{if isset($error) && $error}
    var $login_alert = $("#login-alert");
    $login_alert.removeClass("hide");
    $login_alert.children("span").html("{Language::getWord('LOGIN_ERROR_WRONG_USERNAME')}");
    $("#username").focus();
{else}
    location.reload();
{/if}