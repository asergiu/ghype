<div id="popup-preferences">                            
    <div class="body">
        <div class="arrow"></div>
        <div>
            <div class="row-form">
                <h3>{$PERSON_FIRSTNAME} {$PERSON_LASTNAME}</h3>
                {$PERSON_EMAIL}
            </div>
            
            <div class="footer">
                <button id="logout" class="fl">{$LANG_LOGOUT}</button>
                <button id="change-password" class="fr">{$LANG_P_CHANGE_PASSWORD_BTN}</button>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>