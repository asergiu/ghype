<script type="text/javascript">
    $(function(){
        iePlaceholderFix("#change-password-window");
        
        $("#change-password-window").dialog({
            autoOpen: true,
            title   : "{$LANG_CHANGE_PASSWORD_TITLE}",
            modal   : true,
            width   : 245,
            buttons : {
                "{$LANG_CHANGE_PASSWORD_BTN}" : function(){
                    var window = $(this),
                        old_pass = $("#old_password").val(),
                        new_pass = $("#new_password").val(),
                        repeat_pass = $("#repeat_new_password").val(),
                        validate_container = window.children(".validate-form");
                    
                    if (!old_pass){
                        validate_container.html("{$LANG_INVALID_OLD_PASSWORD}").effect("highlight",{}, 2000);
                        return;
                    }else if ((!new_pass || !repeat_pass) || (new_pass != repeat_pass)){
                        validate_container.html("{$LANG_INVALID_NEW_PASSWORD}").effect("highlight",{}, 2000);
                        return;
                    }
                    
                    $.ajax({
                        url : "Preferences/changePassword",
                        data: "old_password="+old_pass+"&new_password="+new_pass+"&repeat_password="+repeat_pass,
                        type    : "post",
                        dataType: "script"
                    }).complete(function(){
                        window.dialog("close");
                    });
                        
                },
                "{$LANG_CHANGE_PASSWORD_CANCEL}"      : function(){
                    $(this).dialog("close");
                }
            }
        });
    });
</script>
<div id="change-password-window" class="hide">
    <p class="validate-form">{$LANG_CHANGE_PASSWORD_FIELDS}</p>
    <form>
        <input type="password" name="old_password" id="old_password" class="text ui-widget-content ui-corner-all" title='{$language->getWord("username")}' placeholder="{$LANG_CHANGE_PASSWORD_OLD}"/>        
        <input type="password" name="new_password" id="new_password" class="text ui-widget-content ui-corner-all" title='{$language->getWord("username")}' placeholder="{$LANG_CHANGE_PASSWORD_NEW}"/>        
        <input type="password" name="repeat_new_password" id="repeat_new_password" value="" class="text ui-widget-content ui-corner-all" title='{$language->getWord("password")}' placeholder="{$LANG_CHANGE_PASSWORD_REPEAT}"/>
    </form>
</div>