<div style="margin-top: 6px; padding: 9px 3px 10px 3px; border-radius:4px;  -moz-border-radius:4px; -ms-border-radius:4px; -o-border-radius: 4px;-webkit-border-radius: 4px;">
    <table border="0" cellpadding="0" cellspacing="0" class="fr" style="background-color: transparent !important;opacity:0.95;filter:alpha(opacity=95);">
        <tr>
            <td id="signout_td" style="width:88px; cursor: pointer;">
                <div id="signout_div">
                    <div id="signout_btn" class="icon_logout" style="display: inline-block!important;"></div>
                    <span style="font-size: 12px; margin-right:10px;display: inline-block;">Logout</span>
                </div>
            </td>            
            <td id="preferences_td">
                <div id="preferences_div" style="cursor:pointer">
                    <div id="preferences_btn" class="icon_preferences" style="display: inline-block"></div>
                    <b style="color:#da1d01;font-size: 12px">&nbsp;{$PERSON_NAME}</b>
                </div>
            </td>            
        </tr>
    </table>
    <div class="clear"></div>
</div>
