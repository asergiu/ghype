<div class="row " >
    <div class="col-md-12">
        <div class="grid simple" style="border: none">
            <div class="grid-title" style="border: none; border-bottom:1px solid #dddddd; padding-left: 11px">
                <h4>
                    {Language::getWord('MY_ACCOUNT')}
                </h4>
            </div>
            <div  style="border: none; padding: 5px;">
                <div class="row">
                    <div class="col-md-4">
                        <table class="table table-condensed" style="margin-top:10px">
                            <tbody>
                                <tr>
                                    <td style="border-top:none">{Language::getWord('NAME')}:</td>
                                    <td style="border-top:none">{$user_data['FIRSTNAME']} {$user_data['LASTNAME']}</td>
                                </tr> 
                                <tr>
                                    <td>{Language::getWord('USERNAME')}:</td>
                                    <td>{$user_data['USERNAME']}</td>
                                </tr>
                                <tr>
                                    <td>{Language::getWord('PASSWORD')}:</td>
                                    <td>
                                        <a href="javascript:;" class="change-password">{Language::getWord('CHANGE_PASSWORD')}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{Language::getWord('EMAIL')}:</td>
                                    <td>
                                        <a href="#">
                                            {$user_data['EMAIL']}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{Language::getWord('PHONE')}:</td>
                                    <td class="ng-binding">{$user_data['PHONE']}</td>
                                </tr>                                
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div id="change-password-container" class="hide">
                            <form style="margin-top: 10px" id="form-change-password" >
                                <div class="form-group">
                                    <label>{Language::getWord('OLD_PASSWORD')}</label>
                                    <input type="password" name="old_password" class="form-control"  placeholder="{Language::getWord('ENTER_PASSWORD')}">
                                </div>
                                <div class="form-group">
                                    <label>{Language::getWord('NEW_PASSWORD')}</label>
                                    <input type="password" name="new_password" class="form-control"  placeholder="{Language::getWord('ENTER_NEW_PASSWORD')}">
                                </div>
                                <div class="form-group">
                                    <label>{Language::getWord('REPEAT_NEW_PASSWORD')}</label>
                                    <input type="password" name="new_password_repeat" class="form-control"  placeholder="{Language::getWord('ENTER_AGAIN_NEW_PASSWORD')}">
                                </div>
                            </form>
                            <button class="btn btn-default reset-form-password">{Language::getWord('RESET')}</button> <button class="btn btn-primary save-password">        {Language::getWord('CHANGE_PASSWORD')}</button>
                            <div id="change-password-form-error" style="padding: 3px 5px; border: red solid 1px; border-radius: 4px; margin-top: 10px" class="bg-warning hide"></div>
                        </div>
                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>