{if isset($error) && $error}
    var $alert = $("#forget-pass-alert");
    $alert.removeClass("hide").addClass("alert-danger").removeClass("alert-success");
    $alert.children("span").html("{$error}");
    $("#email_forget").focus();
{else}
    var $alert = $("#forget-pass-alert");
    $alert.removeClass("hide").removeClass("alert-danger").addClass("alert-success");
    $alert.children("span").html( "{Language::getWord('RESET_PASSWORD_EMAIL_SENT')}" );
    $("#email_forget").val("");
{/if}