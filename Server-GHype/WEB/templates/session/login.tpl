<script type="text/javascript">
    var doAuth = function(){
            $.ajax({
                url     : "{Routing::getPathByKey('login')}",
                data    : {
                    template: 'session/login-response.tpl',
                    username: escape($('#username').val()),
                    password: escape($('#password').val()),
                    words   : 'login.xml'
                },
                dataType    : "script",
                type : "POST"
            });
        },
        forgetPass = function(){
            $.ajax({
                url     : "{Routing::getPathByKey('forgetPassword')}",
                data    : {
                    email: escape($('#email_forget').val())
                },
                dataType: "script",
                type    : "POST"
            });
        };
        
    $(function(){        
        $("#trigger-login").click( function(){
            doAuth();
            return false;
        });
        //hide login form and show the one for lost password
        $("#forget-password").click(function(){
            $("#login-form").hide();
            $("#forget-form").show();
        });
        //hide forget password form and show the one for login
        $("#back-to_login-form").click(function(){
            $("#forget-form").hide();
            $("#login-form").show();
        });
        $("#login-form, #username, #password,  #trigger-login").on("keypress", function (e) {
            var key = e.which;
            
            if( key === 13 ){  // the enter key code
               $("#trigger-login").click();
               return false;  
             }
        }); 
        
        $("#forget-form,  #email_forget").on("keypress", function (e) {
            var key = e.which;
            
            if( key === 13 ){  // the enter key code
               $("#trigger-forget-password").click();
               return false;  
             }
        });
        $("#trigger-forget-password").click(function(){
            forgetPass();
            return false;
        });
    });
    
</script>
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form"  method="post" id="login-form">
        <h3 class="form-title">{Language::getWord('login_to_your_account')}</h3>
        <div class="alert alert-danger hide" id="login-alert">
            <button class="close" data-close="alert"></button>
            <span> </span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">{Language::getWord('USERNAME')}</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="{Language::getWord('username')}" name="username" id="username"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">{Language::getWord('PASSWORD')}</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="{Language::getWord('password')}" name="password" id="password"/>
            </div>
        </div>
        <div class="form-actions">
            <label class="checkbox">
                <input type="checkbox" name="remember" value="1"/> {Language::getWord('LOGIN_REMEMBER_ME')} 
            </label>
            <button  class="btn blue pull-right" id="trigger-login">
                {Language::getWord('LOGIN_BUTTON')} <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        
        <div class="forget-password">
            <h4>{Language::getWord('LOGIN_FORGOT_PASSWORD')}</h4>
            <p>
                {assign var=string_format value=Language::getWord('LOGIN_NO_WORRIES')}
                {assign var=string_here value=Language::getWord('LOGIN_HERE')}
                {sprintf($string_format, "<a href=\"javascript:;\" id=\"forget-password\"> $string_here </a>")}
            </p>
        </div>
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" id="forget-form">
        <h3>{Language::getWord('LOGIN_FORGET_PASSWORD')}</h3>
        <div class="alert alert-danger hide" id="forget-pass-alert">
            <button class="close" data-close="alert"></button>
            <span> </span>
        </div>
        <p>
            {Language::getWord('LOGIN_FORGET_PASSWORD_TXT')}
        </p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email_forget" id="email_forget"/>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-to_login-form" class="btn">
                <i class="m-icon-swapleft"></i> {Language::getWord('BACK')} 
            </button>
            <button class="btn blue pull-right" id="trigger-forget-password">
                {Language::getWord('SUBMIT')} <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>
<!--END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    {date('Y')} &copy; GHype - {Language::getWord('ALL_RIGHTS_RESERVED')}
</div>
<!-- END COPYRIGHT -->