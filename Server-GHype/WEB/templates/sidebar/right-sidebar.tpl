<div id="right-sidebar">
    <div class="right-sidebar-content">
        <h3 style="text-align: center;padding: 2px;">
            <i class="fa fa-map-marker"></i> <span class="location-address">Str. Izlazului, Nr. 26</span>
        </h3>
        <div class="incall-container">
            <div class="incall-content">
                <div class="incall-body">                                        
                    <div class="incall-body-content">
                        <div id="video-content" style="width: 100%;height:100%;background: greenyellow">
                            
                        </div>
                        <div class="incall-sidebar">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="call-trigger" title="Hold"><i class="fa fa-pause"></i></div>
                                </li>
                                <li>
                                    <div class="call-trigger"><i class="fa fa-microphone"></i></div>
                                </li>
                            </ul>
                        </div>
                        <div class="call-timer">00:21</div>
                    </div>
                </div>
                <div class="incall-footer"></div>
            </div>
        </div>
        <div class="actions">
            <div class="group">
                <button class="btn">Transfer intern</button>&nbsp;
                <button id="eventSocketInchide" class="btn btn-danger">Inchide</button>
            </div>
            <div class="group">
                <button class="btn">Transfer extern</button>&nbsp;
                <button class="btn">Dialer</button>
            </div>
        </div>
    </div>
</div>