<div id="conversejs">
    <a id="toggle-controlbox" class="toggle-controlbox" href="#" style="display: none;">
        <span class="conn-feedback">Contacts</span>
        <span id="online-count" style="">(0)</span>
    </a>
    <div id="controlbox" class="chatbox" style="display: block;">
        <div class="box-flyout" style="height: 400px">
            <div class="dragresize dragresize-tm"></div>
            <div class="chat-head controlbox-head">
                <ul id="controlbox-tabs">
                    <li>
                        <a class="current" href="#login-dialog">Sign in</a>
                    </li>
                    <li>
                        <a class="s" href="#register">Register</a>
                    </li>
                </ul>
                <a class="close-chatbox-button icon-close"></a>
            </div>
            <div class="controlbox-panes">
                <div id="login-dialog" class="controlbox-pane">
                    <form id="converse-login" method="post">
                        <label>XMPP Username:</label>
                        <input placeholder="user@server" name="jid">
                        <label>Password:</label>
                        <input type="password" placeholder="password" name="password">
                        <input class="submit" type="submit" value="Log In">
                        <span class="conn-feedback"></span>
                    </form>
                </div>
                <div id="register" class="controlbox-pane" style="display: none;">
                    <form id="converse-register">
                        <span class="reg-feedback"></span>
                        <label>Your XMPP provider's domain name:</label>
                        <input type="text" placeholder=" e.g. conversejs.org" name="domain">
                        <p class="form-help">
                            Tip: A list of public XMPP providers is available
                            <a class="url" target="_blank" href="https://xmpp.net/directory.php">here</a>
                            .
                        </p>
                        <input class="submit" type="submit" value="Fetch registration form">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


