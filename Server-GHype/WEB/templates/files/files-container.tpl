<div class="row " >
    <div class="col-md-12">
        <div class="grid simple" style="border: none">
            <div class="grid-title" style="border: none; border-bottom:1px solid #dddddd; padding-left: 11px">
                <h4>
                    <span style="display: inline-block;width:17px"><i class="fa fa-square-o check-all hide" style="cursor: pointer"></i></span> {Language::getWord('FILES')}  
                </h4>
            </div>
            <div class="row row__header">
                <div class="col-md-3">{Language::getWord('FILE_NAME')}</div>
                <div class="col-md-1">{Language::getWord('FILE_FROM')}/{Language::getWord('FILE_TO')}</div>
                <div class="col-md-1"></div>
                <div class="col-md-1">{Language::getWord('DATE')}</div>
                <div class="col-md-2">{Language::getWord('FILE_SIZE')}</div>
                <div class="col-md-1">{Language::getWord('FILE_STATUS')}</div>
                <div class="col-md-3">{Language::getWord('FILE_MESSAGE')}</div>
            </div>
            <div  style="border: none; padding: 5px;">
                
                <ul class="list-unstyled custom-list" id="files">
                    
                </ul>
            </div>
        </div>
    </div>
</div>