#!/bin/bash

ls -a /var/www/html/bp/scripts/*.php |while read file; do
  CRON=`cat $file |grep "#CRON:" |cut -f2 -d':'`;
  if [ -n "$CRON" ]; then
    MINUTE=$[`date "+%-H"` * 60 + `date "+%-M"`];
    EXEC=$[( $MINUTE % $CRON )];
    if [ $EXEC -eq 0 ]; then
      /usr/bin/php $file >/dev/null 2>/dev/null;
    fi;
  fi;
done
                            