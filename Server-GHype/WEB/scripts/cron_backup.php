#!/usr/bin/php
#CRON:1
<?PHP
  error_reporting( E_ERROR | E_PARSE );

  defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../' ));
  defined('CONFIG_PATH') || define('CONFIG_PATH', realpath(dirname(__FILE__) . '/../configs'));
  defined('INCLUDE_PATH') || define('INCLUDE_PATH', realpath(dirname(__FILE__) . '/../include'));

  defined('CLASS_PATH') || define('CLASS_PATH', realpath(dirname(__FILE__) . '/../class'));
  defined('CSS_PATH') || define('CSS_PATH', realpath(dirname(__FILE__) . '/../css'));

  defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

  set_include_path(implode(PATH_SEPARATOR, array(INCLUDE_PATH, get_include_path())));

  include_once(INCLUDE_PATH.DIRECTORY_SEPARATOR.'functions.php');
  
  $_SESSION = array();
  
  spl_autoload_register('__autoload');
  
  $app = new Application();
  $app->init();
  
  $_class = new Backup();
  $_class->executeBackup();
?>
