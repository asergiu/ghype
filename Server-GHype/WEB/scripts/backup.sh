#!/bin/bash

# command line arguments

BACKUP_DATABASE=${1}
BACKUP_MEDIA=${2}
BACKUP_DATA=${3}
BACKUP_MONITOR=${4}

MYSQL_HOSTNAME=${5}
MYSQL_USERNAME=${6}
MYSQL_PASSWORD=${7}
MYSQL_DBNAME=${8}

ROOT_DIRECTORY=${9}

DESTINATION=${10}

ACTION=${11}

END_DATE=${12}

# consistency check
if [ -z "${DESTINATION}" ]; then
    exit 1;
elif [ -z "${MYSQL_HOSTNAME}" -a -z "${MYSQL_USERNAME}" -a -z "${MYSQL_PASSWORD}" -a -z "${MYSQL_DBNAME}" ]; then
    exit 2;
elif [ -z "${ROOT_DIRECTORY}" ]; then
    exit 3;
elif [ "${BACKUP_DATABASE}" -eq "0" -a "${BACKUP_MEDIA}" -eq "0" -a "${BACKUP_DATA}" -eq "0" -a "${BACKUP_MONITOR}" -eq "0" ]; then
    exit 4;
else
    /usr/bin/rm -rf ${DESTINATION};
    BACKUP_MEDIA_QUERY="";

    /usr/bin/touch "/tmp/README";
    /usr/bin/echo "Archive created at `/usr/bin/date`" > "/tmp/README";
    /usr/bin/echo "Command line arguments: ${BACKUP_DATABASE} ${BACKUP_MEDIA} ${BACKUP_DATA} ${BACKUP_MONITOR} ${MYSQL_HOSTNAME} ${MYSQL_USERNAME} **** ${MYSQL_DBNAME} ${ROOT_DIRECTORY} ${DESTINATION} ${ACTION} ${END_DATE}" >> "/tmp/README";

    /usr/bin/tar -c --add-file="/tmp/README" -f ${DESTINATION} --remove-files >/dev/null 2>/dev/null
    if [ ! "$?" -eq "0" ]; then
        /usr/bin/rm -rf ${DESTINATION};
        /usr/bin/rm -rf "/tmp/README";
        exit 5;
    fi;
    
    MYSQL_FILENAME="/tmp/mysql.dump";
    /usr/bin/rm -rf ${MYSQL_FILENAME}
    /usr/bin/touch ${MYSQL_FILENAME}

    if [ "${BACKUP_DATABASE}" -eq "1" ]; then
        if [ -z "${END_DATE}" ]; then
            /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} >> ${MYSQL_FILENAME}
        else
            /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} --ignore-table=${MYSQL_DBNAME}.recorded_message --ignore-table=${MYSQL_DBNAME}.cdr --ignore-table=${MYSQL_DBNAME}.bp_monitor >> ${MYSQL_FILENAME}
            /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} --where="created_at <= '${END_DATE}'" recorded_message >> ${MYSQL_FILENAME}
            /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} --where="calldate <= '${END_DATE}'" cdr >> ${MYSQL_FILENAME}
            /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} --where="timestamp <= '${END_DATE}'" bp_monitor >> ${MYSQL_FILENAME}
        fi;
    fi;

    if [ "${BACKUP_MEDIA}" -eq "1" ]; then
        ERROR_FILE="/tmp/error.no";
        /usr/bin/echo "0" > ${ERROR_FILE}

        if [ -z "${END_DATE}" ]; then
            if [ "${BACKUP_DATABASE}" -eq "0" ]; then
                /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} recorded_message >> ${MYSQL_FILENAME}
            fi;
            BACKUP_MEDIA_QUERY="select filename from recorded_message";
        else
            if [ "${BACKUP_DATABASE}" -eq "0" ]; then
                /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} --where="created_at <= '${END_DATE}'" recorded_message >> ${MYSQL_FILENAME}
            fi;
            BACKUP_MEDIA_QUERY="select filename from recorded_message where created_at <= '${END_DATE}'";
        fi;
        
        echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} |while read filename; do
            if [ ! "${filename}" == "filename" ]; then
                if [ -f "${ROOT_DIRECTORY}/${filename}" ]; then
                    /usr/bin/tar -c --add-file="${ROOT_DIRECTORY}/${filename}" -f ${DESTINATION} >/dev/null 2>/dev/null
                    if [ ! "$?" -eq "0" ]; then
                        /usr/bin/echo "1" > ${ERROR_FILE}
                        ERRORNO=1
                        break;
                    fi;
                fi;
            fi;
        done;
        
        ERRORNO=`/usr/bin/cat ${ERROR_FILE}`
        if [ "${ERRORNO}" -eq "1" ]; then
            /usr/bin/rm -rf ${MYSQL_FILENAME}
            /usr/bin/rm -rf ${DESTINATION}
            exit 6;
        fi;
    fi;
    
    if [ "${BACKUP_DATA}" -eq "1" ]; then
        ERROR_FILE="/tmp/error.no";
        /usr/bin/echo "0" > ${ERROR_FILE}

        if [ -z "${END_DATE}" ]; then
            if [ "${BACKUP_DATABASE}" -eq "0" ]; then
                /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} cdr >> ${MYSQL_FILENAME}
            fi;
            BACKUP_MEDIA_QUERY="select monitor_filename from cdr";
        else
            if [ "${BACKUP_DATABASE}" -eq "0" ]; then
                /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} --where="calldate <= '${END_DATE}'" cdr >> ${MYSQL_FILENAME}
            fi;
            BACKUP_MEDIA_QUERY="select monitor_filename from cdr where calldate <= '${END_DATE}'";
        fi;
        
        echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} |while read filename; do
            if [ ! "${filename}" == "monitor_filename" ]; then
                if [ -f "${ROOT_DIRECTORY}/${filename}" ]; then
                    /usr/bin/tar -c --add-file="${ROOT_DIRECTORY}/${filename}" -f ${DESTINATION} >/dev/null 2>/dev/null
                    if [ ! "$?" -eq "0" ]; then
                        /usr/bin/echo "1" > ${ERROR_FILE}
                        ERRORNO=1
                        break;
                    fi;
                fi;
            fi;
        done;

        ERRORNO=`/usr/bin/cat ${ERROR_FILE}`
        if [ "${ERRORNO}" -eq "1" ]; then
            /usr/bin/rm -rf ${MYSQL_FILENAME}
            /usr/bin/rm -rf ${DESTINATION}
            exit 7;
        fi;
    fi;

    if [ "${BACKUP_DATA}" -eq "1" ]; then
        if [ -z "${END_DATE}" ]; then
            if [ "${BACKUP_DATABASE}" -eq "0" ]; then
                /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} bp_monitor >> ${MYSQL_FILENAME}
            fi;
        else
            if [ "${BACKUP_DATABASE}" -eq "0" ]; then
                /usr/bin/mysqldump --no-create-db --no-create-info -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} --where="timestamp <= '${END_DATE}'" bp_monitor >> ${MYSQL_FILENAME}
            fi;
        fi;
    fi;

    /usr/bin/tar -r --add-file=${MYSQL_FILENAME} -f ${DESTINATION} --remove-files >/dev/null 2>/dev/null
    if [ ! "$?" -eq "0" ]; then
        /usr/bin/rm -rf ${DESTINATION};
        /usr/bin/rm -rf ${MYSQL_FILENAME};
        exit 8;
    fi;
    
    if [ "${ACTION}" == "move" ]; then
        if [ "${BACKUP_MEDIA}" -eq "1" ]; then
            if [ -z "${END_DATE}" ]; then
                BACKUP_MEDIA_QUERY="select filename from recorded_message";
            else
                BACKUP_MEDIA_QUERY="select filename from recorded_message where created_at <= '${END_DATE}'";
            fi;
            echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} |while read filename; do
                if [ ! "${filename}" == "filename" ]; then
                    /usr/bin/rm -rf ${ROOT_DIRECTORY}/${filename}
                fi;
            done;
            if [ -z "${END_DATE}" ]; then
                BACKUP_MEDIA_QUERY="delete from recorded_message";
            else
                BACKUP_MEDIA_QUERY="delete from recorded_message where created_at <= '${END_DATE}'";
            fi;
            echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
        fi;

        if [ "${BACKUP_DATA}" -eq "1" ]; then
            if [ -z "${END_DATE}" ]; then
                BACKUP_MEDIA_QUERY="select monitor_filename from cdr";
            else
                BACKUP_MEDIA_QUERY="select monitor_filename from cdr where calldate <= '${END_DATE}'";
            fi;
            echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME} |while read filename; do
                if [ ! "${filename}" == "monitor_filename" ]; then
                    /usr/bin/rm -rf ${ROOT_DIRECTORY}/${filename}
                fi;
            done;
            if [ -z "${END_DATE}" ]; then
                BACKUP_MEDIA_QUERY="delete from cdr";
            else
                BACKUP_MEDIA_QUERY="delete from cdr where calldate <= '${END_DATE}'";
            fi;
            echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
        fi;

        if [ "${BACKUP_MONITOR}" -eq "1" ]; then
            if [ -z "${END_DATE}" ]; then
                BACKUP_MEDIA_QUERY="delete from bp_monitor";
            else
                BACKUP_MEDIA_QUERY="delete from bp_monitor where timestamp <= '${END_DATE}'";
            fi;
            echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
        fi;

        if [ "${BACKUP_DATABASE}" -eq "1" ]; then
            if [ -z "${END_DATE}" ]; then
                BACKUP_MEDIA_QUERY="delete from recorded_message";
                echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
                BACKUP_MEDIA_QUERY="delete from monitor_filename";
                echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
                BACKUP_MEDIA_QUERY="delete from bp_monitor";
                echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
            else
                BACKUP_MEDIA_QUERY="delete from recorded_message where created_at <= '${END_DATE}'";
                echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
                BACKUP_MEDIA_QUERY="delete from cdr where calldate <= '${END_DATE}'";
                echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
                BACKUP_MEDIA_QUERY="delete from bp_monitor where timestamp <= '${END_DATE}'";
                echo "${BACKUP_MEDIA_QUERY}" |/usr/bin/mysql -h ${MYSQL_HOSTNAME} -u ${MYSQL_USERNAME} --password="${MYSQL_PASSWORD}" ${MYSQL_DBNAME};
            fi;
        fi;
    fi;
fi;    

exit 0;
