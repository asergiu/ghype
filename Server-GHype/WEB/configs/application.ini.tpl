[production]

system.database.dbtype = MySQL
system.database.host = 192.168.44.224
system.database.username = flashnet
system.database.password = redeen2F
system.database.dbname = bp
system.database.log = false
system.database.log_filename = "/tmp/database.log"

system.auth.methode = LOCAL

default.language = ro
default.timezone = Europe/Bucharest
default.debug = false
default.hash_data = false
default.date_format = m.d.Y H:i

phpSettings.display_startup_errors = 0
phpSettings.display_errors = 0

info.company = FlashNet
info.company_logo = flashnet_logo.png

interface.display_notifications = 0
interface.display_chat = 1
media.audio_files =  ROOT_PATH /media/audio

map.default.lat = 46.1828172
map.default.lng = 25.1425822
map.default.zoom = 8

dashboard.logs.limit = 100

[development : production]

phpSettings.display_startup_errors = 1
phpSettings.display_errors = 1
default.debug = true
