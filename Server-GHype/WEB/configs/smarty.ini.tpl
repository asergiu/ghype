[production]

smarty.dir = ROOT_PATH /smarty/
smarty.template_dir = ROOT_PATH /templates/, ROOT_PATH /javascript/
smarty.compile_dir = /tmp/smarty
smarty.config_dir = ROOT_PATH /configs
smarty.cache_dir = /tmp/smarty
smarty.caching = 0
smarty.compile_check = true
