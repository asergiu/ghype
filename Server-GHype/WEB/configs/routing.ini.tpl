; this is an INI file
[production]

routes('login').path = /login
routes('login').match=/login/
routes('login').callback = Application@login
routes('login').secured = no

routes('forgetPassword').path = /forgetPassword/
routes('forgetPassword').match = /forgetPassword/
routes('forgetPassword').callback = Application@sendResetPasswordEmail
routes('forgetPassword').secured = no

routes('logout').path = /logout/
routes('logout').match=/logout/
routes('logout').callback = Application@logout

routes('getTemplateContent').path = /getTemplateContent/
routes('getTemplateContent').match = /getTemplateContent/
routes('getTemplateContent').callback = Application@getTemplateContent
routes('getTemplateContent').secured = no

routes('javascript').path = /javascript
routes('javascript').match = /javascript/*
routes('javascript').callback = Application@processJavascriptFile
routes('javascript').secured = no

routes('usersList').path = /loadUsers/
routes('usersList').match = /loadUsers/
routes('usersList').callback = User@generateUsersListView

routes('viewUser').path = /viewUser/
routes('viewUser').match = /viewUser/
routes('viewUser').callback = User@generateUserView
routes('viewUser').language_files(0) = users.xml
routes('viewUser').language_files(1) = preferences.xml

routes('viewAddUser').path = /viewAddUser/
routes('viewAddUser').match = /viewAddUser/
routes('viewAddUser').callback = User@generateUserForm
routes('viewAddUser').language_files = users.xml

routes('saveUserForm').path =/saveUserForm/
routes('saveUserForm').match = /saveUserForm/
routes('saveUserForm').callback = User@parseUserForm
routes('saveUserForm').language_files = users.xml

routes('deleteUser').path = /deleteUser/
routes('deleteUser').callback = User@deleteConfirmation

routes('groupsList').path = /groups/
routes('groupsList').match= /groups/
routes('groupsList').callback = Group@generateListGroupsView

routes('viewGroup').path = /viewGroup/
routes('viewGroup').match = /viewGroup/
routes('viewGroup').callback = Group@generateGroupView
routes('viewGroup').language_files = users.xml

routes('viewFormGroup').path = /viewFormGroup/
routes('viewFormGroup').match = /viewFormGroup/
routes('viewFormGroup').callback = Group@generateGroupForm
routes('viewFormGroup').language_files = users.xml

routes('saveGroupForm').path =/saveGroupForm/
routes('saveGroupForm').match = /saveGroupForm/
routes('saveGroupForm').callback = Group@parseGroupForm
routes('saveGroupForm').language_files = users.xml

routes('deleteGroup').path = /deleteGroup/
routes('deleteGroup').callback = Group@deleteConfirmation

routes('aclList').path = /acl/
routes('aclList').match= /acl/
routes('aclList').callback = ACL@listACL

routes('viewACL').path = /viewACL/
routes('viewACL').match = /viewACL/
routes('viewACL').callback = ACL@generateACLView
routes('viewACL').language_files = acl.xml

routes('viewFormACL').path = /viewFormACL/
routes('viewFormACL').match = /viewFormACL/
routes('viewFormACL').callback = ACL@generateACLForm
routes('viewFormACL').language_files = acl.xml

routes('saveACLForm').path =/saveACLForm/
routes('saveACLForm').match = /saveACLForm/
routes('saveACLForm').callback = ACL@parseACLForm
routes('saveACLForm').language_files = acl.xml


routes('transferedFileListJson').path = /transferedFileListJson/
routes('transferedFileListJson').callback = TransferedFile@generateListJson
routes('transferedFileListJson').language_files = transfered_file.xml

routes('fileList').path = /fileList
routes('fileList').callback = TransferedFile@generateListView
routes('fileList').language_files = transfered_file.xml

routes('generateFormReport').path = /generateFormReport/
routes('generateFormReport').callback = FormReport@generateForm
routes('generateFormReport').language_files = form-report.xml

routes('saveFormReportForm').path = /saveFormReportForm/
routes('saveFormReportForm').callback = FormReport@parseSaveForm
routes('saveFormReportForm').language_files = form-report.xml

routes('getQuestionTypes').path = /getQuestionTypes/
routes('getQuestionTypes').callback = FormReport@questionTypesList
routes('getQuestionTypes').language_files = form-report.xml

routes('showNextQuestion').path = /showNextQuestion/
routes('showNextQuestion').callback = FormReport@getNextQuestion
routes('showNextQuestion').language_files = form-report.xml

routes('autosaveQuestion').path = /autosaveQuestion/
routes('autosaveQuestion').callback = FormReport@saveQuestion
routes('autosaveQuestion').language_files = form-report.xml

routes('saveQuestionObservations').path = /saveQuestionObservations/
routes('saveQuestionObservations').callback = FormReport@saveObservations
routes('saveQuestionObservations').language_files = form-report.xml

routes('loadTimelineData').path = /loadTimelineData/
routes('loadTimelineData').callback = FormReport@getTimelineData
routes('loadTimelineData').language_files = form-report.xml

routes('showBrowser').path = /showBrowser/
routes('showBrowser').callback = FormReport@showBrowser

routes('nodjs-asterisk-action').path = /nodjsAsteriskAction/
routes('nodjs-asterisk-action').callback = Application@nodjsAsteriskAction

routes('clearCache').path = /clearCache/
routes('clearCache').callback = Application@clearCache

routes('loadFormFilterItems').path = /loadFormFilterItems/
routes('loadFormFilterItems').callback = FormReport@loadFormFilterItems
routes('loadFormFilterItems').language_files = form-report.xml

routes('loadBackupList').path = /loadBackupList/
routes('loadBackupList').callback = Backup@loadBackupList

routes('loadBackupElementTemplate').path = /loadBackupElementTemplate
routes('loadBackupElementTemplate').callback = Backup@loadBackupElementTemplate

routes('viewCreateBackup').path = /viewCreateBackup/
routes('viewCreateBackup').callback = Backup@viewCreateBackup

routes('saveCreateBackup').path = /saveCreateBackup/
routes('saveCreateBackup').callback = Backup@saveCreateBackup

routes('deleteCreateBackup').path = /deleteCreateBackup/
routes('deleteCreateBackup').callback = Backup@deleteCreateBackup

routes('myAccount').path = /myAccount/
routes('myAccount').callback = User@getMyAccountPage

routes('changePassword').path = /changePassword/
routes('changePassword').callback = User@changePassword

routes('timelineItemRESTful').path = /timelineItemRESTful/
routes('timelineItemRESTful').match = /timelineItemRESTful/*
routes('timelineItemRESTful').callback = FormReport@timelineItemREST

routes('callScriptData').path = /CallScriptData/
routes('callScriptData').callback = FormReport@getCallScriptData