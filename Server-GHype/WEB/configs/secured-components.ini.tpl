[production]

components.dir = /components/

components.component(0).dir = jquery
components.component(0).javascript(0) = jquery-2.1.1.min.js
components.component(0).javascript(1) = jquery-ui.min.js
components.component(0).javascript(3) = jquery.easing.min.js

components.component(1).dir = bootstrap
components.component(1).css(0) = css/bootstrap.css
components.component(1).css(1) = css/bootstrap-theme.min.css
components.component(1).javascript(0) = js/bootstrap.min.js

components.component(2).dir = font-awesome
components.component(2).css(0) = font-awesome.css

components.component(3).dir = jquery-scrollbar
components.component(3).css(0) = jquery.scrollbar.css
components.component(3).javascript(0) = jquery.scrollbar.min.js

components.component(4).dir = backbone
components.component(4).javascript(0) = underscore.js
components.component(4).javascript(1) = backbone-min.js

components.component(5).dir = requireJS
components.component(5).javascript = require.js
