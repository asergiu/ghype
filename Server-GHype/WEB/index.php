<?PHP
    defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) ));
    defined('CONFIG_PATH') || define('CONFIG_PATH', realpath(dirname(__FILE__) . '/configs'));
    defined('INCLUDE_PATH') || define('INCLUDE_PATH', realpath(dirname(__FILE__) . '/include'));
    defined('CLASS_PATH') || define('CLASS_PATH', realpath(dirname(__FILE__) . '/class'));
    defined('CSS_PATH') || define('CSS_PATH', realpath(dirname(__FILE__) . '/css'));
    defined('MEMCACHED_HOST') || define('MEMCACHED_HOST', '127.0.0.1');
    defined('MEMCACHED_PORT') || define('MEMCACHED_PORT', '11211');
    defined('MEMCACHED_ACTIVE') || define('MEMCACHED_ACTIVE', 1);

    defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
    //finding working dir
    if(($_sitepath = str_replace([$_SERVER['DOCUMENT_ROOT'],'index.php'], ['', ''], str_replace(DIRECTORY_SEPARATOR,'/', $_SERVER['SCRIPT_FILENAME']))) == '/') $_sitepath = false;
    defined('DOCUMENT_ROOT_PATH') || define('DOCUMENT_ROOT_PATH',$_sitepath);

    set_include_path(implode(PATH_SEPARATOR, array(INCLUDE_PATH, get_include_path())));

    include_once(INCLUDE_PATH.DIRECTORY_SEPARATOR.'functions.php');
  
    spl_autoload_register('__autoload');

    $app =  new Application();
    $app->init();
    $app->run();
