/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
AXTEN.ACL = function (){
    //call the parent construnctor
    AXTEN.Module.call( this, "acl-list-container", "acl-container" );
};

//inherit Module
AXTEN.ACL.prototype = new AXTEN.Module();

//correct the constructor pointer because it pionts to Module
AXTEN.ACL.prototype.constructor = AXTEN.ACL;

AXTEN.ACL.prototype.loadList = function(){
    AXTEN.Module.prototype.loadList.call(this, "{Routing::getPathByKey('aclList')}");
};

AXTEN.ACL.prototype.loadItem = function( key, callback ){
    var me = this;
    
    $.ajax({
        url     : "{Routing::getPathByKey('viewACL')}",
        data    : {
            key : key
        },
        type    : 'POST'
    }).done(function( response ){
        try{
            response = $.parseJSON( response );
            if ( response.success && ( typeof callback === "function" ) ){ 
                callback( response );
            }
        }catch(e){
        }
    });
};
AXTEN.ACL.prototype.viewFormModal = function( data ){
    var me = this;
    
    data =  data ? data : {};
    me['modal_acl_add'] = $.ajax({
        url : "{Routing::getPathByKey('viewFormACL')}",
        type: "POST",
        data: data
    }).done(function( response ){
        try{
            response = $.parseJSON(response);
            if ( response.success ){
                var modal_elem = $(response.data).appendTo($("#page-content"));
                modal_elem.modal();
                modal_elem.on("hidden.bs.modal", function(){
                    modal_elem.remove();
                });
            }
        }catch (e){
          
        }
  });
  
};

AXTEN.ACL.prototype.viewEditModal = function( trigger_elem ){
    var me= this,
        item_key = trigger_elem.closest('li.list-item').attr("data-key");

    if ( item_key ) {
        me.viewFormModal({
            key: item_key
        });
    }
};

AXTEN.ACL.prototype.save = function(){
    var me = this,
        form = $("#form-acl"),
        form_values = form.serializeArray(),
        data = {},
        item;

    for (key in form_values){
        item = form_values[key];
        data[item.name] = item.value;
    }
    $.ajax({
        url : "{ROUTING::getPathByKey('saveACLForm')}",
        data: data,
        type: 'POST'
    }).done(function( response ){
        try{
            response = $.parseJSON( response );
            if ( response.success ){
                $("#modal-form-acl").modal('hide');
                me.loadList();
            }else {
                if ( response.hasOwnProperty( "form_errors" ) && ( response.form_errors.length ) ){
                    me.showFormErrors( form, response.form_errors );
                }
            }
        }catch (e){}
    });
};