//Declaring the application namespace
AXTEN.default_configuration = {
  debug: false,
  running: false,
  username: undefined,
  password: undefined,
  domain: undefined
};

AXTEN.templateSettings= {
    evaluate    : /<%([\s\S]+?)%>/g,
    interpolate : /<%=([\s\S]+?)%>/g,
    escape      : /<%-([\s\S]+?)%>/g
}

AXTEN.core = {
    log: function () {
        var config = AXTEN.getConfiguration();
        
        if (config.debug && (typeof console === 'object') && (typeof console.log === 'function')) {
            try{
                console.log.apply(console, arguments);
            }catch (e){
            }
        }
    },
    warn: function (message) {
        var config = AXTEN.getConfiguration();
        
        if (config.debug && typeof window.console !== 'undefined') {
            window.console.warn(message);
        }
    },
    error: function (message) {
        var config = AXTEN.getConfiguration();
        
        if (config.debug && typeof window.console !== 'undefined') {
            window.console.error(message);
        }
    },
    info: function (message) {
        var config = AXTEN.getConfiguration();
        
        if (config.debug && typeof window.console !== 'undefined') {
            window.console.info(message);
        }
    },
    debug: function (message) {
        var config = AXTEN.getConfiguration();
        
        if (config.debug && typeof window.console !== 'undefined') {
            window.console.debug(message);
        }
    }
};
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(elt /*, from*/)
    {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
                ? Math.ceil(from)
                : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++)
        {
            if (from in this &&
                    this[from] === elt)
                return from;
        }
        return -1;
    };
}
AXTEN.click = 'click';//also can be touch if we use a mobile device

AXTEN.handleSidebarAndContentHeight = function(){
    var content=$('.page-content');
    var sidebar=$('.page-sidebar');
    if(!content.attr("data-height")){
        content.attr("data-height",content.height());
    }
    if(sidebar.height()>content.height()){
        content.css("min-height",sidebar.height()+ 120);
    }else{
        content.css("min-height",content.attr("data-height"));
    }
};

AXTEN.doAction = function( action_string, trigger_elem ) {
    var components = [],
        parts;

    if ( action_string ){
        components = action_string.split("@");
        parts = action_string.split("::");
        if ( components.length > 1 ){
            require([components[0]], function(){
                if ( typeof window.AXTEN[components[0]] === "function"){
                    var object = new window.AXTEN[components[0]];
                    if (typeof object[components[1]] === "function" ){
                        object[components[1]]( trigger_elem );
                    }
                }
            });
        } else if ( parts.length > 1 ) { //call a function from a certain namespace
            require( [ parts[ 0 ] ], function(){
                var object = window.AXTEN[ parts[ 0 ] ];
                if ( typeof  object[ parts[ 1 ] ] === "function" ) {
                    object[ parts[1] ]();
                }
            });
        }
        else if ( (action_string in AXTEN) && typeof AXTEN[ action_string ] === "function") {
            AXTEN[action_string]();
        }
    }
};

AXTEN.logout = function() {
    if(AXTEN.socket) {
        AXTEN.socket.emit('system', { type: 'logout', content: { success: true } });
        AXTEN.socket.disconnect();
    }
    $.ajax({
        url : "{Routing::getPathByKey('logout')}",
        dataType    : "script",
        type : "POST"
    });
};

AXTEN.setConfiguration = function( config ){
    $(document).data("BPConfig", config);
};

AXTEN.getConfiguration = function(){
    return $(document).data("BPConfig");
};

AXTEN.init = function ( config ) {
    var resizeTimer;
    
    var new_config = $.extend(true, {}, AXTEN.default_configuration);
    
    $.extend(new_config, config);
    AXTEN.setConfiguration(new_config);
    
    require.config({
        baseUrl: 'javascript',
        paths: {
            "User": "users",
            "Group": "groups",
            "ACL": "acl",
            "ClearCache": "clear_cache",
            "Dashboard": "dashboard",
            "TransferedFile": "files"
        }
    });

    $(document).on(AXTEN.click, "[data-trigger]", function () {
        AXTEN.doAction($(this).attr('data-trigger'), $(this));
    });

    $(document).on(AXTEN.click, '.axt-checkbox', function (event) {
        var $elem = $(this),
                $check_elem = $elem.children("i.check-icon");

        if ($check_elem.hasClass("fa-square-o")) {
            $check_elem.removeClass("fa-square-o").addClass("fa-check-square-o");
            $elem.addClass('item-checked');
        } else {
            $check_elem.removeClass("fa-check-square-o").addClass("fa-square-o");
            $elem.removeClass('item-checked');
        }
    });

    AXTEN.hidePopups = function ( event ){
        var $target = $(event.target),
            popups = $(".pop-autoclose"),
            action = function( item ){
                var $item = $(item);
                
                
                if ( $item.hasClass( "in" ) ){                    
                    if ( $item.has( $target).length ) {
                    } else { //close popup
                        var $parent = $item.parent(),
                            $source = $parent.find( "[aria-describedby='"+$item.prop("id")+"']" );
                        
                        if ( $source.length ){
                            $source.popover("hide");
                        }
                    }
                }
            };
        
        _.each( popups, action);
    };
    $(document).on(AXTEN.click, AXTEN.hidePopups);
    //add an resize event with timeout to trigger less resize events
    $(window).resize(function () {
        if (this.resizeTO)
            clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function () {
            $(this).trigger('resizeEnd');
        }, 500);
        //update content on resize
        clearTimeout(resizeTimer);
    });
    
    AXTEN.checkH264Video = function() {
        if(!!!document.createElement('video').canPlayType) return false;
        var v = document.createElement("video");
        return v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
    };
    
    //activate custom scrollbar to left sidebar
    $('#main-menu-wrapper').scrollbar();

    $('.page-sidebar li > a').on('click', function (e) {
        var parent = $(this).parent().parent(),
                sub = $(this).next();

        if ($(this).next().hasClass('sub-menu') === false) {
            return;
        }

        parent.children('li.open').children('a').children('.arrow').removeClass('open');
        parent.children('li.open').children('a').children('.arrow').removeClass('active');
        parent.children('li.open').children('.sub-menu').slideUp(200);
        parent.children('li').removeClass('open');

        if (sub.is(":visible")) {
            $('.arrow', $(this)).removeClass("open");
            $(this).parent().removeClass("active");
            sub.slideUp(200, function () {
                AXTEN.handleSidebarAndContentHeight();
            });
        } else {
            $('.arrow', $(this)).addClass("open");
            $(this).parent().addClass("open");
            sub.slideDown(200, function () {
                AXTEN.handleSidebarAndContentHeight();
            });
        }
        e.preventDefault();
    });
    $('.trigger-sidebar-toggle').click(function () {
        if ($(this).is('.dropdown-toggle')) {
            $("#sidebar-toggle-2").removeClass('hide');
        } else {
            $("#sidebar-toggle-2").addClass('hide');
        }
        if ($('#main-menu').attr('data-inner-menu') == '1') {
            AXTEN.core.log("Menu is already condensed");
        } else {
            if ($('#main-menu').hasClass('mini')) {
                $('body').removeClass('grey');
                $('body').removeClass('condense-menu');
                $('#main-menu').removeClass('mini');
                $('.page-content').removeClass('condensed');
                $('.scrollup').removeClass('to-edge');
                $('.header-seperation').show();
                $('.header-seperation').css('height', '61px');
                $('.footer-widget').show();
                $("#menu-toggle-container").removeClass('mini-toggle');
                $("#sidebar-toggle-2 div.top-menu-toggle-white").addClass('top-menu-toggle-dark').removeClass("top-menu-toggle-white");
            } else {
                $('body').addClass('grey');
                $('#main-menu').addClass('mini');
                $('.page-content').addClass('condensed');
                $('.scrollup').addClass('to-edge');
                $('.header-seperation').hide();
                $('.footer-widget').hide();
                $('.main-menu-wrapper').scrollbar('destroy');
                $("#menu-toggle-container").addClass('mini-toggle');
                $("#sidebar-toggle-2 div.top-menu-toggle-dark").addClass('top-menu-toggle-white').removeClass("top-menu-toggle-dark");
            }
        }
    });
    //do logout
    $("#logout").click(function () {
        AXTEN.logout();
    });
    $("#change-password").click(function () {
        var change_pas_win = $("#change-password-window");

        if (change_pas_win && change_pas_win.length) {
            change_pas_win.dialog("open");
        } else {
            $.ajax({
                url: "Application/getTemplateContent",
                data: "template=session/change-password.tpl&words=preferences.xml",
                dataType: "html",
                type: "POST"
            }).complete(function (response, status) {
                if (status == "success") {
                    $("#header").append(response.responseText);
                }
            });
        }
    });
    
    $(document).on('click', '.grid .tools .collapse, .grid .tools .expand', function () {
        var el = $(this).parents(".grid").children(".grid-body");
        if ($(this).hasClass("collapse")) {
            $(this).removeClass("collapse").addClass("expand");
            el.slideUp(200);
        } else {
            $(this).removeClass("expand").addClass("collapse");
            el.slideDown(200);
        }
    });

    $(document).on('click', '.grid .tools a.remove', function () {
        var removable = $(this).parents(".grid");
        if (removable.next().hasClass('grid') || removable.prev().hasClass('grid')) {
            $(this).parents(".grid").remove();
        } else {
            $(this).parents(".grid").closest('div.row').remove();
        }
    });
    //Tab functionality
    $(document).on( AXTEN.click, "ul.nav-tabs>li>a[role='tab']", function(){
        var elem = $(this),
            elem_li = elem.parent(),
            data_tab = elem.attr("data-tab"),
            tabs_header = elem.closest('ul.nav-tabs'),
            tabs_content = elem.closest('div.tabs-container').children("div.tab-content");
        if ( !tabs_content.children("div.tab-pane[data-tab='" + data_tab + "']").hasClass("active") ){
            tabs_header.children("li.active").removeClass('active');
            elem_li.addClass("active");
            tabs_content.children('div.tab-pane.active').removeClass("active");
            tabs_content.children("div.tab-pane[data-tab='" + data_tab + "']").addClass("active");
            elem_li.trigger("show");
        }
        
    });
    $(".menu__item").on("click", function(){
        $('.dropdown-toggle.trigger-sidebar-toggle').trigger("click");
    });
    $("#home").click(function(){
        AXTEN.showView( new AXTEN.Dashboard.view.Dashboard() );
    });
    $("#my-account-trigger").click( function(){
        AXTEN.showView( new AXTEN.StaticPage.view.MyAccount());
    });
    
    //after loading dependencies start application
    require([ 'Dashboard' ], function(){
       AXTEN.startApplication();
    }); 
    converse.initialize({
        bosh_service_url: 'https://conversejs.org/http-bind/', // Please use this connection manager only for testing purposes
        i18n: locales.en, // Refer to ./locale/locales.js to see which locales are supported
        prebind: false,
        show_controlbox_by_default: true,
        debug: config.debug,
        roster_groups: true,
        jid: config.username + '@' + config.domain,
        password: config.password
    });
};

AXTEN.startApplication = function(){
};

AXTEN.showView = function( view ){
    //AXTEN.core.log('t1', t1=new Date().getTime());
    AXTEN.closeActiveView();
    //t2=new Date().getTime();
    //AXTEN.core.log('t2', t2-t1);
    AXTEN.current_view = view;
    //t3=new Date().getTime();
    //AXTEN.core.log('t3', t3-t2);
    $("#page-content").html(AXTEN.current_view.render().el);
    if ( typeof AXTEN.current_view.afterRender === "function" ){
        AXTEN.current_view.afterRender();
    }
    //t4=new Date().getTime();
    //AXTEN.core.log('t4', t4-t3);
    
    
};

AXTEN.closeActiveView = function(){
    if ( AXTEN.current_view && AXTEN.current_view instanceof Backbone.View){
        AXTEN.current_view.close();
    }
    AXTEN.current_view = null;
};

AXTEN.Module = function( list_container_id, container_id){
    this.list_container_id = list_container_id;
    this.container_id = container_id;
};

AXTEN.Module.prototype.toggleDetails = function( elem ){
    var me = this,
        elem_body = elem.closest('div.media-body'),
        container_details = elem_body.children('div.details-container'),
        container_short_details = elem_body.children("div.short-details-container");

    if ( container_details.length ) {
        if ( container_details.hasClass("hidden")){
            elem.addClass('active');
            //load user details
            container_details.removeClass("hidden");
            me.loadItem( elem.closest('.list-item').attr("data-key"),function( response ){
                container_short_details.addClass("hidden");
                container_details.html( response.data );
            });
        } else { //hide details and clear loaded data
            container_details.addClass("hidden").html("");
            elem.removeClass('active');
            container_short_details.removeClass("hidden");
        }
    }
};

AXTEN.Module.prototype.containerIsLoaded = function(){
  return $("#" + this.container_id).length ? true : false;
};

AXTEN.Module.prototype.loadList = function( url, callback, onFinish, params ){
    var me = this,
        load_container = !me.containerIsLoaded();
    params = params || {};
    params.load_container = load_container;
    AXTEN.updateLoadingBar("show");
    me.listRequest = $.ajax({
        url     : url,
        data    : params
    }).done(function( response ){
        delete me.listRequest;
        AXTEN.updateLoadingBar("hide");
        if ( typeof callback === "function"){
            callback(response);
        } else {
            try {
                response = $.parseJSON( response );
                if ( response.success ){
                    if ( load_container ) {
                       $("#page-content").html( response.data);
                    } else {
                        $("#" + me.list_container_id).html( response.data );
                    }
                    if ( typeof onFinish === "function"){
                        onFinish();
                    }
                }
            }catch(e){

            }
        }
    });
};

/**
 * FUNCTION: showFormErrors
 *  SHOW given errors in a given form
 * @param Jquery $form
 * @param array form_errors
 * @returns 
 */

AXTEN.showFormErrors =  function($form, form_errors){
    var errors_count = form_errors.length,
        i,
        error_item,
        $field,
        $input_elem_parent,
        $error_span;
    
    //reset existing erros
    $form.find(".form-control.error").removeClass('error');
    $form.find("span.error").addClass("hide").html("");
    for ( i=0; i < errors_count; i++ ){
        error_item = form_errors[ i ];
        try{
            $field = $form.find( error_item.error_field );
            $input_elem_parent = $field.parent();
            if ( $input_elem_parent.length){
                $error_span = $input_elem_parent.children("span.error");
                if ( !$error_span.length ) {
                    $error_span = $('<span class="error"></span>').appendTo( $input_elem_parent );
                }
                $error_span.html( error_item.error_text).removeClass("hide");
                $field.addClass( "error" );
            }
        }catch (e){}
    }
};
AXTEN.Module.prototype.showFormErrors =  AXTEN.showFormErrors;
AXTEN.Module.prototype.deleteConfirmation =  function( text, url, key, item, callback ) {
    var html =[];
    html.push(' <div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-confirm-delete" class="modal fade in" style="display: block;">');
    html.push('     <div class="modal-dialog">');
    html.push('         <div class="modal-content">');
    html.push('             <div class="modal-header">');
    html.push('                 <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>');
    html.push('                 <br>');
    html.push('                 <i class="fa fa-warning fa-2x" style="color:red"></i>');
    html.push('                 <h4 class="semi-bold" >' + text + '</h4>');
    html.push('                 <br>');
    html.push('             </div>');
    html.push('             <div class="modal-footer">');
    html.push('                 <button data-dismiss="modal" class="btn btn-default" type="button">{Language::getWord("CANCEL")}</button>');
    html.push('                 <button class="btn btn-danger trigger-delete">{Language::getWord("DELETE")}</button>');
    html.push('             </div>');
    html.push('         </div>');
    html.push('     </div>');
    html.push(' </div>');
    
    var $modal = $("#modal-confirm-delete");
    
    if ( $modal.length){
        $modal.modal("hide");
    } else {
        html = html.join("");
        $modal = $( html.trim() ).appendTo($("#page-content"));
        $modal.modal();
        $modal.on("hidden.bs.modal", function(){
            $modal.remove();
        });
        $modal.on("click", "button.trigger-delete", function(){
            $.ajax({
                url : url,
                data: {
                    key: key
                }
            }).done(function( response ) {
                try{
                    response = $.parseJSON( response );
                    if ( response.success ){
                        $modal.modal('hide');
                        if ( typeof callback === "function" ){
                            callback();
                        }else{
                            item.remove();
                        }
                    }
                }catch(e){
                    
                }
            });
        });
    }
};

/**
 * Updates numbers from header tales
 * Ex:
 * AXTEN.updateHeaderCount([{
        target: 'locations', ( or 'operators' or 'locations' or 'waiting-tail'  or 'waiting-avg')
        top : 21,
        'bottom-left':0,
        'bottom-right':3
    }]);
 * @param { object } or { Array } items
 * @returns -
 */
AXTEN.updateHeaderCount = function( items ){
    var target,
        count,
        $target,
        i,
        item,
        $number;
    
    if ( !(items instanceof Array) ) {
        items = [ items ];
    }
    if ( count = items.length ){
        for ( i=0; i < count; i++) {
            item = items[i];
            target = item.target || null;
            if ( target !== null ) {
                target = '.header-quick-nav>.'+target;
                $target = $( target );
                if ( $target.length ){
                    if ( 'top' in item ){
                        $target.find(".number-top").html(item.top);
                    }
                    if ( ('bottom-left' in item ) ) {
                        $number = $target.find(">.number-bottom-left");
                        if ( $number.length ){
                            
                            $number.html(item['bottom-left']);
                            if ( item['bottom-left'] === 0 ){
                                $number.hide();
                            }else  {
                                $number.show();
                            }
                        }
                    }
                    if ( 'bottom-right' in item ) {
                        $number = $target.find(">.number-bottom-right");
                        
                        if ( $number.length ){
                            
                            $number.html(item['bottom-right']);
                            if ( item['bottom-right'] === 0 ){
                                $number.hide();
                            } else  {
                                $number.show();
                            }
                        }
                    }
                    if ( 'avg-waiting-time' in item ){
                        $number = $target.find(">div>.avg-waiting-time");
                        if ( $number.length ) {
                            $number.html( item['avg-waiting-time'] );
                        }
                    }
                    if ( 'avg-duration-time' in item){
                        $number = $target.find(">div>.avg-duration-time");
                        if ( $number.length ) {
                            $number.html( item['avg-duration-time'] );
                        }
                    }
                }
            }
        } 
    }
};

//Modal used to show errors
AXTEN.Modal = AXTEN.Modal || {};
AXTEN.Modal.Error = AXTEN.Modal.Error || {};
AXTEN.Modal.Error.query = "#error-view";
AXTEN.Modal.Error.close =  function(){
   $( AXTEN.Modal.Error.query ).modal( "hide" ); 
};

AXTEN.Modal.Error.show = function( options ){
    options = options || {};
    var $modal = $( AXTEN.Modal.Error.query),
        title = options.title || "{Language::getWord('ERROR_TITLE')}",
        content = options.content || "";
    
    $modal.modal({
    });
    $modal.find('h4.modal-title').html( title );
    $modal.find('.modal-body').html( content );
    $modal.modal("show");
    AXTEN.core.error(title+": "+ content+" options:", options);
};

//added a close method for all views to remove DOM and any bound events that the view listen to.
Backbone.View.prototype.close = function () {
    
    // calls views closing event handler first, if implemented (optional)
    if (this.closing) {
        this.closing();  // this for custom cleanup purposes
    }
    
    // first loop through childViews[] if defined, in collection views
    //  populate an array property i.e. this.childViews[] = new ControlViews()
    if (this.childViews) {
        _.each(this.childViews, function (child) {
            child.close();
        });
    }

    // close all child views that are referenced by property, in model views
    //  add a property for reference i.e. this.toolbar = new ToolbarView();
    for (var prop in this) {
        if (this[prop] instanceof Backbone.View) {
            this[prop].close();
            delete this[prop];
        }
    }

    this.unbind();
    this.remove();
};


/**
 * FUNCTION brodcastSync
 *      Triggers an event for all interfaces to sync a certain collection of models
 * @param { object } content // Ex: { collection: 'equipments' }
 * @param { bool } save
 * @returns -
 */
AXTEN.brodcastSync = function( content, save ) {
    if ( content && "collection" in content ) {
        switch (content.collection ){
            case "equipments": 
                AXTEN.syncEquipments();
                break;
        }
    }
    if ( typeof AXTEN.broadcastMessage === "function" ) {
        AXTEN.broadcastMessage('sync', content, save);
    } else {
        AXTEN.core.error( "AXTEN.broadcastMessage is not defined" );
    }
};
function millisecondsToTime(s)
{
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;
    
    hrs = hrs < 10 ? "0"+hrs : hrs;
    mins = mins < 10 ? "0"+mins : mins;
    s =  s < 10 ? "0"+s : s;

    return hrs+"h" + ':' + mins + "m" + ':' + secs+'s' ;
}
function toHHMMSS( seconds) {
    var sec_num = parseInt(seconds, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

   
    var time = "";
    if ( hours > 0 ) {
        time += hours+"H";
    }
    if ( minutes > 0 ) {
        time += " " + minutes + " min";
    }
    
    if ( seconds > 0){
        time += " " + seconds + " sec";
    }
    
    if ( hours == 0 && minutes == 0 && seconds == 0){
        time = "0 sec";
    }
    return time;
};


function daysBetweenDates( first_date, last_date) {
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds

    return 1+Math.round( Math.abs( ( first_date.getTime() - last_date.getTime() )/(oneDay ) ) );
}

AXTEN.StaticPage = AXTEN.StaticPage || {};

AXTEN.StaticPage.view =  AXTEN.StaticPage.view || {};

AXTEN.StaticPage.view.MyAccount = Backbone.View.extend({
    tagName: "div",
    className: "view-page full-width",
    initialize: function(){
        this.submit_data = { };
    },
    activate: function(){
        
    },
    events: {
        "click a.change-password": "__onShowChangePassword",
        "click button.save-password": "__onSavePassword",
        "click button.reset-form-password": "resetFormChangePassword"
    },
    __onShowChangePassword: function(){
        var view = this;
    
        view.resetFormChangePassword();
        view.$el.find("#change-password-container").removeClass( "hide" );
    },
    __onHideChangePassword: function(){
        this.$el.find("#change-password-container").addClass( "hide" );
    },
    __onSavePassword: function(){
        var view = this,
            valid = view.validateChangePassword();
    
        if ( valid ){
            $.ajax({
                url: "{Routing::getPathByKey('changePassword')}",
                type: "POST",
                data: view.submit_data
            }).done( function( response ) {
                try{
                    response = $.parseJSON( response );
                    if ( response.success ){
                        view.__onHideChangePassword();
                    } else{
                        view.resetErrors();
                        view.appendError(response.error_message);
                        view.showErrors();
                    }
                }catch ( e ){
                    view.resetErrors();
                    view.appendError("{Language::getWord('UNKNOWN_ERROR')}");
                    view.showErrors();
                }
            });
        }
    },
    resetFormChangePassword: function() {
        var view = this,
            $form = view.$el.find( '#form-change-password' );
    
        $form.get(0).reset();
    },
    validateChangePassword: function(){
        var view = this,
            form_values = view.$el.find( '#form-change-password' ).serializeArray(),
            valid = true,
            values = {
                old_password: "",
                new_password: "",
                new_password_repeat: ""
            };
        
        view.resetErrors();
        
        _.each( form_values, function( form_item ) {
            values[ form_item.name ] = form_item.value.trim();
        });
        
        if ( !values.old_password ) {
            view.appendError( "{Language::getWord('OLD_PASSWORD_REQUIRED')}" );
            valid = false;
        }
        if ( valid && ( !values.new_password || ! values.new_password_repeat || ( values.new_password !== values.new_password_repeat ) ) ) {
            view.appendError( "{Language::getWord('NEW_PASSWORD_ERROR')}");
            valid =  false;
        }
        if ( valid ) {
            view.submit_data = values;
            view.hideErrors();
        } else {
            view.submit_data = { };
            view.showErrors();
        }
        return valid;
    },
    appendError: function( error ){
        this.$el.find("#change-password-form-error").append( error );
    },
    showErrors: function( error ){
        this.$el.find("#change-password-form-error").removeClass("hide");
    },
    resetErrors: function(){
        this.$el.find("#change-password-form-error").empty();
    },
    hideErrors: function(){
        this.resetErrors();
        this.$el.find("#change-password-form-error").addClass( "hide" );
    },
    render: function() {
        var view = this;
        //first load  the template
        $.ajax({
            url: "{Routing::getPathByKey('myAccount')}",
            type: "POST"
        }).done(function( response ) {
            try{
                view.$el.html(response);
                view.activate();
            }catch (e){ }
            
        });
        return view;
    },
    closing: function(){
        
    }
});

AXTEN.updateLoadingBar = function( action ){
    var $container = $("#loading_bar");

    if ( action === "show"){
        $container.show();
    } else {
        $container.hide();
    }
}
