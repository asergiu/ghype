/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
{if Object::checkAccess( 'ACL_ACCESS_USERS', Object::ACL_READ )}
AXTEN.User = function(){
    //call the parent construnctor
    AXTEN.Module.call( this, "users-list-container", "users-container" );
    this.init();
};
//inherit Module
AXTEN.User.prototype = new AXTEN.Module();

//correct the constructor pointer because it pionts to Module
AXTEN.User.prototype.constructor = AXTEN.Group;

AXTEN.User.initialized = false; //static variable
AXTEN.User.page = 0;

AXTEN.User.prototype.init = function(){
    var me = this;
    me.page = 0;
    me.scroll_event = false;
    if ( !AXTEN.User.initialized ) {
        AXTEN.User.initialized = true;
        
        //add events
        /*$(document).on( AXTEN.click, "#"+me.list_container_id+" .trigger-details", function(){
            
        });*/

        
    }
};

AXTEN.User.prototype.toggleDetails = function( elem ){
    var me = this,
        elem_body = elem.closest('div.media-body'),
        container_details = elem_body.children('div.details-container'),
        container_short_details = elem_body.children("div.short-details-container");

    if ( container_details.length ) {
        if ( container_details.hasClass("hidden")){
            elem.addClass('active');
            //load user details
            container_details.removeClass("hidden");
            me.loadUser( elem.closest('.list-item').attr("data-key"),function( response ){
                container_short_details.addClass("hidden");
                container_details.html( response.data );
            });
        } else { //hide details and clear loaded data
            container_details.addClass("hidden").html("");
            elem.removeClass('active');
            container_short_details.removeClass("hidden");
        }
    }
};

AXTEN.User.prototype.loadList = function(){
    var self = this;
    self.page++;
    AXTEN.Module.prototype.loadList.call(this, "{Routing::getPathByKey('usersList')}",null,function(){
        var $view_page = $(".view-page");

        self.loadMoreItems();
        if ( self.scroll_event === false ){
            self.scroll_event = true;
            $view_page.off("scroll");
            $view_page.on("scroll",function(){
                 if( $view_page.scrollTop() +  $view_page.innerHeight() >= $view_page[0].scrollHeight ){
                    self.loadMoreItems();
                }
            });
        }
    },{ page:self.page });
};

AXTEN.User.prototype.loadMoreItems = function(){
    var self = this;
    var is_loading = false;
    var $view_page = $(".view-page");

    if(is_loading == false){
        self.page++;
        is_loading = true;
        AXTEN.updateLoadingBar("show");
        $.ajax({
            url: "{Routing::getPathByKey('usersList')}",
            type: 'POST',
            data: {
                load_container : false,
                page : self.page
            }
        }).done(function(response){
                is_loading = false;
                AXTEN.updateLoadingBar("hide");
                try{
                    response = $.parseJSON( response );
                    $('#users-list-container').append(response.data);
                    
                    if( $view_page[0].scrollHeight === $view_page.innerHeight()){
                        self.loadMoreItems();
                    }

                }catch(e){}
        });
    }
}


AXTEN.User.prototype.loadUser = function( key, callback ){
    var me = this;
    
    $.ajax({
        url     : "{Routing::getPathByKey('viewUser')}",
        data    : {
            key : key
        },
        type    : 'POST'
    }).done(function( response ){
        try{
            response = $.parseJSON( response );
            if ( response.success && ( typeof callback === "function" ) ){ 
                callback( response );
            }
        }catch(e){
        }
    });
};

AXTEN.User.prototype.viewFormModal = function( data ){
    var me = this;
    
    data =  data ? data : {};
    me['modal_user_add'] = $.ajax({
        url : "{Routing::getPathByKey('viewAddUser')}",
        type: "POST",
        data: data
    }).done(function( response ){
        try{
            response = $.parseJSON(response);
            if ( response.success ){
            var modal_elem = $(response.data).appendTo($("#page-content"));
                modal_elem.modal();
            }
        }catch (e){
          
        }
  });
  
};

AXTEN.User.prototype.viewEditModal = function( trigger_elem ){
    var me= this,
        item_key = trigger_elem.closest('li.list-item').attr("data-key");

    if ( item_key ) {
        me.viewFormModal({
            key: item_key
        });
    }
};

AXTEN.User.prototype.removeModal = function (trigger_elem ){
    if( $("#modal-form-user").length && $("#modal-form-user").attr("aria-hidden") === "true" ){
        $("#modal-form-user").modal('hide');
        $(document.body).children('.modal-backdrop').remove();
        $("#modal-form-user").remove(); 
    }
}

AXTEN.User.prototype.save = function(){
    var me = this,
        form = $("#form-user"),
        form_values = form.serializeArray(),
        data = {},
        item;

    for (key in form_values){
        item = form_values[key];
        data[item.name] = item.value;
    }
    $.ajax({
        url : "{ROUTING::getPathByKey('saveUserForm')}",
        data: data,
        type: 'POST'
    }).done(function( response ){
        try{
            response = $.parseJSON( response );
            if ( response.success ){
                $("#modal-form-user").modal('hide');
                $(document.body).children('.modal-backdrop').remove();
                $("#modal-form-user").remove();
                me.loadList();
            }else {
                if ( response.hasOwnProperty( "form_errors" ) && ( response.form_errors.length ) ){
                    me.showFormErrors( form, response.form_errors );
                }
            }
        }catch (e){}
    });
};

AXTEN.User.prototype.deleteConfirmation = function( $trigger ){
   var item = $trigger.closest('li.list-item'),
        item_key = item.attr("data-key");
   
   if ( item_key ) {
        AXTEN.Module.prototype.deleteConfirmation.call( this, "{Language::getWord('USER_CONFIRM_USER_DELETE')}", "{Routing::getPathByKey('deleteUser')}", item_key, item); 
   }
}
{/if}