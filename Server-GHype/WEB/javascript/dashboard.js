/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
AXTEN.Dashboard = AXTEN.Dashboard || {};
AXTEN.Dashboard.model = AXTEN.Dashboard.model || {};
AXTEN.Dashboard.collection = AXTEN.Dashboard.collection || {};
AXTEN.Dashboard.view = AXTEN.Dashboard.view || {};



AXTEN.Dashboard.init = function(){
    
};

AXTEN.Dashboard.view.Dashboard = Backbone.View.extend({
    tagName: "div",
    className: "dashboard-content full-width",
    initialize: function(){
        var view = this;

        this.childViews = [];
        //this.render();        
    },
    add : function( view ){
            this.childViews.push( view );
    },    
    render: function() {
        var template = [],
            view = this;
        
        template.push( '    <div id="dashboard-map" style="height:60%; border: #CCC solid 1px">');
        template.push( '    </div>');
        template.push( '    <div class="google-legend"></div>');
        template.push( '    <div id="center-content" style="height:40%;overflow:auto">');
        template.push( '        <div class="row" style="margin:0">');
        template.push( '            <div class="col-md-6">');
        template.push( '                <ul id="dashboard-logs" class="cbp_tmtimeline">');
        template.push( '                </ul>');
        template.push( '            </div>');
        template.push( '            <div class="col-md-6">');
        template.push( '                ');
        template.push( '            </div>');
        template.push( '        </div>' );
        template.push( '    </div>');
        this.$el.html( template.join( "" ) );
        
        AXTEN.equipmentsCollection.on("add", view.onAddEquipment, view);
        AXTEN.logsCollection.on( "reset", view.onResetLogs, view);
        this.add( new AXTEN.Dashboard.view.Logs({ 
            el: view.$el.find('#dashboard-logs'),
            collection: AXTEN.logsCollection
        }) );
        return this;
    },
    afterRender: function(){
        var view  = this,
            mapOptions = {
                zoom: parseFloat("{Object::getConfigValue('map.default.zoom')}"),
                center: new google.maps.LatLng( parseFloat("{Object::getConfigValue('map.default.lat')}"), parseFloat("{Object::getConfigValue('map.default.lng')}"))
            };
        //draw google map
        view.map = new AXTEN.Map({
            map_options  : mapOptions,
            container_id : 'dashboard-map',
            container: this.$el.find('#dashboard-map').get(0),
            contextmenu: true,
            menuItems: [
                { id:"ctx_details", className:'', eventName:'location_details', label:'{Language::getWord("LOCATION_DETAILS")}' },
                { id:"ctx_play", className:'', eventName:'location_play', label:'{Language::getWord("LOCATION_PLAY_RECORDS")}' }
            ]
        });
        view.map.AddLegend( this.$el.find( '.google-legend' ).get(0) );
        //draw map bounds
        view.bounds = new google.maps.LatLngBounds();

        if ( AXTEN.equipmentsCollection.length > 0 ){
            view.addEquipmentsToMap();
        }
    },
    addEquipmentsToMap: function(){
        var length = AXTEN.equipmentsCollection.length,
            model;
        
        for ( var i=0; i< length; i++){
            model = this.onAddEquipment(AXTEN.equipmentsCollection.at(i));
        }
    },
    onAddEquipment: function( model ){
        var view = this,
            lat_lng =  new google.maps.LatLng( model.get('lat'), model.get('lng') );
    
        model.addOnMap( view.map );
        model.addMapInfoWindow( view.map );
        view.bounds.extend( lat_lng );
        view.map.map.fitBounds( view.bounds );
    },
    onResetLogs: function(){
        $("#dashboard-logs").empty();
    },
    closing: function(){
        var view = this;
        //remove collection's event from this view
        AXTEN.equipmentsCollection.off("add", view.onAddEquipment, view);
        AXTEN.logsCollection.off( "reset", view.onResetLogs, view);
        delete view.bounds;
        delete view.map;
    }
});

AXTEN.Dashboard.model.Log = Backbone.Model.extend({
    defaults: {
        time: '',
        date: '',
        hour: '',
        text: '',
        category: ""
    }
});

AXTEN.Dashboard.collection.Logs = Backbone.Collection.extend({
    model: AXTEN.Dashboard.model.Log,
    initialize: function(){
        this.on( "add", this.__onAdd);
    },
    __onAdd: function( model ){
        var date_string = model.get("time"),
            tmp_date = date_string.split(" ");
        date_string = tmp_date[0]+"T"+tmp_date[1];
        var date = new Date( date_string ),
            hours = date.getHours(),
            minutes = date.getMinutes(),
            seconds = date.getSeconds();
        
        hours = hours < 10 ? "0"+hours : hours;
        minutes = minutes < 10 ? "0"+minutes : minutes;
        seconds = seconds < 10 ? "0"+seconds : seconds;
        model.set({
            date: $.datepicker.formatDate('dd.mm.yy', date),
            hour: hours+":"+minutes+":"+seconds
        });
    }
});

AXTEN.Dashboard.view.Log = Backbone.View.extend({
    tagName: "li",
    template: _.template(
            '<time datetime="<%= time %>" class="cbp_tmtime">'+
                '<span class="date"><%= date %></span>'+
                '<span class="time"><%= hour %></span>'+
            '</time>'+
            '<div class="cbp_tmicon success animated bounceIn"> <i class="fa fa-long-arrow-right"></i> </div>'+
            '<div class="cbp_tmlabel">'+
                '<div class="p-t-10 p-l-30 p-r-20 p-b-20 xs-p-r-10 xs-p-l-10 xs-p-t-5">'+
                    '<%= text %>'+
                '</div>'+
                '<div class="clearfix"></div>'+
            '</div>'),
    initialize: function(){
        this.model.on('destroy', this.removeView, this); // Adding a destroy announcer..
    },
    render: function(){
        this.$el.html( this.template(this.model.toJSON()) );
        return this;
    },
    removeView: function(){
        this.close();
    }
});

AXTEN.Dashboard.view.Logs = Backbone.View.extend({
    initialize: function(){
        this.render();
        this.collection.on('add', this.__addLog, this);
    },
    render: function(){
        this.collection.each(function( log ){
            this.__addLog( log );
        }, this);
        return this; //return this for chaining
    },
    closing: function(){
        this.collection.off('add', this.__addLog, this);
    },
    __addLog: function( log ){
        var logView = new AXTEN.Dashboard.view.Log({ model: log }),
            $container = this.$el.parent(),
            max_length = parseInt( "{Object::getConfigValue('dashboard.logs.limit')}" );
        this.$el.prepend( logView.render().el );//calling render method manually
        if ( this.collection.length > max_length ) {
            var model = this.collection.shift();
            model.destroy();
        }
    }
});
AXTEN.logsCollection = new AXTEN.Dashboard.collection.Logs([]);
