/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
AXTEN.ClearCache = AXTEN.ClearCache || {};
AXTEN.ClearCache.view = AXTEN.ClearCache.view || {};

AXTEN.ClearCache.view.primaryView = Backbone.View.extend({
    tagName: "div",
    className: "view-page full-width",
    initialize: function(){
        var template = [];
        
        template.push('<div class="row " >' );
        template.push(  '<div class="col-md-12">' );
        template.push(      '<div class="grid simple" style="border: none">' );
        template.push(          '<div class="grid-title" style="border: none; border-bottom:1px solid #dddddd; padding-left: 11px">' );
        template.push(              '<h4> <span style="display: inline-block;width:17px"></span> {Language::getWord("CLEAR_CACHE")}</h4>' );
        template.push(          '</div>' );
        template.push(          '<div  style="border: none; padding: 5px;">' );
        template.push(              '<ul class="list-unstyled custom-list" id="cache-list"></ul>' );
        template.push(          '</div>' );
        template.push(      '</div>' );
        template.push(  '</div>' );
        template.push('</div>' );
        this.template =  _.template( template.join( "" ) );
        
        this.childViews = [];
    },
    add : function( view ){
            this.childViews.push( view );
    },
    events: {
        
    },
    render: function(){
        var view = this;
        
        view.$el.html( view.template() );
        view.loadSections();
        return view;
    },
    loadSections: function(){
        var view = this,
            container = view.$el.find("#cache-list"),
            list = $.parseJSON( '{json_encode(Object::getConfigValue("memcache_section"))}');
            
        _.each( list, function( item ){
            var child_view = new AXTEN.ClearCache.view.SectionView({ model: item });
            
            view.add( child_view );
            container.append( child_view.el );
        });
    },
    closing: function(){
        
    }
});

AXTEN.ClearCache.view.SectionView = Backbone.View.extend({
    tagName: "li",
    initialize: function() {
        var view = this,
            template = [];
    
        template.push( '<div class="row">' );
        template.push( '    <div class="col-sm-3">');
        template.push( '        <%= name %>');
        template.push( '    </div>');
        template.push( '    <div class="col-sm-9">' );
        template.push( '        <button class="btn btn-primary trigger-clear">{Language::getWord("CLEAR")}</button>');
        template.push( '        <div class="spinner hide"> <div class="bounce1"></div>  <div class="bounce2"></div>  <div class="bounce3"></div></div>');
        template.push( '    </div>');
        template.push( '</div>' );
        view.template = _.template( template.join( "" ) );
        view.render();
    },
    events: {
        "click button.trigger-clear": "__onClearSection"
    },
    render: function(){
        var view= this;
        
        view.$el.html( view.template( view.model ) );
        
        return this;
    },
    __onClearSection: function(){
        var view = this;
        
        view.showLoading();
        $.ajax({
            url: "{Routing::getPathByKey('clearCache')}",
            type: "POST",
            data: {
                clear: view.model.key
            }
        }).done( function( response ){
            view.hideLoading();
        });
    },
    showLoading: function(){
        this.$el.find( "button.trigger-clear").addClass( "hide" );
        this.$el.find( ".spinner" ).removeClass( "hide" );
    },
    hideLoading: function(){
        this.$el.find( ".spinner" ).addClass( "hide" );
        this.$el.find( "button.trigger-clear").removeClass( "hide" );
    },
    closing: function(){
        delete this.model;
    }
});
AXTEN.ClearCache.showPage = function() {
    AXTEN.showView( new AXTEN.ClearCache.view.primaryView() );
};