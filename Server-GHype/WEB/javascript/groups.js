/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
AXTEN.Group = function(){
    //call the parent construnctor
    AXTEN.Module.call( this, "groups-list-container", "groups-container" );
    this.init();
};

//inherit Module
AXTEN.Group.prototype = new AXTEN.Module();

//correct the constructor pointer because it pionts to Module
AXTEN.Group.prototype.constructor = AXTEN.Group;

AXTEN.Group.initialized = false; //static variable
AXTEN.Group.prototype.init = function(){
    var me = this;
    
    if ( !AXTEN.Group.initialized ) {
        AXTEN.Group.initialized = true;
        //add events
        /*$(document).on( AXTEN.click, "#"+me.list_container_id+" .trigger-details", function(){
            
        });*/
    }
};

AXTEN.Group.prototype.toggleDetails = function( elem ){
    var me = this,
        elem_body = elem.closest('div.media-body'),
        container_details = elem_body.children('div.details-container'),
        container_short_details = elem_body.children("div.short-details-container");

    if ( container_details.length ) {
        if ( container_details.hasClass("hidden")){
            elem.addClass('active');
            //load item details
            container_details.removeClass("hidden");
            me.loadItem( elem.closest('.list-item').attr("data-key"),function( response ){
                container_short_details.addClass("hidden");
                container_details.html( response.data );
            });
        } else { //hide details and clear loaded data
            container_details.addClass("hidden").html("");
            elem.removeClass('active');
            container_short_details.removeClass("hidden");
        }
    }
};

AXTEN.Group.prototype.loadContainer = function( options ){
    
};
AXTEN.Group.prototype.loadList = function(){
    AXTEN.Module.prototype.loadList.call(this, "{Routing::getPathByKey('groupsList')}");
};

AXTEN.Group.prototype.loadItem = function( key, callback ){
    var me = this;
    
    $.ajax({
        url     : "{Routing::getPathByKey('viewGroup')}",
        data    : {
            key : key
        },
        type    : 'POST'
    }).done(function( response ){
        try{
            response = $.parseJSON( response );
            if ( response.success && ( typeof callback === "function" ) ){ 
                callback( response );
            }
        }catch(e){
        }
    });
};

AXTEN.Group.prototype.viewFormModal = function( data ){
    var me = this;
    
    data =  data ? data : {};
    me['modal_group_add'] = $.ajax({
        url : "{Routing::getPathByKey('viewFormGroup')}",
        type: "POST",
        data: data
    }).done(function( response ){
        try{
            response = $.parseJSON(response);
            if ( response.success ){
            var modal_elem = $(response.data).appendTo($("#page-content"));                
                modal_elem.modal();
                modal_elem.on("hidden.bs.modal", function(){
                    modal_elem.remove();
                });
            }
        }catch (e){
          
        }
  });
  
};

AXTEN.Group.prototype.viewEditModal = function( trigger_elem ){
    var me= this,
        item_key = trigger_elem.closest('li.list-item').attr("data-key");

    if ( item_key ) {
        me.viewFormModal({
            key: item_key
        });
    }
};
AXTEN.Group.prototype.save = function(){
    var me = this,
        form = $("#form-group"),
        form_values = form.serializeArray(),
        data = {},
        item;

    for (key in form_values){
        item = form_values[key];
        data[item.name] = item.value;
    }
    $.ajax({
        url : "{ROUTING::getPathByKey('saveGroupForm')}",
        data: data,
        type: 'POST'
    }).done(function( response ){
        try{
            response = $.parseJSON( response );
            if ( response.success ){
                $("#modal-form-group").modal('hide');
                me.loadList();
            } else {
                if ( response.hasOwnProperty( "form_errors" ) && ( response.form_errors.length ) ){
                    me.showFormErrors( form, response.form_errors );
                }
            }
        }catch (e){}
    });
};

AXTEN.Group.prototype.deleteConfirmation = function( $trigger ){
   var item = $trigger.closest('li.list-item'),
        item_key = item.attr("data-key");
   
   if ( item_key ) {
        AXTEN.Module.prototype.deleteConfirmation.call( this, "{Language::getWord('USER_CONFIRM_GROUP_DELETE')}", "{Routing::getPathByKey('deleteGroup')}", item_key, item); 
   }
}