$(document).ready(function() {
     //add an resize event with timeout to trigger less resize events
    $(window).resize(function() {
        if(this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function() {
            $(this).trigger('resizeEnd');
        }, 300);        
    });
    $.ajax({
        url: "getTemplateContent",
        data: "template=session/login.tpl&words=login.xml",
        type: "post"
    }).done(function(data){
        $("#login-window").html(data);        
    });   
    $(window).bind("resizeEnd", function(){        
        $("#login-window").dialog("option","position","center");
    });
      
    $.backstretch(
        [
            "images/login/bg/1.jpg",
            "images/login/bg/2.jpg",
            "images/login/bg/3.jpg",
            "images/login/bg/4.jpg"
        ], 
        {
            fade: 1000,
            duration: 8000
        }
    );
});