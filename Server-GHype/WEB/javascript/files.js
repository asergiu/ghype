AXTEN.TransferedFile = AXTEN.TransferedFile || {};
AXTEN.TransferedFile.view = AXTEN.TransferedFile.view || {};
AXTEN.TransferedFile.collection = AXTEN.TransferedFile.collection || {};
AXTEN.TransferedFile.model = AXTEN.TransferedFile.model || {};

AXTEN.TransferedFile.model.File = Backbone.Model.extend({
	defaults: {
		to: '',
        from: '',
		timestamp: null,
		filename: '',
		filesize: 0,
		status: '',
		message: '',
        uid: 0
	}
});

AXTEN.TransferedFile.collection.FileList = Backbone.Collection.extend({
    pageNumber: 0,
    loading: false,
	model: AXTEN.TransferedFile.model.File,
	url: "{Routing::getPathByKey('transferedFileListJson')}",
    fetchNewItems:function(){
        var self = this
        if ( !self.loading ) {
            self.pageNumber++;
            self.loading = true;
            AXTEN.updateLoadingBar("show");

            self.fetch({
                data: {
                    page: this.pageNumber
                },
                success: function( response ){
                    self.loading = false;
                    
                    if ( response && ('length' in response) && response.length >0 && ('current_view' in AXTEN) && AXTEN.current_view instanceof AXTEN.TransferedFile.view.primaryView && AXTEN.current_view.$el[0].scrollHeight===AXTEN.current_view.$el.innerHeight()){
                        AXTEN.current_view.filesView.collection.fetchNewItems();
                    }else if ( response && ('length' in response) && response.length === 0 && self.pageNumber ===1 && AXTEN.current_view instanceof AXTEN.TransferedFile.view.primaryView){
                        AXTEN.current_view.filesView.$el.html("{Language::getWord('FILE_NO_ITEM')}");
                    }
                    AXTEN.updateLoadingBar("hide");
                },
                error: function(){
                    self.loading = false;
                    AXTEN.updateLoadingBar("hide");
                }

            });
        }
    }
});

AXTEN.TransferedFile.view.File = Backbone.View.extend({
	tagName: "li",
	initialize: function(){
		var template = [];

        template.push('<div class="row">');
        template.push(' <div class="col-md-3"><span aria-hidden="true" class="glyphicon glyphicon-file file__status--<%= status %>"></span> <%= filename %> <a href="{Routing::getPathByKey("downloadFile")}?uid=<%= uid %>" title="Download" target="_blank"><i class="glyphicon glyphicon-download"></i></a></div>');
        template.push(' <div class="col-md-2"><%= from %> <div style="color: #006699"><span class="glyphicon glyphicon-circle-arrow-right"></span></div><%= to %></div>');
        template.push(' <div class="col-md-1"><%= timestamp %></div>');
        template.push(' <div class="col-md-2"><%= filesize %> Bytes</div>');
        template.push(' <div class="col-md-1 file__status--<%= status %>"><%= status %></div>');
        template.push(' <div class="col-md-3"><%= message %></div>')
        template.push('</div>')
        this.template = _.template( template.join(""),null,  AXTEN.templateSettings);
        this.model.on('change', this.render, this);
        this.model.on('destroy', this.close, this); // Adding a destroy announcer..
	},
    render: function(){
        var view = this;
        
        view.$el.html( view.template( view.model.toJSON() ) );
        
        return view;
    },
    closing: function() {
        var view = this;
        
        view.model.off('change', view.render, view);
        view.model.off('destroy', view.close, view);
    }
});

AXTEN.TransferedFile.view.FileList =  Backbone.View.extend({
	initialize: function(){
        var view =  this;
        
        view.collection = new AXTEN.TransferedFile.collection.FileList();
        view.collection.on( "add", view.__onAdd, view );
        view.collection.fetchNewItems();
    },
    render: function(){
        var view = this;
        
        view.collection.each(function( model ) {
            view.__onAdd( model );
        });
    },
    __onAdd: function( model ){
        var view = this,
            fileView = new AXTEN.TransferedFile.view.File({ model: model });
        
        view.$el.append( fileView.render().el );
    },
    closing: function(){
        var view = this;
        
        view.collection.off( "add", view.__onAdd, view );
    }
});

AXTEN.TransferedFile.view.primaryView = Backbone.View.extend({
	tagName: "div",
    className: "view-page full-width",
    initialize: function(){

    },
    activate: function(){
        var view = this;
      
        view.filesView =  new AXTEN.TransferedFile.view.FileList({
            el: view.$el.find("#files")
        });

    },
    events:{
        'scroll': '__onScroll'
    },
    __onScroll: function(){
        var view = this;
       
        if( view.$el.scrollTop() +  view.$el.innerHeight() >= view.$el[0].scrollHeight ){
            view.filesView.collection.fetchNewItems();
        }

    },
    render: function(){
        var view = this;
        //first load  the template
        $.ajax({
            url: "{Routing::getPathByKey('fileList')}",
            type: "POST"
        }).done(function( response ) {
            try {
                response = $.parseJSON( response );
                view.$el.html(response.data);
                view.activate();
            } catch (e){ }
            
        });
        return view;
    },
    closing: function(){
    	
    }
});

AXTEN.TransferedFile.showPage = function() {
    AXTEN.showView( new AXTEN.TransferedFile.view.primaryView() );
};