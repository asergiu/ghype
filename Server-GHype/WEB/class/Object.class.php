<?php
/**
 * Available methods:
 *      - public static function loadConfig( $config )
 *      - public static function getConfigValue($parameter, $default_value = false)
 *      - public function getValueFromArray($array, $parameter, $default_value = false)
 *      - public function getCookieValue($parameter, $default_value = false) 
 *      - public function getSessionValue($parameter, $default_value = false)
 *      - protected function checkAccess($object = null, $right = 4) : boolean
 */
class Object {
    static private $configs = array();
    static private $template;
    static private $language;
    const ACL_READ = 4;
    const ACL_CREATE = 8;
    const ACL_UPDATE = 2;
    const ACL_DELETE = 1;

    public function __construct( $configs= NULL ) {
        if ( $configs ) {
            self::loadConfig( $configs );
        }
    }
  /**
   * FUNCTION: loadConfig
   *    Loads application configuration
   * @param array $config
   */
  public static function loadConfig( $config ){
      self::$configs = $config;
  }
  
  /**
   * FUNCTION: loadTemplate
   *    associate a smarty template instance to current application
   * @param type $template
   */
  public static function loadTemplate(Template $template ) {
      self::$template = $template;
  }
  
  /**
   * FUNCTION: loadLanguage
   * 
   *    Loads application language
   * 
   * @param Language $language
   */
  public static function loadLanguage(Language $language ) {
      self::$language = $language;
  }
  
  /**
   * FUNCTION: getConfig
   *    Return an array with application configuration if defined
   * @return array
   */
  public static function getConfig() {
      return self::$configs;
  }
  
  public static function getTemplate(){
      
      return self::$template;
  }
  
  /**
   * FUNCTION: getLanguage
   *        Return a language instance if loaded
   * @return Language
   */
  public static function getLanguage(){
      return self::$language;
  }


  /**
   * get parameter value from settings
   * @param string $parameter
   * @return mixed
   */
  public static function getConfigValue($parameter, $default_value = false) {
    $_parameters = explode(".", $parameter);
    $_result = self::$configs;
    
    if ( count( $_parameters ) && count( $_result ) ) {
        foreach ($_parameters as $_parameter) {
          $_found = false;
          foreach ($_result as $_key => $_value) {
            if($_key == $_parameter) {
              $_result = $_value;
              $_found = true;
              break;
            } 
          }  
          if(!$_found) {
            $_result = $default_value;
            break;
          }
        }
    }
    
    return $_result;
  }
  
  /**
   * get value from array
   * @param array $array 
   * @param mixed $default_value
   * @return mixed
   */
  public static function getValueFromArray($array, $parameter, $default_value = false) {
    if($array !== false && is_array($array) && array_key_exists($parameter, $array) && isSet($array[$parameter])) $return = $array[$parameter];
    else $return = $default_value;
    return $return;
  }
  
  /**
   * get value from cookie
   * @param array $array  
   * @param mixed $default_value
   * @return mixed
  */
  public function getCookieValue($parameter, $default_value = false) {
    return self::getValueFromArray($_COOKIE, $parameter, $default_value);
  }

  /**
   * get value from session
   * @param array $array   
   * @param mixed $default_value
   * @return mixed
   */
  public static function getSessionValue($parameter, $default_value = false) {
    return self::getValueFromArray($_SESSION, $parameter, $default_value);
  }

  /**
   * set value in session
   * @param array $array   
   * @param mixed $default_value
   * @return mixed
   */
   public function setSessionValue($parameter, $value, $open_session = false, $close_session = false) {
       if($open_session) $_session_id = session_start();
       $_SESSION[$parameter] = $value;
       if($close_session) close_session($_session_id);
   }
  
  /**
   * get value from request
   * @param array $array   
   * @param mixed $default_value
   * @return mixed
   */
  public function getRequestValue($parameter, $default_value = false) {
    return self::getValueFromArray($_REQUEST, str_replace('.', '_', $parameter), $default_value);
  }
  
  /**
   * get value from all configuration and/or apache variables
   * @param array $array
   * @param mixed $default_value
   * @return mixed
   */
  public function getValue($parameter, $default_value = false) {
    if(($return = $this->getRequestValue($parameter)) == false) {
      if(($return = $this->getCookieValue($parameter)) == false) {
        if(($return = self::getSessionValue($parameter)) == false) {
          $return = $this->getConfigValue($parameter, $default_value);
        }
      }  
    }
    return $return;
  }
  
    public function getRangeFromArray(&$source=array(),$range_delimitator=', ', $delimitator='..'){
        $ranges = NULL;   
        if (is_array($source)){
            $source = array_unique($source); //remove duplicates
            sort($source);
            if (count($source)>2){             
                $total_items = count($source);

                for ($i=0; $i<$total_items-1;$i++){
                    $item = $source[$i];
                    if ($ranges==NULL){
                        $range_item = new stdClass();
                        $range_item->start  = $item;
                        $range_item->end    = $item;
                        $ranges = array($range_item);
                    }else{
                        $last_range = $ranges[count($ranges)-1];
                        if (($item-$last_range->end)==1){
                            $last_range->end = $item;
                        }else{
                            $range_item = new stdClass();
                            $range_item->start  = $item;
                            $range_item->end    = $item;
                            $ranges[] = $range_item;
                        }
                    }
                }
                if ($ranges){
                    $source = array();
                    foreach ($ranges as $range_item){
                        if ($range_item->start < $range_item->end){
                            $source[] = "{$range_item->start}{$delimitator}{$range_item->end}";
                        }else{
                            $source[] = $range_item->start;
                        }
                    }
                }                        
            }
            $source = implode('; ', $source);
        }        
    }

    /**
     * Function checkAccess
     * @param <type> $object
     * @param <type> $right
     * @return boolean
     */
    public static function checkAccess($object = null, $right = 4) {
        $_person_id = (int) self::getSessionValue('PERSON_ID');
        $result = false;
        if ($_person_id) {
            $_rights = self::getSessionValue("RIGHTS");
            if (is_array($_rights) && count($_rights) > 0) {
                foreach ($_rights as $_right) {
                    if (($_right['CRUD'] & $right) && $_right['ACO_NAME'] == $object || (($_right['CRUD'] & $right) && $_right['ACO_NAME'] == 'ACL_ACCESS_ALL')) {
                        if ($_right['ACCESS_TYPE'] == "deny") {
                            $result = FALSE;
                            break;
                        }   
                        if ($_right['ACCESS_TYPE'] == "permit") {
                            $result = TRUE;
                            break;
                        }
                    }
                }
            }
        }else
            $result = FALSE;
        return $result;
    }

    public function __destruct() {
      self::$configs = NULL;
    }
    
    /**
     *  Replace / with with DIRECTORY_SEPARATOR  accordoding to environment (windows or linux)
     * @param string $path
     * @return string
     */
    public static function cleanPath( $path ) {
        return str_replace('/', DIRECTORY_SEPARATOR, $path);
    }
}

?>
