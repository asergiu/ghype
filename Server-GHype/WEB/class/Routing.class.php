<?php

/*
 *          Application Class that treats all  paths
 * 
 *  Available methos:
 *      - public static function loadRoutes;
 *      - public static function getRoutes: array;
 *      - public static function getPathData: array;
 *      - public static function getPathByKey: string;
 */

/**
 * Description of Routing
 *
 * @author scorp
 */
class Routing extends Application{
    private static $routes = array();
    private static $current_path = '/';
    //put your code here
    function __construct() {
        $routes = self::$routes;
        
        if ( count( $routes ) === 0 ) {
            self::loadRoutes( self::getConfigValue( 'routes' ) );
        }
    }
    
    /**
     * FUNCTION loadRoutes
     *      Load the application routes
     * @param array $new_routes
     */
    public static function loadRoutes( $new_routes = array()){
        if (is_array($new_routes)){
            self::$routes = $new_routes;
        }
    }
    
    /**
     * FUNCTION getRoutes
     *      List with all application's paths
     * @return array
     */
    public static function getRoutes(){
        if ( count( self::$routes ) === 0 ) {
            self::loadRoutes( self::getConfigValue( 'routes' ) );
        }
        
        return self::$routes;
    }
    
    /**
     * FUNCTION getPathCallbackData
     *      Return a list with a class and it's method associated to a path
     * @param string $path
     * @return array
     */
    public static function getPathCallbackData( $path ) {
        $_class = $_method = NULL;
        $_routes = self::getRoutes();
        $_extension = substr($path, -3, 3);
        $path =parse_url($path)['path'];
        if ( $_extension != '.js' && Application::hasIsActive() ){
            $path = Application::decrypt( str_replace('/', '', $path) );
        }
        if (substr($path, 0,1) !== '/'){
            $path ='/'.$path;
        }
        if ( count( $_routes) ) {
            $secured_method = TRUE;
            foreach ( $_routes as $key => $route ) {
                if (!isset($route['path'])){
                    continue;
                }
                $route['match'] = !isset($route['match']) ? $route['path'] : $route['match'];
                $route['match'] = str_replace('*', '([^/]+)', $route['match']);
                $regexp = '#^'.$route['match'].'?#'; //#/javascript/?([^/]+)#
                
                preg_match( $regexp, $path, $matches );
                if ( count( $matches ) ){
                    list($_class, $_method) = explode('@', $route['callback']);
                    if ( isset($route['secured']) && strtolower( $route['secured'] ) === 'no' ){
                        $secured_method = FALSE;
                    }
                    if ( $secured_method && !self::isAuthenticated() ){
                        $_class = 'Application';
                        $_method= 'sessionExperied';
                    } else if ( isset ( $route['language_files'] ) && $route['language_files'] ){
                        if (!is_array( $route['language_files'] ) ) {
                            $route['language_files'] = array( $route['language_files'] );
                        }
                        $_language =  new Language();
                        foreach ( $route['language_files'] as $lang_file){
                            $_language->loadFile( $lang_file );
                        }

                    }
                    
                    break;
                }
            }
        }
        return array($_class, $_method);
    }
    
    /**
     * FUNCTION: getPathByKey
     *      Return URL path for a routing item
     * @param string $key
     * @return string
     */
    public static function getPathByKey( $key ){
        $_path = NULL;
        $_routes = self::getRoutes();
        if ( count($_routes) && isset( $_routes[$key]) ) {
            $_path = $_routes[$key]['path'];
            if ( Application::hasIsActive() ){
                $_path = '/'.Application::encrypt( $_path );
            }
        }
        return str_replace('//', '/', Application::getWorkingDirectory().$_path);
    }
    
    public static function getPathDataMatches( $key, $url ){
        $_matches = array();
        $_routes = self::getRoutes();
        if ( count($_routes) && isset( $_routes[$key]) ) {
            $_route = $_routes[$key];
            $_route['match'] = str_replace('*', '([^/]+)', $_route['match']);
            $_regexp = '#'.$_route['match'].'?#'; //#/javascript/?([^/]+)#
            preg_match( $_regexp, $url, $_matches );
        }
        return $_matches;
    }
    
    public static function getCurrentPath(){
        return self::$current_path;
    }
    
    public static function getRelativePath( $path ){
        return DOCUMENT_ROOT_PATH .($path[0] === '/' ? '' : '/') . $path;
    }
}
