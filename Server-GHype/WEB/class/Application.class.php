<?php

class Application extends Object {

    protected $template = NULL;
    protected $language = NULL;
    protected $template_file = false;
    protected $database = null;
    protected $has_error = false;
    protected $has_notification = false;
    protected $debug = false;
    protected static $start_time;
    protected static $error_404 = false;
    protected static $memcache = NULL;
    public static $memcache_available = FALSE;

    public function __construct() {
        $settings = Object::getConfig();
        $template = Object::getTemplate();
        $language = Object::getLanguage();
        //$language = Object::
        parent::__construct($settings);
        $this->template = $template;
        $this->language = $language;
        $this->template_file = "index.tpl";
        if (is_array($settings) && isset($settings['default']) && isset($settings['default']['debug']) && $settings['default']['debug'] == "1") {
            $this->debug = TRUE;
        }
        $this->assign("DEBUG", $this->debug);

        $this->database = new Database($settings);
    }

    public static function isAuthenticated() {
        if (self::getSessionValue("PERSON_ID", false) == false) {
            return false;
        } else {
            $_rights = self::getSessionValue("RIGHTS");
            if (is_array($_rights) && count($_rights) > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getTemplateContent() {
        if (self::isAuthenticated() || $this->getRequestValue("template") == "session/login.tpl" || $this->getRequestValue("template") == "session/login-response.tpl"){
            $this->template_file = $this->getRequestValue("template", "empty.tpl");
            if (($_words = $this->getRequestValue("words")) !== false && $this->language != null)
                $this->language->loadFile($_words);
        }else{
            $this->setExpiredSession();
        }
    }

    public function login() {
        $this->getTemplateContent();
        if(($_auth_methode = $this->getConfigValue("system.auth.methode")) != false && strtoupper($_auth_methode) == "LDAP") {
            $_hostname = $this->getConfigValue("system.auth.ldap.server");
            $_port = $this->getConfigValue("system.auth.ldap.port", 389);
            if($_hostname != false && $_port != false) {
                if(($_lconn = @ldap_connect($_hostname, $_port)) != false) {
                    ldap_set_option($_lconn, LDAP_OPT_PROTOCOL_VERSION, $this->getConfigValue("system.auth.ldap.version", 3));
                    ldap_set_option($_lconn, LDAP_OPT_NETWORK_TIMEOUT, $this->getConfigValue("system.auth.ldap.timeout", 5));
                    ldap_set_option($_lconn, LDAP_OPT_REFERRALS, $this->getConfigValue("system.auth.ldap.referrals", 0));
                    
                    if(($_dn = $this->getConfigValue("system.auth.ldap.dn")) != false) {
                        $_username = "uid=".$this->getRequestValue("username").",".$_dn;
                    } else {
                        $_username = $this->getRequestValue("username");
                    }
                    $_password = $this->getRequestValue("password");

                    if ($_username != false && $_password != false) {
                        if($bind = @ldap_bind($_lconn, $_username, $_password)) {

                            $_sql_query = "SELECT a.id, v.`n-family` AS firstname, v.`n-given` AS lastname, v.email FROM authreg a"
                            . " LEFT JOIN vcard v on a.`collection-owner`=v.`collection-owner` WHERE a.username ='" . $_username . "'";
                            if (($_row = $this->select($_sql_query)) != false && count($_row) == 1) {
                                $_person_id = $_row[0]["ID"];
                                $this->setSessionValue("USERNAME", $_username, true, true);
                                $this->setSessionValue("PERSON_ID", $_person_id, true, true);
                                $this->setSessionValue("PERSON_PASSWORD", $_row[0]["PASSWORD"], true, true);
                                $this->setSessionValue("REALM", $_row[0]["REALM"], true, true);
                                $this->setSessionValue("PERSON_FIRSTNAME", $_row[0]["FIRSTNAME"], true, true);
                                $this->setSessionValue("PERSON_LASTNAME", $_row[0]["LASTNAME"], true, true);
                                $this->setSessionValue("PERSON_EMAIL", $_row[0]["EMAIL"], true, true);
                                $this->setSessionValue("RIGHTS", $this->getPersonRights($_row[0]["ID"]), TRUE, TRUE);
                                $this->setSessionValue("PERSON_ACL_NAME", $this->getUserAcl($_row[0]["ID"]), TRUE, TRUE);
                            } else {
                                $this->assign("error", true);
                            }
                        } else {
                            $this->assign("error", true);
                        }
                    } else {
                        $this->assign("error", true);
                    }
                    ldap_close($_lconn);
                } else {
                    $this->assign("error", true);
                }
            } else {
                $this->assign("error", true);
            }
        } else {
            $_username = $this->getRequestValue("username");
            $_password = $this->getRequestValue("password");
            
            $_password = urldecode($_password);// CEVA FACE URLENCODE LA POST
            
            if ($_username != false && $_password != false) {
                $_sql_query = "select a.id, a.username, a.password, a.realm, v.`n-family` as firstname, v.`n-given` as lastname, v.email from authreg a left join vcard v on a.`collection-owner`=v.`collection-owner` where a.username='".$_username."'";
                if (($_row = $this->select($_sql_query)) != false && count($_row) == 1) {
                    if (crypt($_password, $_row[0]["PASSWORD"]) === $_row[0]["PASSWORD"]) {
                        $_person_id = $_row[0]["ID"];
                        $this->setSessionValue("USERNAME", $_username, true, true);
                        $this->setSessionValue("PERSON_ID", $_person_id, true, true);
                        $this->setSessionValue("PERSON_PASSWORD", $_password, true, true);
                        $this->setSessionValue("REALM", $_row[0]["REALM"], true, true);
                        $this->setSessionValue("PERSON_FIRSTNAME", $_row[0]["FIRSTNAME"], true, true);
                        $this->setSessionValue("PERSON_LASTNAME", $_row[0]["LASTNAME"], true, true);
                        $this->setSessionValue("PERSON_EMAIL", $_row[0]["EMAIL"], true, true);

                        $this->setSessionValue("RIGHTS", $this->getPersonRights($_row[0]["ID"]), TRUE, TRUE);
                        $this->setSessionValue("PERSON_ACL_NAME", $this->getUserAcl($_row[0]["ID"]), TRUE, TRUE);
                        $this->setSessionValue("PERSON_USERNAME", $_row[0]["USERNAME"], true, true);                        
                    //} 
                    }
                    else {
                        $this->assign("error", true);
                    }
                } else {
                    $this->assign("error", true);
                }
            } else {
                $this->assign("error", true);
            }
        }
    }

    /**
     *  FUNCTION: getPersonRights
     *      Get list with user rights
     * @param type $user_id
     * @return null
     */
    protected  function getPersonRights($user_id){
         $_sql_select = "SELECT aco.name as aco_name, aamn.access_type, aamn.crud FROM org_acl_aco_mn aamn LEFT JOIN org_aco aco ON aamn.org_aco_id=aco.id"
        . "  LEFT JOIN org_acl_authreg_mn aclmn ON aamn.org_acl_id=aclmn.org_acl_id WHERE  aclmn.authreg_id=" . $user_id;

        if(($rows = $this->select($_sql_select)) != false && count($rows) > 0) {
            return $rows;
        }else{
            return NULL;
        }
    }

    protected function getUserAcl($user_id){
        $_sql_select = "SELECT acl.name FROM org_acl_authreg_mn aclmn LEFT JOIN org_acl acl ON acl.id = aclmn.org_acl_id WHERE aclmn.authreg_id ="
        . $user_id;
        if(($rows = $this->select($_sql_select)) != false && count($rows) > 0) {
            $result = array();
            foreach ($rows as $row){
                $result[] = $row['NAME'];
            }
            return implode(",", $result);
        }else{
            return NULL;
        }
    }
    public function logout() {
        session_start();
        $session_id = session_id();
        session_unset();
        $_SESSION = array();
        close_session($session_id);
        $this->template_file = "session/logout.tpl";
    }

    public function sendResetPasswordEmail(){
        $_email = $this->getRequestValue('email');
        
        if ( isValidEmail($_email) ) {
            
        } else {
            $this->assign('error', Language::getWord('INVALID_EMAIL'));
        }
        $this->template_file = 'session/forget-password-response.tpl';
        
    }
    public function setErrorMessage($string) {
        $this->has_error = TRUE;
        $this->assign("error", trim(htmlentities(str_replace(array("\\r\\n", "\\n", "\\r"), "<br>", $string))));
    }

    public function setNotificationMessage($string) {
        $this->has_notification = TRUE;
        $this->assign("notification", trim(htmlentities(str_replace(array("\\r\\n", "\\n", "\\r"), "<br>", $string))));
    }

    public function display($format = NULL) {
        switch ($format) {
            case ".xml":
                header("content-type:  text/xml");
                break;
        }
        //if is index application page then we include js& css libraries
        if ($this->template_file == "index.tpl") {
            $this->includeComponents();
        }

        if ($this->has_error /* && !$this->isAuthenticated() */) {
            $this->template_file = "error.tpl";
        }
        if ($this->has_notification /* && !$this->isAuthenticated() */) {
            $this->template_file = "notification.tpl";
        }
        if ($this->template != NULL && $this->template_file != false) {
            print ($this->fetch($this->template_file));
        }
    }

    public function assign($key, $value) {
        if ($this->template != null)
            $this->template->assign($key, $value);
    }

    public function assign_by_ref($tpl_var, &$value) {
        if ($this->template != null)
            $this->template->assignByRef($tpl_var, $value);
    }

    public function get_template_vars($key = null) {
        if ($this->template != null)
            return $this->template->get_template_vars($key);
    }

    public function loadLanguageFile($file, $assign_to_template = true, $template = null) {
        if ($this->language != NULL) {
            $this->language->loadFile($file, $assign_to_template, $template);
        }
    }

    public function fetch($template = null, $cache_id = null, $compile_id = null, $parent = null, $display = false, $merge_tpl_vars = true, $no_output_filter = false) {
        if (self::$error_404 == TRUE) {
            header('HTTP/1.0 404 Not Found');
            echo "<h1>404 Not Found</h1>";
            echo "The page that you have requested could not be found.";
        } else {
            $this->assign("time_duration", $this->calculateTime());
            if ($this->template != null) {
                return $this->template->fetch($template, $cache_id, $compile_id, $parent, $display, $merge_tpl_vars, $no_output_filter);
            } else
                return NULL;
        }
    }
    public function displayJSONResponse($response){
        $this->template_file = "empty.tpl";
        print json_encode($response);
    }
    public function compactJavascript($string) {
        if ($this->template != null)
            return $this->template->compactJavascript($string);
    }

    public function plsql($sql_query) {
        if ($this->database != NULL)
            return $this->database->plsql($sql_query);
        else
            return false;
    }

    public function select($sql_query, $bind_array = array()) {
        if ($this->database != NULL)
            return $this->database->select($sql_query, $bind_array);
        else
            return false;
    }

    public function insert($sql_query, $bind_array = array()) {
        if ($this->database != NULL)
            return $this->database->insert($sql_query, $bind_array);
        else
            return false;
    }

    public function update($sql_query, $bind_array = array()) {
        if ($this->database != NULL)
            return $this->database->update($sql_query, $bind_array);
        else
            return false;
    }
    protected function query($sql_query){
        if ($this->database != NULL){
            return $this->database->query($sql_query);
        }else return FALSE;
    }

    protected function escape($string){
        if ($this->database != NULL){
            return $this->database->escape($string);
        }
    }

    public function commit() {
        if ($this->database != NULL){
            return $this->database->commit();
        } else return FALSE;
    }

    public function rollback() {
        if ($this->database != NULL)
            $this->database->rollback();
    }

    public function autocommit($new_value){
        if ( $this->database !== NULL ) {
            return $this->database->autocommit( $new_value );
        } else return FALSE;
    }
    
    function getWord($key, $force = TRUE) {
        if ($this->language != NULL){
            $language = $this->language;
            return $language::getWord($key, $force);
        }
        else
            return false;
    }

    public function getRowDataByObjId($obj_id, $table) {
        $result = null;
        if ($obj_id) {
            $_sql = "Select * from {$table} where object_id='{$obj_id}'";
            if (($_row = $this->select($_sql)) != false && count($_row) == 1) {
                $result = $_row[0];
            }
        }
        return $result;
    }

    public static function getTime() {
        $time = microtime();
        $time = explode(" ", $time);
        return $time[0] + $time[1];
    }

    /**
     * FUNCTION: calculateTime
     *      return loading time in seconds
     * 
     * PARAMETERS:
     * 
     * RETURN:
     *      String
     */
    public static function calculateTime() {
        $start_time = self::$start_time;
        $end_time = self::getTime();
        return round($end_time - $start_time, 4) . "s";
    }

    /**
     * FUNCTION: setStartTime
     *      set application start time, and with end time we find loading time
     */
    public static function setStartTime() {
        $time = microtime();
        $time = explode(" ", $time);

        self::$start_time = $time[0] + $time[1];
    }

    protected function includeComponents() {
        $settings = self::getConfig();
        if ((array_key_exists('components', $settings) && isSet($settings['components']))) {
            if ((array_key_exists('dir', $settings['components']) && isSet($settings['components']['dir']))) {
                defined('COMPONENTS_PATH') || define('COMPONENTS_PATH', $settings['components']['dir']);
            }
            if ((array_key_exists('component', $settings['components']) && isSet($settings['components']['component']) && is_array($settings['components']['component']))) {
                $_css_include = array();
                $_js_include = array();
                foreach ($settings['components']['component'] as $component) {
                    $_component_dir = COMPONENTS_PATH;
                    if ((array_key_exists('dir', $component) && isSet($component['dir']))) {
                        $_component_dir .= $component['dir'];
                    }
                    if ((array_key_exists('css', $component) && isSet($component['css']))) {
                        if (is_array($component['css'])) {
                            foreach ($component['css'] as $css) {
                                array_push($_css_include, $_component_dir . '/' . $css);
                            }
                        } else {
                            array_push($_css_include, $_component_dir . '/' . $component['css']);
                        }
                    }
                    if ((array_key_exists('javascript', $component) && isSet($component['javascript']))) {
                        if (is_array($component['javascript'])) {
                            foreach ($component['javascript'] as $js) {
                                if (substr( $js, 0, 4) === 'http' ) {
                                    array_push($_js_include, $js);
                                }else {
                                    array_push($_js_include, DOCUMENT_ROOT_PATH . $_component_dir . '/' . $js);
                                }
                            }
                        } else {
                            array_push($_js_include, DOCUMENT_ROOT_PATH . $_component_dir . '/' . $component['javascript']);
                        }
                    }
                }
                $this->assign("css_include_list", $_css_include);
                $this->assign("js_include_list", $_js_include);
            }
        }
    }

    protected function escapeQuery($query) {
        return str_replace("'", "''", $query);
    }

    /**
     * FUNCTION: logApplicationError
     *      store error to DB
     *  
     * PARAMETERS:
     *      String $parameters
     * @param type $error_code
     * @param type $error_message
     */
    public function logApplicationError($parameters, $error_code = 0, $error_message) {
        $this->plsql("begin log_application_error('interface', '" . $this->escapeQuery($parameters) . "', '" . $error_code . "', '" . $this->escapeQuery($error_message) . "'); end;");
    }

    /**
     * FUNCTION: set404Error
     *  mark this page as not found and log error to DB
     * 
     * PARAMETERS:
     * 
     * RETURNS:
     */
    public function set404Error() {
        self::$error_404 = true;

        $this->logApplicationError($_SERVER['REQUEST_URI'], 404, "Page not found! Request:" . json_encode($_REQUEST));
    }

    /**
     * FUNCTION: logError
     *      Method used by JS to save an error
     * 
     * PARAMETERS:
     *      - $_POST:
     *          STRING <msg>    : error message;
     *          STRING <where>  : error source;
     * 
     * RETURNS:
     *  -
     */
    public function logError() {
        $error_message = $_POST['msg'];
        $error_source = $_POST['where'];
        if ($error_message && $error_source) {
            $this->logApplicationError($error_source, 0, $error_message);
        }
        $this->template_file = 'empty.tpl';
    }
    protected function getExpiredSessionResponse(){
        $response         = new stdClass();
        $response->success= FALSE;
        $response->action = "logout";
        $response->error_message= $this->getWord('EXPIRED_SESSION_MESSAGE');
        return $response;
    }
    protected function setExpiredSession($show_js_tag=TRUE){
        $this->assign('expired_response', json_encode($this->getExpiredSessionResponse()));
        $this->assign("show_js_tag", $show_js_tag);
        $this->template_file = "session/session-expired.tpl";
    }

    protected function noAccessMessage($show_js_tag=TRUE){
        $this->template_file = "session/no-access.tpl";
    }
    
    /**
     * FUNCTION: getWorkingDirectory
     *  Return workiing directory
     * @return string
     */
    static function getWorkingDirectory(){
        return DOCUMENT_ROOT_PATH;
    }
    
    public function processJavascriptFile(){
        $_file = CURRENT_URL;
        $ext = pathinfo($_file, PATHINFO_EXTENSION);
        if ( file_exists( $_SERVER['DOCUMENT_ROOT']. DOCUMENT_ROOT_PATH . $_file ) && $ext === 'js'  ) { 
            $_url_matches = Routing::getPathDataMatches('javascript', $_file);
            if ( count($_url_matches) >1 ) {
                header('Content-Type: application/javascript');
                $this->template->setCacheLifetime(3600);//1 hour
                $this->template_file = $_url_matches[1];
            } else {
                $this->template_file = 'empty.tpl';
            }
        }
    }
    
    protected function setAjaxResponse( $_response, $_force_print = false, $_force_response = false ) {        
        if ( !$_force_response && !self::isAuthenticated() ) {
            $_tmp_response = new stdClass();
            $_tmp_response->success = true;
            $_tmp_response->data["execute"] = array();
            array_push($_tmp_response->data["execute"], array("fnct" => "redirectToLogin", "params" => false ));
            print json_encode( $_tmp_response );
            $this->template_file = "empty.tpl";
            exit();
        } else {
            $js_template = $this->getRequestValue( 'js_template', FALSE );
       
            if ( $js_template ) {
                $this->loadJsTemplate( $_response, $js_template, TRUE );
            }
            if(( $_delayed_response = self::getSessionValue( "DELAYED-AJAX-RESPONSE", false )) != false && isSet( $_response->data ) && isSet( $_response->success ) && $_response->success ) {
                if(( $_lock = self::getSessionValue( "DELAYED-AJAX-RESPONSE-LOCK", false )) != false ) {
                    $this->removeSessionParameter( "DELAYED-AJAX-RESPONSE-LOCK", true, true );
                } else {
                    $_response->data = array_merge_recursive( $_response->data, $_delayed_response );
                    $this->removeSessionParameter( "DELAYED-AJAX-RESPONSE", true, true );
                }
            }
            if( $_force_print ) {
                print json_encode( $_response );
                $this->template_file = "empty.tpl";
                exit();
            } else {
                $this->assign( "JSON_RESPONSE", json_encode( $_response ) );
                $this->template_file = "empty.tpl";
            }
        }
    }
    
    public function sessionExperied(){
        $_response =  new Response();
        $_response->success = FALSE;
        $_response->error_message = Language::getWord( 'EXPIRED_SESSION_MESSAGE' );
        $_response->action = 'reload';
        
        $this->setAjaxResponse($_response);
    }
    
    public function init(){        
        /* Initialize session */
        $session_id = init_session();
        // session_unset();
        close_session($session_id);
        Application::setStartTime();
        $_sitepath = DOCUMENT_ROOT_PATH;
        if ( MEMCACHED_ACTIVE == 1){
            // Connection creation
            if( class_exists('Memcache') ){ 
                self::$memcache = new Memcache;
                self::$memcache_available = @self::$memcache->connect(MEMCACHED_HOST, MEMCACHED_PORT);
            }
        }
        $this->deleteFromCache( $this->getCacheKey( 'settings' ) );//TODO: line must deleted on production
        $settings = $this->getFromCache( $this->getCacheKey( 'settings' ) );
        if ( empty($settings) ) {
            $settings = getSettings();
            $this->storeInCache( $this->getCacheKey( 'settings' ), $settings );
        }
        /* Read configuration */
        if( $settings !== false) {
            Object::loadConfig( $settings );           
            if((array_key_exists('phpSettings', $settings) && isSet($settings['phpSettings']))) {
                if((array_key_exists('display_errors', $settings['phpSettings']) && isSet($settings['phpSettings']['display_errors']))) {
                    ini_set('display_errors', $settings['phpSettings']['display_errors']);
                    error_reporting(E_ALL);
                }
                if((array_key_exists('display_startup_errors', $settings['phpSettings']) && isSet($settings['phpSettings']['display_startup_errors']))) {
                    ini_set('display_errors', $settings['phpSettings']['display_startup_errors']);
                    error_reporting(E_ALL);
                }
            }

            if((array_key_exists('smarty', $settings) && isSet($settings['smarty']))) {
                if((array_key_exists('dir', $settings['smarty']) && isSet($settings['smarty']['dir']))) {
                    define('SMARTY_DIR', $settings['smarty']['dir'].DIRECTORY_SEPARATOR);
                    include_once($settings['smarty']['dir'].DIRECTORY_SEPARATOR.'Smarty.class.php');
                }
            }
        }


        /* Template section */
        $template = new Template();
        $template->assign('SITEPATH', $_sitepath);
        if(isSet($_SERVER['SERVER_NAME'])) $template->assign("SITEPATH_HTTP", $_SERVER['SERVER_NAME'] .$_sitepath);

        Object::loadTemplate($template);

        /* Language section */
        $language = new Language();
        $language->loadFile("common.xml", TRUE);
        
        Object::loadLanguage($language);
        
    }
    
    public function run(){
        $_sitepath = DOCUMENT_ROOT_PATH;
        if(($_link = str_replace("<start>".$_sitepath, "/", "<start>".$_SERVER['REQUEST_URI'])) != false) {
            $_link = str_replace('//', '/', $_link);
            defined('CURRENT_URL') || define('CURRENT_URL', $_link);
            list($_class, $_operation) = Routing::getPathCallbackData( $_link );

            if(preg_match('/(?i)msie [1-7]/',$_SERVER['HTTP_USER_AGENT'], $matchesb)){
                // if IE<=7
                $browser_version = NULL;
                switch (strtolower($matchesb[0])){
                    case 'msie 6':
                        $browser_version = "6";
                        break;
                    case 'msie 7':
                        $browser_version  ="7";
                }     
                if ($browser_version){
                    $_class = FALSE;
                    $_operation = FALSE;
                    $this->template->assign('browser_version', $browser_version);
                    $this->template->assign('old_browser', TRUE);
                }
            }
            if($_class != false && __autoload($_class)) {
                $_class = new $_class();
            } else {
                $_class = new Application();
            }
            $_class->assign("AUTHENTICATED", Application::isAuthenticated());

            if($_operation != false && method_exists($_class, $_operation)){ 
                $_class->$_operation();                
            }else if($_operation){// marked as 404 page
                $_class->set404Error();
            }
            $_class->display();
        }
    }
    
    public static function encrypt($string, $key =NULL) {
        $key= (!$key ? session_id() : $key);
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
    }

    public static function decrypt($encrypted, $key=NULL) {
        $key= (!$key ? session_id() : $key);
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
    }

    /**
     * FUNCTION: hasIsActive
     *      If data encryption is activated then the returned value will be true, else false.
     * @return Boolean
     */
    public static function hasIsActive(){
        return Object::getConfigValue('default.hash_data', FALSE);
    }
    
    public static function storeInCache( $key, $value, $flag = 0, $expire=NULL){
        if (self::$memcache_available){
            @self::$memcache->set($key, $value, $flag, $expire);
        }
    }
    
    public static function getFromCache($key){
        if (self::$memcache_available){
            return self::$memcache->get($key);
        } else return NULL;
    }
    
    /**
     * FUNCTION: deleteFromCache
     * 
     *  Deletes any <key> from the server. The <time> parameter is the amount of 
     * time in seconds (or Unix time until which) the client wishes the server to 
     * refuse add and replace commands for this key. For this amount of time, the 
     * item is put into a delete queue, which means that it won't possible to 
     * retrieve it by the get command, but add and replace command with this key 
     * will also fail (the set command will succeed, however). After the time 
     * passes, the item is finally deleted from server memory. The parameter 
     * <time> defaults to 0 (which means that the item will be deleted immediately 
     * and further storage commands with this key will succeed).
     * 
     * @param string $key
     * @param int $time
     */
    public static function deleteFromCache($key, $time=0){
        if (self::$memcache_available){
            return self::$memcache->delete($key, $time);
        } else return FALSE;
    }

    public function sendEmail($data){
	
	$email      = $data->send_to;
	$yourname   = "FlashNet";
	$youremail  = "FlashNet<no-replay@flashnet.ro>";
        $response   = FALSE;
        if ($email){
            //==== START COD DE PERSONALIZAT =================================================

            //definesc subiectul e-mail-ului
            $subject 			= $data->subject;
            
            // definesc mesajul care se trimite pe e-mail
            $msg ="<html><head></head><body>";
            $msg.= "{$data->message}";


            //==== END COD DE PERSONALIZAT =================================================
            $msg.="</body></html>";

            $antete  = "MIME-Version: 1.0\r\n";
            $antete .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $antete .= "From: ".$youremail."\r\n";
            $response = mail($email,$subject,$msg,$antete);
        }
	return $response;
    }

    protected function getCacheKey( $_value ) {
        if ( $this->isAuthenticated() ){
            $_value .= "_secured";
        }else{
            $_value .= "_login";
        }
        
        return $_value;
    }
    
    public function clearItemFromCache( $_item ) {
        switch ( $_item ) {
            case 'aco':
                $_sql = 'SELECT id from org_aco';
                $_aco_list= $this->select($_sql);
                $this->deleteFromCache( 'aco_tree' );
                if ( is_array( $_aco_list ) && count( $_aco_list) ) {
                    foreach ( $_aco_list as $_aco_item ) {
                        $this->deleteFromCache( 'aco_tree_' . $_aco_item[ 'ID' ] );
                    }
                }
                break;
            case 'settings':
                $this->deleteFromCache( $_item . '_login');
                $this->deleteFromCache( $_item . '_secured');
                break;
            case 'type':
                $_sql = 'select description from type';
                $_types_list = $this->select( $_sql );
                if ( is_array( $_types_list ) && count( $_types_list ) ) {
                    foreach ( $_types_list as $_type ) {
                        $this->deleteFromCache( 'type_' . $_type[ 'DESCRIPTION' ]  );
                    }
                }
                break;
            case 'language':
                $_selected_language = $this->getConfigValue( 'default.language' );
                $_dir = ROOT_PATH . DIRECTORY_SEPARATOR . "lang" . DIRECTORY_SEPARATOR . $_selected_language . DIRECTORY_SEPARATOR;
                if ( is_dir( $_dir ) ) {
                    $_files = array_diff( scandir($_dir), [ '.', '..' ] );
                    if ( count( $_files ) ) {
                        foreach ( $_files as $_file ) {
                            $this->deleteFromCache( $_file );
                        }
                    }
                }
                break;
            default :
                $this->deleteFromCache( $_item );
        }
    }
    
    /**
     * FUNCTION: clearCache
     * 
     *      Clears data from Memcache
     */
    public function clearCache() {
        $_response = new Response();
        $what = $this->getRequestValue( 'clear' );
        $_available_section = $this->getConfigValue('memcache_section');
        
        if ( is_array( $_available_section ) && count($_available_section ) ) {
            $_available = [];
            foreach ( $_available_section as $_section ) {
                $_available[] = $_section['key'];
            }
            if ( $what === "all" ) {
                $_response->success = TRUE;
                foreach ($_available as $_item ) {
                    $this->clearItemFromCache( $_item ); 
                }
            } else if (in_array( $what, $_available ) ) {
                $_response->success = TRUE;
                $this->clearItemFromCache( $what );
            }
        }        
        
        $this->setAjaxResponse( $_response, TRUE );
    }
}

?>
