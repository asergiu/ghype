<?php
class TransferedFile extends Application{
    /**
     * FUNCTION: generateListView
     *      Generates a list with transfered files.
     * 
     *      If user hasn't FULL ACCESS then list will be filtered to his files(sent and received)
     * 
     */
    public function generateListView(){
        $_load_container = $this->getRequestValue( 'load_container' );
        $_response = new Response();
        $_response->success = TRUE;
        
        $_response->data = $this->template->fetch( 'files/files-container.tpl' );
        $this->setAjaxResponse($_response, TRUE);
    }
    protected function getList( $options = array()){
        $_list = array();


        $_page = $this->getRequestValue( 'page' );
        $_page_limit = $this->getConfigValue('files.page_limit', 30);
        $_where = array();
        $_username = $this->getSessionValue( 'USERNAME' ) . '@' . $this->getSessionValue( 'REALM' );
        if ( !self::checkAccess( 'ACL_ACCESS_ALL', self::ACL_READ ) ) {
            $_where[] = "(f.to='" . $_username . "' and f.type='pull') or (f.from='" . $_username . "' and f.type='push')";
        }
        $_sql  = 'SELECT f.uniqueid, f.to, f.from, f.timestamp, f.filename, f.filesize, f.status, f.message, f.timestamp '
                . 'from `file-transfer` f '
                . ( count( $_where) ? " where ".implode(" and ", $_where): "")
                . 'order by timestamp desc limit '.( ($_page-1) * $_page_limit).','.$_page_limit;
        $_result = $this->select($_sql);
        if ( $_result ){            
            if ( isset($options['loadFiles']) && $options['loadFiles'] ) {
                $_date_format = $this->getConfigValue('default.date_format', 'm.d.Y H:i');
            }
            $_list = $_result;
        }
        
        return $_list;
    }
    
	public function generateListJson(){
        $_list = $this->getList();
        
        $_response =  [];
        
        if ( is_array( $_list ) && count( $_list ) ) {
            foreach ( $_list as $_item ){
                $_object = new stdClass();
                $_object->uid = $_item[ 'UNIQUEID' ];
                $_object->to = $_item[ 'TO' ];
                $_object->from = $_item[ 'FROM' ];
                $_object->timestamp = $_item[ 'TIMESTAMP' ];
                $_object->filename = $_item[ 'FILENAME' ];
                $_object->filesize = $_item[ 'FILESIZE' ];
                $_object->status = $_item[ 'STATUS' ];
                $_object->message = $_item[ 'MESSAGE' ];
                $_response[] = $_object;
            }
        }
        $this->setAjaxResponse( $_response, TRUE);
    }

    public function downloadFile(){
        $response =  new Response();
        $_uid = $this->getRequestValue('uid');
        if ( $_uid ){
            $_sql = "select filename, filesize from `file-transfer` where uniqueid='" . $_uid . "' and type='push'";
            $_result = $this->select( $_sql );
            if ( $_result && count($_result) ) {
                $file = $_result[0];
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file['FILENAME']).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . $file['FILESIZE']);
                readfile('http://jabber.tvarita.ro/file-transfers/'.$_uid);
            }
           
            

        }
        exit;
    }
}
?>