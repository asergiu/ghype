<?PHP

  class Database extends Object {
    private $conn_type = 0;
    private $conn = null;
    private $last_error_message = "";
    private $log_queries = false;
    private $log_query_file = false;
    private $conn_list = null;

    public function __construct($settings) {
      parent::__construct($settings);
      $this->conn = null;
      $this->conn_list = null;
      if(($this->getConfigValue("system.database.dbtype")) == "Oracle") {
        $_host = $this->getConfigValue("system.database.host");
        $_username = $this->getConfigValue("system.database.username");
        $_password = $this->getConfigValue("system.database.password");
        $_dbname = $this->getConfigValue("system.database.dbname");
        $this->log_queries = $this->getConfigValue("system.database.log");
        $this->log_query_file = $this->getConfigValue("system.database.log_filename");
        if($_host != false && $_username != false && $_password != false && $_dbname != false) {
          if(($this->conn = @oci_connect($_username, $_password, $_host)) == false) {
            $_error_obj = oci_error();
            $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", htmlentities($_error_obj['message']));
          } else {
            $this->conn_type = 1;
          }
        }
      } else if(($this->getConfigValue("system.database.dbtype")) == "MySQL") { 
        $_host = $this->getConfigValue("system.database.host");
        $_username = $this->getConfigValue("system.database.username");
        $_password = $this->getConfigValue("system.database.password",'');
        $_dbname = $this->getConfigValue("system.database.dbname");
        $this->log_queries = $this->getConfigValue("system.database.log");
        $this->log_query_file = $this->getConfigValue("system.database.log_filename");
        if($_host != false && $_username != false && $_password !== false && $_dbname != false) {
          if(($this->conn = @mysqli_connect($_host, $_username, $_password, $_dbname)) == false) {
            $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", "Can not connect to database!");
            die($this->last_error_message);
          } else {
            $this->conn_type = 2;
          }
        }
      }
    }
    
    public function readDistributedDatabases() {
      
    }

    private function millisec_date($format_string, $_start_timestamp) {
      $_timestamp = microtime(true);
      $_diff = $_timestamp - $_start_timestamp;
      $_diff_ms = floor($_diff);
      
      $milliseconds = round(($_diff - $_diff_ms) * 1000000);
      return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format_string), $_diff - 2 * 3600);
    }
    
    private function log_query($string, $start_date) {
      if($this->log_queries && ($this->log_query_file != false)) {
        file_put_contents($this->log_query_file, $this->millisec_date('H:i:s.u', $start_date)." - ".str_replace("\n", " ", trim($string))."\n", FILE_APPEND);
      }
    }
    
    /**
     * Functia de executie a unui bloc pl/sql
     *  
     * @sql_query  string  Continutul interogarii
     * @return  TRUE in caz de succes sau FALSE in caz de eroare
    */ 
    public function plsql($sql_query) {
      $result = false;
      $_return_result = false;
      
      if($this->conn != null && $this->conn_type == 1) {
        $_start_date = microtime(true);
        if(($_stmt = @oci_parse($this->conn, $sql_query)) != false) {
          if(@oci_bind_by_name($_stmt, ":result", $result, 64, SQLT_CHR)) $_return_result = true;
          if(!@oci_execute($_stmt, OCI_DEFAULT)) {
            $_error_obj = oci_error($_stmt);
            $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", htmlentities($_error_obj['message']));
            if($_error_obj["offset"]) {
              $this->last_error_message .= '<br>'.substr ($_error_obj["sqltext"], 0, $_error_obj["offset"]).'*'.substr ($_error_obj["sqltext"], $_error_obj["offset"]);
            }
          } else {
            if(!$_return_result) {
              $result = true;
            }
          }
          oci_free_statement($_stmt);
        }
        $this->log_query($sql_query, $_start_date);
      } else if($this->conn != null && $this->conn_type == 2) {
        $_start_date = microtime(true);
        if(($_stmt = mysqli_query($this->conn, $sql_query)) != false) {
          if($result == false) $result = array();
          if(gettype($_stmt) == "boolean") {
          } else {
            while($_row = mysqli_fetch_array($_stmt, MYSQLI_ASSOC)) {
              array_push($result, array_change_key_case($_row, CASE_UPPER));
            }
            mysqli_free_result($_stmt);
          }
        } else {
          $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", mysqli_error($this->conn));
          $result = false;
        }
      }

      return $result;
    }

    /**
    * Functia de interogare a bazei de date
    *
    * @sql_query  string  Continutul interogarii
    * @return  array  Rezultatul interogarii sau FALSE in caz de eroare
    */
    public function select($sql_query, $bind_array=array()) {
      $result = false;
      
      if($this->conn != null && $this->conn_type == 1) {
        $_start_date = microtime(true);
        if(($_stmt = @oci_parse($this->conn, $sql_query)) != false) {            
          if (count($bind_array)){             
              foreach ($bind_array as $var => $value) {                  
                  oci_bind_by_name($_stmt, ":{$var}", $bind_array[$var]);
              }
          }          
          if(!@oci_execute($_stmt, OCI_DEFAULT)) {            
            $_error_obj = oci_error($_stmt);            
            $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", htmlentities($_error_obj['message']));
            if($_error_obj["offset"]) {
              $this->last_error_message .= '<br>'.substr ($_error_obj["sqltext"], 0, $_error_obj["offset"]).'*'.substr ($_error_obj["sqltext"], $_error_obj["offset"]);
            }
            $this->plsql("begin log_application_error('interface', '".$this->escape($sql_query)."', '".$_error_obj["code"]."', '".$this->escape($this->last_error_message)."'); end;");
          } else {
            if($result == false) $result = array();
            while(($_row = oci_fetch_assoc($_stmt))) {              
              array_push($result, $_row);
            }
          }
        } else {
          $this->plsql("begin log_application_error('interface', '".$sql_query."', '-1', 'OCI Parse error'); end;");
        }
        $this->log_query($sql_query, $_start_date);
      } else if($this->conn != null && $this->conn_type == 2) {
        $_start_date = microtime(true);
        if(($_stmt = mysqli_query($this->conn, $sql_query)) != false) { 
          if($result == false) $result = array();
          while($_row = mysqli_fetch_array($_stmt, MYSQLI_ASSOC)) {
            array_push($result, array_change_key_case($_row, CASE_UPPER));
          }
          mysqli_free_result($_stmt);
        } else {
          $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", mysqli_error($this->conn));
          $result = false;
        }
      }

      return $result;
    }
    
    private function execute_insert_on_remote_db($id) {
      if($this->conn_list == null) $this->connect_to_remote_db();
    }
    
    private function execute_update_on_remote_db($id) {
      if($this->conn_list == null) $this->connect_to_remote_db();
    }
    
    /**
    * Functia de introducere a datelor in baza de date
    *
    * @sql_query  string  Continutul interogarii
    * @return  number  Revine cu ID-ul unic al liniei din baza de date, sau FALSE in caz de eroare
    */
    public function insert($sql_query, $bind_array=array()) {
      $result = false;
      
      if($this->conn != null && $this->conn_type == 1) {
        $_start_date = microtime(true);
        if(($_stmt = @oci_parse($this->conn, $sql_query." returning id into :id")) != false) {          
          if (count($bind_array)){
              foreach ($bind_array as $var => $value) {                  
                  oci_bind_by_name($_stmt, ":{$var}", $bind_array[$var]);
              }
          }
          oci_bind_by_name($_stmt, ':id', $result, 32);          
          if(@oci_execute($_stmt, OCI_DEFAULT) != false) {
          } else {
            $result = false;
            $_error_obj = oci_error($_stmt);            
            $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", htmlentities($_error_obj['message']));
            if($_error_obj["offset"]) {
              $this->last_error_message .= '<br>'.substr ($_error_obj["sqltext"], 0, $_error_obj["offset"]).'*'.substr ($_error_obj["sqltext"], $_error_obj["offset"]);
            }
            $this->plsql("begin log_application_error('interface', '".$this->escape($sql_query)."', '".$_error_obj["code"]."', '".$this->escape($this->last_error_message)."'); end;");
          }
        }    
        $this->log_query($sql_query, $_start_date);
      } else if($this->conn != null && $this->conn_type == 2) {
            $_start_date = microtime(true);
            if (($_stmt = mysqli_query($this->conn, $sql_query)) != false) {
                if (($_local_id = mysqli_insert_id($this->conn)) != 0) {
                    $result = $_local_id;
                }
                else
                    $result = true;
            } else {
                $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", mysqli_error($this->conn));
                $result = false;
            }
            $this->log_query($sql_query, $_start_date);
      }
      return $result;
    }

    /**
    * Functia de actualizare a datelor in baza de date
    *
    * @sql_query  string  Continutul interogarii
    * @return  number  Revine cu ID-ul unic al liniei din baza de date, sau FALSE in caz de eroare
    */
    public function update($sql_query, $bind_array=array()) {
      $result = false;

      if($this->conn != null && $this->conn_type == 1) {
        $_start_date = microtime(true);
        if(($_stmt = @oci_parse($this->conn, $sql_query)) != false) {
          if (count($bind_array)){
              foreach ($bind_array as $var => $value) {                  
                  oci_bind_by_name($_stmt, ":{$var}", $bind_array[$var]);
              }
          }
          if(@oci_execute($_stmt, OCI_DEFAULT) != false) {
            if(($_local_id = mysqli_insert_id($this->conn)) != 0) {
              $result = $this->execute_update_on_remote_db($_local_id);
            } else $result = true;
          } else {
            $result = false;
            $_error_obj = oci_error($_stmt);            
            $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", htmlentities($_error_obj['message']));
            if($_error_obj["offset"]) {
              $this->last_error_message .= '<br>'.substr ($_error_obj["sqltext"], 0, $_error_obj["offset"]).'*'.substr ($_error_obj["sqltext"], $_error_obj["offset"]);
            }
            $this->plsql("begin log_application_error('interface', '".$this->escape($sql_query)."', '".$_error_obj["code"]."', '".$this->escape($this->last_error_message)."'); end;");
          }
        }    
        $this->log_query($sql_query, $_start_date);
      } else if($this->conn != null && $this->conn_type == 2) {
        $_start_date = microtime(true);
        if(($_stmt = mysqli_query($this->conn, $sql_query)) != false) {
          $result = true;
        } else {
          $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", mysqli_error($this->conn));
          $result = false;
        }
        $this->log_query($sql_query, $_start_date);
      }
      return $result;
    }
    
    public function query($sql_query){
        $result = FALSE;
        if($this->conn != null && $this->conn_type == 2) {
            $_start_date = microtime(true);
            if(($_stmt = mysqli_query($this->conn, $sql_query)) != false) {
              $result = true;
            } else {
              $this->last_error_message = str_replace(array("\r\n", "\n", "\r"), "<br>", mysqli_error($this->conn));
              $result = false;
            }
            $this->log_query($sql_query, $_start_date);
        }
        return $result;
    }
    /**
    * Functia de deconectare de la baza de date
    *
    */
    public function disconnect() {
      if($this->conn != null && $this->conn_type == 1) @oci_close($this->conn);
      else if($this->conn != null && $this->conn_type == 2) mysqli_close($this->conn);
      $this->conn = null;
    }
    
    public function getErrorMessage() {
      return $this->last_error_message;
    }
    
    public function commit() {
        $_result = FALSE;
        if ( !empty ($this->conn ) ) {
            if( $this->conn_type == 1) {
                @oci_commit($this->conn);
            }else if ($this->conn_type == 2 ) {
                $_result = mysqli_commit( $this->conn );
            }
        }
        return $_result;
    }

    public function rollback() {
        $_result = FALSE;
        if( !empty ( $this->conn ) ) {
            if( $this->conn_type == 1 ) @oci_rollback($this->conn);
            else if ( $this->conn_type == 2 ){
                $_result = mysqli_rollback( $this->conn );
            }
        }
        
        return $_result;
    }
    
    public function escape($query) {
      if($this->conn_type == 1) return str_replace("'", "''", $query);
      else if($this->conn != null && $this->conn_type == 2) return $this->conn->real_escape_string($query);
    }
    
    
    public function autocommit( $new_value ) {
        $_result = FALSE;
        if ( $this->conn_type == 2 && !empty( $this->conn ) ) {
            $_result = mysqli_autocommit( $this->conn, $new_value );
        }
        
        return $_result;
    }
    
  }

?>
