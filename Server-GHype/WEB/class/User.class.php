<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author scorp
 */
class User extends Application{
    //put your code here
    
    /**
     * FUNCTION: generateUsersListView
     *      Generate a list view with all users
     */
    public function generateUsersListView(){

        $_response = new Response();
        $_response->success = TRUE;
        if ( self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_READ ) ) {
            $_load_container = $this->getRequestValue( 'load_container' );

            $_page = $this->getRequestValue('page');
            $this->assign('page', $_page);
            $this->assign('users', $this->getUsers( $_page ));  
            $_response->data = $this->template->fetch( $_load_container!= 'false' ? 'users/users-container.tpl' : 'users/users-list.tpl');
        } else{
            $_response->success = FALSE;
            $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
        }
        
        $this->setAjaxResponse($_response, TRUE);
    }
    
    /**
     * FUNCTION: getUsers
     *      Returns an array with users
     * @return array
     */
    public function getUsers( $_page=-1 ){
        $_result = array();
        
        $_limit_period = $this->getConfigValue('user_list_period');
        $_limit_start = ($_page -1)*$_limit_period;
        
        if ( self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_READ ) ) {
            
            $_sql_list="SELECT a.id, a.username, a.id AS hkey, v.`n-family` as firstname, v.`n-given` AS lastname, v.email, v.tel as phone, "
                ."GROUP_CONCAT(g.name SEPARATOR ', ') as groups FROM authreg a LEFT JOIN vcard v ON a.`collection-owner`=v.`collection-owner` "
                ."LEFT JOIN org_authreg_group_mn pg_mn ON a.id=pg_mn.authreg_id LEFT JOIN org_group g ON g.id=pg_mn.org_group_id "
                ."GROUP BY a.id order by firstname, lastname "
                .($_page >0 ? "LIMIT $_limit_start,$_limit_period":"");
            $_users = $this->select($_sql_list);
            if ( $_users && count( $_users ) ) {
                $_result = $_users;
            }
        }
        return $_result;
    }
    
    /**
     * FUNCTION: generateUserView
     *  Generate view details for a certain user
     * @param type $id
     */
    public function generateUserView( $id = NULL ){
        $id = $id == NULL ? (int)  $this->getRequestValue('key') : $id;
        $_response =  new Response();
        $_response->success =  TRUE;
        if ( self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_READ ) ) {
            $_user_data = $this->getUserData($id);
            $this->assign('user_data', $_user_data);
            $_response->data = $this->fetch('users/user-view.tpl');
        } else {
            $_response->success = FALSE;
            $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
        }
        $this->setAjaxResponse($_response, TRUE);
    }
    
    /**
     * FUNCTION: getUserData
     *  Returns informations about a certain user
     * @param type $id
     * @return array
     */
    public function getUserData( $id ) {
        $fields = implode("`, `", $this->userFields());
        $_sql = "SELECT `$fields` from authreg a left join vcard v on a.`collection-owner` = v.`collection-owner`"
        ." where id=".$id;

        $_result = $this->select( $_sql );

        if ( $_result && count($_result)===1 ) {
            return $_result[0];
        } else {
            return NULL;
        }
    }
    
    /**
     * FUNCTION: generateUserForm
     *  Generates a form for adding/editing an user
     */
    public function generateUserForm(){
        $_key = $this->getRequestValue('key');
        $_response = new Response();
        $_response->success = TRUE;
        if ( self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_CREATE ) || self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_UPDATE ) ){
            if ( $_key && self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_UPDATE ) ) {
                $_user_data = $this->getUserData($_key);
                if ( $_user_data ) {
                    $this->assign('item_key', $_key);
                    $this->assign('user_data', $_user_data);
                    $this->assign('selected_groups', $this->getUserGroups($_key));
                    $this->assign('selected_role', $this->getUserAcl( $_key ));
                } else {
                    $_response->success =  FALSE;
                    $_response->error_message = $this->getWord('INVALID_DATA');
                }
            }
            $this->generateACLsList();
            $_group_object =  new Group();
            $this->assign('groups', $_group_object->getList() );
            $_response->data = $this->fetch('users/user-form.tpl');
        } else {
            $_response->success = FALSE;
            $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
        }
        
        $this->setAjaxResponse($_response, TRUE);
    }
    
    /**
    *  FUNCTION: userFields
    *   Return array of expected user fields
    *   @param add array of elements to be added to userfFields 
    *   @param ignore = array of elements to be removed from userFields
    *   @param ignore_plus Bool  True to add to ignore False when ignore only specify array
    *   @param translate  fildname with other name ?? needed for api or different reasons
    */
    private function userFields($translate = FALSE, $add = NULL, $ignore=NULL, $ignore_plus = FALSE){

        $_sql_columns = "(SELECT COLUMN_NAME FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_NAME`='authreg')"
        ."  UNION (SELECT COLUMN_NAME FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_NAME`='vcard')";
        $_result_userFields  = $this->select($_sql_columns);

        $userFields = [];
        if($_result_userFields && is_array($_result_userFields) && count($_result_userFields) ) {
            foreach ($_result_userFields as $key => $value) {
                $userFields[] = $value["COLUMN_NAME"];
            }
        }

        if($add and is_array($add) and !empty($add)){
            $userFields = array_merge($userFields,$add);
        }
        
        $ignore_def = ['collection-owner', 'object-sequence', 'fn', 'id', 'mailer', 'n-prefix', 'n-suffix', 'agent-extval', 'sort-string',
         'uid', 'photo-type', 'photo-binval', 'photo-extval', 'logo-type', 'logo-binval', 'logo-extval', 'sound-phonetic', 'sound-binval',
          'sound-extval', 'key-type', 'key-cred', 'rev'];
        if($ignore and is_array($ignore)){  # $ignore can be empty array when we need all fields
            $ignore_def = $ignore_plus === TRUE ? $ignore : array_unique(array_merge($ignore_def, $ignore));
        }

        $userFields = array_unique(array_diff($userFields, $ignore_def));
        if($translate and ! empty($translate)){
            $translateField = function(&$field) use ($translate) {
                $field = array_key_exists($field, $translate) ? $translate[$field] : $field;
            };
            array_walk($userFields, $translateField);
        }
        return $userFields;

    }

    /**
    * FUNCTION: checkUsernameAvailability
    *  Checks if an username is available
    * @param  string $username 
    * @return FALSE, if username is not available
    *         TRUE, otherwise
    */
    private function checkUsernameAvailability( $username ){
        $_sql_select = "SELECT count(*) as USERNAMENUMBER FROM authreg WHERE username='" . $username . "'";
        $_result_select = $this->select( $_sql_select);

        if( $_result_select[0] && is_array($_result_select[0]) && count($_result_select[0]) ){
            if( $_result_select[0]["USERNAMENUMBER"] === "0") {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * FUNCTION: parseUserForm
     *      Validated data from am add/update user
     */
    public function parseUserForm(){
        $_response =  new Response();
        if ( self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_CREATE ) || self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_UPDATE ) ) {
            $_errors = FALSE;
            $userFields = $this->userFields(['n-family'=>'firstname', 'n-given'=>'lastname','tel'=>'phone']);
             
            foreach($userFields as $field){
                ${"_user_$field"} = $this->getValue("user_$field");
            }

            // 2 fields use different rules
            foreach(['username', 'item_key'] as $field){
                ${"_user_$field"} = $this->getValue("$field");

            }

            $_person_id = $this->getValue('item_key');

            /*Checking for errors*/
            if (!$_user_username){
                $_response->addFormError('input[name="username"]', Language::getWord('USERS_ERRORS_USERNAME_IS_MANDATORY'));
                $_errors = TRUE;
            } elseif( !$_person_id && !$this->checkUsernameAvailability( $_user_username ) ){
                $_response->addFormError('input[name="username"]', Language::getWord('USERS_ERRORS_USERNAME_ALREADY_TAKEN'));
                $_errors = TRUE;
            }
            if (!$_user_firstname) {
                $_response->addFormError('input[name="user_firstname"]', Language::getWord('USERS_ERRORS_FISTNAME_IS_MANDATORY'));
                $_errors = TRUE;
            }
            if (!$_user_lastname){
                $_response->addFormError('input[name="user_lastname"]', Language::getWord('USERS_ERRORS_LASTNAME_IS_MANDATORY'));
            }
            if (!$_user_email){
                $_response->addFormError('input[name="user_email"]', Language::getWord('USERS_ERRORS_EMAIL_IS_MANDATORY'));
                $_errors = TRUE;
            }
            elseif(!isValidEmail($_user_email)){
                $_response->addFormError('input[name="user_email"]', Language::getWord('USERS_ERRORS_EMAIL_IS_NOT_VALID'));
                $_errors = TRUE;
            }
            if($_user_role === "role") {
                $_response->addFormError('select[name="user_role"]', Language::getWord('USER_ROLE_IS_MANDATORY'));
                $_errors = TRUE;
            }

            // $_user_status = strtolower($_user_status) === 'active' ? 'yes' : 'no';
            // if ( $_person_id && !self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_UPDATE )){
            if (!self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_UPDATE )){
                $_response->success = FALSE;
                $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
            } else {
                if ( $_errors === FALSE ){
                    $_selected_groups = array();
                    foreach ( $_POST as $_post_name => $_post_value ) {
                        if ( substr($_post_name, 0, 6)== 'group_' ) {
                            $_tmp_data  = explode( '_', $_post_name );
                            if ( count( $_tmp_data ) == 2 ) {
                                $_selected_groups[] = $_tmp_data[ 1 ];
                            }
                        }
                    }
                    $_selected_acl = $_POST['user_role'];

                    $user=[];
                    $fieldTranslate = ['firstname'=>'n-family','lastname'=>'n-given','phone'=>'tel'];
                    foreach($userFields as $field){
                        $dbfield = array_key_exists($field, $fieldTranslate) ? $fieldTranslate[$field] : $field;
                        if(array_key_exists($dbfield, $user)) continue;  # why dubs ??
                        $user[$dbfield] = ${"_user_$field"};
                    }
                    $_user_id = $this->saveUser( $_person_id, $user );
                    if ( $_user_id ) {
                        $this->saveUserACL( $_user_id, $_selected_acl);
                        $this->saveUserGroups( $_user_id, $_selected_groups );
                        $_response->success = TRUE;
                    }
                }
            }
        } else {
            $_response->success = FALSE;
            $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
        }
        $this->setAjaxResponse($_response, TRUE);
    }

    /**
     * Query builder
     * @param array $data
     */
    private function createUserInsertQuery($data){     
        $keys = [];
        $values = [];
        foreach($data as $key=>$value){
            $keys[] = "`$key`";
            $value = $this->escape($value);
            $values[] = $value ? "'$value'" : 'NULL';
        }
        $return = [implode(', ', $keys), implode(", ", $values)];
        return ($return);
    }

    /**
    * FUNCTION: insertQuery
    *    Returns TRUE if a new user is inserted succesfully, FALSE otherwise
    * @param table_name = name of the table
    * @param data = an array with data
    */
    private function insertQuery($table_name,$data)
    {
        //get columns name from table
        $_sql_insert = "SELECT COLUMN_NAME FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_NAME`='" . $table_name . "'";
        $_result = $this->select($_sql_insert); //array of column names

        if ( $_result && is_array( $_result) && count( $_result) ){ 
            //create an array with keys from $_result and values from $data
            $fields = [];
            $realm = $this->getConfigValue('realm');
            foreach ($_result as $key => $value) 
            {
                $column_name = $value["COLUMN_NAME"];
                if($column_name === 'collection-owner') {
                    $fields[ $column_name ] = $data["username"] . "@" . $realm;
                } elseif($column_name === 'realm') {
                    $fields[ $column_name ] = $realm;
                } elseif( !isset($data[$column_name]) ) {
                    $fields[ $column_name ] = NULL;
                } else {
                    $fields[ $column_name ] = $data[ $column_name ];
                }
            }
            list($keys, $values) = $this->createUserInsertQuery($fields);
            $_sql_insert = "INSERT INTO " . $table_name . "(" . $keys . ") VALUES(". $values .")";
            return $this->query($_sql_insert);
        } else {
            return FALSE;
        }
    }

    protected function saveUser($id, $data){
        $_result = TRUE;
        
        if (!$id) {
            $_sql_query = 'LOCK TABLES authreg WRITE,vcard WRITE';
            if( $this->update( $_sql_query )) {
                if ( !isset($data['password']) || ( isset($data['password']) && empty($data['password'])) ) {
                    $password =  create_password();
                    $salt = crypt($password); //in PHP version >= 5.6.0 raises E_NOTICE security warning if salt is omitted. 
                    $crypt_password = crypt($password, $salt);
                    $data['password'] = $crypt_password;

                }

                $_result_insert_authreg = $this->insertQuery("authreg",$data);
                
                //don't insert in vcard if the user already exists in authreg
                if($_result_insert_authreg) {
                    $_result_insert_vcard = $this->insertQuery("vcard",$data);
                }

                if ( $_result_insert_authreg && $_result_insert_vcard ) {
                    $realm = $this->getConfigValue('realm');
                    $_sql = "SELECT id FROM authreg WHERE `collection-owner`='" . $data["username"] . "@" . $realm . "'";
                    $_result_sql = $this->select($_sql);
                    $_result = $_result_sql[0]['ID'];

                    $_email_data =  new stdClass();
                    $_email_data->send_to = $data['email'];
                    $_email_data->subject = 'Login informations';
                    $this->assign('username', $data['username']);
                    $this->assign('password', $password);
                    $_email_data->message = $this->fetch('emails/new_account.tpl');
                    $this->sendEmail($_email_data);
                } else {
                    $_result = NULL;
                }

                $_sql_query = 'unlock tables';
                $this->update( $_sql_query );
            }
        } else {
            $_ignored_fields = ['username','realm', 'password']; //fields ignored at update

            $_sql_col_owner = "SELECT `collection-owner` FROM authreg WHERE id=".$id;
            $_result_col_owner = $this->select($_sql_col_owner);
            $_col_owner_str = "'" . $_result_col_owner[0]['COLLECTION-OWNER'] . "'";

            $_sql_update = 'UPDATE vcard v set ';
            $_i = 0;
            $_count = count($data);
            if ( $_count ) {
                foreach ( $data as $_field=>$_value ) {
                    $_i++;
                    if( !in_array( $_field,$_ignored_fields ) ) {
                        $_sql_update .= "`{$_field}`='{$_value}'";
                        if ($_i < $_count){
                            $_sql_update .= ",";
                        }
                    }
                }
                $_sql_update .= ' where v.`collection-owner`='.$_col_owner_str;
                $_result = $this->update($_sql_update);
                if ( $_result ) {
                    $_result = $id;
                }
            }
        }
        return $_result;
    }

    /**
     * FUNCTION: saveUserACL
     *  Saves ACL of an user
     * @param int $person_id = person id
     * @param int $acl_name = acl name
     * @return BOOL
     */
    public function saveUserACL( $person_id, $acl_name ) {
        $_result = FALSE;
        $_sql_select = "SELECT id FROM org_acl where name='" . $acl_name . "'";
        $_result_select = $this->select( $_sql_select );

        if( $_result_select && count($_result_select) === 1 ) {
            $_sql = "SELECT org_acl_id FROM org_acl_authreg_mn WHERE authreg_id=" . $person_id ;
            $_result = $this->select($_sql);

            if( $_result && count($_result) ) {
                $_sql_delete = "DELETE FROM org_acl_authreg_mn WHERE authreg_id=" . $person_id;
                $this->query($_sql_delete);
            }
            $acl_id = $_result_select[0]['ID'];
            $_sql_insert = "INSERT INTO org_acl_authreg_mn(org_acl_id, authreg_id) VALUES(" . $acl_id . "," . $person_id . ")";
            $_result = $this->query($_sql_insert);
        }
        return $_result;
    }

    /**
     * FUNCTION: saveUserGroups
     *  Updates the groups list associated to an user
     * @param int $person_id
     * @param array $data
     * @return BOOL
     */
    public function saveUserGroups( $person_id, $data ) {
        $_result = FALSE;
       
        if ( is_array( $data ) ) {
            if (count( $data ) ) {
                $_sql_selected_groups = 'SELECT id, org_group_id FROM org_authreg_group_mn WHERE authreg_id='.$person_id;
                $_result_groups = $this->select( $_sql_selected_groups );
                if ( $_result_groups && count( $_result_groups ) ) {
                    $_delete_groups = [];
                    foreach ( $_result_groups as $_tmp_group ) {
                            if ( !in_array($_tmp_group['ORG_GROUP_ID'], $data) ){ //db item is not in new list, so we delete it
                            $_delete_groups[] = $_tmp_group[ 'ID' ];
                        }else{//  is already in the DB                       
                            unset($data[array_search($_tmp_group['ORG_GROUP_ID'], $data)]);
                        }
                    }
                   
                    if ( count( $_delete_groups ) ) {
                        $_sql_delete = 'DELETE FROM org_authreg_group_mn WHERE authreg_id='.$person_id.' AND id in(' . implode(',', $_delete_groups ) . ')';
                        $_result = $this->query( $_sql_delete );
                    }
                }
                
                if ( count( $data )){
                    foreach ($data as $_new_item ){
                        if ( $_new_item ) {
                            $_sql_insert = 'INSERT INTO org_authreg_group_mn(org_group_id, authreg_id) VALUES('.$_new_item.', '. $person_id .')';
                            $_result = $this->query( $_sql_insert );
                        }
                    }
                }
            } else { //delete all groups items for selected user
                $_sql_delete = 'DELETE FROM org_authreg_group_mn WHERE authreg_id='.$person_id;
                $_result = $this->query( $_sql_delete );
            }
        }
        return $_result;
    }
    /**
     * FUNCTION: getUserGroups
     *      return a list with groups associated to an user
     * @param int $person_id
     */
    public function getUserGroups( $person_id ) {
        $_result = array();
        $_sql_selected_groups = 'SELECT org_group_id FROM org_authreg_group_mn WHERE authreg_id='.$person_id;
        $_selected_groups = $this->select( $_sql_selected_groups );
        if ( $_selected_groups && count($_selected_groups)){
            foreach ($_selected_groups as $_group){
                $_result[] = $_group[ 'ORG_GROUP_ID' ];
            }
        }
        
        return $_result;
    }
    
    public static function getCurrentUserId(){
        return self::getSessionValue("PERSON_ID", false);
    }
    
    public function deleteConfirmation(){
        $_response =  new Response();
        
        if ( self::checkAccess( 'ACL_ACCESS_USERS', self::ACL_DELETE ) ) {
            $_key = $this->getRequestValue('key');
            if ( $this->autocommit( FALSE ) ) { //set autocommit to false 
                $_errors = FALSE;
                $_sql_delete_user = "DELETE a,v FROM authreg a LEFT JOIN vcard v ON a.`collection-owner`=v.`collection-owner` WHERE a.id='".$_key."'"; 
                $_result_delete_user = $this->query( $_sql_delete_user);
                
                if ( $_result_delete_user ){
                    $_sql_delete_groups = 'DELETE FROM org_authreg_group_mn WHERE authreg_id='.$_key;
                    $_result_delete_groups = $this->query( $_sql_delete_groups );
                    if ( $_result_delete_groups ) {
                        $_sql_delete_acl = 'DELETE FROM org_acl_authreg_mn WHERE authreg_id='.$_key;
                        $_result_delete_acl = $this->query($_sql_delete_acl);
                        if ( !$_result_delete_acl ) {
                            $_errors = TRUE;
                        }
                    } else {
                        $_errors = TRUE;
                    }
                } else {
                    $_errors = TRUE;
                }
                
                if ( $_errors ) {
                    $this->rollback();
                } else {
                    $this->commit();
                    $_response->success = TRUE;
                }
                $this->autocommit( TRUE );//change back automit as it was
            }
        }
        $this->setAjaxResponse( $_response, TRUE );
    }
    
    public function getMyAccountPage() {
        $_person_id = $this->getSessionValue( 'PERSON_ID' );
        $_sql = "SELECT v.`n-family` as firstname, v.`n-given` as lastname, a.username, a.password, v.email, v.tel as phone"
        ." FROM authreg a LEFT JOIN vcard v on a.`collection-owner`=v.`collection-owner`where a.id='".$_person_id."'";
        $_result = $this->select( $_sql );
        
        if ( $_result && is_array($_result) && count( $_result ) ) {
            $_result = $_result[0];
        }
        $this->assign('user_data', $_result);
        $this->template_file = 'session/my-account.tpl';
    }
    
    public function changePassword(){
        $_response =  new Response();
        $_errors = [];
        $_old_password = trim($this->getRequestValue( 'old_password' ));
        $_new_password = trim($this->getRequestValue( 'new_password' ));
        $_new_password_repeat = trim($this->getRequestValue( 'new_password_repeat' ));
        if ( !$_old_password ){
            $_errors[] = Language::getWord('OLD_PASSWORD_REQUIRED');
        }
        if ( !$_new_password || !$_new_password_repeat || ( $_new_password!== $_new_password_repeat ) ) {
            $_errors[] = Language::getWord( 'NEW_PASSWORD_ERROR' );
        }
        
        if ( count($_errors) ===0 ) {
            $_person_id = $this->getSessionValue( 'PERSON_ID' );
            $_sql_password = "SELECT password from authreg where id=" . $_person_id;
            $_result_password = $this->select( $_sql_password );
            if ( $_result_password && is_array( $_result_password ) && count( $_result_password ) ){
                $_result_password = $_result_password[0];
                $salt = "\$6\$rounds=50000$";
                $salter = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ./";
                srand(time(0));
                for($i=0;$i<22;$i++){
                    $salt[16+$i]=$salter[rand()%64];
                }
                $salt[38]='\0';
                $_old_password_crypt = crypt($_old_password, $_result_password[ 'PASSWORD' ]);
                $_new_password_crypt = crypt($_new_password, $salt);
                //if ( crypt($_result_password['PASSWORD'], $_result_password[ 'PASSWORD' ]) === $_old_password_crypt ){ 
                // alternative_start
                // $_new_password_crypt = password_hash($_new_password,PASSWORD_BCRYPT,['cost'=>9]);
                // if(password_verify($_old_password,$_result_password['PASSWORD'])) {
                // alternative_end
                if ($_old_password_crypt === $_result_password['PASSWORD']){    
                    // $_sql_update = "UPDATE authreg set password='".$_new_password_crypt."'where username='".$_person_id."'";
                    $_sql_update = "UPDATE authreg set password='".$_new_password_crypt."'where id=" . $_person_id;
                    $_response->success = $this->query( $_sql_update );
                }else{
                    $_response->error_message = Language::getWord('WRONG_PASSWORD');
                }
            }
        } else {
            $_response->error_message = implode('; ', $_errors);
        }
        /*
         $password =  create_password();
                    $salt = crypt($password);
                    $crypt_password = crypt($password, $salt);
         */
        $this->setAjaxResponse($_response, TRUE);
    }

    /**
     * FUNCTION: getACL
     *      Returns an array with ACLs id and name
     * @return array
     */
    public function getACLs( ) {
        $_result = array();
            $_sql_list="SELECT name FROM org_acl ORDER BY name";
            $_ACLs = $this->select($_sql_list);
            if ( $_ACLs && count( $_ACLs ) ) {
                $_result = $_ACLs;
            }
        return $_result;
    }

    /**
     * FUNCTION: generateAcls
     *      Return a list with all ACL names
     */
    public function generateACLsList(){
        $acls = $this->getACLs();
        $_result = array();
        foreach ($acls as $key => $value) {
            $_result[] = $value['NAME'];
        }
        $this->assign('roles', $_result);
    }
}
