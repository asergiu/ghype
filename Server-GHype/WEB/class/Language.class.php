<?PHP

class Language extends DomDocument {
    protected $dir = null;
    protected static $words = null;
    protected static $template = null;
    private static $loaded_files = array();

    
    public function extract_keys($xp, $query) {
        $array = array();
        if ($xp !== false && $query !== false) {
            if (($result = $xp->query($query)) !== false) {
                foreach ($result as $o) {
                    $attributes = $o->attributes;
                    $key = '';
                    $value = '';
                    foreach ($attributes as $attribute) {
                        if ($attribute->nodeName == "key")
                            $key = $attribute->nodeValue;
                        if ($attribute->nodeName == "value")
                            $value = $attribute->nodeValue;
                    }
                    if ($key) {
                        $value = str_replace("//#", "&#", $value);
                        $array[$key] = $value;
                    }
                }
            }
        }
        return $array;
    }

    public function __construct() {
        $settings = Object::getConfig();
        $template = Object::getTemplate();
        if ((array_key_exists('default', $settings) && isSet($settings['default']))) {
            if ((array_key_exists('language', $settings['default']) && isSet($settings['default']['language']))) {
                $_selected_language = $settings['default']['language'];
            }
        }
        parent::__construct($_selected_language);
        self::$template = $template;
        $template->assign("default_language", $_selected_language);
        $this->dir = ROOT_PATH . DIRECTORY_SEPARATOR . "lang" . DIRECTORY_SEPARATOR . $_selected_language . DIRECTORY_SEPARATOR;
    }

    public function loadFile($file, $assign_to_template = false, $template = null) {
        //load a language file is isn't already loaded
        //Application::deleteFromCache($file);
        $_words = Application::getFromCache($file);
        if ($_words){
            if (self::$words == false)
                self::$words = $_words;
            else
                self::$words = array_merge(self::$words, $_words);
        }else if (!in_array($file, self::$loaded_files) && ($this->load($this->dir . $file) !== false ) ) {
            if (($_xp = new domxpath($this)) != null) {
                if (($_words = $this->extract_keys($_xp, "/definitions/words/word")) != null) {
                    if ($assign_to_template && $template != null) {
                        if (is_array($_words))
                            while (list($KEY, $VALUE) = each($_words)) {
                                $template->assign("LANG_" . $KEY, $VALUE);
                            }
                    } else if ($assign_to_template && self::$template != null) {
                        if (is_array($_words))
                            while (list($KEY, $VALUE) = each($_words)) {
                                self::$template->assign("LANG_" . $KEY, $VALUE);
                            }
                    }
                }
                Application::storeInCache($file, $_words);
                if (self::$words == false)
                    self::$words = $_words;
                else
                    self::$words = array_merge(self::$words, $_words);
            }
        }
    }

    /**
     * FUNCTION: getWord
     *      Translate a word if is defined in a languafe file.
     * 
     * @param type $key
     * @param type $force
     * @return boolean or string
     */
    public static function getWord($key, $force = true) {
        if (strpos($key, "LANG_") !== false)
            $key = substr($key, 4);
        if (isSet(self::$words[$key])){
            $_word = self::$words[$key];
            $_word = str_replace('\n', '<br>', $_word);
            return $_word;
        }
        else if (!$force)
            return false;
        else
            return $key;
    }

}

?>
