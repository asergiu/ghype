<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Response
 *
 * @author scorp
 */
class Response {
    public $success;
    public $error_message;
    public $form_errors = [];
    public $data;
    
    public function __construct() {
        $this->success = FALSE;
    }
    
    public function addFormError( $field_query, $error ){
        $this->form_errors[] = [
            'error_field'   => $field_query,
            'error_text'    => $error
        ];
    }
}
