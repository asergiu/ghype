<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ACL
 *
 * @author scorp
 */
class ACL extends Application{
    
    public function listACL(){       
        $_load_container = $this->getRequestValue( 'load_container' );
        $_response = new Response();
        $_response->success = TRUE;
        
        $_sql_query = 'select id, id as hkey, name, description from org_acl order by name';
        $_ACLs = $this->select($_sql_query);
        $this->assign('items', $_ACLs);
        $_response->data = $this->template->fetch( $_load_container!= 'false' ? 'acl/acl-container.tpl' : 'acl/acl-list.tpl');
        $this->setAjaxResponse($_response, TRUE);
    }
    
    protected function getACOs( $acl_id, $access_type = NULL, $options ) {
        $_result  = FALSE;
        if (self::checkAccess("ACL_MODULE_SECURITY",  self::ACL_READ)){ //testam daca are drept de citire
            $_acl_id = (int)$this->getValue('key');
            $_access = $this->getValue('access');
            $words = array();
            $errors = array();

            /*Checking for errors*/
            if (!$_acl_id) $errors[]        = $words["SECURITY_MISSING_ACL_ID"];
            //if (!$_access) $errors[] = $words['SECURITY_ACCESS_TYPE_IS_MANDATORY'];
            //if ($_access && !in_array($_access, array('permit','deny','restricted'))) $errors[] = $words['SECURITY_ACCESS_TYPE_IS_INVALID'];

            if (!count($errors)){
                $where = array();
                $acces_type = NULL;
                switch ($_access){
                    case 'permit' :
                        $acces_type = "ACL_ACCESS_TYPE_PERMIT";
                        break;
                    case 'deny':
                        $acces_type = "ACL_ACCESS_TYPE_DENY";
                        break;
                     case 'restricted':
                         $acces_type = "ACL_ACCESS_TYPE_RESTRICTED_TO";
                         break;
                }
                //$where[] = "access_type='{$acces_type}'";
                $where[] = "org_acl_id={$_acl_id}";
                $where[] = "aamn.crud>0";
                //$response = $this->getList(array("ws.value as name","aco.name as aconame","aamn.crud"), array("org_aco as aco","left join org_acl_aco_mn as aamn on aamn.org_aco_id=aco.id","left join words as ws on aco.name=ws.identifier"), $where, array("aco.parent_id"));
                $_sql = 'SELECT aco.name as aconame,aamn.crud, aamn.access_type FROM org_aco as aco left join org_acl_aco_mn as aamn on aamn.org_aco_id=aco.id '.(count($where) ? ' WHERE '.implode(' AND ', $where) : '' ).' ORDER BY aco.parent_id';
                $_result = $this->select($_sql);
                if ( $options && is_array($options) && isset($options['access_group']) && $options['access_group']){
                    $_tmp_result = array();
                }
                if (is_array($_result) && count($_result)){
                    foreach ($_result as &$access_item){
                        $access_item['RIGHT'] = ($access_item['CRUD'] & 8 ?'c' :'').($access_item['CRUD'] & 4 ?'r' :'').($access_item['CRUD'] &2  ?'u' :'').($access_item['CRUD'] & 1 ?'d' :'');
                        if ( $options && is_array($options) && isset($options['access_group']) && $options['access_group']){
                            $_tmp_result[$access_item['ACCESS_TYPE']][] = $access_item;
                        }
                    }
                }
                if ( isset($_tmp_result)){
                    $_result = $_tmp_result;
                }
            }
        }
        return $_result;
    }
    
    /**
     * FUNCTION: getACOsTree
     *      generate a tree with ACOs
     * @param int $parent_id
     * @return array
     */
    public function getACOsTree( $parent_id = NULL ) {
        $_cache_key = 'aco_tree' . ($parent_id ? '_'.$parent_id : '');
        //$this->deleteFromCache($_cache_key);        //cod temporar
        $_response = $this->getFromCache( $_cache_key );
        if ( empty($_response ) ) {
            $_response = array();
            $_where = array();
            if ( $parent_id ){
                $_where[] = 'parent_id='.$parent_id;
            } else{
                $_where[] = 'parent_id is null';
            }
            $_sql = 'SELECT id, id as hkey, parent_id, name, crud_mask from org_aco '.(count($_where) ? ' WHERE '.implode(' AND ', $_where) :'').' order by parent_id, name';
            $_result= $this->select($_sql);
            if ( $_result && count($_result)){
                foreach ($_result as &$access_item){
                    $access_item['RIGHT'] = ($access_item['CRUD_MASK'] & 8 ?'c' :'').($access_item['CRUD_MASK'] & 4 ?'r' :'').($access_item['CRUD_MASK'] &2  ?'u' :'').($access_item['CRUD_MASK'] & 1 ?'d' :'');
                    $access_item['CHILDRENS'] = $this->getACOsTree($access_item['ID']);
                }
                $_response = $_result;
            }
            $this->storeInCache($_cache_key, $_response);
        }
        return $_response;
    }
    
    public function generateACLView( $id=NULL ){
        $id = $id == NULL ? (int)  $this->getRequestValue('key') : $id;
        $_response =  new Response();
        $_response->success =  TRUE;
        $_ACOs= $this->getACOs($id, NULL, ['access_group'=>TRUE]);
        $this->assign('aco_list', $_ACOs);
        $_response->data = $this->fetch('acl/acl-view.tpl');
        $this->setAjaxResponse($_response, TRUE);
    }
    
    /**
     * FUNCTION genetateAclForm
     *  Generate a form dor add/edit an ACL item
     */
    public function generateACLForm() {
        $_key = $this->getRequestValue('key');
        $_response = new Response();
        $_response->success = TRUE;
        if ( $_key ) {
            $_acl_data = $this->getACLData($_key);
            if ( $_acl_data ) {
                $this->assign('item_key', $_key);
                $this->assign('acl_data', $_acl_data);     
                $this->assign('selected_ACOs', $this->getAclACOS( $_key ) );
                $this->assign('selected_persons', $this->getACLPersons( $_key ) );
            } else {
                $_response->success =  FALSE;
                $_response->error_message = $this->getWord('INVALID_DATA');
            }
        }
        $_user_instance = new User();
        $_users = $_user_instance->getUsers();
        $this->assign('aco_tree', $this->getACOsTree());
        $this->assign('users', $_users);
        $_response->data = $this->fetch('acl/acl-form.tpl');
        $this->setAjaxResponse($_response, TRUE);
    }
    
    /**
     * FUNCTION: getAclData
     *  Returns informations about a certain ACL
     * @param type $id
     * @return array
     */
    public function getACLData( $id ) {
        $_sql = 'SELECT name, description from org_acl where id='.$id;
        $_result = $this->select( $_sql );
        if ( $_result && count($_result)===1 ) {
            return $_result[0];
        } else {
            return NULL;
        }
    }
    
    /**
     * FUNCTION: parseACLForm
     *      Validated data from am add/update ACL
     */
    public function parseACLForm(){
        $_response =  new Response();
        $_errors =FALSE;

        $_acl_name        = $this->getValue('acl_name');
        $_acl_description = $this->getValue('acl_description');
        $_acl_id          = $this->getValue('item_key');
        /*Checking for errors*/
        if (!$_acl_name){
            $_response->addFormError('input[name="acl_name"]', $this->getWord('ACL_ERRORS_ACL_NAME_IS_MANDATORY'));
            $_errors = TRUE;
        }
        
        if ( !$_errors ) {
            $_acl_id = $this->saveACL($_acl_id, [
                'name'          => $_acl_name,
                'description'   => $_acl_description
            ]);
            if ( $_acl_id ){
                //store persons and ACO for curent ACL
                $_selected_ACOs = array();
                $_selected_persons = array();
                $_permissions = ['c','r','u','d'];
                if ( count($_POST)){
                    foreach ($_POST as $_post_name => $_post_value){
                        if (substr($_post_name, 0, 7)== 'permit_'){
                            $_tmp_data = explode('_', $_post_name);
                            $_tmp_key = implode( '_', [ 'permit', $_acl_id, $_tmp_data[ 2 ]]);
                            if ( count( $_tmp_data ) == 3 && !isset( $_selected_ACOs[ $_tmp_key ] ) ) {
                                $_selected_ACOs[ $_tmp_key ] = [
                                    'type'  => 'permit',
                                    'id'    => $_tmp_data[ 2 ],
                                    'crud'  => ''
                                ];                                
                            }
                        }else if ( substr($_post_name, 0, 5)== 'deny_' ){
                            $_tmp_data = explode('_', $_post_name);
                            $_tmp_key = implode( '_', [ 'deny', $_acl_id, $_tmp_data[ 2 ]]);
                            if ( count( $_tmp_data ) == 3 && !isset( $_selected_ACOs[ $_tmp_key ] ) ) {
                                $_selected_ACOs[ $_tmp_key ] = [
                                    'type' => 'deny',
                                    'id'    => $_tmp_data[ 2 ],
                                    'crud'  => ''
                                ];                                
                            }
                        } else if ( substr( $_post_name, 0, 11 ) == 'acl_person_' ) {
                            $_tmp_data = explode( 'acl_person_', $_post_name);
                            if ( count( $_tmp_data ) == 2 && $_post_value === 'on' ) {
                                $_selected_persons[] = $_tmp_data[1];
                            }
                        }                        
                    }
                    
                    if ( count( $_selected_ACOs ) ) {
                        $_total_permissions = count( $_permissions );
                        foreach ( $_selected_ACOs as &$_aco ) {
                            foreach ( $_permissions as $_k => $_permission ) {
                                $_post_key = implode('_', [ $_aco['type'], $_permission, $_aco['id'] ] ); 
                                if ( isset( $_POST[ $_post_key ] ) && $_POST[ $_post_key ] == 'on'  ) {
                                    $_aco[ 'crud' ] .= '1';
                                } else {
                                    $_aco[ 'crud' ] .= '0';
                                }
                                if ( ($_k + 1) == $_total_permissions ){
                                    $_aco[ 'crud' ] = bindec( $_aco[ 'crud' ] );
                                }
                            }
                        }
                    }
                }
                $this->updateACOs( $_acl_id, $_selected_ACOs );
                $this->updatePersons( $_acl_id, $_selected_persons );
                $_response->success = TRUE;
            }
        }else {
            $_response->error_message = '';
        }
        $this->setAjaxResponse($_response, TRUE);
    }

    /**
     * FUNCTION saveACL
     *      Add a new ACL or update an existing ACL
     * @param int $id
     * @param array $data
     * @return type
     */
    protected function saveACL($id, $data){
        $_result = FALSE;
        
        if (!$id) {
            $_sql_insert = 'Insert into org_acl(name, description) '
                    . 'values(\'' . $this->escape($data['name']) . '\',\'' . $this->escape($data['description']). '\')' ;
            $_result = $this->insert($_sql_insert);
        } else {
            $_sql_update = 'UPDATE org_acl set ';
            $_i = 0;
            $_count = count($data);
            if ( $_count ) {
                foreach ( $data as $_field=>$_value ) {
                    $_i++;
                    $_sql_update .= "{$_field}='{$_value}'";
                    if ($_i < $_count){
                        $_sql_update .= ",";
                    }
                }
                $_sql_update .= ' where id='.$id;
                $_result = $this->update($_sql_update);
                if ( $_result ) {
                    $_result = $id;
                }
            }
        }
        return $_result;
    }
    
    /**
     * FUNCTION getAclACOS
     *  
     * @param int $acl_id
     * @return array
     */
    public function getAclACOS( $acl_id){
        $_result = array();
        $_sql = 'SELECT org_aco_id aco_id, access_type, crud FROM org_acl_aco_mn WHERE org_acl_id='.$acl_id;
        $_selected_ACOs = $this->select( $_sql );
        if ( $_selected_ACOs && count($_selected_ACOs)){
            foreach ($_selected_ACOs as $_aco){
                $_result[ $_aco[ 'ACO_ID' ]] = $_aco;
            }
        }
        return $_result;
    }
    
    /**
     * FUNCTION: getACLPersons
     *      return a list with persons associated to an ACO
     * @param int $acl_id
     */
    public function getACLPersons( $acl_id ) {
        $_result = array();
        $_sql = 'SELECT authreg_id from org_acl_authreg_mn where org_acl_id=' . $acl_id;
        $_selected_persons = $this->select( $_sql );
        if ( $_selected_persons && count($_selected_persons)){
            foreach ($_selected_persons as $_person){
                $_result[] = $_person[ 'AUTHREG_ID' ];
            }
        }
        
        return $_result;
    }
    
    /**
     *  FUNCTION:updateACOs
     * 
     *  Update the list of ACOs for selected ACL
     * @param type $acl_id
     * @param type $data
     * @return type
     */
    protected function updateACOs( $acl_id, $data = [] ){
        $_result = FALSE;
        
        if ( is_array( $data ) ){
            if (count($data)){
                $_sql_selected_ACOS = 'SELECT id, org_aco_id, access_type, crud FROM org_acl_aco_mn WHERE org_acl_id='.$acl_id;
                $_result_ACOs = $this->select( $_sql_selected_ACOS );
                if ( $_result_ACOs && count( $_result_ACOs ) ) {
                    $_delete_ACOs   = [];
                    foreach ( $_result_ACOs as $_tmp_aco ) {
                        $_tmp_key = implode('_', [$_tmp_aco[ 'ACCESS_TYPE'], $acl_id, $_tmp_aco[ 'ORG_ACO_ID' ] ] );
                        if ( !isset( $data[ $_tmp_key ] ) ){ //db item is not in new list, so we delete it
                            $_delete_ACOs[] = $_tmp_aco[ 'ID' ];
                            unset( $data[ $_tmp_key] ); 
                        } else{
                            if( (int)$_tmp_aco[ 'CRUD' ] != (int)$data[ $_tmp_key ]['crud'] ) { //update item
                                $_sql_update = 'UPDATE org_acl_aco_mn set crud='.( (int) $data[ $_tmp_key ]['crud'] ).' WHERE id=' . $_tmp_aco[ 'ID' ];
                                $this->update( $_sql_update );
                            }
                            unset( $data[ $_tmp_key] ); 
                        }
                       
                    }
                   
                    if ( count( $_delete_ACOs ) ) {
                        $_sql_delete = 'DELETE FROM org_acl_aco_mn WHERE org_acl_id='.$acl_id.' AND id in(' . implode(',', $_delete_ACOs ) . ')';
                        $_result = $this->query( $_sql_delete );
                    }
                }
                if ( count( $data )){
                    foreach ($data as $_new_item ){
                        if ( $_new_item[ 'id' ] ) {
                            $_sql_insert = 'INSERT INTO org_acl_aco_mn(org_acl_id, org_aco_id, access_type, crud) VALUES('.$acl_id.', '. $_new_item[ 'id' ] .', \''. $_new_item['type'].'\', '. $_new_item['crud'] .')';
                            $_result = $this->insert( $_sql_insert );
                        }
                    }
                }
            } else { //delete all ACO items for selected ACL
                $_sql_delete = 'DELETE FROM org_acl_aco_mn WHERE org_acl_id='.$acl_id;
                $_result = $this->query( $_sql_delete );
            }
        }
        
        return $_result;
    }
    
    protected function updatePersons( $acl_id, $data ){
        $_result = FALSE;
        if ( is_array( $data ) ){
            if (count( $data ) ) {
                $_sql_selected_persons = 'SELECT id, authreg_id FROM org_acl_authreg_mn WHERE org_acl_id='.$acl_id;
                $_result_persons = $this->select( $_sql_selected_persons );
                if ( $_result_persons && count( $_result_persons ) ) {
                    $_delete_persons = [];
                    foreach ( $_result_persons as $_tmp_person ) {
                        if ( !in_array($_tmp_person['AUTHREG_ID'], $data) ){ //db item is not in new list, so we delete it    
                            $_delete_persons[] = $_tmp_person[ 'ID' ];
                        } else {// else : is already in the DB
                            unset($data[array_search($_tmp_person['AUTHREG_ID'], $data)]);
                        }  
                    }
                   
                    if ( count( $_delete_persons ) ) {
                        $_sql_delete = 'DELETE FROM org_acl_authreg_mn WHERE org_acl_id='.$acl_id.' AND id in(' . implode(',', $_delete_persons ) . ')';
                        $_result = $this->query( $_sql_delete );
                    }
                }
                if ( count( $data )){
                    foreach ($data as $_new_item ){
                        if ( $_new_item ) {
                            $_sql_insert = 'INSERT INTO org_acl_authreg_mn(org_acl_id, authreg_id) VALUES('.$acl_id.', '. $_new_item .')';
                            $this->insert( $_sql_insert );
                        }
                    }
                }
            } else { //delete all person items for selected ACL
                $_sql_delete = 'DELETE FROM org_acl_authreg_mn WHERE org_acl_id='.$acl_id;
                $_result = $this->query( $_sql_delete );
            }
        }
        
        return $_result;
    }
}
