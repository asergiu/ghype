<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Group
 *
 * @author scorp
 */
class Group extends Application{

    /**
     * FUNCTION generateListGroupsView
     *      Generates a view containing list groups
     *      
     */
    public function generateListGroupsView(){       
        $_response = new Response();
        $_response->success = TRUE;
        
        //checking for read access
        if ( self::checkAccess( 'ACL_ACCESS_GROUPS', self::ACL_READ ) ) {
            $_load_container = $this->getRequestValue( 'load_container' );

            $_groups = $this->getList();
            $this->assign('groups', $_groups);
            $_response->data = $this->template->fetch( $_load_container!= 'false' ? 'groups/groups-container.tpl' : 'groups/groups-list.tpl');
        } else{
            $_response->success = FALSE;
            $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
        }
        $this->setAjaxResponse($_response, TRUE);
    }
    
    /**
     * FUNCTION: getList
     *      Return an array with groups
     * @return type
     */
    public function getList(){
        $_list = array();
        $_sql_query = 'select id, id as hkey, name, description, active from org_group order by name';
        $_result = $this->select($_sql_query);
        if ( $_result ) {
            $_list = $_result;
        }
        return $_list;
    }
    /**
     * FUNCTION: generateGroupView
     *  Generate view details for a certain group
     * @param type $id
     */
    public function generateGroupView( $id = NULL ){
        $_response =  new Response();
        $_response->success =  TRUE;
        
        if ( self::checkAccess( 'ACL_ACCESS_GROUPS', self::ACL_READ ) ) {
            $id = $id == NULL ? (int)  $this->getRequestValue('key') : $id;
            $this->assign('users', $this->getGroupUsers($id));
            $_response->data = $this->fetch('groups/group-view.tpl');
        } else {
            $_response->success = FALSE;
            $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
        }
        $this->setAjaxResponse($_response, TRUE);
    }
    
    /**
     * FUNCTION: getGroupUsers
     *      Return a lista with users from a selected group
     * @param int $group_id
     * @return array
     */
    protected function getGroupUsers( $group_id ){
        $_sql = "SELECT v.`n-family` as firstname, v.`n-given` as lastname, a.id as person_hkey FROM authreg a LEFT JOIN vcard v ON"
        ." a.`collection-owner`=v.`collection-owner` LEFT JOIN org_authreg_group_mn ag_mn on a.id=ag_mn.authreg_id where ag_mn.org_group_id ="
        . $group_id;

        $_result = $this->select($_sql);
        if (!is_array($_result)){
            $_result = array();
        }
        return $_result;
    }
    
    /**
     * FUNCTION: generateGroupForm
     *      generates a form used for adding/editing a group
     */
    public function generateGroupForm(){
        $_key = $this->getRequestValue('key');
        $_response = new Response();
        $_response->success = TRUE;
        if ( self::checkAccess( 'ACL_ACCESS_GROUPS', self::ACL_READ ) ) {
            if ( $_key ) {
                $_group_data = $this->getGroupData($_key);
                if ( $_group_data ) {
                    $this->assign('item_key', $_key);
                    $this->assign('group_data', $_group_data);
                } else {
                    $_response->success =  FALSE;
                    $_response->error_message = $this->getWord('INVALID_DATA');
                }
            }
            $_response->data = $this->fetch('groups/group-form.tpl');
        } else {
            $_response->success = FALSE;
            $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
        }
        $this->setAjaxResponse($_response, TRUE);
    }
    
    /**
     * FUNCTION: getGroupData
     *  Returns informations about a certain group
     * @param type $id
     * @return array
     */
    public function getGroupData( $id ) {
        $_sql = 'SELECT name, description, active from org_group where id='.$id;
        $_result = $this->select( $_sql );
        if ( $_result && count($_result)===1 ) {
            return $_result[0];
        } else {
            return NULL;
        }
    }
    
    /**
     * FUNCTION: parseGroupForm
     *      Validated data from am add/update user
     */
    public function parseGroupForm(){
        //print_r($_POST);
        $_response =  new Response();
        $_errors = FALSE;

        if ( self::checkAccess( 'ACL_ACCESS_GROUPS', self::ACL_CREATE ) || self::checkAccess( 'ACL_ACCESS_GROUPS', self::ACL_UPDATE ) ) {
            $_group_name        = $this->getValue('group_name');
            $_group_description = $this->getValue('group_description');
            $_group_id          = $this->getValue('item_key');
            $_group_status      = $this->getValue('group_status');
            /*Checking for errors*/
            if (!$_group_name){
                $_response->addFormError( 'input[name="group_name"]',Language::getWord('USERS_ERRORS_GROUP_NAME_IS_MANDATORY') );
                $_errors = TRUE;
            }

            $_group_status = strtolower($_group_status) === 'active' ? 'yes' : 'no';
            //checking for update right
            if ( $_group_id && !self::checkAccess( 'ACL_ACCESS_GROUPS', self::ACL_UPDATE ) ) {
                $_response->success = FALSE;
                $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
            } else if ( $_errors === FALSE ){
                $_group_id = $this->saveGroup($_group_id, [
                    'name'          => $_group_name,
                    'description'   => $_group_description,
                    'active'        => $_group_status
                ]);
                if ( $_group_id ){
                    $_response->success = TRUE;
                }
            }else {
                $_response->error_message = Language::getWord('FILL_MARKED_FIELDS');
            }
        } else {
            $_response->success = FALSE;
            $_response->error_message = $this->getWord( 'NO_RIGHT_FOR_THIS_ACTION' );
        }
        $this->setAjaxResponse($_response, TRUE);
    }

    /**
     * FUNCTION saveGroup
     *      Add a new group or update an existing group
     * @param int $id
     * @param array $data
     * @return type
     */
    protected function saveGroup($id, $data){
        $_result = FALSE;
        
        if (!$id) {
            //set status if not defined
            if ( !isset($data['active']) || (isset($data['active']) && empty($data['active'])) ){
                $data['active'] = 'no';
            }
            $_sql_insert = 'Insert into org_group(name, description, active) '
                    . 'values(\'' . $this->escape($data['name']) . '\',\'' . $this->escape($data['description']).'\',\'' . $data['active'] . '\')' ;
            $_result = $this->insert($_sql_insert);
        } else {
            $_sql_update = 'UPDATE org_group set ';
            $_i = 0;
            $_count = count($data);
            if ( $_count ) {
                foreach ( $data as $_field=>$_value ) {
                    $_i++;
                    $_sql_update .= "{$_field}='{$_value}'";
                    if ($_i < $_count){
                        $_sql_update .= ",";
                    }
                }
                $_sql_update .= ' where id='.$id;
                $_result = $this->update($_sql_update);
                if ( $_result ) {
                    $_result = $id;
                }
            }
        }
        return $_result;
    }

    public function deleteConfirmation(){
        $_response =  new Response();
        
        if ( self::checkAccess( 'ACL_ACCESS_GROUPS', self::ACL_DELETE ) ) {
            $_key = $this->getRequestValue('key');
            if ( $this->autocommit( FALSE ) ) { //set autocommit to false 
                $_errors = FALSE;
                
                $_sql_delete_group = "DELETE FROM org_group WHERE id='".$_key."'"; 
                $_result_delete_group = $this->query( $_sql_delete_group);
                
                if ( $_result_delete_group ){
                    $_sql_delete_users_group = 'DELETE FROM org_authreg_group_mn WHERE org_group_id='.$_key;
                    $_result_delete_users_group = $this->query( $_sql_delete_users_group );
                    if ( !$_result_delete_users_group ) {
                        $_errors = TRUE;
                    }
                } else {
                    $_errors = TRUE;
                }
                
                if ( $_errors ) {
                    $this->rollback();
                } else {
                    $this->commit();
                    $_response->success = TRUE;
                }
                $this->autocommit( TRUE );//change back autocommit as it was
            }
        }
        
        $this->setAjaxResponse( $_response, TRUE );
    }
}
