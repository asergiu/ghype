<?php

class Template extends Smarty {
  
  function __construct( $load = true ) {
      parent::__construct();
      
      if($load) {
          $configs = Object::getConfig();
          $this->assign('CONF', $configs);
          $_template_dir = Object::cleanPath( Object::getConfigValue( 'smarty.template_dir' ) );
          $template_dirs = explode(",", $_template_dir);
          if (count($template_dirs))
              foreach ($template_dirs as &$template_dir)
                  $template_dir = trim ($template_dir);
              
          $this->setTemplateDir($template_dirs);
          $this->setCompileDir( Object::cleanPath( Object::getConfigValue('smarty.compile_dir') ) );
          $this->setConfigDir( Object::cleanPath( Object::getConfigValue( 'smarty.config_dir' ) ) );
          $this->setCacheDir( Object::cleanPath( Object::getConfigValue( 'smarty.cache_dir' ) ) );
          $this->caching = Object::getConfigValue('smarty.caching', false);
          
          /* Read application version & revision information */
          $_version = Object::getConfigValue("application.version", false);
          
          /* Timezone */
          if(($_timezone = Object::getConfigValue("default.timezone", false)) != false) {
              date_default_timezone_set($_timezone);
              $this->assign("second", date('s') + 0);
              $this->assign("minute", date("i") + 0);
              $this->assign("hour", date("H") + 0);
              $this->assign("day", date("d") + 0);
              $this->assign("month", date("n") + 0);
              $this->assign("month_name", date("F"));
              $this->assign("year", date("Y") + 0);
          }
          
          /* Session variables */
          reset($_SESSION);
          while (list($_key, $_val) = each($_SESSION)) {
              $this->assign($_key, $_val);
          }
      }
  }
  
  function compactJavascript($string) {
      $_string = preg_replace("/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/", "", $string);
      $_string = str_replace(array("\r\n", "\r", "\t", "\n", '  ', '    ', '     '), '', $_string);
      $_string = preg_replace(array('(( )+\))', '(\)( )+)'), ')', $_string);
      $_string = preg_replace(array('(( )+\()', '(\(( )+)'), '(', $_string);
      $_string = preg_replace(array('(( )+\})', '(\}( )+)'), '}', $_string);
      $_string = preg_replace(array('(( )+\{)', '(\{( )+)'), '{', $_string);
      $_string = preg_replace(array('(( )+\=)', '(\=( )+)'), '=', $_string);
      $_string = preg_replace(array('(( )+\<)', '(\<( )+)'), '<', $_string);
      $_string = preg_replace(array('(( )+\>)', '(\>( )+)'), '>', $_string);
      $_string = preg_replace(array('(( )+\,)', '(\,( )+)'), ',', $_string);
      return $_string;
  }
  
  public function get_template_vars($key = null) {
      return $this->tpl_vars;
  }

  public function set_template_vars($array) {
      $this->tpl_vars = $array;
  }
  
  public function __destruct() {
      parent::__destruct();
  }
}

?>
