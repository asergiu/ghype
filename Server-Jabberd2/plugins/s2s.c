/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002-2003 Jeremie Miller, Thomas Muldowney,
 *                         Ryan Eatmon, Robert Norris
 *   
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

/** @file plugins/plugins.c
  * @brief xmpp plugins
  * $Revision: 1.0 $
  */    

#include "plugins.h"
#include "s2s/s2s.h"

int s2s_plugin_init(sx_env_t env, sx_plugin_t p) {
    plugins_t ctx = (plugins_t) p->private;
    log_write(ctx->s2s->log, LOG_NOTICE, "[plugins] loading plugins for s2s...");
}

