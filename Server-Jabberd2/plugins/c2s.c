/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002-2003 Jeremie Miller, Thomas Muldowney,
 *                         Ryan Eatmon, Robert Norris
 *   
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

/** @file plugins/plugins.c
  * @brief xmpp plugins
  * $Revision: 1.0 $
  */    

#include <mysql/mysql.h>
#include <openssl/sha.h>
#include <pthread.h>
#include "plugins.h"
#include "c2s/c2s.h"

static void _c2s_plugin_features(sx_t s, sx_plugin_t p, nad_t nad) {
    int ns;
    
    ns = nad_add_namespace(nad, uri_PLUGIN_SI, NULL);
    nad_append_elem(nad, ns, "file", 1);

    ns = nad_add_namespace(nad, uri_PLUGIN_SI_FILE_TRANSFER, NULL);
    nad_append_elem(nad, ns, "file", 1);

    ns = nad_add_namespace(nad, uri_PLUGIN_SI_BYTESTREAM, NULL);
    nad_append_elem(nad, ns, "file", 1);
}

static int storage_execute(plugins_t ctx, char *sql_query, int sql_query_length) {
    int result = FALSE, status = -1;
    const char *mysql_host_ptr = config_get_one(ctx->c2s->config, "authreg.mysql.host", 0),
               *mysql_port_ptr = config_get_one(ctx->c2s->config, "authreg.mysql.port", 0),
               *mysql_dbname_ptr = config_get_one(ctx->c2s->config, "authreg.mysql.dbname", 0),
               *mysql_username_ptr = config_get_one(ctx->c2s->config, "authreg.mysql.user", 0),
               *mysql_password_ptr = config_get_one(ctx->c2s->config, "authreg.mysql.pass", 0);

#ifdef PLUGIN_DEBUG
    fprintf(stdout, "\nPLUGIN C2S: SQL: %s\n", sql_query);
#endif

    if(mysql_host_ptr != NULL && mysql_dbname_ptr != NULL && mysql_username_ptr != NULL && mysql_password_ptr != NULL) {
        MYSQL *conn = NULL;
        
        if((conn = mysql_init(NULL)) != NULL) {
            unsigned int connect_timeout = 10, readwrite_timeout = 10;
            my_bool reconnect = TRUE;
            mysql_options(conn, MYSQL_OPT_CONNECT_TIMEOUT, (char*)(&connect_timeout));
            mysql_options(conn, MYSQL_OPT_READ_TIMEOUT, (char*)(&readwrite_timeout));
            mysql_options(conn, MYSQL_OPT_RECONNECT, (my_bool*)(&reconnect));
            mysql_options(conn, MYSQL_OPT_LOCAL_INFILE, NULL);
            mysql_options(conn, MYSQL_OPT_USE_EMBEDDED_CONNECTION, NULL);
            if(mysql_real_connect(conn, mysql_host_ptr, mysql_username_ptr, mysql_password_ptr, mysql_dbname_ptr, 0, NULL, CLIENT_MULTI_RESULTS | CLIENT_MULTI_STATEMENTS | CLIENT_REMEMBER_OPTIONS)) {
                if(mysql_real_query(conn, sql_query, sql_query_length) == 0) {
                    result = TRUE;
                    do {
                        MYSQL_RES* mysql_res = NULL;
                        if((mysql_res = mysql_store_result(conn)) != NULL) {
                            mysql_free_result(mysql_res); 
                        }
                        if((status = mysql_next_result(conn)) > 0) result = FALSE; 
                    } while(status == 0);
                } else {
                    log_write(ctx->c2s->log, LOG_ERR, "can not execute MySQL query: \"%s\"", sql_query);
                }
            } else {
                log_write(ctx->c2s->log, LOG_ERR, "can not connect to MySQL server on \"%s\"", mysql_host_ptr);
                result = FALSE;
            }
            mysql_close(conn);
        }
    }
    
    return result;
}

struct _file_transfer_st * create_file_transfer(plugins_t ctx, unsigned short int type, const char* uniqueid, const char* id, const char* sid, const char* from, const char* from_session_id, const char* to, const char* filename, const char* size, const char* desc) {
    char tmp_buffer[128];
    int iseed, irand;
    struct _file_transfer_st *ft = NULL, *result = NULL;

    if((ft = (struct _file_transfer_st *) calloc(1, sizeof(struct _file_transfer_st))) != NULL) {
        memset(ft, '\0', sizeof(struct _file_transfer_st));
        ft->type = type;
        ft->error_code = FILE_TRANSFER_NO_ERROR;
        ft->executed = FALSE;

        if(id != NULL) ft->id = strdup(id);
        if(sid != NULL) ft->sid = strdup(sid);
        if(from != NULL) ft->from = strdup(from);
        if(from_session_id != NULL) ft->from_session_id = strdup(from_session_id);
        if(to != NULL) ft->to = strdup(to);

        if(filename != NULL) ft->filename = strdup(filename);
        if(size != NULL) ft->filesize = strtoul(size, NULL, 10);
        if(desc != NULL) ft->description = strdup(desc);
        
        if(uniqueid == NULL) {
            iseed = time(NULL);
            srand(iseed);
            irand = (unsigned int)( 1000000.0 * rand_r(&iseed) / ( RAND_MAX + 1.0 ) );
            snprintf(tmp_buffer, 65535, "UID%07d%lu", irand, time(NULL));
            ft->unique_id = strdup(tmp_buffer);
        } else ft->unique_id = strdup(uniqueid);

        ft->timestamp = time(NULL);

#ifdef PLUGIN_DEBUG
        fprintf(stdout, "\nPLUGIN C2S MEMORY: create request \"%s\" for file \"%s\": sid = %s, from: %s\n", ft->id, ft->filename, ft->sid, from);
#endif
        
        ft->next = ctx->transfer;
        ctx->transfer = ft;
        result = ft;
    }
    
    return result;
}

void free_file_transfer(plugins_t ctx, struct _file_transfer_st *ft) {

    _sx_debug(ZONE, "PLUGIN C2S: [%lu] free memory for file \"%s\" with size %lu, id = %s, sid = %s\n", ft->timestamp, ft->filename, ft->filesize, ft->id, ft->sid);

    if(ft->unique_id != NULL) free(ft->unique_id);

    if(ft->id != NULL) free(ft->id);
    if(ft->sid != NULL) free(ft->sid);
    if(ft->from != NULL) free(ft->from);
    if(ft->from_session_id != NULL) free(ft->from_session_id);
    if(ft->to != NULL) free(ft->to);
    if(ft->to_session_id != NULL) free(ft->to_session_id);
    if(ft->filename != NULL) free(ft->filename);
    if(ft->description != NULL) free(ft->description);
    if(ft->sha1_hash != NULL) free(ft->sha1_hash);

    free(ft);
}

void free_file_transfer_list(plugins_t ctx) {
    struct _file_transfer_st *ft = NULL, *ft_next = NULL;

    if(sem_wait(&(ctx->semaphore)) == 0) {
        for(ft = ctx->transfer; ft != NULL; ) {
            ft_next = ft->next;
            free_file_transfer(ctx, ft);
            ft = ft_next;
        }
        
        ctx->transfer = NULL;
        sem_post(&(ctx->semaphore));
    }
}

void update_hash_in_file_transfer(plugins_t ctx, file_transfer_t ft, const char* hash, int request) {
    if(ft->sha1_hash != NULL) free(ft->sha1_hash);
    ft->sha1_hash = strdup(hash);
}

void free_file_transfer_element(plugins_t ctx, file_transfer_t ft) {
    file_transfer_t tmp = NULL;

#ifdef PLUGIN_DEBUG
        fprintf(stdout, "\nPLUGIN C2S MEMORY: delete request \"%s\" for file \"%s\"\n", ft->id, ft->filename);
#endif
    
    if(ft == ctx->transfer) {
        ctx->transfer = ctx->transfer->next;
    } else {
        for(tmp = ctx->transfer; tmp->next != NULL; tmp = tmp->next) {
            if(tmp->next == ft) {
                tmp->next = ft->next;
                break;
            }
        }
    }
    
    free_file_transfer(ctx, ft);
}

int finish_file_transfer(plugins_t ctx, file_transfer_t ft, int request, int free_memory) {
    int sql_query_len = -1, result = FALSE;
    char sql_query[65535];
    struct _file_transfer_st *tmp = NULL;
    
    if(ft->executed && ft->error_code != FILE_TRANSFER_NO_ERROR && ft->sid != NULL) {
        sql_query_len = snprintf(sql_query, 65535, "update `file-transfer` set status = \"error\", message = \"error code is %d\", partialsize = NULL where sid = \"%s\"", ft->error_code, ft->sid);
    } else if(ft->error_code == FILE_TRANSFER_NO_ERROR) {
        sql_query_len = snprintf(sql_query, 65535, "insert into `file-transfer` (uniqueid, sid, type, `from`, `to`, timestamp, filename, filesize, description, hash, status, partialsize) values(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", now(), \"%s\", %lu, \"%s\", \"%s\", \"activate\", NULL)", 
        (ft->unique_id != NULL ? ft->unique_id : "null"), 
        (ft->sid != NULL ? ft->sid : "null"), 
        (ft->type == FILE_TRANSFER_PUSH ? "push" : "pull"),
        (ft->from != NULL ? ft->from : "null"),
        (ft->to != NULL ? ft->to : "null"),
        (ft->filename != NULL ? ft->filename : "null"),
        ft->filesize,
        (ft->description != NULL ? ft->description : "null"),
        (ft->sha1_hash != NULL ? ft->sha1_hash : "null"));
    } else {
        sql_query_len = snprintf(sql_query, 65535, "insert into `file-transfer` (uniqueid, sid, type, `from`, `to`, timestamp, filename, filesize, description, status, message, partialsize) values(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", now(), \"%s\", %lu, \"%s\", \"error\", \"error code is %d\", NULL)", 
        (ft->unique_id != NULL ? ft->unique_id : "null"), 
        (ft->sid != NULL ? ft->sid : "null"), 
        (ft->type == FILE_TRANSFER_PUSH ? "push" : "pull"),
        (ft->from != NULL ? ft->from : "null"),
        (ft->to != NULL ? ft->to : "null"),
        (ft->filename != NULL ? ft->filename : "null"),
        ft->filesize,
        (ft->description != NULL ? ft->description : "null"), ft->error_code);
    }
    ft->executed = TRUE;

    result = storage_execute(ctx, sql_query, sql_query_len);
    
    if(free_memory) free_file_transfer_element(ctx, ft);
    
    return result;
}

void set_file_transfer_error(plugins_t ctx, file_transfer_t ft, short int error_code) {
    ft->error_code = error_code;

    _sx_debug(ZONE, "PLUGIN C2S: SET ERROR = %d\n", error_code);
    finish_file_transfer(ctx, ft, TRUE, TRUE);
}

file_transfer_t get_file_transfer_based_on_id(plugins_t ctx, const char* id) {
    file_transfer_t result = NULL;
    struct _file_transfer_st *ft = NULL;
    
    for(ft = ctx->transfer; ft != NULL; ft = ft->next) {
        if(ft->id != NULL && strcasecmp(ft->id, id) == 0) {
            result = ft;
            break;
        }
    }

    return result;
}

file_transfer_t get_file_transfer_based_on_sid(plugins_t ctx, const char* sid) {
    file_transfer_t result = NULL;
    struct _file_transfer_st *ft = NULL;
    
    for(ft = ctx->transfer; ft != NULL; ft = ft->next) {
        if(ft->sid != NULL && strcasecmp(ft->sid, sid) == 0) {
            result = ft;
            break;
        }
    }

    return result;
}

file_transfer_t get_file_transfer(plugins_t ctx, const char* from, const char* to, const char* sid) {
    file_transfer_t result = NULL;
    struct _file_transfer_st *ft = NULL;
    
    for(ft = ctx->transfer; ft != NULL; ft = ft->next) {
        if(ft->from != NULL && ft->to != NULL && ft->sid != NULL &&
           strcasecmp(ft->from, from) == 0 && strcasecmp(ft->to, to) == 0 && strcasecmp(ft->sid, sid) == 0) {
            result = ft;
            break;
        }
    }

    return result;
}

static int contact_peer(sx_t s, plugins_t ctx, file_transfer_t ft, nad_t nad) {
    int len = -1, ns, ns1, ns2, ns3, ns4, ns5, iq, si, file, feature, x, field, option;
    char tmp_buffer[128];
    char out_buff[65535];

    free(nad->elems);
    free(nad->attrs);
    free(nad->cdata);
    free(nad->nss);
    free(nad->depths);
    memset(nad, '\0', sizeof(struct nad_st));
    nad->scope = -1;
    
    ns = nad_add_namespace(nad, uri_CLIENT, NULL);
    iq = nad_append_elem(nad, ns, "iq", 0);
    nad_append_attr(nad, iq, "to", ft->to);
    nad_append_attr(nad, iq, "from", config_get_one(ctx->c2s->config, "streamhost.jid", 0));
    nad_append_attr(nad, iq, "id", ft->id);
    nad_append_attr(nad, iq, "type", "set");
    
    ns1 = nad_add_namespace(nad, uri_PLUGIN_SI, NULL);
    si = nad_append_elem(nad, ns1, "si", 1);
    nad_append_attr(nad, si, "profile", uri_PLUGIN_SI_FILE_TRANSFER);
    nad_append_attr(nad, si, "id", ft->sid);
    
    snprintf(tmp_buffer, 128, "%s\0", ft->unique_id);
    nad_append_attr(nad, si, "uid", tmp_buffer);

    ns2 = nad_add_namespace(nad, uri_PLUGIN_SI_FILE_TRANSFER, NULL);
    file = nad_append_elem(nad, ns3, "file", 2);
    
    snprintf(tmp_buffer, 128, "%lu\0", ft->filesize);  
    nad_append_attr(nad, file, "size", tmp_buffer);    
    nad_append_attr(nad, file, "name", ft->filename);

    ns3 = nad_add_namespace(nad, uri_PLUGIN_SI_FEATURE_NEG, NULL);
    feature = nad_append_elem(nad, ns3, "feature", 2);
    
    ns4 = nad_add_namespace(nad, uri_XDATA, NULL);
    x = nad_append_elem(nad, ns4, "x", 3);
    nad_append_attr(nad, x, "type", "form");
    
    ns5 = nad_add_namespace(nad, "", NULL);
    field = nad_append_elem(nad, ns5, "field", 4);
    nad_append_attr(nad, field, "type", "list-single"); 
    nad_append_attr(nad, field, "var", "stream-method");
            
    option = nad_append_elem(nad, ns5, "option", 5);
    nad_insert_elem(nad, option, -1, "value", uri_PLUGIN_SI_BYTESTREAM);
    
    debug_packet("OUT", nad);
    
    return TRUE;
}

static int _c2s_plugin_process(sx_t s, sx_plugin_t p, nad_t nad) {
    int attr, cmd, ns, si, file, feature, date, hash, name, size, desc, len, to, id, x, field, option, value, query, sid, mode, action, error, uid, from;
    char root[VALUE_MAX_LENGTH], desc_str[VALUE_MAX_LENGTH], date_str[VALUE_MAX_LENGTH], name_str[VALUE_MAX_LENGTH], size_str[VALUE_MAX_LENGTH], hash_str[VALUE_MAX_LENGTH], from_str[VALUE_MAX_LENGTH];
    char to_str[VALUE_MAX_LENGTH], id_str[VALUE_MAX_LENGTH], sid_str[VALUE_MAX_LENGTH], mode_str[VALUE_MAX_LENGTH], value_str[VALUE_MAX_LENGTH], error_str[VALUE_MAX_LENGTH], uid_str[VALUE_MAX_LENGTH];
    char out_buff[65535];
    file_transfer_t ft = NULL;
    plugins_t ctx = (plugins_t) p->private;
    
    if(sem_wait(&(ctx->semaphore)) == 0) {
        int routed_request = FALSE, iq = 0;
    
        root[0] = desc_str[0] = date_str[0] = name_str[0] = size_str[0] = to_str[0] = from_str[0] = id_str[0] = sid_str[0] = mode_str[0] = value_str[0] = uid_str[0] = '\0';
        
        snprintf(root, VALUE_MAX_LENGTH, "%.*s", NAD_ENAME_L(nad, 0), NAD_ENAME(nad, 0));
        if(strcmp(root, "route") == 0) {
            if((iq = nad_find_elem(nad, 0, -1, "iq", 1)) >= 0) {
                routed_request = TRUE;
            } else {
                sem_post(&(ctx->semaphore));
                return 1;
            }
        } else if(strcmp(root, "iq") == 0) {
            routed_request = FALSE;
            iq = 0;
        } else if(strcmp(root, "ghype") == 0) {
            if(strchr(s->auth_id, '/') == NULL && s->session_id != NULL) {
                snprintf(from_str, VALUE_MAX_LENGTH, "%s/%s\0", s->auth_id, s->session_id);
            } else {
                snprintf(from_str, VALUE_MAX_LENGTH, "%s\0", s->auth_id);
            }
            nad_set_attr(nad, 0, -1, "from", from_str, 0);

            sem_post(&(ctx->semaphore));
            return 1;
        } else {
            sem_post(&(ctx->semaphore));
            return 1;
        }

        debug_packet("IN", nad);

        if((to = nad_find_attr(nad, iq, -1, "to", NULL)) >= 0) {
            snprintf(to_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, to), NAD_AVAL(nad, to));
        }

        if((from = nad_find_attr(nad, iq, -1, "from", NULL)) >= 0) {
            snprintf(from_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, from), NAD_AVAL(nad, from));
        }

        if((id = nad_find_attr(nad, iq, -1, "id", NULL)) >= 0) {
            snprintf(id_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, id), NAD_AVAL(nad, id));
        }
        
        if(nad_find_attr(nad, iq, -1, "type", "error") >= 0) {
            if(to_str[0] != '\0' && id_str[0] != '\0') {
                file_transfer_t ft = NULL;
                int error_code = FILE_TRANSFER_SOCKS5_ERROR;
                
                if((error = nad_find_elem(nad, iq, -1, "error", 1)) >= 0) {
                    if((value = nad_find_attr(nad, error, -1, "code", NULL)) >= 0) {
                        snprintf(error_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, value), NAD_AVAL(nad, value));
                        error_code = strtoul(error_str, NULL, 10);
                    }
                }
                
                if((ft = get_file_transfer_based_on_id(ctx, id_str)) != NULL) {
                    set_file_transfer_error(ctx, ft, error_code);
                    
                    nad_free(nad);
                    sem_post(&(ctx->semaphore));
                    return 0;
                }
            }
        } else if((ns = nad_find_scoped_namespace(nad, uri_PLUGIN_IBB, NULL)) >= 0) {
            char out_buff[65535];
            len = snprintf(out_buff, 65535, "<iq type=\"error\" id=\"%s\" from=\"%s\" to=\"%s\"><error type='cancel'><service-unavailable xmlns='%s'/></error></iq>\0", id_str, to_str, s->auth_id, uri_STANZA_ERR);

            jqueue_push(s->wbufq, _sx_buffer_new(out_buff, len, NULL, NULL), 0);
            s->want_write = 1;
            
            nad_free(nad);
            sem_post(&(ctx->semaphore));
            return 0;
        } else {
            if((ns = nad_find_scoped_namespace(nad, uri_PLUGIN_SI, NULL)) >= 0 && (si = nad_find_elem(nad, iq, ns, "si", 1)) >= 0 && nad_find_attr(nad, iq, -1, "type", "set") >= 0 && nad_find_attr(nad, si, -1, "profile", uri_PLUGIN_SI_FILE_TRANSFER) >= 0) {
                if((sid = nad_find_attr(nad, si, -1, "id", NULL)) >= 0) {
                    snprintf(sid_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, sid), NAD_AVAL(nad, sid));
                }

                if((uid = nad_find_attr(nad, si, -1, "uid", NULL)) >= 0) {
                    snprintf(uid_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, uid), NAD_AVAL(nad, uid));
                }

                if(sid_str[0] != '\0' && (feature = nad_find_elem(nad, si, -1, "feature", 1)) >= 0 && nad_find_namespace(nad, feature, uri_PLUGIN_SI_FEATURE_NEG, NULL) >= 0) {
                    if((file = nad_find_elem(nad, si, -1, "file", 1)) >= 0 && nad_find_namespace(nad, file, uri_PLUGIN_SI_FILE_TRANSFER, NULL) >= 0) {
                        if((date = nad_find_attr(nad, file, -1, "date", NULL)) >= 0) {
                            snprintf(date_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, date), NAD_AVAL(nad, date));
                        }
                        if((name = nad_find_attr(nad, file, -1, "name", NULL)) >= 0) {
                            snprintf(name_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, name), NAD_AVAL(nad, name));
                        }
                        if((hash = nad_find_attr(nad, file, -1, "hash", NULL)) >= 0) {
                            snprintf(hash_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, hash), NAD_AVAL(nad, hash));
                        }
                        if((size = nad_find_attr(nad, file, -1, "size", NULL)) >= 0) {
                            snprintf(size_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, size), NAD_AVAL(nad, size));
                        }
                        if((desc = nad_find_elem(nad, file, -1, "desc", 1)) >= 0) {
                            snprintf(desc_str, VALUE_MAX_LENGTH, "%.*s", NAD_CDATA_L(nad, desc), NAD_CDATA(nad, desc));
                        }
                        
                        if(name_str[0] != '\0' && size_str[0] != '\0') {
                            char tmp_buff[128], *from_ptr = NULL;
                            
                            if(routed_request) {

                                if(strcasecmp(s->auth_id, "jabberd@jabberd-router") == 0 && from_str[0] != '\0') {
                                    nad_set_attr(nad, iq, -1, "id", id_str, 0);
                                    snprintf(sid_str, VALUE_MAX_LENGTH, "%s2", id_str);
                                    nad_set_attr(nad, si, -1, "id", sid_str, 0);

                                    if((from_ptr = strchr(from_str, '/')) != NULL) *from_ptr++ = '\0';
                                    if(uid_str[0] != '\0') create_file_transfer(ctx, FILE_TRANSFER_PULL, uid_str, id_str, sid_str, from_str, from_ptr, to_str, name_str, size_str, desc_str);
                                    else create_file_transfer(ctx, FILE_TRANSFER_PULL, NULL, id_str, sid_str, from_str, from_ptr, to_str, name_str, size_str, desc_str);
                                } else {
                                    generate_unique_id(tmp_buff, "pull");
                                    snprintf(id_str, VALUE_MAX_LENGTH, "%s1", tmp_buff);
                                    
                                    nad_set_attr(nad, iq, -1, "id", id_str, 0);
                                    snprintf(sid_str, VALUE_MAX_LENGTH, "%s2", tmp_buff);
                                    nad_set_attr(nad, si, -1, "id", sid_str, 0);

                                    if(uid_str[0] != '\0') create_file_transfer(ctx, FILE_TRANSFER_PULL, uid_str, id_str, sid_str, s->auth_id, s->session_id, to_str, name_str, size_str, desc_str);
                                    else create_file_transfer(ctx, FILE_TRANSFER_PULL, NULL, id_str, sid_str, s->auth_id, s->session_id, to_str, name_str, size_str, desc_str);
                                }
                                sem_post(&(ctx->semaphore));
                                return 1;
                            } else {
                                nad_free(nad);

                                if(create_file_transfer(ctx, FILE_TRANSFER_PUSH, NULL, id_str, sid_str, s->auth_id, s->session_id, to_str, name_str, size_str, desc_str) != NULL) {
                                    len = snprintf(out_buff, 65535, "<iq type=\"result\" id=\"%s\" from=\"%s\" to=\"%s\"><si xmlns=\"%s\" profile=\"%s\"><feature xmlns=\"%s\"><x xmlns=\"%s\" type=\"submit\"><field var=\"stream-method\"><value>http://jabber.org/protocol/bytestreams</value></field></x></feature></si></iq>\0", 
                                      id_str, to_str, s->auth_id, uri_PLUGIN_SI, uri_PLUGIN_SI_FILE_TRANSFER, 
                                      uri_PLUGIN_SI_FEATURE_NEG, uri_XDATA);
                                } else {
                                    len = snprintf(out_buff, 65535, "<iq type=\"error\" id=\"%s\" from=\"%s\" to=\"%s\"><error type='auth'><forbidden xmlns='%s'/></error></iq>\0", id_str, to_str, s->auth_id, uri_STANZA_ERR);
                                }

                                jqueue_push(s->wbufq, _sx_buffer_new(out_buff, len, NULL, NULL), 0);
                                s->want_write = 1;
#ifdef PLUGIN_DEBUG
                                fprintf(stdout, "\nPLUGIN C2S OUT: %s\0\n", out_buff);
#endif
                                sem_post(&(ctx->semaphore));
                                return 0;
                            }
                        }
                    }
                }
            } else if((ns = nad_find_scoped_namespace(nad, uri_PLUGIN_SI, NULL)) >= 0 && (si = nad_find_elem(nad, iq, ns, "si", 1)) >= 0 && nad_find_attr(nad, iq, -1, "type", "result") >= 0) {
                if((ft = get_file_transfer_based_on_id(ctx, id_str)) != NULL) {
                    if(ft->error_code != FILE_TRANSFER_NO_ERROR) {
                        len = snprintf(out_buff, 65535, "<iq xmlns=\"jabber:client\" id=\"%s\" from=\"%s\" to=\"%s\" type='error'><error type='cancel'><forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/><text xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'>Offer Declined</text></error></iq>\0", id_str, to_str, s->auth_id);
                        jqueue_push(s->wbufq, _sx_buffer_new(out_buff, len, NULL, NULL), 0);
                        s->want_write = 1;
                        
                        nad_free(nad);
                        sem_post(&(ctx->semaphore));
                        return 0;
                    } else if((feature = nad_find_elem(nad, si, -1, "feature", 1)) >= 0 && nad_find_namespace(nad, feature, uri_PLUGIN_SI_FEATURE_NEG, NULL) >= 0) {
                        if((x = nad_find_elem(nad, feature, -1, "x", 1)) >= 0 && nad_find_namespace(nad, x, uri_XDATA, NULL) >= 0 && nad_find_attr(nad, x, -1, "type", "submit") >= 0) {
                            if((field = nad_find_elem(nad, x, -1, "field", 1)) >= 0 && nad_find_attr(nad, field, -1, "var", "stream-method") >= 0) {
                                if((value = nad_find_elem(nad, field, -1, "value", 1)) >= 0) {
                                    snprintf(value_str, VALUE_MAX_LENGTH, "%.*s", NAD_CDATA_L(nad, value), NAD_CDATA(nad, value));
                                    if(strcasecmp(value_str, uri_PLUGIN_SI_BYTESTREAM) == 0) {
                                        int config_error = TRUE;
                                        if(ctx != NULL && ctx->c2s != NULL && ctx->c2s->config != NULL) {
                                            const char *host_ptr = config_get_one(ctx->c2s->config, "streamhost.host", 0),
                                                       *port_ptr = config_get_one(ctx->c2s->config, "streamhost.port", 0),
                                                       *jid_ptr = config_get_one(ctx->c2s->config, "streamhost.jid", 0);

                                            if(host_ptr != NULL && port_ptr != NULL && jid_ptr != NULL) {
                                                unsigned char hash[SHA_DIGEST_LENGTH], hash_str[VALUE_MAX_LENGTH];
                                                char sha1_string[65535];
                                                int sha1_string_length = -1, i;
                                                
                                                memset(hash_str, '\0', VALUE_MAX_LENGTH);
                                                if(strstr(s->auth_id, "/") != NULL) {
                                                    sha1_string_length = snprintf(sha1_string, 65535, "%s%s%s", ft->sid, to_str, s->auth_id);
                                                } else {
                                                    sha1_string_length = snprintf(sha1_string, 65535, "%s%s%s/%s", ft->sid, to_str, s->auth_id, s->session_id);
                                                }
#ifdef PLUGIN_DEBUG
                                                fprintf(stdout, "\nPLUGIN C2C SSH: sid = %s, to = %s, from = %s, auth_id = %s, session_id = %s, sha = %s\n", ft->sid, to_str, ft->to, s->auth_id, s->session_id, sha1_string);
#endif                                                
                                                memset(hash, '\0', SHA_DIGEST_LENGTH);
                                                SHA1(sha1_string, sha1_string_length, hash);
                                                
                                                for(i = 0; i < SHA_DIGEST_LENGTH; i++) {
                                                    sprintf(hash_str + strlen(hash_str), "%02x", hash[i]);
                                                }
                                                update_hash_in_file_transfer(ctx, ft, hash_str, FALSE);
                                                
                                                if(finish_file_transfer(ctx, ft, FALSE, FALSE)) {
                                                    len = snprintf(out_buff, 65535, "<iq type='set' id='%s' to='%s' from='%s'><query xmlns='http://jabber.org/protocol/bytestreams' sid='%s'><streamhost jid='%s/filetransfer' host='%s' port='%s'/></query></iq>\0", ft->id, ft->to, to_str, ft->sid, jid_ptr, host_ptr, port_ptr);
                                                    config_error = FALSE;
                                                }
                                            }
                                        }
                                        if(config_error) {
                                            len = snprintf(out_buff, 65535, "<iq xmlns=\"jabber:client\" id=\"%s\" from=\"%s\" to=\"%s\" type='error'><error type='cancel'><forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/><text xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'>Offer Declined</text></error></iq>\0", id_str, to_str, s->auth_id);
                                        }
                                    } else {
                                        len = snprintf(out_buff, 65535, "<iq xmlns=\"jabber:client\" id=\"%s\" from=\"%s\" to=\"%s\" type='error'><error type='cancel'><forbidden xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/><text xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'>Offer Declined</text></error></iq>\0", id_str, to_str, s->auth_id);
                                    }
                                
                                    jqueue_push(s->wbufq, _sx_buffer_new(out_buff, len, NULL, NULL), 0);
                                    s->want_write = 1;
#ifdef PLUGIN_DEBUG
                                    fprintf(stdout, "\nPLUGIN C2S OUT: %s\0\n", out_buff);
#endif
                                    nad_free(nad);
                                    sem_post(&(ctx->semaphore));
                                    return 0;
                                }
                            }
                        }
                    }
                }
            
            } else if((query = nad_find_elem(nad, iq, -1, "query", 1)) >= 0 && nad_find_attr(nad, iq, -1, "type", "result") >= 0 && (feature = nad_find_elem(nad, query, -1, "streamhost-used", 1)) >= 0) {
                const char *jid_ptr = config_get_one(ctx->c2s->config, "streamhost.jid", 0);
                if((ft = get_file_transfer_based_on_id(ctx, id_str)) != NULL) {
                    // KNOWN BUG: send activate to SOCK5 service
                    free_file_transfer_element(ctx, ft);
                    
                    nad_free(nad);
                    sem_post(&(ctx->semaphore));
                    return 0;
                }
            } else if((ns = nad_find_scoped_namespace(nad, uri_PLUGIN_SI_BYTESTREAM, NULL)) >= 0 && (query = nad_find_elem(nad, iq, ns, "query", 1)) >= 0 && nad_find_attr(nad, iq, -1, "type", "get") >= 0) {
                int config_error = TRUE;

                if((sid = nad_find_attr(nad, query, -1, "sid", NULL)) >= 0) {
                    snprintf(sid_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, sid), NAD_AVAL(nad, sid));
                }

                if(ctx != NULL && ctx->c2s != NULL && ctx->c2s->config != NULL) {
                    const char *host_ptr = config_get_one(ctx->c2s->config, "streamhost.host", 0),
                               *port_ptr = config_get_one(ctx->c2s->config, "streamhost.port", 0),
                               *jid_ptr = config_get_one(ctx->c2s->config, "streamhost.jid", 0);
                         
                    if(host_ptr != NULL && port_ptr != NULL && jid_ptr != NULL) {
                        if(sid_str[0] != '\0' && (ft = get_file_transfer_based_on_sid(ctx, sid_str)) != NULL && ft->unique_id != NULL) {
                            len = snprintf(out_buff, 65535, "<iq type=\"result\" id=\"%s\" from=\"%s\" to=\"%s\"><query xmlns=\"%s\"><streamhost uid=\"%s\" host=\"%s\" jid=\"%s\" port=\"%s\"/></query></iq>\0", id_str, to_str, s->auth_id, uri_PLUGIN_SI_BYTESTREAM, ft->unique_id, host_ptr, jid_ptr, port_ptr);
                        } else {
                            len = snprintf(out_buff, 65535, "<iq type=\"result\" id=\"%s\" from=\"%s\" to=\"%s\"><query xmlns=\"%s\"><streamhost host=\"%s\" jid=\"%s\" port=\"%s\"/></query></iq>\0", id_str, to_str, s->auth_id, uri_PLUGIN_SI_BYTESTREAM, host_ptr, jid_ptr, port_ptr);
                        }
                        config_error = FALSE;
                    }
                }
                if(config_error) {
                    len = snprintf(out_buff, 65535, "<iq type=\"error\" id=\"%s\" from=\"%s\" to=\"%s\"><error type='auth'><forbidden xmlns='%s'/></error></iq>\0", id_str, to_str, s->auth_id, uri_STANZA_ERR);
                }
                
                jqueue_push(s->wbufq, _sx_buffer_new(out_buff, len, NULL, NULL), 0);
                s->want_write = 1;
                
                nad_free(nad);
                sem_post(&(ctx->semaphore));
                return 0;
            } else if((ns = nad_find_scoped_namespace(nad, uri_PLUGIN_SI_BYTESTREAM, NULL)) >= 0 && (query = nad_find_elem(nad, iq, ns, "query", 1)) >= 0 && nad_find_attr(nad, iq, -1, "type", "set") >= 0) {
                char out_buff[65535], host_str[VALUE_MAX_LENGTH], jid_str[VALUE_MAX_LENGTH], port_str[VALUE_MAX_LENGTH], hash_str[VALUE_MAX_LENGTH];
                int streamhost = query, host, jid, port;
                int request_error = TRUE, own_streamhost_received = FALSE, sha1_streamhost_received = FALSE, streamhost_received = FALSE;

                if((sid = nad_find_attr(nad, query, -1, "sid", NULL)) >= 0) {
                    snprintf(sid_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, sid), NAD_AVAL(nad, sid));
                }

                if(to_str[0] != '\0' && sid_str[0] != '\0') {
                    if((ft = get_file_transfer(ctx, s->auth_id, to_str, sid_str)) != NULL) {
                        int nad_changed = FALSE, finish_protocol = FALSE;
                        memset(hash_str, '\0', VALUE_MAX_LENGTH);
                        for(streamhost = nad_find_elem(nad, streamhost, -1, "streamhost", 1); ; streamhost = nad_find_elem(nad, streamhost, -1, "streamhost", 0)) {
                            if(streamhost < 0) break;
                            streamhost_received = TRUE;
                            
                            host_str[0] = jid_str[0] = port_str[0] = '\0';
                            
                            if((host = nad_find_attr(nad, streamhost, -1, "host", NULL)) >= 0) {
                                snprintf(host_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, host), NAD_AVAL(nad, host));
                            }

                            if((jid = nad_find_attr(nad, streamhost, -1, "jid", NULL)) >= 0) {
                                snprintf(jid_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, jid), NAD_AVAL(nad, jid));
                            }

                            if((port = nad_find_attr(nad, streamhost, -1, "port", NULL)) >= 0) {
                                snprintf(port_str, VALUE_MAX_LENGTH, "%.*s", NAD_AVAL_L(nad, port), NAD_AVAL(nad, port));
                            }

                            if(host_str[0] != '\0' && jid_str[0] != '\0' && port_str[0] != '\0' && strcasecmp(jid_str, config_get_one(ctx->c2s->config, "streamhost.jid", 0)) == 0) {
                                own_streamhost_received = TRUE;
                            } else if(!sha1_streamhost_received && host_str[0] != '\0' && jid_str[0] != '\0' && port_str[0] != '\0' && strcasecmp(jid_str, config_get_one(ctx->c2s->config, "streamhost.jid", 0)) != 0) {
                                unsigned char hash[SHA_DIGEST_LENGTH];
                                char sha1_string[65535];
                                int sha1_string_length = -1, i;
                                
                                sha1_string_length = snprintf(sha1_string, 65535, "%s%s%s", sid_str, jid_str, to_str);
                                memset(hash, '\0', SHA_DIGEST_LENGTH);
                                SHA1(sha1_string, sha1_string_length, hash);
                                for(i = 0; i < SHA_DIGEST_LENGTH; i++) {
                                    sprintf(hash_str + strlen(hash_str), "%02x", hash[i]);
                                }
                                sha1_streamhost_received = TRUE;
                            }

                            if(own_streamhost_received && sha1_streamhost_received) {
                                update_hash_in_file_transfer(ctx, ft, hash_str, TRUE);

                                if(finish_file_transfer(ctx, ft, TRUE, FALSE)) {
                                    len = snprintf(out_buff, 65535, "<iq type=\"result\" id=\"%s\" from=\"%s\" to=\"%s\"><query xmlns=\"%s\"><streamhost-used jid=\"%s\"/></query></iq>\0", id_str, to_str, s->auth_id, uri_PLUGIN_SI_BYTESTREAM, jid_str);
                                    request_error = FALSE;
                                    
                                    nad_changed = contact_peer(s, ctx, ft, nad);
                                    finish_protocol = TRUE;
                                }
                                break;
                            }
                        }

                        if(streamhost_received) {
                            if(request_error) {
                                len = snprintf(out_buff, 65535, "<iq type=\"error\" id=\"%s\" from=\"%s\" to=\"%s\"><error type='auth'><forbidden xmlns='%s'/></error></iq>\0", id_str, to_str, s->auth_id, uri_STANZA_ERR);
                                set_file_transfer_error(ctx, ft, FILE_TRANSFER_STEAMHOST_ERROR);
                            }
                            
                            jqueue_push(s->wbufq, _sx_buffer_new(out_buff, len, NULL, NULL), 0);
                            s->want_write = 1;
                        }
                        
                        if(finish_protocol) free_file_transfer_element(ctx, ft);
                        
                        if(nad_changed) {
                            sem_post(&(ctx->semaphore));
                            return 1;
                        } else {
                            sem_post(&(ctx->semaphore));
                            nad_free(nad);
                            return 0;
                        }
                    }
                    
                    if(!streamhost_received && (action = nad_find_elem(nad, query, -1, "activate", 1)) >= 0 && to_str[0] != '\0' && strcasecmp(to_str, config_get_one(ctx->c2s->config, "streamhost.jid", 0)) == 0) {
                        // KNOWN BUG: send activate to SOCK5 service, and when is confirmed send respons to sender
                        len = snprintf(out_buff, 65535, "<iq type=\"result\" id=\"%s\" from=\"%s\" to=\"%s\"></iq>\0", id_str, to_str, s->auth_id);

                        jqueue_push(s->wbufq, _sx_buffer_new(out_buff, len, NULL, NULL), 0);
                        s->want_write = 1;

                        nad_free(nad);
                        sem_post(&(ctx->semaphore));
                        return 0;
                    }
                }
            }
        }
        sem_post(&(ctx->semaphore));
    }

    return 1;
}

static void _c2s_plugin_free(sx_t s, sx_plugin_t p) {
    plugins_t ctx = (plugins_t) p->private;

    log_write(ctx->c2s->log, LOG_NOTICE, "[plugins] free memory allocated by C2S");
    
    free_file_transfer_list(ctx);
}

static void* watch_file_transfer(void *arg) {
    struct timespec tspec;
    plugins_t ctx = (plugins_t)arg;
    int res = -1;
    
    while(TRUE) {
        tspec.tv_sec = time(NULL) + 5;
        tspec.tv_nsec = 0;
        if((res = sem_timedwait(&(ctx->semaphore), &tspec)) == 0) {
            struct _file_transfer_st *ft = NULL;
            int retry = TRUE;
            
            while(retry) {
                retry = FALSE;
                for(ft = ctx->transfer; ft != NULL; ft = ft->next) {
                    if((time(NULL) - ft->timestamp) > 60) {
                        _sx_debug(ZONE, "PLUGIN C2S: timeout on sid \"%s\"\n", ft->sid);

                        free_file_transfer_element(ctx, ft);
                        retry = TRUE;
                        break;
                    }
                }
            }
        
            sem_post(&(ctx->semaphore));
            sleep(10);
        } else {
            sleep(1);
        }
    }
    
    ctx->thread_id = -1;
    return NULL;
}
  
int c2s_plugin_init(sx_env_t env, sx_plugin_t p) {
    pthread_attr_t attr;
    plugins_t ctx = (plugins_t) p->private;
    log_write(ctx->c2s->log, LOG_NOTICE, "[plugins] loading plugins for c2s...");
    
    if(pthread_attr_init(&attr) == 0) {
        if(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) == 0) {
            if(pthread_create(&(ctx->thread_id), &attr, &watch_file_transfer, ctx) == 0) {
                log_write(ctx->c2s->log, LOG_NOTICE, "[plugins] thread for c2c started");
            } else {
                log_write(ctx->c2s->log, LOG_ERR, "[plugins] can not start thread for c2c...");
            }
        } else {
            log_write(ctx->c2s->log, LOG_ERR, "[plugins] can not configure thread for c2c...");
        }
    } else {
        log_write(ctx->c2s->log, LOG_ERR, "[plugins] can not initialize thread for c2c...");
    }
    
    p->features = _c2s_plugin_features;
    p->process = _c2s_plugin_process;
    p->free = _c2s_plugin_free;
}
