/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002-2003 Jeremie Miller, Thomas Muldowney,
 *                         Ryan Eatmon, Robert Norris
 *   
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

/** @file plugins/plugins.c
  * @brief xmpp plugins
  * $Revision: 1.0 $
  */    

#include "plugins.h"
#include "router/router.h"

static void _router_plugin_features(sx_t s, sx_plugin_t p, nad_t nad) {
    int ns;
    
    ns = nad_add_namespace(nad, uri_PLUGIN_SI, NULL);
    nad_append_elem(nad, ns, "file", 1);
  
    ns = nad_add_namespace(nad, uri_PLUGIN_SI_FILE_TRANSFER, NULL); 
    nad_append_elem(nad, ns, "file", 1);

    ns = nad_add_namespace(nad, uri_PLUGIN_SI_BYTESTREAM, NULL);
    nad_append_elem(nad, ns, "file", 1);
}

int router_plugin_init(sx_env_t env, sx_plugin_t p) {
    plugins_t ctx = (plugins_t) p->private;
    log_write(ctx->router->log, LOG_NOTICE, "[plugins] loading plugins for router...");
    
    p->features = _router_plugin_features;
}
