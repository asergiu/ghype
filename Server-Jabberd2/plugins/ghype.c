
#include "plugins.h"
#include <mysql/mysql.h>

#define MYSQL_CONNECT \
    if(g->hostname != NULL && g->dbname != NULL && g->user != NULL && g->pass != NULL) { \
        MYSQL *conn = NULL; \
        if((conn = mysql_init(NULL)) != NULL) { \
            unsigned int connect_timeout = 10, readwrite_timeout = 10, i = 0; \
            my_bool reconnect = TRUE; \
            mysql_options(conn, MYSQL_OPT_CONNECT_TIMEOUT, (char*)(&connect_timeout)); \
            mysql_options(conn, MYSQL_OPT_READ_TIMEOUT, (char*)(&readwrite_timeout)); \
            mysql_options(conn, MYSQL_OPT_RECONNECT, (my_bool*)(&reconnect)); \
            mysql_options(conn, MYSQL_OPT_LOCAL_INFILE, NULL); \
            mysql_options(conn, MYSQL_OPT_USE_EMBEDDED_CONNECTION, NULL); \
            if(mysql_real_connect(conn, g->hostname, g->user, g->pass, g->dbname, 0, NULL, CLIENT_MULTI_RESULTS | CLIENT_MULTI_STATEMENTS | CLIENT_REMEMBER_OPTIONS)) {

#define MYSQL_PREPARE_ANSWER( answer_str, answer_ns, answer_prefix ) \
                if(mysql_real_query(conn, sql_query, strlen(sql_query)) == 0) { \
                    if(answer_str != NULL) { \
                        ns = nad_add_namespace(response, answer_ns, answer_prefix); \
                        answer = nad_append_elem(response, ns, answer_str, 0); \
                    } \
                    result = TRUE; \
                    do { \
                        MYSQL_RES* mysql_res = NULL; \
                        if((mysql_res = mysql_store_result(conn)) != NULL) { \
                            MYSQL_ROW mysql_row = NULL; \
                            for(i = 0; i < mysql_num_rows(mysql_res); i++) { \
                                if((mysql_row = mysql_fetch_row(mysql_res)) != NULL) {

#define MYSQL_CLOSE_ANSWER \
                                } \
                            } \
                            mysql_free_result(mysql_res); \
                        } \
                        if((status = mysql_next_result(conn)) > 0) result = FALSE; \
                    } while(status == 0); \
                } else { \
                }   

#define MYSQL_EXECUTE \
        if(mysql_real_query(conn, sql_query, strlen(sql_query)) == 0) { \
            result = TRUE; \
            do { \
                MYSQL_RES* mysql_res = NULL; \
                if((mysql_res = mysql_store_result(conn)) != NULL) { \
                    mysql_free_result(mysql_res); \
                } \
                if((status = mysql_next_result(conn)) > 0) result = FALSE; \
            } while(status == 0); \
        } else { \
        }

#define MYSQL_DISCONNECT \
            } else { \
            } \
            mysql_close(conn); \
        } \
    }

int ghype_get_date_utc(nad_t response, mod_ghype_t g) {
    int result = FALSE, status, ns, answer, message, delay, mark, ack;
    char sql_query[4000], tmp_sql_query[4000];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 4000);
        snprintf(sql_query, 4000, "select date_format(CONVERT_TZ(now(), @@session.time_zone, '+00:00'), \'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\') as time");

        fprintf(stdout, "SQL: %s\n", sql_query);
        
        MYSQL_PREPARE_ANSWER("dates", uri_GHYPE, NULL);

            if(mysql_row[0] != NULL) {
                message = nad_append_elem(response, answer, "date", 1);
                nad_set_attr(response, message, -1, "xmlns", uri_CLIENT, 0);
                nad_set_attr(response, message, -1, "type", "utc", 0);
                nad_set_attr(response, message, -1, "id", "", 0);
                nad_set_attr(response, message, -1, "value", mysql_row[0], 0);
            }

        MYSQL_CLOSE_ANSWER

    MYSQL_DISCONNECT

    return result;
}

int ghype_get_messages(nad_t response, mod_ghype_t g, const char* owner, const char* from, const char* until, int start, int chunk, const char* with) {
    int result = FALSE, status, ns, answer, message, delay, mark, ack;
    char sql_query[4000], tmp_sql_query[4000];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 4000);
        if(from[0] != '\0' && until[0] != '\0') {
            if(with[0] != '\0') snprintf(sql_query, 4000, " \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\') as time, content, `object-sequence`, 1 as sent from `message-logs` where `from` = '%s' and time between unix_timestamp('%s') and unix_timestamp('%s') and `to` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 1 as sent from `message-logs` where `from` = '%s' and time between unix_timestamp('%s') and unix_timestamp('%s') and `to` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 0 as sent from `queue` where `type` = 'chat' and `from` = '%s' and time between unix_timestamp('%s') and unix_timestamp('%s') and `to` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 0 as sent from `queue` where `type` = 'chat' and `from` = '%s' and time between unix_timestamp('%s') and unix_timestamp('%s') and `to` = '%s' ", owner, from, until, with,  with, from, until, owner, owner, from, until, with,  with, from, until, owner);
            else snprintf(sql_query, 4000, " \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\') as time, content, `object-sequence`, 1 as sent from `message-logs` where `from` = '%s' and time between unix_timestamp('%s') and unix_timestamp('%s') \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 1 as sent from `message-logs` where `to` = '%s' and time between unix_timestamp('%s') and unix_timestamp('%s') \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\') as time, content, `object-sequence`, 0 as sent from `queue` where `type` = 'chat' and `from` = '%s' and time between unix_timestamp('%s') and unix_timestamp('%s') \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 0 as sent from `queue` where `type` = 'chat' and `to` = '%s' and time between unix_timestamp('%s') and unix_timestamp('%s') ", owner, from, until, owner, from, until, owner, from, until, owner, from, until);
        } else {
            if(with[0] != '\0') snprintf(sql_query, 4000, " \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\') as time, content, `object-sequence`, 1 as sent from `message-logs` where `from` = '%s' and `to` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 1 as sent from `message-logs` where `from` = '%s' and `to` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 0 as sent from `queue` where `type` = 'chat' and `from` = '%s' and `to` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 0 as sent from `queue` where `type` = 'chat' and `from` = '%s' and `to` = '%s' ", owner, with,  with, owner, owner, with,  with, owner);
            else snprintf(sql_query, 4000, " \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\') as time, content, `object-sequence`, 1 as sent from `message-logs` where `from` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 1 as sent from `message-logs` where `to` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\') as time, content, `object-sequence`, 0 as sent from `queue` where `type` = 'chat' and `from` = '%s' \
                union all \
                select id, `from`, `to`, date_format(CONVERT_TZ(from_unixtime(time), @@session.time_zone, '+00:00'),\'%%Y-%%m-%%dT%%H:%%i:%%S.000Z\'), content, `object-sequence`, 0 as sent from `queue` where `type` = 'chat' and `to` = '%s' ", owner, owner, owner, owner);
        }
        
        memcpy(tmp_sql_query, sql_query, 4000);
        
        if(start != -1 && chunk != -1) {
            snprintf(sql_query, 4000, "select `from`, `to`, `time`, content, `object-sequence`, sent, id from ( %s ) tmp order by `time` desc limit %d, %d", tmp_sql_query, start, chunk);
        } else {
            snprintf(sql_query, 4000, "select `from`, `to`, `time`, content, `object-sequence`, sent, id from ( %s ) tmp order by `time` desc", tmp_sql_query);
        }
        
        fprintf(stdout, "SQL: %s\n", sql_query);
        
        MYSQL_PREPARE_ANSWER("messages", uri_GHYPE, NULL);

            if(mysql_row[0] != NULL && mysql_row[1] != NULL && mysql_row[2] != NULL && mysql_row[3] != NULL && mysql_row[4] != NULL && mysql_row[5] != NULL && mysql_row[6] != NULL) {
                message = nad_append_elem(response, answer, "message", 1);
//                message = nad_insert_elem(response, answer, -1, "message", NULL);
                nad_set_attr(response, message, -1, "xmlns", uri_CLIENT, 0);
                nad_set_attr(response, message, -1, "type", "chat", 0);
                
                nad_set_attr(response, message, -1, "id", mysql_row[6], 0);
                nad_set_attr(response, message, -1, "from", mysql_row[0], 0);
                nad_set_attr(response, message, -1, "to", mysql_row[1], 0);
                nad_insert_elem(response, message, -1, "body", mysql_row[3]);
                
                delay = nad_insert_elem(response, message, -1, "delay", NULL);
                nad_set_attr(response, delay, -1, "xmlns", uri_URN_DELAY, 0);
                nad_set_attr(response, delay, -1, "stamp", mysql_row[2], 0);
                
                mark = nad_insert_elem(response, message, -1, "markable", NULL);
                nad_set_attr(response, mark, -1, "xmlns", "urn:xmpp:chat-markers:0", 0);
                
                if(strcasecmp(mysql_row[5], "1") == 0) ack = nad_insert_elem(response, message, -1, "acknowledged", NULL);
                else ack = nad_insert_elem(response, message, -1, "received", NULL);
                nad_set_attr(response, ack, -1, "xmlns", "urn:xmpp:chat-markers:0", 0);
                nad_set_attr(response, ack, -1, "id", "", 0);
            }

        MYSQL_CLOSE_ANSWER

    MYSQL_DISCONNECT

    return result;
}

int ghype_get_validation(nad_t response, mod_ghype_t g, const char* owner, const char* user) {
    int result = FALSE, found = FALSE, status, ns, answer, message, delay, mark, ack;
    char sql_query[512];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "select jid from ( select concat(username, '@', realm) as jid  from `authreg` ) as auth where jid = '%s'", user);

        fprintf(stdout, "SQL: %s\n", sql_query);
        
        ns = nad_add_namespace(response, uri_GHYPE, NULL);

        if(mysql_real_query(conn, sql_query, strlen(sql_query)) == 0) {
            result = TRUE;
            do {
                MYSQL_RES* mysql_res = NULL;
                if((mysql_res = mysql_store_result(conn)) != NULL) {
                    MYSQL_ROW mysql_row = NULL;
                    for(i = 0; i < mysql_num_rows(mysql_res); i++) {
                        if((mysql_row = mysql_fetch_row(mysql_res)) != NULL) {
                            found = TRUE;
                        }
                    }
                    mysql_free_result(mysql_res);
                }
                if((status = mysql_next_result(conn)) > 0) result = FALSE;
            } while(status == 0);
        }
        
        if(found) {
            answer = nad_insert_elem(response, -1, -1, "validation", "true");
            nad_set_attr(response, answer, -1, "user", user, 0);
        } else {
            answer = nad_insert_elem(response, -1, -1, "validation", "false");
            nad_set_attr(response, answer, -1, "user", user, 0);
        }

    MYSQL_DISCONNECT
    return result;
}

int ghype_get_contacts(nad_t response, mod_ghype_t g, const char* owner, const char* string) {
    int result = FALSE, status, ns, answer, message, delay, mark, ack, contact;
    char sql_query[512];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "select tmp.jid, ri.`to`, ri.`from`, ri.`ask` from ( select concat(username, '@', realm) as jid  from `authreg` ) as tmp left outer join ( select `jid`, `to`, `from`, `ask` from `roster-items` where `collection-owner` = '%s' ) as ri on tmp.jid = ri.jid", owner);

        fprintf(stdout, "SQL: %s\n", sql_query);
        
        MYSQL_PREPARE_ANSWER("contacts", uri_GHYPE, NULL);

            if(mysql_row[0] != NULL) {
                contact = nad_insert_elem(response, answer, -1, "contact", mysql_row[4]);
                nad_set_attr(response, contact, -1, "jid", mysql_row[0], 0);
//                if(mysql_row[1] != NULL) nad_set_attr(response, contact, -1, "to", mysql_row[1], 0);
//                if(mysql_row[2] != NULL) nad_set_attr(response, contact, -1, "from", mysql_row[2], 0);
//                if(mysql_row[3] != NULL) nad_set_attr(response, contact, -1, "ask", mysql_row[3], 0);
            }
        
        MYSQL_CLOSE_ANSWER

    MYSQL_DISCONNECT
    return result;
}

int ghype_get_files(nad_t response, mod_ghype_t g, const char* owner, const char* from, const char* until, const char* type, const char* option) {
    int result = FALSE, status, ns, answer, file;
    char sql_query[512];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        if(type == NULL || type[0] == '\0') {
            if(option[0] != '\0') snprintf(sql_query, 512, "select `from`, `to`, timestamp, uniqueid, filename, description from `file-transfer` where timestamp between '%s' and '%s' and ((`to` = '%s' and `from` = '%s') or (`from` = '%s' and `to` = '%s')) and status = 'finish'", from, until, owner, option, owner, option);
            else snprintf(sql_query, 512, "select `from`, `to`, timestamp, uniqueid, filename, description from `file-transfer` where timestamp between '%s' and '%s' and (`from` = '%s' or `to` = '%s') and status = 'finish'", from, until, owner, owner);
        } else if(strcasecmp(type, "pull") == 0) {
            if(option[0] != '\0') snprintf(sql_query, 512, "select `from`, `to`, timestamp, uniqueid, filename, description from `file-transfer` where `type` = '%s' and timestamp between '%s' and '%s' and `to` = '%s' and `from` = '%s' and status = 'finish'", type, from, until, owner, option);
            else snprintf(sql_query, 512, "select `from`, `to`, timestamp, uniqueid, filename, description from `file-transfer` where `type` = '%s' and timestamp between '%s' and '%s' and `to` = '%s' and status = 'finish'", type, from, until, owner);
        } else {
            if(option[0] != '\0') snprintf(sql_query, 512, "select `from`, `to`, timestamp, uniqueid, filename, description from `file-transfer` where `type` = '%s' and timestamp between '%s' and '%s' and `from` = '%s' and `to` = '%s' and status = 'finish'", type, from, until, owner, option);
            else snprintf(sql_query, 512, "select `from`, `to`, timestamp, uniqueid, filename, description from `file-transfer` where `type` = '%s' and timestamp between '%s' and '%s' and `from` = '%s' and status = 'finish'", type, from, until, owner);
        }
        
        fprintf(stdout, "SQL: %s\n", sql_query);
        
        MYSQL_PREPARE_ANSWER("files", uri_GHYPE, NULL);

            if(mysql_row[0] != NULL && mysql_row[1] != NULL && mysql_row[2] != NULL && mysql_row[3] != NULL && mysql_row[4] != NULL) {
                file = nad_insert_elem(response, answer, -1, "file", mysql_row[4]);
                nad_set_attr(response, file, -1, "from", mysql_row[0], 0);
                nad_set_attr(response, file, -1, "to", mysql_row[1], 0);
                nad_set_attr(response, file, -1, "data", mysql_row[2], 0);
                nad_set_attr(response, file, -1, "uid", mysql_row[3], 0);
                if(mysql_row[5] != NULL) nad_set_attr(response, file, -1, "desc", mysql_row[5], 0);
            }
        
        MYSQL_CLOSE_ANSWER

    MYSQL_DISCONNECT

    return result;
}

int ghype_get_file(nad_t response, mod_ghype_t g, const char* owner, const char* option, const char* str_sm, const char* str_c2s, const char* str_id) {
    int result = FALSE, status, ns, ns1, ns2, ns3, ns4, ns5, answer, si, file, feature, x, field, opt;
    char sql_query[512], tmp_buff[128];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "select sid, `from`, `to`, timestamp, filename, filesize, description from `file-transfer` where `type` = 'push' and uniqueid = '%s' and status = 'finish'", option);
        
        fprintf(stdout, "SQL: %s\n", sql_query);

        MYSQL_PREPARE_ANSWER("iq", uri_CLIENT, NULL);
            ns1 = nad_append_namespace(response, answer, uri_SESSION, "sc");
            nad_set_attr(response, answer, ns1, "sm", str_sm, 0);
            nad_set_attr(response, answer, ns1, "c2s", str_c2s, 0);
                            
            if(mysql_row[0] != NULL && mysql_row[1] != NULL && mysql_row[2] != NULL && mysql_row[3] != NULL && mysql_row[4] != NULL && mysql_row[5] != NULL) {
                snprintf(tmp_buff, 128, "%s/offline", mysql_row[1]);
                nad_set_attr(response, answer, ns, "from", tmp_buff, 0);
                nad_set_attr(response, answer, ns, "to", owner, 0);
                nad_set_attr(response, answer, ns, "type", "set", 0);
                if(str_id[0] != '\0') nad_set_attr(response, answer, ns, "id", str_id, 0);
                else {
                    generate_unique_id(tmp_buff, "pull");
                    nad_set_attr(response, answer, ns, "id", tmp_buff, 0);
                }
                
                ns1 = nad_add_namespace(response, uri_PLUGIN_SI, NULL);
                si = nad_append_elem(response, ns1, "si", 1);
                nad_set_attr(response, si, -1, "profile", uri_PLUGIN_SI_FILE_TRANSFER, 0);
                nad_set_attr(response, si, -1, "id", mysql_row[0], 0);
                nad_set_attr(response, si, -1, "uid", option, 0);
                
                ns2 = nad_add_namespace(response, uri_PLUGIN_SI_FILE_TRANSFER, NULL);
                file = nad_append_elem(response, ns2, "file", 2);
                nad_set_attr(response, file, -1, "date", mysql_row[3], 0);
                nad_set_attr(response, file, -1, "name", mysql_row[4], 0);
                nad_set_attr(response, file, -1, "size", mysql_row[5], 0);
                if(mysql_row[6] != NULL && mysql_row[6][0] != '\0') nad_set_attr(response, file, -1, "desc", mysql_row[6], 0);
                
                ns3 = nad_add_namespace(response, uri_PLUGIN_SI_FEATURE_NEG, NULL);
                feature = nad_append_elem(response, ns3, "feature", 2);
                
                ns4 = nad_add_namespace(response, uri_XDATA, NULL);
                x = nad_append_elem(response, ns4, "x", 3);
                nad_append_attr(response, x, "type", "form");
                
                ns5 = nad_add_namespace(response, "", NULL);
                field = nad_append_elem(response, ns5, "field", 4);
                nad_append_attr(response, field, "type", "list-single");
                nad_append_attr(response, field, "var", "stream-method");
                
                opt = nad_append_elem(response, ns5, "option", 5);
                nad_insert_elem(response, opt, -1, "value", uri_PLUGIN_SI_BYTESTREAM);
            }
        MYSQL_CLOSE_ANSWER

    MYSQL_DISCONNECT

    return result;
}

int ghype_get_preferences(nad_t response, mod_ghype_t g, const char* owner) {
    int result = FALSE, preference, ns, answer, status;
    char sql_query[512], tmp_buff[128];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "select name, value from `preferences` where `collection-owner` = '%s' order by `object-sequence`", owner);
        
        fprintf(stdout, "SQL: %s\n", sql_query);

        MYSQL_PREPARE_ANSWER("preferences", uri_GHYPE, NULL);

            if(mysql_row[0] != NULL && mysql_row[1] != NULL) {
                preference = nad_insert_elem(response, answer, -1, "preference", mysql_row[1]);
                nad_set_attr(response, preference, -1, "name", mysql_row[0], 0);
            }
        
        MYSQL_CLOSE_ANSWER

    MYSQL_DISCONNECT

    return result;
}

int ghype_del_preferences(nad_t response, mod_ghype_t g, const char* owner) {
    int result = FALSE, preference, status;
    char sql_query[512];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "delete from `preferences` where `collection-owner` = '%s'", owner);
        
        fprintf(stdout, "SQL: %s\n", sql_query);
        
        MYSQL_EXECUTE

    MYSQL_DISCONNECT

    return result;
}

int ghype_set_preference(nad_t response, mod_ghype_t g, const char* owner, const char* name, const char* value) {
    int result = FALSE, status;
    char sql_query[512];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "replace into `preferences` (`collection-owner`, `name`, `value`) values('%s', '%s', '%s')", owner, name, value);
        
        fprintf(stdout, "SQL: %s\n", sql_query);
        
        MYSQL_EXECUTE

    MYSQL_DISCONNECT

    return result;
}

int ghype_get_version(nad_t response, mod_ghype_t g, const char* owner, const char *so) {
    int result = FALSE, version, ns, answer, status;
    char sql_query[512], tmp_buff[128];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "select target_version, data from `updates` where so = '%s' order by data desc limit 1", so);
        
        fprintf(stdout, "SQL: %s\n", sql_query);

        MYSQL_PREPARE_ANSWER("version", uri_GHYPE, NULL);

            if(mysql_row[0] != NULL && mysql_row[1] != NULL) {
                version = nad_insert_elem(response, answer, -1, "version", mysql_row[0]);
                nad_set_attr(response, version, -1, "data", mysql_row[1], 0);
            }
        
        MYSQL_CLOSE_ANSWER

    MYSQL_DISCONNECT

    return result;
}

int ghype_get_update(nad_t response, mod_ghype_t g, const char* owner, const char* source, const char* version, const char *so) {
    int result = FALSE, update, ns, answer, status, ready = FALSE;
    char sql_query[512], tmp_buff[128];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "select data, target_version, type, filename from `updates` where so = '%s' and source_version = '%s' and target_version = '%s'", so, source, version);
        
        fprintf(stdout, "SQL: %s\n", sql_query);

        MYSQL_PREPARE_ANSWER("updates", uri_GHYPE, NULL);

            if(mysql_row[0] != NULL && mysql_row[1] != NULL && mysql_row[2] != NULL && mysql_row[3] != NULL) {
                update = nad_insert_elem(response, answer, -1, "update", mysql_row[3]);
                nad_set_attr(response, update, -1, "data", mysql_row[0], 0);
                nad_set_attr(response, update, -1, "version", mysql_row[1], 0);
                nad_set_attr(response, update, -1, "type", mysql_row[2], 0);
                ready = TRUE;
            }
        
        MYSQL_CLOSE_ANSWER
        
        if(!ready) {
            memset(sql_query, '\0', 512);
            snprintf(sql_query, 512, "select data, target_version, type, filename from `updates` where so = '%s' and isNull(source_version) and target_version = '%s'", so, version);
            
            fprintf(stdout, "SQL: %s\n", sql_query);

            MYSQL_PREPARE_ANSWER(NULL, NULL, NULL);

                if(mysql_row[0] != NULL && mysql_row[1] != NULL && mysql_row[2] != NULL && mysql_row[3] != NULL) {
                    update = nad_insert_elem(response, answer, -1, "update", mysql_row[3]);
                    nad_set_attr(response, update, -1, "data", mysql_row[0], 0);
                    nad_set_attr(response, update, -1, "version", mysql_row[1], 0);
                    nad_set_attr(response, update, -1, "type", mysql_row[2], 0);
                }
            
            MYSQL_CLOSE_ANSWER
        }

    MYSQL_DISCONNECT

    return result;
}

int ghype_set_version(nad_t response, mod_ghype_t g, const char* owner, const char* version, const char *data, const char *so, const char *app) {
    int result = FALSE, status;
    char sql_query[512];
    
    MYSQL_CONNECT
        memset(sql_query, '\0', 512);
        snprintf(sql_query, 512, "replace into `versions` (`collection-owner`, `so`, `app`, `data`, `version`) values('%s', '%s', '%s', '%s', '%s')", owner, so, app, data, version);
        
        fprintf(stdout, "SQL: %s\n", sql_query);
        
        MYSQL_EXECUTE

    MYSQL_DISCONNECT

    return result;
}
