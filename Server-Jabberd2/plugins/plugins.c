/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002-2003 Jeremie Miller, Thomas Muldowney,
 *                         Ryan Eatmon, Robert Norris
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

/** @file plugins/plugins.c
  * @brief xmpp plugins
  * $Revision: 1.0 $
  */

#include "plugins.h"

void debug_packet(const char *dir, nad_t nad) {
#ifdef PLUGIN_DEBUG
    const char *debug_buff;
    char tmp_buffer[2048];
    int debug_buff_len;
  
    nad_print(nad, 0, &debug_buff, &debug_buff_len);
    memset(tmp_buffer, '\0', 2048);
    if(debug_buff_len < 2000) memcpy(tmp_buffer, debug_buff, debug_buff_len);
    else memcpy(tmp_buffer, debug_buff, 2000);
    fprintf(stdout, "\nPLUGIN %s: %s\n\0", dir, tmp_buffer);
    fflush(stdout);
#endif
}

void generate_unique_id(char *buffer, const char* prefix) {
    int iseed, irand;
        
    iseed = time(NULL);
    srand(iseed);
    irand = (unsigned int)( 100.0 * rand_r(&iseed) / ( RAND_MAX + 1.0 ) );
    memset(buffer, '\0', 128);
    snprintf(buffer, 128, "%s%03d%lu", prefix, irand, time(NULL));
}

struct _ghype_error_st _ghype_errors[] = {
    { "bad-request",                "modify",   "400" },    /* ghype_err_BAD_REQUEST */
    { "query-message-error",        "cancel",   "501" },    /* ghype_err_QUERY_MESSAGE */
    { "feature-not-implemented",    "cancel",   "501" },    /* ghype_err_FEATURE_NOT_IMPLEMENTED */
    { "unknown-error",              "cancel",   "501" },    /* ghype_err_UNKNOWN_ERROR */
    { NULL,                         NULL,       NULL  }
};

void nad_set_error(nad_t nad, int root, int code) {
    int ns, elem, err;
     
    nad_set_attr(nad, root, -1, "type", "error", 5);
    elem = nad_insert_elem(nad, root, -1, "error", NULL);
 
    err = code;
    if(err >= stanza_err_BAD_REQUEST && err < stanza_err_LAST) {
        err = err - stanza_err_BAD_REQUEST;
        if(_stanza_errors[err].code != NULL) nad_set_attr(nad, elem, -1, "code", _stanza_errors[err].code, 0);
        if(_stanza_errors[err].type != NULL) nad_set_attr(nad, elem, -1, "type", _stanza_errors[err].type, 0);
        if(_stanza_errors[err].name != NULL) {
            ns = nad_add_namespace(nad, uri_STANZA_ERR, NULL);
            nad_insert_elem(nad, elem, ns, _stanza_errors[err].name, NULL);
        }
    } else {
        if(!(err >= ghype_err_BAD_REQUEST && err < ghype_err_LAST)) err = ghype_err_UNKNOWN_ERROR;
        err = err - ghype_err_BAD_REQUEST;
        if(_ghype_errors[err].code != NULL) nad_set_attr(nad, elem, -1, "code", _ghype_errors[err].code, 0);
        if(_ghype_errors[err].type != NULL) nad_set_attr(nad, elem, -1, "type", _ghype_errors[err].type, 0);
        if(_ghype_errors[err].name != NULL) {
            ns = nad_add_namespace(nad, uri_GHYPE_ERROR, NULL);
            nad_insert_elem(nad, elem, ns, _ghype_errors[err].name, NULL);
        }
    }
}

int ghype_protocol(nad_t nad, mod_ghype_t g, int root, const char* where, int ns, const char* owner, const char* to) {
    int handled = FALSE, query, item, attr;
    char str_owner[512], *ptr = NULL;
    nad_t response = NULL;

    debug_packet("IN", nad);
    
    if(owner != NULL) {
        snprintf(str_owner, 512, "%s\0", owner);
        if((ptr = strchr(str_owner, '/')) != NULL) *ptr = '\0';
    
        if(strcasecmp(where, "in_sess") == 0 && (query = nad_find_elem(nad, root, ns, "query", 1)) >= 0 && (attr = nad_find_attr(nad, root, -1, "type", NULL)) >= 0 && NAD_AVAL_L(nad, attr) == 3 && strncmp("get", NAD_AVAL(nad, attr), 3) == 0) {
            if((attr = nad_find_attr(nad, item, -1, "obfuscate", NULL)) >= 0 && NAD_AVAL_L(nad, attr) == 3 && strncmp("yes", NAD_AVAL(nad, attr), 3) == 0) {
                // TODO: if obfuscate then transform the message. Not implemented yet
            }
            if((response = nad_new()) != NULL) {
                item = query;
                for(item = nad_find_elem(nad, item, -1, "item", 1); ; item = nad_find_elem(nad, item, -1, "item", 0)) {
                    if(item < 0) break;

                    if((attr = nad_find_attr(nad, item, -1, "ask", NULL)) >= 0) {
                        if(NAD_AVAL_L(nad, attr) == 8 && strncmp("messages", NAD_AVAL(nad, attr), 8) == 0) {
                            int from, until, with, start, chunk, int_start = -1, int_chunk = -1;
                            char str_from[20], str_until[20], str_with[512], str_start[20], str_chunk[20];
                            
                            str_from[0] = str_until[0] = str_with[0] = str_start[0] = str_chunk[0] = '\0';

                            if((from = nad_find_attr(nad, item, -1, "from", NULL)) >= 0 && (until = nad_find_attr(nad, item, -1, "until", NULL)) >= 0) {
                                snprintf(str_from, 20, "%.*s\0", NAD_AVAL_L(nad, from), NAD_AVAL(nad, from));
                                snprintf(str_until, 20, "%.*s\0", NAD_AVAL_L(nad, until), NAD_AVAL(nad, until));
                            }

                            if((start = nad_find_attr(nad, item, -1, "start", NULL)) >= 0 && (chunk = nad_find_attr(nad, item, -1, "chunk", NULL)) >= 0) {
                                snprintf(str_start, 20, "%.*s\0", NAD_AVAL_L(nad, start), NAD_AVAL(nad, start));
                                snprintf(str_chunk, 20, "%.*s\0", NAD_AVAL_L(nad, chunk), NAD_AVAL(nad, chunk));
                                int_start = atoi(str_start);
                                int_chunk = atoi(str_chunk);
                            }
                            if(int_start == -1 || int_chunk == -1) { int_start = -1; int_chunk = -1; }
                            
                            if((with = nad_find_attr(nad, item, -1, "with", NULL)) >= 0) {
                                snprintf(str_with, 512, "%.*s\0", NAD_AVAL_L(nad, with), NAD_AVAL(nad, with));
                                if((ptr = strchr(str_with, '/')) != NULL) *ptr = '\0';
                            } else str_with[0] = '\0';
                            
                            if(str_from[0] != '\0' || str_until[0] != '\0' || int_start != -1 || int_chunk != -1) {
                                if((handled = ghype_get_messages(response, g, str_owner, str_from, str_until, int_start, int_chunk, str_with))) {
                                    nad_set_attr(nad, root, -1, "type", "result", 6);
                                } else {
                                    nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                                }
                            }
                        } else if(NAD_AVAL_L(nad, attr) == 3 && strncmp("utc", NAD_AVAL(nad, attr), 3) == 0) {
                          if((handled = ghype_get_date_utc(response, g))) {
                            nad_set_attr(nad, root, -1, "type", "result", 6);
                          } else {
                            nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                          }
                        } else if(NAD_AVAL_L(nad, attr) == 8 && strncmp("contacts", NAD_AVAL(nad, attr), 8) == 0) {
                            int string;
                            char str_string[512];
                            if((string = nad_find_attr(nad, item, -1, "string", NULL)) >= 0) {
                                snprintf(str_string, 512, "%.*s\0", NAD_AVAL_L(nad, string), NAD_AVAL(nad, string));
                                if((handled = ghype_get_contacts(response, g, str_owner, str_string))) {
                                    nad_set_attr(nad, root, -1, "type", "result", 6);
                                } else {
                                    nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                                }
                            } else {
                              nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                            }
                        } else if(NAD_AVAL_L(nad, attr) == 5 && strncmp("files", NAD_AVAL(nad, attr), 5) == 0) {
                            int from, until, type, option;
                            char str_from[20], str_until[20], str_type[20], str_option[512];

                            if((from = nad_find_attr(nad, item, -1, "from", NULL)) >= 0 && (until = nad_find_attr(nad, item, -1, "until", NULL)) >= 0) {
                                snprintf(str_from, 20, "%.*s\0", NAD_AVAL_L(nad, from), NAD_AVAL(nad, from));
                                snprintf(str_until, 20, "%.*s\0", NAD_AVAL_L(nad, until), NAD_AVAL(nad, until));
                                if((type = nad_find_attr(nad, item, -1, "type", NULL)) >= 0) {
                                    snprintf(str_type, 20, "%.*s\0", NAD_AVAL_L(nad, type), NAD_AVAL(nad, type));
                                } else str_type[0] = '\0';
                                if((option = nad_find_attr(nad, item, -1, "option", NULL)) >= 0) {
                                    snprintf(str_option, 512, "%.*s\0", NAD_AVAL_L(nad, option), NAD_AVAL(nad, option));
                                    if((ptr = strchr(str_option, '/')) != NULL) *ptr = '\0';
                                } else str_option[0] = '\0';

                                if((handled = ghype_get_files(response, g, str_owner, str_from, str_until, str_type, str_option))) {
                                    nad_set_attr(nad, root, -1, "type", "result", 6);
                                } else {
                                    nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                                }
                            }
                        } else if(NAD_AVAL_L(nad, attr) == 11 && strncmp("preferences", NAD_AVAL(nad, attr), 11) == 0) {
                            if((handled = ghype_get_preferences(response, g, str_owner))) {
                                nad_set_attr(nad, root, -1, "type", "result", 6);
                            } else {
                                nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                            }
                        } else if(NAD_AVAL_L(nad, attr) == 7 && strncmp("version", NAD_AVAL(nad, attr), 7) == 0) {
                            int so;
                            char str_so[20];
                            
                            if((so = nad_find_attr(nad, item, -1, "so", NULL)) >= 0) {
                                snprintf(str_so, 20, "%.*s\0", NAD_AVAL_L(nad, so), NAD_AVAL(nad, so));
                            } else str_so[0] = '\0';
                            if((handled = ghype_get_version(response, g, str_owner, str_so))) {
                                nad_set_attr(nad, root, -1, "type", "result", 6);
                            } else {
                                nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                            }
                        } else if(NAD_AVAL_L(nad, attr) == 6 && strncmp("update", NAD_AVAL(nad, attr), 6) == 0) {
                            int version, source, so;
                            char str_version[20], str_source[20], str_so[20];

                            if((version = nad_find_attr(nad, item, -1, "version", NULL)) >= 0 && (so = nad_find_attr(nad, item, -1, "so", NULL)) >= 0 && (source = nad_find_attr(nad, item, -1, "source-version", NULL)) >= 0) {
                                snprintf(str_version, 20, "%.*s\0", NAD_AVAL_L(nad, version), NAD_AVAL(nad, version));
                                snprintf(str_so, 20, "%.*s\0", NAD_AVAL_L(nad, so), NAD_AVAL(nad, so));
                                snprintf(str_source, 20, "%.*s\0", NAD_AVAL_L(nad, source), NAD_AVAL(nad, source));

                                if((handled = ghype_get_update(response, g, str_owner, str_source, str_version, str_so))) {
                                    nad_set_attr(nad, root, -1, "type", "result", 6);
                                } else {
                                    nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                                }
                            }
                        } else if(NAD_AVAL_L(nad, attr) == 10 && strncmp("validation", NAD_AVAL(nad, attr), 10) == 0) {
                            int user;
                            char str_user[128];

                            if((user = nad_find_attr(nad, item, -1, "user", NULL)) >= 0) {
                                snprintf(str_user, 128, "%.*s\0", NAD_AVAL_L(nad, user), NAD_AVAL(nad, user));

                                if((handled = ghype_get_validation(response, g, str_owner, str_user))) {
                                    nad_set_attr(nad, root, -1, "type", "result", 6);
                                } else {
                                    nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                                }
                            }
                          
                        }
                    }
                }
            }
        } else if(strcasecmp(where, "in_sess") == 0 && (query = nad_find_elem(nad, root, ns, "file-transfer", 1)) >= 0 && (attr = nad_find_attr(nad, root, -1, "type", NULL)) >= 0 && NAD_AVAL_L(nad, attr) == 3 && strncmp("get", NAD_AVAL(nad, attr), 3) == 0) {
            if((response = nad_new()) != NULL) {
                char str_option[512], str_sm[512], str_c2s[512], str_id[512];
                int elem;
                snprintf(str_option, 512, "%.*s", NAD_CDATA_L(nad, query), NAD_CDATA(nad, query));
                if((attr = nad_find_attr(nad, root, -1, "sm", NULL)) >= 0) snprintf(str_sm, 512, "%.*s\0", NAD_AVAL_L(nad, attr), NAD_AVAL(nad, attr));
                else str_sm[0] = '\0';
                if((attr = nad_find_attr(nad, root, -1, "c2s", NULL)) >= 0) snprintf(str_c2s, 512, "%.*s\0", NAD_AVAL_L(nad, attr), NAD_AVAL(nad, attr));
                else str_c2s[0] = '\0';
                if((attr = nad_find_attr(nad, root, -1, "id", NULL)) >= 0) snprintf(str_id, 512, "%.*s\0", NAD_AVAL_L(nad, attr), NAD_AVAL(nad, attr));
                else str_id[0] = '\0';

                if((handled = ghype_get_file(response, g, str_owner, str_option, str_sm, str_c2s, str_id))) {
                    nad_drop_elem(nad, root);
                    elem = nad_insert_nad(nad, 0, response, 0);
                    nad_append_namespace(nad, elem, uri_CLIENT, NULL);
                    nad_append_namespace(nad, elem, uri_SESSION, "sc");

                    nad_free(response);
                    response = NULL;
                } else {
                    nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                }
            }
        } else if(strcasecmp(where, "in_sess") == 0 && (query = nad_find_elem(nad, root, ns, "preferences", 1)) >= 0 && (attr = nad_find_attr(nad, root, -1, "type", NULL)) >= 0 && NAD_AVAL_L(nad, attr) == 3 && strncmp("set", NAD_AVAL(nad, attr), 3) == 0) {
            if((response = nad_new()) != NULL) {
                int pref_result = TRUE;
                item = query;

                if((handled = ghype_del_preferences(response, g, str_owner))) {
                    for(item = nad_find_elem(nad, item, -1, "preference", 1); ; item = nad_find_elem(nad, item, -1, "preference", 0)) {
                        char str_name[512], str_value[512];
                        if(item < 0) break;

                        snprintf(str_value, 512, "%.*s", NAD_CDATA_L(nad, item), NAD_CDATA(nad, item));
                        if((attr = nad_find_attr(nad, item, -1, "name", NULL)) >= 0) snprintf(str_name, 512, "%.*s\0", NAD_AVAL_L(nad, attr), NAD_AVAL(nad, attr));
                        else str_name[0] = '\0';
                        
                        if(str_name[0] != '\0' && str_value[0] != '\0') {
                            if((handled = ghype_set_preference(response, g, str_owner, str_name, str_value))) {
                            } else {
                                pref_result = FALSE;
                                break;
                            }
                        }
                    }
                } else {
                    pref_result = FALSE;
                }
                    
                if(pref_result) {
                    nad_set_attr(nad, root, -1, "type", "result", 6);
                    handled = TRUE;
                } else {
                    nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                    handled = FALSE;
                }
            }
        } else if(strcasecmp(where, "in_sess") == 0 && (query = nad_find_elem(nad, root, ns, "version", 1)) >= 0 && (attr = nad_find_attr(nad, root, -1, "type", NULL)) >= 0 && NAD_AVAL_L(nad, attr) == 3 && strncmp("set", NAD_AVAL(nad, attr), 3) == 0) {
            if((response = nad_new()) != NULL) {
                char str_data[20], str_version[20], str_so[20], str_app[256];
                int elem;

                snprintf(str_version, 20, "%.*s", NAD_CDATA_L(nad, query), NAD_CDATA(nad, query));
                if((attr = nad_find_attr(nad, query, -1, "data", NULL)) >= 0) snprintf(str_data, 20, "%.*s\0", NAD_AVAL_L(nad, attr), NAD_AVAL(nad, attr));
                else str_data[0] = '\0';
                if((attr = nad_find_attr(nad, query, -1, "so", NULL)) >= 0) snprintf(str_so, 20, "%.*s\0", NAD_AVAL_L(nad, attr), NAD_AVAL(nad, attr));
                else str_so[0] = '\0';
                if((attr = nad_find_attr(nad, query, -1, "application-id", NULL)) >= 0) snprintf(str_app, 256, "%.*s\0", NAD_AVAL_L(nad, attr), NAD_AVAL(nad, attr));
                else str_app[0] = '\0';

                if((handled = ghype_set_version(response, g, str_owner, str_version, str_data, str_so, str_app))) {
                    nad_set_attr(nad, root, -1, "type", "result", 6);
                } else {
                    nad_set_error(nad, root, ghype_err_QUERY_MESSAGE);
                }
            }
        
        } else if(strcasecmp(where, "pkt_user") == 0) {
//            fprintf(stdout, "PKT-USER pck from %s to %s\n", owner, to);
//            handled = TRUE;
        }
    }

    if(handled && response != NULL) {
        nad_drop_elem(nad, query);
        nad_insert_nad(nad, root, response, 0);
    }
    
    
    if(response != NULL) nad_free(response);
    if(!handled) {
        fprintf(stdout, "PLUGIN UNHANDLED packet in %s from %s to %s\n", where, owner, to);
    } else {
        debug_packet("OUT", nad);
    }
    return handled;
}

static void _plugins_unload(sx_plugin_t p) {
    plugins_t ctx = (plugins_t) p->private;
    if(ctx != NULL) {
        if(ctx->thread_id != -1) {
            pthread_cancel(ctx->thread_id);
            ctx->thread_id = -1;
        }
        sem_destroy(&(ctx->semaphore));
        if(ctx->module != NULL) free(ctx->module);
        free(ctx);
        p->private = NULL;
    }
}

/** plugins initialiser */
/** args: source c2s/s2s/sm/router */
int plugin_init(sx_env_t env, sx_plugin_t p, va_list args) {
    const char *module = NULL;
    plugins_t ctx;
    
    if(p->private != NULL) return 1;
    if((ctx = (plugins_t) calloc(1, sizeof(struct _plugins_st))) != NULL) {
        module = va_arg(args, const char *);
        ctx->module = strdup(module);
        ctx->c2s = NULL;
        ctx->s2s = NULL;
        ctx->sm = NULL;
        ctx->router = NULL;
        ctx->transfer = NULL;
        ctx->thread_id = -1;
        sem_init(&(ctx->semaphore), 0, 1);
        p->private = (void*) ctx;
        p->unload = _plugins_unload;
        
        if(strcasecmp(ctx->module, "c2s") == 0) {
            ctx->c2s = va_arg(args, struct c2s_st *);
            c2s_plugin_init(env, p);
        } else if(strcasecmp(ctx->module, "s2s") == 0) {
            ctx->s2s = va_arg(args, struct s2s_st *);
            s2s_plugin_init(env, p);
        } else if(strcasecmp(ctx->module, "sm") == 0) {
            ctx->sm = va_arg(args, struct sm_st *);
            sm_plugin_init(env, p);
        } else if(strcasecmp(ctx->module, "router") == 0) {
            ctx->router = va_arg(args, struct router_st *);
            router_plugin_init(env, p);
        }
    }

    return 0;
}
