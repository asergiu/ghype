/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002 Jeremie Miller, Thomas Muldowney,
 *                    Ryan Eatmon, Robert Norris
 *
 * This program is free software; you can redistribute it and/or drvify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

/** @file plugins/plugins.h
  * @brief data structures and prototypes for plugins
  * $Revision: 1 $
  */

#ifndef INCL_AXTEN_PLUGINS_H   
#define INCL_AXTEN_PLUGINS_H

#ifdef HAVE_CONFIG_H
  #include <config.h>
#endif

#include "mio/mio.h"
#include "sx/sx.h"

struct sm_st;
struct router_st;
struct c2s_st;
struct s2s_st;

#ifdef HAVE_SIGNAL_H
  #include <signal.h>
#endif
#ifdef HAVE_SYS_STAT_H
  #include <sys/stat.h>
#endif

#include <expat.h>
#include <semaphore.h>
#include <util/util.h>

#ifdef _WIN32
  #ifdef _USRDLL
    #define DLLEXPORT  __declspec(dllexport)
    #define PLUGINS_API     __declspec(dllimport)
  #else
    #define DLLEXPORT  __declspec(dllimport)
    #define PLUGINS_API     __declspec(dllexport)
  #endif
#else
  #define DLLEXPORT
  #define PLUGINS_API
#endif

#define	FILE_TRANSFER_NO_ERROR		0
#define	FILE_TRANSFER_INCOMPLETTE	-1
#define FILE_TRANSFER_STEAMHOST_ERROR	-2 
#define FILE_TRANSFER_SOCKS5_ERROR	-3
#define FILE_TRANSFER_TIMEOUT		-4

#define FILE_TRANSFER_PUSH		1
#define FILE_TRANSFER_PULL              2

typedef struct _file_transfer_st {
  char *filename;
  char *description;
  unsigned long int filesize;
  char *unique_id;
  
  unsigned short int type;
  char *id;
  char *sid;
  char *from;
  char *from_session_id;
  char *to;
  char *to_session_id;
  char *sha1_hash;
  time_t timestamp;
  
  short int error_code;
  
  unsigned short int executed;
  
  struct _file_transfer_st *next;
} *file_transfer_t;

#define ghype_err_BAD_REQUEST              (200)
#define ghype_err_QUERY_MESSAGE            (201)
#define ghype_err_FEATURE_NOT_IMPLEMENTED  (202)
#define ghype_err_UNKNOWN_ERROR            (203)
#define ghype_err_LAST                     (204)

typedef struct _ghype_error_st {
  const char  *name;
  const char  *type;
  const char  *code;
} *ghype_error_t;

JABBERD2_API struct _ghype_error_st _ghype_errors[];
JABBERD2_API void nad_set_error(nad_t nad, int root, int code);

typedef struct _mod_ghype_st {
    int version;
    char *hostname;
    char *port;
    char *dbname;
    char *user;
    char *pass;
} *mod_ghype_t;
    
JABBERD2_API int ghype_protocol(nad_t nad, mod_ghype_t g, int root, const char* where, int ns, const char* from, const char* to);
JABBERD2_API int ghype_get_messages(nad_t response, mod_ghype_t g, const char* owner, const char* from, const char* until, int start, int chunk, const char* with);
JABBERD2_API int ghype_get_date_utc(nad_t response, mod_ghype_t g);
JABBERD2_API int ghype_get_files(nad_t response, mod_ghype_t g, const char* owner, const char* from, const char* until, const char* type, const char* option);
JABBERD2_API int ghype_get_file(nad_t response, mod_ghype_t g, const char* owner, const char* option, const char* str_sm, const char* str_c2s, const char* str_id);
JABBERD2_API int ghype_get_preferences(nad_t response, mod_ghype_t g, const char* owner);
JABBERD2_API int ghype_set_preference(nad_t response, mod_ghype_t g, const char* owner, const char* name, const char* value);
JABBERD2_API int ghype_del_preferences(nad_t response, mod_ghype_t g, const char* owner);
JABBERD2_API int ghype_get_version(nad_t response, mod_ghype_t g, const char* owner, const char *so);
JABBERD2_API int ghype_get_update(nad_t response, mod_ghype_t g, const char* owner, const char* source, const char* version, const char *so);
JABBERD2_API int ghype_set_version(nad_t response, mod_ghype_t g, const char* owner, const char* version, const char *data, const char *so, const char *app);
JABBERD2_API int ghype_get_contacts(nad_t response, mod_ghype_t g, const char* owner, const char* string);
JABBERD2_API int ghype_get_validation(nad_t response, mod_ghype_t g, const char* owner, const char* user);

typedef struct _plugins_st {
  char *module;
  struct c2s_st *c2s;
  struct router_st *router;
  struct s2s_st *s2s;
  struct sm_st *sm;
  
  pthread_t thread_id;
  sem_t semaphore;
  
  struct _file_transfer_st *transfer;
} *plugins_t;

#define VALUE_MAX_LENGTH		128

#define uri_PLUGIN_SI                   "http://jabber.org/protocol/si"
#define uri_PLUGIN_SI_FILE_TRANSFER     "http://jabber.org/protocol/si/profile/file-transfer"
#define uri_PLUGIN_SI_FEATURE_NEG       "http://jabber.org/protocol/feature-neg"
#define uri_PLUGIN_SI_BYTESTREAM        "http://jabber.org/protocol/bytestreams"
#define uri_PLUGIN_IBB			"http://jabber.org/protocol/ibb"

JABBERD2_API void debug_packet(const char *dir, nad_t nad);
JABBERD2_API void generate_unique_id(char *buffer, const char* prefix);

PLUGINS_API int plugin_init(sx_env_t env, sx_plugin_t p, va_list args);
PLUGINS_API int c2s_plugin_init(sx_env_t env, sx_plugin_t p);
PLUGINS_API int s2s_plugin_init(sx_env_t env, sx_plugin_t p);
PLUGINS_API int sm_plugin_init(sx_env_t env, sx_plugin_t p);
PLUGINS_API int router_plugin_init(sx_env_t env, sx_plugin_t p);

#endif
