/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002-2003 Jeremie Miller, Thomas Muldowney,
 *                         Ryan Eatmon, Robert Norris
 *   
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

/** @file plugins/plugins.c
  * @brief xmpp plugins
  * $Revision: 1.0 $
  */    

#include "plugins.h"
#include "sm/sm.h"

static void _sm_plugin_features(sx_t s, sx_plugin_t p, nad_t nad) {
    plugins_t ctx = (plugins_t) p->private;
}

static int _sm_plugin_process(sx_t s, sx_plugin_t p, nad_t nad) {
/*
    int ghype = -1, routed_request = FALSE;
    char root[VALUE_MAX_LENGTH];

    snprintf(root, VALUE_MAX_LENGTH, "%.*s", NAD_ENAME_L(nad, 0), NAD_ENAME(nad, 0));
    if(strcmp(root, "route") == 0) {
        if((ghype = nad_find_elem(nad, 0, -1, "ghype", 1)) >= 0) {
            routed_request = TRUE;
        } else {
            return 1;
        }
    } else if(strcmp(root, "ghype") == 0) {
        routed_request = FALSE;
        ghype = 0;
    } else {
        return 1;
    }

*/
    debug_packet("IN", nad);
    
    return 1;
}

int sm_plugin_init(sx_env_t env, sx_plugin_t p) {
    plugins_t ctx = (plugins_t) p->private;
    if(ctx->sm != NULL) {
        log_write(ctx->sm->log, LOG_NOTICE, "[plugins] loading plugins for sm...");

        p->features = _sm_plugin_features;
        p->process = _sm_plugin_process;
    }
}
