/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002 Jeremie Miller, Thomas Muldowney,
 *                    Ryan Eatmon, Robert Norris
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

#include "sm.h"

/** @file sm/mod_offline.c
  * @brief offline storage
  * @author Robert Norris
  * $Date: 2005/08/17 07:48:28 $
  * $Revision: 1.26 $
  */

typedef struct _mod_offline_st {
    int dropmessages;
    int storeheadlines;
    int dropsubscriptions;
    int userquota;
} *mod_offline_t;

static void _offline_log_msg_db(sm_t sm, const char *id, const char *from, const char *to, time_t t, const char *content) {
    os_t os = NULL;
    os_object_t o = NULL;
    st_ret_t ret;
    
    if((os = os_new()) != NULL && (o = os_object_new(os)) != NULL) {
        os_object_put(o, "id", id, os_type_STRING);
        os_object_put(o, "from", from, os_type_STRING);
        os_object_put(o, "to", to, os_type_STRING);
        os_object_put_time(o, "time", &t);
        os_object_put(o, "content", content, os_type_STRING);
        if((ret = storage_put(sm->st, "message-logs", from, os)) != st_SUCCESS) {
            log_write(sm->log, LOG_ERR, "Unable to write log to database");
        }
        os_free(os);
    }
}

static void _offline_log_pkt_db(sm_t sm, pkt_t pkt) {
    char str_id[255], str_from[255], str_to[255], str_value[2048], *ptr = NULL;
    time_t t = time(NULL);
    int attr, elem;
    
    str_id[0] = str_from[0] = str_to[0] = str_value[0] = '\0';
    
    if((attr = nad_find_attr(pkt->nad, 1, -1, "id", NULL)) >= 0) {
        snprintf(str_id, 255, "%.*s\0", NAD_AVAL_L(pkt->nad, attr), NAD_AVAL(pkt->nad, attr));
    }

    if((attr = nad_find_attr(pkt->nad, 1, -1, "from", NULL)) >= 0) {
        snprintf(str_from, 255, "%.*s\0", NAD_AVAL_L(pkt->nad, attr), NAD_AVAL(pkt->nad, attr));
        if((ptr = strchr(str_from, '/')) != NULL) *ptr = '\0';
    }
    
    if((attr = nad_find_attr(pkt->nad, 1, -1, "to", NULL)) >= 0) {
        snprintf(str_to, 255, "%.*s\0", NAD_AVAL_L(pkt->nad, attr), NAD_AVAL(pkt->nad, attr));
        if((ptr = strchr(str_to, '/')) != NULL) *ptr = '\0';
    }
    
    if((elem = nad_find_elem(pkt->nad, 1, -1, "body", 1)) >= 0) {
        snprintf(str_value, 2048, "%.*s", NAD_CDATA_L(pkt->nad, elem), NAD_CDATA(pkt->nad, elem));
    }
    
    if(str_id[0] != '\0' && str_from[0] != '\0' && str_to[0] != '\0' && str_value[0] != '\0') {
        _offline_log_msg_db(sm, str_id, str_from, str_to, t, str_value);
    }
}

static mod_ret_t _offline_in_sess(mod_instance_t mi, sess_t sess, pkt_t pkt) {
    st_ret_t ret;
    os_t os;
    os_object_t o;
    nad_t nad;
    pkt_t queued;
    int ns, elem, attr, is_chat_content;
    char cttl[15], cstamp[18];
    time_t ttl, stamp;

    /* if they're becoming available for the first time */
    if(pkt->type == pkt_PRESENCE && sess->pri >= 0 && pkt->to == NULL && sess->user->top == NULL) {

        ret = storage_get(pkt->sm->st, "queue", jid_user(sess->jid), NULL, &os);
        if(ret != st_SUCCESS) {
            log_debug(ZONE, "storage_get returned %d", ret);
            return mod_PASS;
        }
        
        if(os_iter_first(os))
            do {
                char *type = NULL;
                o = os_iter_object(os);
                
                if(os_object_get_str(os, o, "type", &type) && type != NULL && strcasecmp(type, "chat") == 0) is_chat_content = TRUE;
                else is_chat_content = FALSE;

                if(!is_chat_content && os_object_get_nad(os, o, "content", &nad)) {
                    queued = pkt_new(pkt->sm, nad_copy(nad));
                    if(queued == NULL) {
                        log_debug(ZONE, "invalid queued packet, not delivering");
                    } else {
                        /* check expiry as necessary */
                        if((ns = nad_find_scoped_namespace(queued->nad, uri_EXPIRE, NULL)) >= 0 &&
                           (elem = nad_find_elem(queued->nad, 1, ns, "x", 1)) >= 0 &&
                           (attr = nad_find_attr(queued->nad, elem, -1, "seconds", NULL)) >= 0) {
                            snprintf(cttl, 15, "%.*s", NAD_AVAL_L(queued->nad, attr), NAD_AVAL(queued->nad, attr));
                            ttl = atoi(cttl);

                            /* it should have a x:delay stamp, because we stamp everything we store */
                            if((ns = nad_find_scoped_namespace(queued->nad, uri_DELAY, NULL)) >= 0 &&
                               (elem = nad_find_elem(queued->nad, 1, ns, "x", 1)) >= 0 &&
                               (attr = nad_find_attr(queued->nad, elem, -1, "stamp", NULL)) >= 0) {
                                snprintf(cstamp, 18, "%.*s", NAD_AVAL_L(queued->nad, attr), NAD_AVAL(queued->nad, attr));
                                stamp = datetime_in(cstamp);

                                if(stamp + ttl <= time(NULL)) {
                                    log_debug(ZONE, "queued packet has expired, dropping");
                                    pkt_free(queued);
                                    continue;
                                }
                            }
                        }

                        log_debug(ZONE, "delivering queued packet to %s", jid_full(sess->jid));
                        pkt_sess(queued, sess);
                    }
                } else {
                    int ns, r, msg, tmp;
                    int t;
                    char *from = NULL, *to = NULL, *id = NULL, *content = NULL, *tm = NULL;
                    
                    if(os_object_get_str(os, o, "id", &id) && os_object_get_str(os, o, "from", &from) && os_object_get_str(os, o, "to", &to) && 
                       os_object_get_str(os, o, "content", &content) && os_object_get_int(os, o, "time", &t)) {
                       nad_t nad;
                       
                       if((nad = nad_new()) != NULL) {
                           ns = nad_add_namespace(nad, uri_COMPONENT, NULL);
                           nad_append_elem(nad, ns, "route", 0);
                           nad_set_attr(nad, 0, -1, "from", "sm", 0);
                           nad_set_attr(nad, 0, -1, "to", "jabber.axten.ro", 0);
                           
                           if((queued = pkt_new(pkt->sm, nad)) == NULL) {
                               log_debug(ZONE, "invalid queued packet, not delivering");
                               nad_free(nad);
                           } else {
                               char stamp[30];
                               nad_set_attr(queued->nad, 0, -1, "from", "sm", 0);

                               ns = nad_add_namespace(queued->nad, uri_CLIENT, NULL);
                               msg = nad_insert_elem(queued->nad, 0, ns, "message", NULL);
                               nad_set_attr(queued->nad, msg, -1, "from", from, 0);
                               nad_set_attr(queued->nad, msg, -1, "to", to, 0);
                               nad_set_attr(queued->nad, msg, -1, "type", type, 0);
                               nad_set_attr(queued->nad, msg, -1, "id", id, 0);

                               ns = nad_add_namespace(queued->nad, "urn:xmpp:chat-markers:0", NULL);
                               nad_insert_elem(queued->nad, msg, -1, "markable", NULL);

                               ns = nad_add_namespace(queued->nad, uri_URN_DELAY, NULL);
                               tmp = nad_insert_elem(queued->nad, msg, ns, "delay", NULL);
                               datetime_out(t, dt_DATETIME, stamp, 30);
                               nad_set_attr(queued->nad, tmp, -1, "stamp", stamp, 0);
                               
                               nad_insert_elem(queued->nad, msg, -1, "body", content);
                           }
                           
                           _offline_log_msg_db(sess->user->sm, id, from, to, t, content);

                           log_debug(ZONE, "delivering queued packet to %s", jid_full(sess->jid));
                           pkt_sess(queued, sess);
                       }
                    } else {
                        log_debug(ZONE, "invalid queued packet, not delivering");
                    }
                }
            } while(os_iter_next(os));

        os_free(os);

        /* drop the spool */
        storage_delete(pkt->sm->st, "queue", jid_user(sess->jid), NULL);
    }

    /* pass it so that other modules and mod_presence can get it */
    return mod_PASS;
}

static mod_ret_t _offline_pkt_user(mod_instance_t mi, user_t user, pkt_t pkt) {
    mod_offline_t offline = (mod_offline_t) mi->mod->private;
    int ns, elem, attr;
    os_t os;
    os_object_t o;
    pkt_t event = NULL;
    st_ret_t ret;
    int queuesize;

    /* if there's a resource, send it direct */
    if(*pkt->to->resource != '\0') {
        sess_t sess;

        /* find the session */
        sess = sess_match(user, pkt->to->resource);

        /* and send it straight there */
        if(sess != NULL) {
            if(pkt->type & pkt_MESSAGE || pkt->type & pkt_S10N) {
                _offline_log_pkt_db(user->sm, pkt);
            }
            pkt_sess(pkt, sess);
            return mod_HANDLED;
        }

        /* no session */
        if(pkt->type & pkt_PRESENCE) {
            pkt_free(pkt);
            return mod_HANDLED;

        } else if(pkt->type & pkt_IQ)
            return -stanza_err_SERVICE_UNAVAILABLE;
    }

    /* send messages to the top sessions */
    if(user->top != NULL && (pkt->type & pkt_MESSAGE || pkt->type & pkt_S10N)) {
        sess_t scan;

        /* loop over each session */
        for(scan = user->sessions; scan != NULL; scan = scan->next) {
            /* don't deliver to unavailable sessions */
            if(!scan->available)
                continue;

            /* skip negative priorities */
            if(scan->pri < 0)
                continue;

            /* headlines go to all, other to top priority */
            if(pkt->type != pkt_MESSAGE_HEADLINE && scan->pri < user->top->pri)
                continue;
    
            /* deliver to session */
            _offline_log_pkt_db(user->sm, pkt);

            log_debug(ZONE, "delivering message to %s", jid_full(scan->jid));
            pkt_sess(pkt_dup(pkt, jid_full(scan->jid), jid_full(pkt->from)), scan);
        }

        pkt_free(pkt);
        return mod_HANDLED;
    }

    /* if user quotas are enabled, count the number of offline messages this user has in the queue */
    if(offline->userquota > 0) {
        ret = storage_count(user->sm->st, "queue", jid_user(user->jid), NULL, &queuesize);

        log_debug(ZONE, "storage_count ret is %i queue size is %i", ret, queuesize);

        /* if the user's quota is exceeded, return an error */
        if (ret == st_SUCCESS && (pkt->type & pkt_MESSAGE) && queuesize >= offline->userquota)
           return -stanza_err_SERVICE_UNAVAILABLE;
    }

    /* save messages and s10ns for later */
    if((pkt->type & pkt_MESSAGE && !offline->dropmessages) ||
       (pkt->type & pkt_S10N && !offline->dropsubscriptions)) {
        char str_data[255], *ptr = NULL;
        time_t t = time(NULL);

        /* check type of the message and drop headlines and groupchat */
        if((((pkt->type & pkt_MESSAGE_HEADLINE) == pkt_MESSAGE_HEADLINE) && !offline->storeheadlines) ||
            (pkt->type & pkt_MESSAGE_GROUPCHAT) == pkt_MESSAGE_GROUPCHAT) {
            log_debug(ZONE, "not saving message (type 0x%X) for later", pkt->type);
            pkt_free(pkt);
            return mod_HANDLED;
        } else if((ns = nad_find_scoped_namespace(pkt->nad, "urn:xmpp:chat-markers:0", NULL)) >= 0 &&
                  (elem = nad_find_elem(pkt->nad, 1, ns, "markable", 1)) >= 0 && (
                      (elem = nad_find_elem(pkt->nad, 1, ns, "received", 1)) >= 0 ||
                      (elem = nad_find_elem(pkt->nad, 1, ns, "acknowledged", 1)) >= 0
                  )) {
            log_debug(ZONE, "not saving message (type 0x%X) for later", pkt->type);
            pkt_free(pkt);
            return mod_HANDLED;
        }

	log_debug(ZONE, "saving packet for later");
	
	if((elem = nad_find_elem(pkt->nad, 1, -1, "delay", 1)) < 0) pkt_delay(pkt, time(NULL), user->jid->domain);

        /* new object */
        if((os = os_new()) != NULL && (o = os_object_new(os)) != NULL) {
            int is_chat_content = FALSE, message_is_not_valid = FALSE;
            
            if((ns = nad_find_scoped_namespace(pkt->nad, "urn:xmpp:chat-markers:0", NULL)) >= 0 &&
               (elem = nad_find_elem(pkt->nad, 1, ns, "markable", 1)) >= 0 &&
               nad_find_elem(pkt->nad, 1, ns, "received", 1) < 0 && 
               nad_find_elem(pkt->nad, 1, ns, "acknowledged", 1) < 0) {
               
                event = pkt_create(user->sm, "message", NULL, jid_full(pkt->from), jid_full(pkt->to));
            }
            
            if((attr = nad_find_attr(pkt->nad, 1, -1, "id", NULL)) >= 0) {
                snprintf(str_data, 255, "%.*s\0", NAD_AVAL_L(pkt->nad, attr), NAD_AVAL(pkt->nad, attr));
                os_object_put(o, "id", str_data, os_type_STRING);
                if(event != NULL) nad_set_attr(event->nad, 1, -1, "id", str_data, 0);
                if(str_data[0] == '\0') message_is_not_valid = TRUE;
            }

            if((attr = nad_find_attr(pkt->nad, 1, -1, "type", NULL)) >= 0) {
                snprintf(str_data, 255, "%.*s\0", NAD_AVAL_L(pkt->nad, attr), NAD_AVAL(pkt->nad, attr));
                os_object_put(o, "type", str_data, os_type_STRING);
                if(event != NULL) nad_set_attr(event->nad, 1, -1, "type", str_data, 0);
                if(strcasecmp(str_data, "chat") == 0) is_chat_content = TRUE;
                if(str_data[0] == '\0') message_is_not_valid = TRUE;
            }
            
            if((attr = nad_find_attr(pkt->nad, 1, -1, "from", NULL)) >= 0) {
                snprintf(str_data, 255, "%.*s\0", NAD_AVAL_L(pkt->nad, attr), NAD_AVAL(pkt->nad, attr));
                if((ptr = strchr(str_data, '/')) != NULL) *ptr = '\0';
                if(str_data[0] == '\0') message_is_not_valid = TRUE;
                os_object_put(o, "from", str_data, os_type_STRING);
            }
            
            if((attr = nad_find_attr(pkt->nad, 1, -1, "to", NULL)) >= 0) {
                snprintf(str_data, 255, "%.*s\0", NAD_AVAL_L(pkt->nad, attr), NAD_AVAL(pkt->nad, attr));
                if((ptr = strchr(str_data, '/')) != NULL) *ptr = '\0';
                if(str_data[0] == '\0') message_is_not_valid = TRUE;
                os_object_put(o, "to", str_data, os_type_STRING);
            }
            
            os_object_put(o, "time", &t, os_type_INTEGER);
            
            if(is_chat_content) {
                if((elem = nad_find_elem(pkt->nad, 1, -1, "body", 1)) >= 0) {
                    char str_value[2048];
                    memset(str_value, '\0', 2048);
                    snprintf(str_value, 2048, "%.*s", NAD_CDATA_L(pkt->nad, elem), NAD_CDATA(pkt->nad, elem));
                    if(str_value[0] != '\0') {
                        os_object_put(o, "content", str_value, os_type_STRING);
                    } else {
                        message_is_not_valid = TRUE;
                    }
                } else {
                    message_is_not_valid = TRUE;
                }
            } else {
                os_object_put(o, "content", pkt->nad, os_type_NAD);
            }
            
            if(event != NULL) {
                int tmp;
                
                ns = nad_add_namespace(event->nad, "urn:xmpp:chat-markers:0", NULL);
                nad_append_elem(event->nad, ns, "markable", 2);
                tmp = nad_append_elem(event->nad, -1, "received", 2);
                nad_set_attr(event->nad, tmp, -1, "xmlns", "urn:xmpp:chat-markers:0", 0);
                nad_set_attr(event->nad, tmp, -1, "id", "", 0);
            }
            
            if(!message_is_not_valid) {
                /* store it */
                switch(storage_put(user->sm->st, "queue", jid_user(user->jid), os)) {
                    case st_FAILED:
                        if(event != NULL) pkt_free(event);
                        os_free(os);
                        return -stanza_err_INTERNAL_SERVER_ERROR;
                        
                    case st_NOTIMPL:
                        if(event != NULL) pkt_free(event);
                        os_free(os);
                        return -stanza_err_SERVICE_UNAVAILABLE;     /* xmpp-im 9.5#4 */
                        
                    default:
                        os_free(os);
                        
                        if(event != NULL) pkt_router(event);
                }
            } else {
                if(event != NULL) pkt_free(event);
                os_free(os);
            }
            
            pkt_free(pkt);
            return mod_HANDLED;
        }
    }

    return mod_PASS;
}

static void _offline_user_delete(mod_instance_t mi, jid_t jid) {
    os_t os;
    os_object_t o;
    nad_t nad;
    pkt_t queued;
    int ns, elem, attr, is_chat_content;
    char cttl[15], cstamp[18];
    time_t ttl, stamp;

    log_debug(ZONE, "deleting queue for %s", jid_user(jid));

    /* bounce the queue */
    if(storage_get(mi->mod->mm->sm->st, "queue", jid_user(jid), NULL, &os) == st_SUCCESS) {
        if(os_iter_first(os))
            do {
                char *type = NULL;
                o = os_iter_object(os);

                if(os_object_get_str(os, o, "type", &type) && type != NULL && strcasecmp(type, "chat") == 0) is_chat_content = TRUE;
                else is_chat_content = FALSE;

                if(!is_chat_content && os_object_get_nad(os, o, "content", &nad)) {
                    queued = pkt_new(mi->mod->mm->sm, nad_copy(nad));
                    if(queued == NULL) {
                        log_debug(ZONE, "invalid queued packet, not delivering");
                    } else {
                        /* check expiry as necessary */
                        if((ns = nad_find_scoped_namespace(queued->nad, uri_EXPIRE, NULL)) >= 0 &&
                           (elem = nad_find_elem(queued->nad, 1, ns, "x", 1)) >= 0 &&
                           (attr = nad_find_attr(queued->nad, elem, -1, "seconds", NULL)) >= 0) {
                            snprintf(cttl, 15, "%.*s", NAD_AVAL_L(queued->nad, attr), NAD_AVAL(queued->nad, attr));
                            ttl = atoi(cttl);

                            /* it should have a x:delay stamp, because we stamp everything we store */
                            if((ns = nad_find_scoped_namespace(queued->nad, uri_DELAY, NULL)) >= 0 &&
                               (elem = nad_find_elem(queued->nad, 1, ns, "x", 1)) >= 0 &&
                               (attr = nad_find_attr(queued->nad, elem, -1, "stamp", NULL)) >= 0) {
                                snprintf(cstamp, 18, "%.*s", NAD_AVAL_L(queued->nad, attr), NAD_AVAL(queued->nad, attr));
                                stamp = datetime_in(cstamp);

                                if(stamp + ttl <= time(NULL)) {
                                    log_debug(ZONE, "queued packet has expired, dropping");
                                    pkt_free(queued);
                                    continue;
                                }
                            }
                        }

                        log_debug(ZONE, "bouncing queued packet from %s", jid_full(queued->from));
                        pkt_router(pkt_error(queued, stanza_err_ITEM_NOT_FOUND));
                    }
                } else {
                    int ns, r, msg, tmp;
                    int t;
                    char *from = NULL, *to = NULL, *id = NULL, *content = NULL, *tm = NULL;
                    
                    if(os_object_get_str(os, o, "id", &id) && os_object_get_str(os, o, "from", &from) && os_object_get_str(os, o, "to", &to) && 
                       os_object_get_str(os, o, "content", &content) && os_object_get_int(os, o, "time", &t)) {
                       nad_t nad;
                       
                       if((nad = nad_new()) != NULL) {
                           ns = nad_add_namespace(nad, uri_COMPONENT, NULL);
                           nad_append_elem(nad, ns, "route", 0);
                           nad_set_attr(nad, 0, -1, "from", "sm", 0);
                           nad_set_attr(nad, 0, -1, "to", "jabber.axten.ro", 0);
                           
                           if((queued = pkt_new(mi->mod->mm->sm, nad)) == NULL) {
                               log_debug(ZONE, "invalid queued packet, not delivering");
                               nad_free(nad);
                           } else {
                               char stamp[30];
                               nad_set_attr(queued->nad, 0, -1, "from", "sm", 0);

                               ns = nad_add_namespace(queued->nad, uri_CLIENT, NULL);
                               msg = nad_insert_elem(queued->nad, 0, ns, "message", NULL);
                               nad_set_attr(queued->nad, msg, -1, "from", from, 0);
                               nad_set_attr(queued->nad, msg, -1, "to", to, 0);
                               nad_set_attr(queued->nad, msg, -1, "type", type, 0);
                               nad_set_attr(queued->nad, msg, -1, "id", id, 0);

                               ns = nad_add_namespace(queued->nad, "urn:xmpp:chat-markers:0", NULL);
                               nad_insert_elem(queued->nad, msg, -1, "markable", NULL);

                               ns = nad_add_namespace(queued->nad, uri_URN_DELAY, NULL);
                               tmp = nad_insert_elem(queued->nad, msg, ns, "delay", NULL);
                               datetime_out(t, dt_DATETIME, stamp, 30);
                               nad_set_attr(queued->nad, tmp, -1, "stamp", stamp, 0);
                               
                               nad_insert_elem(queued->nad, msg, -1, "body", content);
                           }
                           log_debug(ZONE, "bouncing queued packet from %s", jid_full(queued->from));
                           pkt_router(pkt_error(queued, stanza_err_ITEM_NOT_FOUND));
                       }
                    } else {
                        log_debug(ZONE, "invalid queued packet, not delivering");
                    }
                }
            } while(os_iter_next(os));

        os_free(os);
    }
    
    storage_delete(mi->sm->st, "queue", jid_user(jid), NULL);
}

static void _offline_free(module_t mod) {
    mod_offline_t offline = (mod_offline_t) mod->private;

    free(offline);
}

DLLEXPORT int module_init(mod_instance_t mi, const char *arg) {
    module_t mod = mi->mod;
    const char *configval;
    mod_offline_t offline;

    if(mod->init) return 0;

    offline = (mod_offline_t) calloc(1, sizeof(struct _mod_offline_st));

    configval = config_get_one(mod->mm->sm->config, "offline.dropmessages", 0);
    if (configval != NULL)
        offline->dropmessages = 1;

    configval = config_get_one(mod->mm->sm->config, "offline.storeheadlines", 0);
    if (configval != NULL)
        offline->storeheadlines = 1;

    configval = config_get_one(mod->mm->sm->config, "offline.dropsubscriptions", 0);
    if (configval != NULL)
        offline->dropsubscriptions = 1;

    offline->userquota = j_atoi(config_get_one(mod->mm->sm->config, "offline.userquota", 0), 0);

    mod->private = offline;

    mod->in_sess = _offline_in_sess;
    mod->pkt_user = _offline_pkt_user;
    mod->user_delete = _offline_user_delete;
    mod->free = _offline_free;

    feature_register(mod->mm->sm, "msgoffline");

    return 0;
}
