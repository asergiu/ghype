/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002 Jeremie Miller, Thomas Muldowney,
 *                    Ryan Eatmon, Robert Norris
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

#include "sm.h"

/** @file sm/mod_iq_ping.c
  * @brief xmpp ping
  * @author Tomasz Sieprawski
  * $Date: 2007/04/06 xx:xx:xx $
  * $Revision: 1.0 $
  */

static int ns_FILE_TRANSFER = 0;
#define urn_FILE_TRANSFER                   "http://jabber.org/protocol/si"

typedef struct _mod_iq_file_transfer_st {
    int userquota;
} *mod_iq_file_transfer_t;

static mod_ret_t _iq_file_transfer_pkt_user(mod_instance_t mi, user_t user, pkt_t pkt) {
    mod_iq_file_transfer_t ft = (mod_iq_file_transfer_t) mi->mod->private;
    
    if((pkt->type & pkt_IQ_SET) && pkt->ns == ns_FILE_TRANSFER) {
        int ns, elem, attr;
        os_object_t o;
        time_t t = time(NULL);
        st_ret_t ret;
        pkt_t event;
        os_t os;

        if(user->top != NULL) {
            int handled = FALSE;
            sess_t sess;
            
            for(sess = user->sessions; sess != NULL; sess = sess->next) {
                if(!sess->available) continue;
                if(sess->pri < 0) continue;
                
                fprintf(stdout, "\nSM MODULE: delivering message to %s (from %s)\n", jid_full(pkt->to), jid_full(pkt->from));

                pkt_sess(pkt_dup(pkt, jid_full(pkt->to), jid_full(pkt->from)), sess);
                handled = TRUE;
            }
            
            if(handled) {
                pkt_free(pkt);
                return mod_HANDLED;
            }
        } 
        
        if(ft->userquota > 0) {
            int queuesize;
            
            ret = storage_count(user->sm->st, "queue", jid_user(user->jid), NULL, &queuesize);
            
            fprintf(stdout, "\nSM MODULE: storage_count ret is %i queue size is %i\n", ret, queuesize);

            if (ret == st_SUCCESS && queuesize >= ft->userquota) return -stanza_err_SERVICE_UNAVAILABLE;
        }
        
        fprintf(stdout, "\nSM MODULE: saving packet for later\n");
        
        /* new object */
        os = os_new();
        o = os_object_new(os);
        os_object_put(o, "type", "nad", os_type_STRING);
        os_object_put(o, "time", &t, os_type_INTEGER);
        os_object_put(o, "content", pkt->nad, os_type_NAD);
        
        switch(storage_put(user->sm->st, "queue", jid_user(user->jid), os)) {
            case st_FAILED:
                os_free(os);
                return -stanza_err_INTERNAL_SERVER_ERROR;
            case st_NOTIMPL:
                os_free(os);
                return -stanza_err_SERVICE_UNAVAILABLE;
            default:
                os_free(os);

                if((ns = nad_find_scoped_namespace(pkt->nad, uri_EVENT, NULL)) >= 0 && (elem = nad_find_elem(pkt->nad, 1, ns, "x", 1)) >= 0 && nad_find_elem(pkt->nad, elem, ns, "offline", 1) >= 0 && nad_find_elem(pkt->nad, elem, ns, "id", 1) < 0) {
                    event = pkt_create(user->sm, "message", NULL, jid_full(pkt->from), jid_full(pkt->to));

                    attr = nad_find_attr(pkt->nad, 1, -1, "type", NULL);
                    if(attr >= 0) nad_set_attr(event->nad, 1, -1, "type", NAD_AVAL(pkt->nad, attr), NAD_AVAL_L(pkt->nad, attr));

                    ns = nad_add_namespace(event->nad, uri_EVENT, NULL);
                    nad_append_elem(event->nad, ns, "x", 2);
                    nad_append_elem(event->nad, ns, "offline", 3);

                    nad_append_elem(event->nad, ns, "id", 3);
                    attr = nad_find_attr(pkt->nad, 1, -1, "id", NULL);
                    if(attr >= 0) nad_append_cdata(event->nad, NAD_AVAL(pkt->nad, attr), NAD_AVAL_L(pkt->nad, attr), 4);

                    pkt_router(event);
                }

                pkt_free(pkt);
                return mod_HANDLED;
        }
    }
    
    return mod_PASS;
}

static void _iq_file_transfer_free(module_t mod) {
    mod_iq_file_transfer_t ft = (mod_iq_file_transfer_t) mod->private;
    free(ft);

    sm_unregister_ns(mod->mm->sm, urn_FILE_TRANSFER);
    feature_unregister(mod->mm->sm, urn_FILE_TRANSFER);
}

DLLEXPORT int module_init(mod_instance_t mi, const char *arg) {
    mod_iq_file_transfer_t ft;
    module_t mod = mi->mod;

    if(mod->init) return 0;
    ft = (mod_iq_file_transfer_t) calloc(1, sizeof(struct _mod_iq_file_transfer_st));

    ft->userquota = j_atoi(config_get_one(mod->mm->sm->config, "offline.userquota", 0), 0);

    mod->private = ft;
    
    mod->pkt_user = _iq_file_transfer_pkt_user;
    mod->free = _iq_file_transfer_free;

    ns_FILE_TRANSFER = sm_register_ns(mod->mm->sm, urn_FILE_TRANSFER);
    feature_register(mod->mm->sm, urn_FILE_TRANSFER);

    return 0;
}
