/*
 * jabberd - Jabber Open Source Server
 * Copyright (c) 2002 Jeremie Miller, Thomas Muldowney,
 *                    Ryan Eatmon, Robert Norris
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA02111-1307USA
 */

#include "sm.h"
#include "plugins/plugins.h"

/** @file sm/mod_ghype.c
  * @brief xmpp ghype
  * $Revision: 1.0 $
  */

static int ns_GHYPE = 0;

static mod_ret_t _ghype_reply(mod_ghype_t g, const char* where, pkt_t pkt) {
    mod_ret_t result = mod_PASS;
    int ns, elem;
    
    ns = nad_find_scoped_namespace(pkt->nad, uri_GHYPE, NULL);
    if(ghype_protocol(pkt->nad, g, 1, where, ns, (pkt->from == NULL ? NULL : jid_full(pkt->from)), (pkt->to == NULL ? NULL : jid_full(pkt->to)))) {
        result = mod_HANDLED;
    } else {
        nad_set_error(pkt->nad, 1, ghype_err_FEATURE_NOT_IMPLEMENTED);
    }
    
    return result;
}

static mod_ret_t _ghype_in_sess(mod_instance_t mi, sess_t sess, pkt_t pkt) {
    mod_ghype_t g = (mod_ghype_t) mi->mod->private;
    mod_ret_t result = mod_PASS;
    if((pkt->type & pkt_GHYPE)) {

//            fprintf(stdout, "SESS PCK- from to %s/%s to %s\n", jid_full(pkt->from), pkt->from->resource, jid_full(pkt->to));

        if(pkt->from == NULL || jid_compare_user(pkt->from, sess->jid) != 0) {
            if(pkt->from != NULL) jid_free(pkt->from);
            pkt->from = jid_dup(sess->jid);
            nad_set_attr(pkt->nad, 1, -1, "from", jid_full(pkt->from), 0);
            fprintf(stdout, "CHANGING from to %s\n", jid_full(pkt->from));
        }

        if(pkt->to == NULL) {
            if((result = _ghype_reply(g, "in_sess", pkt)) == mod_HANDLED) pkt_sess(pkt, sess);
        } else {
//            pkt_router(pkt);
//            result = mod_HANDLED;
        }
    }
    
    return result;
}

mod_ret_t _ghype_in_router(mod_instance_t mi, pkt_t pkt) {
    mod_ghype_t g = (mod_ghype_t) mi->mod->private;
    mod_ret_t result = mod_PASS;
    if((pkt->type & pkt_GHYPE) && pkt->to == NULL) {
        if((result = _ghype_reply(g, "in_router", pkt)) == mod_HANDLED) pkt_router(pkt_tofrom(pkt));
    }
    
    return result;
}

static mod_ret_t _ghype_pkt_user(mod_instance_t mi, user_t user, pkt_t pkt) {
    mod_ghype_t g = (mod_ghype_t) mi->mod->private;
    mod_ret_t result = mod_PASS;
    if((pkt->type & pkt_GHYPE)) {
        if(pkt->to != NULL && *pkt->to->resource != '\0') {
            sess_t sess;
            if((sess = sess_match(user, pkt->to->resource)) != NULL) {
                pkt_sess(pkt, sess);
                result = mod_HANDLED;
            }
        } else if(user->top != NULL) {
            int handled = FALSE;
            sess_t sess;
            
            for(sess = user->sessions; sess != NULL; sess = sess->next) {
                if(!sess->available) continue;
                if(sess->pri < 0) continue;
                
                fprintf(stdout, "\nSM MODULE: delivering message to %s (from %s)\n", jid_full(pkt->to), jid_full(pkt->from));
                
                pkt_sess(pkt_dup(pkt, jid_full(pkt->to), jid_full(pkt->from)), sess);
                handled = TRUE;
            }
            
            if(handled) {
                pkt_free(pkt);
                return mod_HANDLED;
            }
        }
    }
    
    return result;
}
    
static mod_ret_t _ghype_pkt_sm(mod_instance_t mi, pkt_t pkt) {
    mod_ghype_t g = (mod_ghype_t) mi->mod->private;
    mod_ret_t result = mod_PASS;
    if((pkt->type & pkt_GHYPE) && pkt->to == NULL) {
        if((result = _ghype_reply(g, "pkt_sm", pkt)) == mod_HANDLED) pkt_router(pkt_tofrom(pkt));
    }
    
    return result;
}

static void _ghype_free(module_t mod) {
    mod_ghype_t g = (mod_ghype_t) mod->private;
    if(g->hostname != NULL) free(g->hostname);
    if(g->port != NULL) free(g->port);
    if(g->dbname != NULL) free(g->dbname);
    if(g->user != NULL) free(g->user);
    if(g->pass != NULL) free(g->pass);
    free(g);
    
    sm_unregister_ns(mod->mm->sm, uri_GHYPE);
    feature_unregister(mod->mm->sm, uri_GHYPE);
}

DLLEXPORT int module_init(mod_instance_t mi, const char *arg) {
    module_t mod = mi->mod;
    mod_ghype_t g;
    char *cfg = NULL;

    if(mod->init) return 0;
    g = (mod_ghype_t) calloc(1, sizeof(struct _mod_ghype_st));
    memset(g, '\0', sizeof(struct _mod_ghype_st));
    if((cfg = config_get_one(mod->mm->sm->config, "storage.mysql.host", 0)) != NULL) g->hostname = strdup(cfg);
    if((cfg = config_get_one(mod->mm->sm->config, "storage.mysql.port", 0)) != NULL) g->port = strdup(cfg);
    if((cfg = config_get_one(mod->mm->sm->config, "storage.mysql.dbname", 0)) != NULL) g->dbname = strdup(cfg);
    if((cfg = config_get_one(mod->mm->sm->config, "storage.mysql.user", 0)) != NULL) g->user = strdup(cfg);
    if((cfg = config_get_one(mod->mm->sm->config, "storage.mysql.pass", 0)) != NULL) g->pass = strdup(cfg);
    mod->private = g;

    mod->in_sess = _ghype_in_sess;

    mod->in_router = _ghype_in_router;
    mod->pkt_user = _ghype_pkt_user;
    mod->pkt_sm = _ghype_pkt_sm;
    mod->free = _ghype_free;

    ns_GHYPE = sm_register_ns(mod->mm->sm, uri_GHYPE);
    feature_register(mod->mm->sm, uri_GHYPE);

    return 0;
}
