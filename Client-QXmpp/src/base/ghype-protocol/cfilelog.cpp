#include <QDomElement>
#include <QTextStream>
#include <QXmlStreamWriter>

#include "cfilelog.h"
#include "QXmppUtils.h"

#include <qdebug.h>

class CFileLogPrivate : public QSharedData
{
public:
    QDateTime stamp;
    QString uid;
    QString filename;
};

CFileLog::CFileLog(const QString &from, const QString &to): QXmppStanza(from, to)
  , d(new CFileLogPrivate)
{

}

CFileLog::CFileLog(const CFileLog &other): QXmppStanza(other)
  , d(other.d)
{

}

CFileLog::~CFileLog()
{

}

void CFileLog::setStamp(const QDateTime &stamp)
{
    d->stamp = stamp;
}

QDateTime CFileLog::stamp() const
{
    return d->stamp;
}

void CFileLog::setUid(const QString &uid)
{
    d->uid = uid;
}

QString CFileLog::uid() const
{
    return d->uid;
}

void CFileLog::setFilename(const QString &filename)
{
    d->filename = filename;
}

QString CFileLog::filename() const
{
    return d->filename;
}


CFileLog& CFileLog::operator=(const CFileLog &other)
{
    QXmppStanza::operator=(other);
    d = other.d;
    return *this;
}

void CFileLog::parse(const QDomElement &element)
{
    setFrom(element.attribute("from"));
    setTo(element.attribute("to"));
    setStamp(QXmppUtils::datetimeFromString(element.attribute("data")));
    setUid(element.attribute("uid"));
    setFilename(element.text());
}

void CFileLog::toXml(QXmlStreamWriter *xmlWriter) const
{
    xmlWriter->writeStartElement("file");
    helperToXmlAddAttribute(xmlWriter, "from", from());
    helperToXmlAddAttribute(xmlWriter, "to", to());
    helperToXmlAddAttribute(xmlWriter, "date", d->stamp.toString("yyyy-MM-dd hh:mm:ss"));
    helperToXmlAddAttribute(xmlWriter, "uid", d->uid);

    xmlWriter->writeCharacters(d->filename);

    xmlWriter->writeEndElement();
}
