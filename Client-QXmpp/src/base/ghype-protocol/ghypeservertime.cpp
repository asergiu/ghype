#include "ghypeservertime.h"
#include "QXmppUtils.h"

#include <QDomElement>
#include <QXmlStreamWriter>

GHypeServerTime::GHypeServerTime()
{
    generateAndSetNextId();
}

GHypeServerTime::~GHypeServerTime()
{

}

void GHypeServerTime::parse(const QDomElement &element){
    QXmppStanza::parse(element);
    QDomElement elem = element.firstChildElement("dates");
    elem = elem.firstChildElement("date");
    const QString str = elem.attribute("value");
    m_serverUTC = QXmppUtils::datetimeFromString(str);
}

void GHypeServerTime::toXml(QXmlStreamWriter *xmlWriter) const
{
    xmlWriter->writeStartElement("ghype");
    helperToXmlAddAttribute(xmlWriter,"xmlns","jabber:client");
    helperToXmlAddAttribute(xmlWriter,"id",id());
    helperToXmlAddAttribute(xmlWriter,"from",from());
    helperToXmlAddAttribute(xmlWriter,"type","get");
    helperToXmlAddAttribute(xmlWriter,"obfuscate","no");

    xmlWriter->writeStartElement("query");
    helperToXmlAddAttribute(xmlWriter,"xmlns","http://jabber.org/protocol/ghype");

    xmlWriter->writeStartElement("item");
    helperToXmlAddAttribute(xmlWriter,"ask","utc");

    xmlWriter->writeEndElement();
    xmlWriter->writeEndElement();
    xmlWriter->writeEndElement();
}

QDateTime GHypeServerTime::getServerTime() const{
    return m_serverUTC;
}
