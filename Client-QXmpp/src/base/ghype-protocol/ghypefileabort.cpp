#include "ghypefileabort.h"

#include <QDomNodeList>
#include <QXmppUtils.h>

GHypeFileAbort::GHypeFileAbort(const QString &from, const QString &to, const QString &uid, const QString &filename) :
    QXmppStanza(from, to),
    d(new GHypeFileAbortPrivate)
{
    d->uid = uid;
    d->filename = filename;
    generateAndSetNextId();
}

GHypeFileAbort::GHypeFileAbort(const GHypeFileAbort &other) :
    QXmppStanza(other),
    d(other.d)
{
}

GHypeFileAbort &GHypeFileAbort::operator=(const GHypeFileAbort &other)
{
    QXmppStanza::operator =(other);
    d = other.d;
    return *this;
}

QString GHypeFileAbort::uid() const
{
    return d->uid;
}

QString GHypeFileAbort::filename() const
{
    return d->filename;
}

void GHypeFileAbort::parse(const QDomElement &element)
{
    QXmppStanza::parse(element);

    QDomNodeList elemList = element.elementsByTagName("file-transfer-abort");
    int size = elemList.size();

    if (size) {
        QDomElement el = element.elementsByTagName("file-transfer-abort").at(0).toElement();
        d->uid = el.attribute("uid");
        d->filename = el.text();
    }
}

void GHypeFileAbort::toXml(QXmlStreamWriter *xmlWriter) const
{
    xmlWriter->writeStartElement("ghype"); // start ghype
    helperToXmlAddAttribute(xmlWriter, "xmlns", "jabber:client");
    helperToXmlAddAttribute(xmlWriter, "id", id());
    helperToXmlAddAttribute(xmlWriter, "from", from());
    helperToXmlAddAttribute(xmlWriter, "to", to());
    helperToXmlAddAttribute(xmlWriter, "type", "set");
    helperToXmlAddAttribute(xmlWriter, "obfuscate", "no");

    xmlWriter->writeStartElement("file-transfer-abort"); // file-transfer-abort
    helperToXmlAddAttribute(xmlWriter, "xmlns", "http://jabber.org/protocol/ghype");
    helperToXmlAddAttribute(xmlWriter, "uid", d->uid);
    xmlWriter->writeCharacters(d->filename);
    xmlWriter->writeEndElement(); // end file-transfer-abort

    xmlWriter->writeEndElement(); // end ghype
}

