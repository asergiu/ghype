#include "ghypefileack.h"

#include <QXmppUtils.h>
#include <QSharedData>
#include <QDebug>


GHypeFileAck::GHypeFileAck(const QString &from, const QString &to, const QString &uid, const QString &filename):
    QXmppStanza(from, to),
    d(new GHypeFileAckPrivate)
{
    d->m_uid = uid;
    d->m_filename = filename;
    generateAndSetNextId();
}

GHypeFileAck::GHypeFileAck(const GHypeFileAck &other)
    :QXmppStanza(other),
      d(other.d){

}

GHypeFileAck& GHypeFileAck::operator =(const GHypeFileAck &other)
{
    QXmppStanza::operator =(other);
    d = other.d;
    return *this;
}

QString GHypeFileAck::uid() const
{
    return d->m_uid;
}

QString GHypeFileAck::filename() const
{
    return d->m_filename;
}

void GHypeFileAck::parse(const QDomElement &element)
{
    QXmppStanza::parse(element);

    int errCode = error().code();
    if (errCode) {
        qDebug() << Q_FUNC_INFO << "Received file-transfer-confirm with error: " << errCode;
        return;
    }

    QDomNodeList elemList = element.elementsByTagName("file-transfer-confirm");
    int size = elemList.size();

    if(size != 0){
        QDomElement el = element.elementsByTagName("file-transfer-confirm").at(0).toElement();
        d->m_uid = el.attribute("uid");
        d->m_filename = el.text();
    }
}

void GHypeFileAck::toXml(QXmlStreamWriter *writer) const
{

    writer->writeStartElement("ghype"); // start ghype
    helperToXmlAddAttribute(writer, "xmlns", "jabber:client");
    helperToXmlAddAttribute(writer, "id", id());
    helperToXmlAddAttribute(writer, "from", from());
    helperToXmlAddAttribute(writer, "to", to());
    helperToXmlAddAttribute(writer, "type", "set");
    helperToXmlAddAttribute(writer, "obfuscate", "no");

    writer->writeStartElement("file-transfer-confirm"); // file-transfer-confirm
    helperToXmlAddAttribute(writer, "xmlns", "http://jabber.org/protocol/ghype");
    helperToXmlAddAttribute(writer, "uid", d->m_uid);
    writer->writeCharacters(d->m_filename);
    writer->writeEndElement(); // end file-transfer-confirm

    writer->writeEndElement(); // end ghype
}
