#ifndef GHYPEMESSAGELOGS
#define GHYPEMESSAGELOGS

#include <QDateTime>
#include <QStringList>

#include "QXmppStanza.h"

class GHypeLogsPrivate;
class QXmppMessage;

class QXMPP_EXPORT GHypeMessageLogs : public QXmppStanza
{
public:
    GHypeMessageLogs(const QString& from = QString(), const QDateTime& dateFrom = QDateTime(), const QDateTime& dateTo = QDateTime(), const QString& withBareJid = QString());
    GHypeMessageLogs(QString from, int chunkStart, int chunkNo, QString withBareJid);
    GHypeMessageLogs(const GHypeMessageLogs &other);

    GHypeMessageLogs& operator=(const GHypeMessageLogs &other);
    ~GHypeMessageLogs();

    QList<QXmppStanza*> getMessages() const;
    QString getWithBareJid() const;
    void parse(const QDomElement &element);
    void toXml(QXmlStreamWriter *xmlWriter) const;

private:
    QSharedDataPointer<GHypeLogsPrivate> d;
};

#endif // GHYPEMESSAGELOGS

