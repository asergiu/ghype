#include "ghypejidvalidation.h"

#include <QDomElement>
#include <QDebug>
#include <QXmlStreamWriter>
#include "QXmppUtils.h"
#include "QXmppMessage.h"

class GHypeJidValidationPrivate : public QSharedData
{
public:
    QString m_userToValidate;
    bool m_isValid;
};

GHypeJidValidation::GHypeJidValidation(const QString& from, const QString& userToValidate)
    :QXmppStanza(from)
    , d(new GHypeJidValidationPrivate)
{
    d->m_userToValidate = userToValidate;
    d->m_isValid = false;
    generateAndSetNextId();
}

GHypeJidValidation& GHypeJidValidation::operator=(const GHypeJidValidation &other)
{
    QXmppStanza::operator=(other);
    d = other.d;
    return *this;
}

GHypeJidValidation::~GHypeJidValidation()
{

}

QPair<QString, bool> GHypeJidValidation::getValidationData() const
{
    return QPair<QString, bool>(d->m_userToValidate, d->m_isValid);
}

void GHypeJidValidation::toXml(QXmlStreamWriter *xmlWriter) const
{
    xmlWriter->writeStartElement("ghype");
    helperToXmlAddAttribute(xmlWriter, "xmlns", "jabber:client");
    helperToXmlAddAttribute(xmlWriter, "id", id());
    helperToXmlAddAttribute(xmlWriter, "from", from());
    helperToXmlAddAttribute(xmlWriter, "type", "get");
    helperToXmlAddAttribute(xmlWriter, "obfuscate", "no");

    xmlWriter->writeStartElement("query");
    helperToXmlAddAttribute(xmlWriter,"xmlns", "http://jabber.org/protocol/ghype");

    xmlWriter->writeStartElement("item");

    helperToXmlAddAttribute(xmlWriter,"ask", "validation");

    helperToXmlAddAttribute(xmlWriter,"user", d->m_userToValidate);

    xmlWriter->writeEndElement();
    xmlWriter->writeEndDocument();
    xmlWriter->writeEndElement();
}

void GHypeJidValidation::parse(const QDomElement &element)
{
    QXmppStanza::parse(element);
    //  response format is:
    //    <ghype obfuscate='no' type='result' from='kzoltan@jabber.axten.ro' id='72eqwdgq37'>
    //          <validation user='cineva@jabber.axten.ro>false</validation>
    //    </ghype>
    QDomNodeList elemList = element.elementsByTagName("validation");
    if(elemList.size() > 0){
        QDomElement element = elemList.at(0).toElement();
        d->m_userToValidate = element.attribute("user");
        d->m_isValid = element.text().compare("true", Qt::CaseInsensitive) == 0;
        qDebug()<<"user "<<d->m_userToValidate<< " is valid ? "<<d->m_isValid;
    }
    else{
        qDebug()<<"Invalid server response to user validation";
    }

}
