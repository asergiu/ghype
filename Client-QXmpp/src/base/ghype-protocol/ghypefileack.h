#ifndef GHYPEFILEACK_H
#define GHYPEFILEACK_H

#include <QXmppStanza.h>
#include <QDomElement>
#include <QSharedDataPointer>

class GHypeFileAckPrivate: public QSharedData{

public:
    QString m_uid;
    QString m_filename;
};
class QXMPP_EXPORT GHypeFileAck : public QXmppStanza
{
public:
    GHypeFileAck(const QString &from = QString(), const QString &to = QString(), const QString &uid = QString(), const QString &filename = QString());
    GHypeFileAck(const GHypeFileAck &other);
    GHypeFileAck &operator =(const GHypeFileAck &other);

    QString uid() const;
    QString filename() const;

    void parse(const QDomElement &element);
    void toXml(QXmlStreamWriter *writer) const;

private:
    QSharedDataPointer<GHypeFileAckPrivate> d;
};

#endif // GHYPEFILEACK_H
