#ifndef CFILELOG
#define CFILELOG

#include <QDateTime>
#include "QXmppStanza.h"

class CFileLogPrivate;

class QXMPP_EXPORT CFileLog : public QXmppStanza{

public:
    CFileLog(const QString& from = QString(), const QString& to = QString());
    CFileLog(const CFileLog &other);
    ~CFileLog();

    CFileLog& operator=(const CFileLog &other);

    void setStamp(const QDateTime &stamp);
    QDateTime stamp() const;

    void setUid(const QString &uid);
    QString uid() const;

    void setFilename(const QString &filename);
    QString filename() const;

    void parse(const QDomElement &element);
    void toXml(QXmlStreamWriter *xmlWriter) const;

private:
    QSharedDataPointer<CFileLogPrivate> d;
};

#endif // CFILELOG

