#ifndef GHYPEFILEABORT_H
#define GHYPEFILEABORT_H

#include "QXmppStanza.h"

class GHypeFileAbortPrivate : public QSharedData {
public:
    QString uid;
    QString filename;
};

class QXMPP_EXPORT GHypeFileAbort : public QXmppStanza
{
public:
    GHypeFileAbort(const QString &from = QString(), const QString &to = QString(), const QString &uid = QString(), const QString &filename = QString());
    GHypeFileAbort(const GHypeFileAbort &other);
    GHypeFileAbort &operator=(const GHypeFileAbort &other);

    QString uid() const;
    QString filename() const;
    void parse(const QDomElement &element);
    void toXml(QXmlStreamWriter *xmlWriter) const;

private:
    QSharedDataPointer<GHypeFileAbortPrivate> d;
};

#endif // GHYPEFILEABORT_H
