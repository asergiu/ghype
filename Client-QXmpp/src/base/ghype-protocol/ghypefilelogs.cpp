#include "ghypefilelogs.h"

#include <QDomElement>
#include <QXmlStreamWriter>
#include "QXmppUtils.h"
#include "QXmppMessage.h"

class GHypeFileLogsPrivate : public QSharedData
{

public:
    QDateTime m_dateFrom;
    QDateTime m_dateTo;
    QString m_withBareJid;
    QList<QXmppMessage> m_messages;
    QList<GHypeFileTransferItem*> m_fileTransfers;
    QString m_requestType;
    QString m_direction;
};

GHypeFileLogs::GHypeFileLogs(const QString& from, const QDateTime& dateFrom, const QDateTime& dateTo, const QString &reqType, const QString& withBareJid)
    :QXmppStanza(from)
    , d(new GHypeFileLogsPrivate)
{
    d->m_dateFrom = dateFrom;
    d->m_dateTo = dateTo;
    d->m_withBareJid = withBareJid;
    d->m_requestType = reqType;
    generateAndSetNextId();
}

GHypeFileLogs::GHypeFileLogs(const GHypeFileLogs &other)
    :QXmppStanza(other)
    , d(other.d)
{

}

GHypeFileLogs& GHypeFileLogs::operator=(const GHypeFileLogs &other)
{
    QXmppStanza::operator=(other);
    d = other.d;
    return *this;
}

GHypeFileLogs::~GHypeFileLogs()
{}

QList<GHypeFileTransferItem *> GHypeFileLogs::fileTransfers() const
{
    return d->m_fileTransfers;
}

QString GHypeFileLogs::getWithBareJid() const
{
    return d->m_withBareJid;
}

void GHypeFileLogs::parse(const QDomElement &element){
    QXmppStanza::parse(element);

    parseFileTransferLog(element);
}

void GHypeFileLogs::toXml(QXmlStreamWriter *xmlWriter) const
{
    fileTransferToXml(xmlWriter);
}

void GHypeFileLogs::parseFileTransferLog(const QDomElement &element)
{
    int size = element.elementsByTagName("file").size();

    for(int i = 0; i < size; i++){
        QDomElement el = element.elementsByTagName("file").at(i).toElement();
        QString from = el.attribute("from");
        QString to = el.attribute("to");
        QString date = el.attribute("data");


        QDateTime stamp;
        stamp = QDateTime::fromString(date, "yyyy-MM-dd HH:mm:ss");

        QString filename = el.text();
        QString sid = el.attribute("uid");
        QString description = el.attribute("desc");

        GHypeFileTransferItem* transferItem = new GHypeFileTransferItem();
        transferItem->setFileName(filename);
        transferItem->setUid(sid);
        transferItem->setFrom(from);
        transferItem->setTo(to);
        transferItem->setDate(stamp);
        transferItem->setDescription(description);


        d->m_fileTransfers.append(transferItem);
    }
}


void GHypeFileLogs::fileTransferToXml(QXmlStreamWriter *xmlWriter) const
{
    xmlWriter->writeStartElement("ghype");
    helperToXmlAddAttribute(xmlWriter, "xmlns", "jabber:client");
    helperToXmlAddAttribute(xmlWriter, "id", id());
    helperToXmlAddAttribute(xmlWriter, "from", from());
    helperToXmlAddAttribute(xmlWriter, "type", "get");
    helperToXmlAddAttribute(xmlWriter, "obfuscate", "no");

    xmlWriter->writeStartElement("query");
    helperToXmlAddAttribute(xmlWriter,"xmlns", "http://jabber.org/protocol/ghype");

    xmlWriter->writeStartElement("item");

    helperToXmlAddAttribute(xmlWriter,"ask", "files");

    helperToXmlAddAttribute(xmlWriter,"from", d->m_dateFrom.toString("yyyy-MM-dd HH:mm:ss"));
    helperToXmlAddAttribute(xmlWriter,"until", d->m_dateTo.toString("yyyy-MM-dd HH:mm:ss"));

    if(!d->m_withBareJid.isEmpty())
        helperToXmlAddAttribute(xmlWriter,"option", d->m_withBareJid);

    if(!d->m_requestType.isEmpty())
        helperToXmlAddAttribute(xmlWriter, "type", d->m_requestType);

    xmlWriter->writeEndElement();
    xmlWriter->writeEndDocument();
    xmlWriter->writeEndElement();
}

QString GHypeFileTransferItem::from() const
{
    return m_from;
}

void GHypeFileTransferItem::setFrom(const QString &from)
{
    m_from = from;
}
QString GHypeFileTransferItem::to() const
{
    return m_to;
}

void GHypeFileTransferItem::setTo(const QString &to)
{
    m_to = to;
}
QString GHypeFileTransferItem::fileName() const
{
    return m_fileName;
}

void GHypeFileTransferItem::setFileName(const QString &fileName)
{
    m_fileName = fileName;
}
QString GHypeFileTransferItem::uid() const
{
    return m_uid;
}

void GHypeFileTransferItem::setUid(const QString &uid)
{
    m_uid = uid;
}
QString GHypeFileTransferItem::description() const
{
    return m_description;
}

void GHypeFileTransferItem::setDescription(const QString &description)
{
    m_description = description;
}

QDateTime GHypeFileTransferItem::date() const
{
    return m_date;
}

void GHypeFileTransferItem::setDate(QDateTime date)
{
    m_date = date;
}








