#include "ghypemessagelogs.h"

#include <QDomElement>
#include <QXmlStreamWriter>
#include "QXmppUtils.h"
#include "QXmppMessage.h"
#include "cfilelog.h"
#include "QXmppTransferManager.h"

#include <qdebug.h>

class GHypeLogsPrivate : public QSharedData
{
public:
    QDateTime m_dateFrom;
    QDateTime m_dateTo;
    bool m_retrieveChunk;// if false => retrieve messages by date, else by blocks of messages
    int m_chunkStart;
    int m_chunkNo;
    QString m_withBareJid;
    QList<QXmppStanza*> m_messages;
};

GHypeMessageLogs::GHypeMessageLogs(const QString& from, const QDateTime& dateFrom, const QDateTime& dateTo, const QString& withBareJid)
    :QXmppStanza(from)
    , d(new GHypeLogsPrivate)
{
    d->m_dateFrom = dateFrom;
    d->m_dateTo = dateTo;
    d->m_retrieveChunk = false;
    d->m_withBareJid = withBareJid;
    generateAndSetNextId();
}

GHypeMessageLogs::GHypeMessageLogs(QString from, int chunkStart, int chunkNo, QString withBareJid)
    :QXmppStanza(from)
    , d(new GHypeLogsPrivate)
{
    d->m_chunkStart = chunkStart;
    d->m_chunkNo = chunkNo;
    d->m_retrieveChunk = true;
    d->m_withBareJid = withBareJid;
    generateAndSetNextId();
}

GHypeMessageLogs::GHypeMessageLogs(const GHypeMessageLogs &other)
    :QXmppStanza(other)
    , d(other.d)
{

}

GHypeMessageLogs& GHypeMessageLogs::operator=(const GHypeMessageLogs &other)
{
    QXmppStanza::operator=(other);
    d = other.d;
    return *this;
}

GHypeMessageLogs::~GHypeMessageLogs()
{
    for(int i=0;i<d->m_messages.size();i++)
    {
        delete d->m_messages.at(i);
    }

    d->m_messages.clear();
}

QList<QXmppStanza*> GHypeMessageLogs::getMessages() const
{
    return d->m_messages;
}

QString GHypeMessageLogs::getWithBareJid() const
{
    return d->m_withBareJid;
}

void GHypeMessageLogs::parse(const QDomElement &element)
{
    QXmppStanza::parse(element);

    QDomNodeList elemList = element.elementsByTagName("messages").at(0).childNodes();

    int size = elemList.size();
    for(int i=0;i<size;i++)
    {
        QString tagName = elemList.at(i).nodeName();
        if(tagName == "message")
        {
            QXmppMessage *message = new QXmppMessage();
            message->parse(elemList.at(i).toElement());

            d->m_messages.append(message);
        }
        else if(tagName == "file")
        {
            CFileLog *file = new CFileLog();
            file->parse(elemList.at(i).toElement());

            d->m_messages.append(file);
        }
    }
}

void GHypeMessageLogs::toXml(QXmlStreamWriter *xmlWriter) const
{
    xmlWriter->writeStartElement("ghype");
    helperToXmlAddAttribute(xmlWriter, "xmlns", "jabber:client");
    helperToXmlAddAttribute(xmlWriter, "id", id());
    helperToXmlAddAttribute(xmlWriter, "from", from());
    helperToXmlAddAttribute(xmlWriter, "type", "get");
    helperToXmlAddAttribute(xmlWriter, "obfuscate", "no");

    xmlWriter->writeStartElement("query");
    helperToXmlAddAttribute(xmlWriter,"xmlns", "http://jabber.org/protocol/ghype");

    xmlWriter->writeStartElement("item");

    helperToXmlAddAttribute(xmlWriter,"ask", "messages");

    if(d->m_retrieveChunk){
        helperToXmlAddAttribute(xmlWriter,"start", QString::number(d->m_chunkStart));
        helperToXmlAddAttribute(xmlWriter,"chunk", QString::number(d->m_chunkNo));
    }
    else{
        helperToXmlAddAttribute(xmlWriter,"from", d->m_dateFrom.toString("yyyy-MM-dd hh:mm:ss"));
        helperToXmlAddAttribute(xmlWriter,"until", d->m_dateTo.toString("yyyy-MM-dd hh:mm:ss"));
    }

    helperToXmlAddAttribute(xmlWriter,"with", d->m_withBareJid);

    xmlWriter->writeEndElement();
    xmlWriter->writeEndDocument();
    xmlWriter->writeEndElement();

}
