#include "ghypefilerequest.h"

#include <QDomElement>
#include <QXmlStreamWriter>
#include "QXmppUtils.h"

class GHypeFileRequestPrivate : public QSharedData
{
public:
    QString uid;
};

GHypeFileRequest::GHypeFileRequest(const QString& uid, const QString& from)
    :QXmppStanza(from)
    , d(new GHypeFileRequestPrivate)
{

    d->uid = uid;
    generateAndSetNextId();
}

GHypeFileRequest::GHypeFileRequest(const GHypeFileRequest &other)
    :QXmppStanza(other)
    , d(other.d)
{
}

GHypeFileRequest& GHypeFileRequest::operator=(const GHypeFileRequest &other)
{
    QXmppStanza::operator=(other);
    d = other.d;
    return *this;
}

GHypeFileRequest::~GHypeFileRequest()
{
}

void GHypeFileRequest::parse(const QDomElement &element)
{
    //request will be parsed
}

void GHypeFileRequest::toXml(QXmlStreamWriter *xmlWriter) const
{
    xmlWriter->writeStartElement("ghype");
    helperToXmlAddAttribute(xmlWriter, "xmlns", "jabber:client");
    helperToXmlAddAttribute(xmlWriter, "id", id());
    helperToXmlAddAttribute(xmlWriter, "from", from());
    helperToXmlAddAttribute(xmlWriter, "type", "get");
    helperToXmlAddAttribute(xmlWriter, "obfuscate", "no");

    xmlWriter->writeStartElement("file-transfer");
    helperToXmlAddAttribute(xmlWriter,"xmlns", "http://jabber.org/protocol/ghype");

    xmlWriter->writeCharacters(d->uid);

    xmlWriter->writeEndElement();
    xmlWriter->writeEndElement();
}

