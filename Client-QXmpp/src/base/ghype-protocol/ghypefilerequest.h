#ifndef GHYPEFILEREQUEST
#define GHYPEFILEREQUEST

#include "QXmppStanza.h"

class GHypeFileRequestPrivate;

class QXMPP_EXPORT GHypeFileRequest : public QXmppStanza{

public:
    GHypeFileRequest(const QString &uid = "", const QString& from = QString());
    GHypeFileRequest(const GHypeFileRequest &other);
    ~GHypeFileRequest();

    GHypeFileRequest& operator=(const GHypeFileRequest &other);

    void parse(const QDomElement &element);
    void toXml(QXmlStreamWriter *xmlWriter) const;

private:
    QSharedDataPointer<GHypeFileRequestPrivate> d;
};

#endif // GHYPEFILEREQUEST

