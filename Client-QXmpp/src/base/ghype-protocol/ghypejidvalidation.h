#ifndef GHYPEJIDVALIDATION
#define GHYPEJIDVALIDATION

#include "QXmppStanza.h"
#include <QPair>
class GHypeJidValidationPrivate;
class QXmppMessage;

class QXMPP_EXPORT GHypeJidValidation : public QXmppStanza
{
public:
    GHypeJidValidation(const QString& from = QString(), const QString& userToValidate = QString());
    GHypeJidValidation(const GHypeJidValidation &other);

    GHypeJidValidation& operator=(const GHypeJidValidation &other);
    ~GHypeJidValidation();

    QPair<QString, bool> getValidationData() const;
    void parse(const QDomElement &element);
    void toXml(QXmlStreamWriter *xmlWriter) const;

private:
    QSharedDataPointer<GHypeJidValidationPrivate> d;
};

#endif // GHYPEJIDVALIDATION

