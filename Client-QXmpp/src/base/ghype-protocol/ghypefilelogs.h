#ifndef GHYPEFILELOGS
#define GHYPEFILELOGS

#include <QDateTime>
#include <QStringList>

#include "QXmppStanza.h"

class GHypeFileLogsPrivate;
class QXmppMessage;

class QXMPP_EXPORT GHypeFileTransferItem : public QObject{

    Q_OBJECT
    Q_PROPERTY(QString from READ from WRITE setFrom)
    Q_PROPERTY(QString to READ to WRITE setTo)
    Q_PROPERTY(QString uid READ uid WRITE setUid)
    Q_PROPERTY(QString description READ description WRITE setDescription)
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName)
    Q_PROPERTY(QDateTime date READ date WRITE setDate)
public:

    QString from() const;
    void setFrom(const QString &from);

    QString to() const;
    void setTo(const QString &to);

    QString fileName() const;
    void setFileName(const QString &fileName);

    QString uid() const;
    void setUid(const QString &uid);

    QString description() const;
    void setDescription(const QString &description);

    QDateTime date() const;
    void setDate(QDateTime date);


private:
    QString m_from;
    QString m_to;
    QString m_fileName;
    QString m_uid;
    QString m_description;
    QDateTime m_date;

};

class QXMPP_EXPORT GHypeFileLogs : public QXmppStanza
{
public:
    GHypeFileLogs(const QString& from = QString(), const QDateTime& dateFrom = QDateTime(), const QDateTime& dateTo = QDateTime(),
    const QString& reqType = QString("pull"), const QString& withBareJid = QString());
    GHypeFileLogs(const GHypeFileLogs &other);

    GHypeFileLogs& operator=(const GHypeFileLogs &other);
    ~GHypeFileLogs();

    QList<GHypeFileTransferItem*> fileTransfers() const;

    QString getWithBareJid() const;
    void parse(const QDomElement &element);
    void toXml(QXmlStreamWriter *xmlWriter) const;

private:
    QSharedDataPointer<GHypeFileLogsPrivate> d;

    void parseFileTransferLog(const QDomElement &element);
    void fileTransferToXml(QXmlStreamWriter *xmlWriter) const;

};
#endif // GHYPEFILELOGS

