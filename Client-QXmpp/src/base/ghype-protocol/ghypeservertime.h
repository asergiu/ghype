#ifndef GHYPESERVERTIME_H
#define GHYPESERVERTIME_H

#include <QDateTime>

#include "QXmppStanza.h"

class QXMPP_EXPORT GHypeServerTime : public QXmppStanza
{
public:
    GHypeServerTime();
    ~GHypeServerTime();
    void parse(const QDomElement &element);
    void toXml(QXmlStreamWriter *writer) const;
    QDateTime getServerTime() const;
private:
    QDateTime m_serverUTC;
};

#endif // GHYPESERVERTIME_H

