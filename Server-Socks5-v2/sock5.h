#ifndef __SOCK5_H
#define __SOCK5_H

#include <unistd.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <syslog.h>
#include <sys/ioctl.h>
#include <asm/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <semaphore.h>
#include <pthread.h>

#include <mysql/mysql.h>

#define true	1
#define false	0

#ifndef DEF_ETCFILE
#define DEF_ETCFILE "/usr/local/jabberd/etc/socks5.conf"
#endif

#define PID_FILE "/var/run/sock5"

#define MAX_SYN_BACKLOG		10
#define READ_BUFFER_SIZE	10240

typedef unsigned char byte;

#pragma pack(1)

struct sock5_client_memory {
  int sockfd;
  struct config *cfg;
  MYSQL *mysql_conn;
  int thread_ready;
};

#define FILE_TRANSFER_TYPE_PUSH	1
#define FILE_TRANSFER_TYPE_PULL	2

#define FILE_TRANSFER_ACTIVATE	1
#define FILE_TRANSFER_PARTIAL	2
#define FILE_TRANSFER_FINISH	3
#define FILE_TRANSFER_ERROR	4

struct transfer_details {
  unsigned short int status;
  unsigned short int type;
  char *filename;
  char *uniqueid;
  char *hash;
  unsigned char hash_length;
  unsigned long int filesize;
  unsigned long int partial_filesize;
  unsigned long int read_bytes;
  int fd;
};

/*
  structure: config
  Contine variabilele de configurare
*/
struct config {
  char *config_filename;

  unsigned long int listen_ip_address;
  unsigned short int listen_port;
  unsigned short int sndrcv_timeout;
  
  char *jid;
  
  char *upload_directory;
  char *log_filename;
  
  // MySQL parameters

  char* mysql_host;
  unsigned short int mysql_port;
  char* mysql_username;
  char* mysql_password;
  char* mysql_database;
  unsigned int mysql_connect_timeout;
  unsigned int mysql_readwrite_timeout;
  unsigned int mysql_inactivity_timeout;
  
  unsigned int socket_timeout;
  unsigned int wait_for_push_timeout;
};

/* global */
#define SETSIG(sig, fun, fla)       \
  do {                              \
    sa.sa_handler = fun;            \
    sa.sa_flags = fla;              \
    sigaction(sig, &sa, NULL);      \
  } while (0);

extern char* filename;
extern struct config *cfg;
extern int var_debug;
extern pthread_mutex_t mysql_mutex;

/* config.c */
	struct config *read_config(struct config **, char*);
	void clean_config(struct config*);

/* utils.c */
        void _log(const char* file, unsigned int line, const int lvl, const char *format, ...);
        char *intoa(unsigned long addr);

        void sock5_process_server(struct config *cfg, MYSQL *conn);

/* database.c */
        struct transfer_details* read_transfer_details(MYSQL *conn, int sockfd, struct config *cfg, char* domain_name, unsigned long int chunk_filesize, char* hash);
        void update_transfer_details(MYSQL *conn, int sockfd, struct config *cfg, char* domain_name, int error, struct transfer_details* details);
        int check_file_transfer(MYSQL *conn, int sockfd, struct config *cfg, char* uniqueid);
        int seek_transfer_details(MYSQL *conn, int sockfd, struct config *cfg, unsigned char ack, unsigned long int client_filesize, char *client_hash, struct transfer_details *ftransfer);
        void clear_transfer_details(MYSQL *conn, int sockfd, struct config *cfg);
        void truncate_transfer_details(MYSQL *conn, int sockfd, struct config *cfg, struct transfer_details *ftransfer);
        void update_transfer_status(MYSQL *conn, int sockfd, struct config *cfg, char* domain_name, struct transfer_details* details);

#define log(lvl, format, args...) do { \
    _log(__FILE__, __LINE__, lvl, format, ##args); \
  } while (0)

#endif
