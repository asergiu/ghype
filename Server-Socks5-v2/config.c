#include <stdio.h>
#ifndef __USE_ISOC99
#define __USE_ISOC99
#endif
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/time.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#define SYSLOG_NAMES
#include <syslog.h>
#include <math.h>
#include <stdlib.h>

#include "sock5.h"

struct config *read_config(struct config **config, char* fname) {
  struct config *cfg = NULL;
  FILE *file;
                                                                                                                             
  if(*config == NULL) {
    if((cfg = (struct config*)malloc(sizeof(struct config))) != NULL) {
      memset(cfg, '\0', sizeof(struct config));
      cfg->config_filename = strdup(fname);
      *config = cfg;
    }
  } else {
    cfg = *config;
    if(cfg->config_filename != NULL) free(cfg->config_filename);
    cfg->config_filename = strdup(fname);
  }
  
  if(cfg != NULL) {
    if((file = fopen(fname, "r")) != NULL) {
      char buffer[1024];
      int line = 0;
      while(fgets(buffer, sizeof(buffer), file)) {
        char *cmt = strchr(buffer, '\n');
        if(cmt) *cmt = '\0';
                                                                                                                             
        line++;
                                                                                                                             
        cmt = strchr(buffer, '#');
        if(cmt) {
          if(cmt == buffer) *cmt = '\0';
          if(cmt && ( *(cmt-1) == '\n' || *(cmt-1) == '\r' || isspace(*(cmt-1)))) *cmt = '\0';
        }
                                                                                                                             
        while(isspace(*buffer)) memmove(buffer, buffer+1, strlen(buffer));
        if(!*buffer) continue;
                                                                                                                             
        cmt = strchr(buffer, '\0');
        cmt--;
        while(isspace(*cmt)) {
          *cmt = '\0';
          cmt--;
        }

        if(*buffer) {
          char *kwd = buffer;
          char *value = buffer + strcspn(buffer, " \t");
          *value++ = '\0';
          while(isspace(*value)) value++;

          if(strcasecmp(kwd, "LogFilename") == 0) {
            cfg->log_filename = strdup(value);
            log(LOG_INFO, "LogFilename = \"%s\"", value);
          } else if(strcasecmp(kwd, "UploadDirectory") == 0) {
            cfg->upload_directory = strdup(value);
            log(LOG_INFO, "UploadDirectory = \"%s\"", value);
          } else if(strcasecmp(kwd, "JID") == 0) {
            cfg->jid = strdup(value);
            log(LOG_INFO, "JID = \"%s\"", value);

          } else if(strcasecmp(kwd, "MySQLHost") == 0) {
            cfg->mysql_host = strdup(value);
            log(LOG_INFO, "MySQLHost = \"%s\"", cfg->mysql_host);
          } else if(strcasecmp(kwd, "MySQLPort") == 0) {
            cfg->mysql_port = strtoul(value, NULL, 10);
            log(LOG_INFO, "MySQLPort = \"%d\"", cfg->mysql_port);
          } else if(strcasecmp(kwd, "MySQLUsername") == 0) {
            cfg->mysql_username = strdup(value);
            log(LOG_INFO, "MySQLUsername = \"%s\"", cfg->mysql_username);
          } else if(strcasecmp(kwd, "MySqlPassword") == 0) {
            cfg->mysql_password = strdup(value);
            log(LOG_INFO, "MySqlPassword = \"*******\"");
          } else if(strcasecmp(kwd, "MySQLConnectTimeout") == 0) {
            cfg->mysql_connect_timeout = strtoul(value, NULL, 10);
            log(LOG_INFO, "MySQLConnectTimeout = \"%d\"", cfg->mysql_connect_timeout);
          } else if(strcasecmp(kwd, "MySQLReadWriteTimeout") == 0) {
            cfg->mysql_readwrite_timeout = strtoul(value, NULL, 10);
            log(LOG_INFO, "MySQLReadWriteTimeout = \"%d\" sec", cfg->mysql_readwrite_timeout);
          } else if(strcasecmp(kwd, "MySQLInactivityTimeout") == 0) {
            cfg->mysql_inactivity_timeout = strtoul(value, NULL, 10);
            log(LOG_INFO, "MySQLInactivityTimeout = \"%d\" sec", cfg->mysql_inactivity_timeout);
          } else if(strcasecmp(kwd, "MySQLDatabase") == 0) {
            cfg->mysql_database = strdup(value);
            log(LOG_INFO, "MySQLDatabase = \"%s\"", cfg->mysql_database);

          } else if(strcasecmp(kwd, "SocketReadTimeout") == 0) {
            cfg->socket_timeout = strtoul(value, NULL, 10);
            log(LOG_INFO, "SocketReadTimeout = \"%d\" sec", cfg->socket_timeout);
          } else if(strcasecmp(kwd, "WaitForPushTimeout") == 0) {
            cfg->wait_for_push_timeout = strtoul(value, NULL, 10);
            log(LOG_INFO, "WaitForPushTimeout = \"%d\" sec", cfg->wait_for_push_timeout);

          } else if(strcasecmp(kwd, "ListenIPAddress") == 0) {
            cfg->listen_ip_address = inet_addr(value);
            log(LOG_INFO, "ListenIPAddress = \"%s\"", intoa(cfg->listen_ip_address));
          } else if(strcasecmp(kwd, "ListenPort") == 0) {
            cfg->listen_port = strtoul(value, NULL, 10);
            log(LOG_INFO, "ListenPort = \"%d\"", cfg->listen_port);
          } else if(strcasecmp(kwd, "SendReciveTimeout") == 0) {
            cfg->sndrcv_timeout = strtoul(value, NULL, 10);
            log(LOG_INFO, "send/receive timeout = \"%d\"", cfg->sndrcv_timeout);
          } else {
            log(LOG_WARNING, "unknown keyword %s in line %d", kwd, line);
          }
        }
      }
      fclose(file);
    } else {
      log(LOG_ERR, "can not open config file \"%s\": %s", fname, strerror(errno));
      free(cfg);
      cfg = NULL;
    }
  }
  
  return cfg;
}

void clean_config(struct config *cfg) {
  if(cfg != NULL) {
    if(cfg->config_filename != NULL) free(cfg->config_filename);
    if(cfg->jid != NULL) free(cfg->jid);
    if(cfg->upload_directory != NULL) free(cfg->upload_directory);
    if(cfg->log_filename != NULL) free(cfg->log_filename);
    if(cfg->mysql_host != NULL) free(cfg->mysql_host);
    if(cfg->mysql_username != NULL) free(cfg->mysql_username);
    if(cfg->mysql_password != NULL) free(cfg->mysql_password);
    if(cfg->mysql_database != NULL) free(cfg->mysql_database);

    free(cfg);
    cfg = NULL;
  }
  log(LOG_INFO, "free struct config");
}
