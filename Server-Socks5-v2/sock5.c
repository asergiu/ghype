#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <syslog.h>
#include <signal.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <stdarg.h>
#include <pthread.h>

#include "sock5.h"

#define SOCK5_STEP_START			 0
#define SOCK5_STEP_READ_AUTH_LENGTH		 1
#define SOCK5_STEP_READ_AUTH			 2
#define SOCK5_STEP_WRITE_AUTH			 3
#define SOCK5_STEP_READ_COMMAND			 4
#define SOCK5_STEP_WRITE_FINISH			 5
#define SOCK5_STEP_READ_3_DOMAIN		 6
#define SOCK5_STEP_READ_3_DOMAIN_LENGTH		 7
#define SOCK5_STEP_WRITE_ACCEPT			 8
#define SOCK5_STEP_EXCHANGE_DATA			 9
#define SOCK5_STEP_READ_8_DOMAIN		10
#define SOCK5_STEP_READ_8_DOMAIN_LENGTH		11
#define SOCK5_STEP_READ_8_CHUNK_SIZE		12
#define SOCK5_STEP_READ_8_HASH_LENGTH		13
#define SOCK5_STEP_READ_8_HASH			14
#define SOCK5_STEP_WRITE_ACCEPT_ACK		15
#define SOCK5_STEP_READ_ACK			16
#define SOCK5_STEP_WRITE_ACK			17

#define ADVANCE_READ_BUFFER \
  memmove(read_buffer, read_buffer + sock5_step_length, read_buffer_length - sock5_step_length); \
  read_buffer_length -= sock5_step_length;

void* accept_sock5_client(void *arg) {
  struct sock5_client_memory *memory = (struct sock5_client_memory *)arg;
  char buffer[1024], read_buffer[READ_BUFFER_SIZE], write_buffer[READ_BUFFER_SIZE], *ptr = NULL;
  char sock5_domain_name[128], sock5_file_hash[128];
  unsigned long int read_buffer_length = 0, write_buffer_length = 0, sock5_step_length = 0, sock5_new_step_length = 0, partial_file_size = 0;
  unsigned char sock5_domain_name_length = 0, transfer_client_ack = 0;
  unsigned short destination_port = 0;
  struct transfer_details *file_transfer_details = NULL;
  int retval = -1, nbytes = -1, wbytes = -1, tbytes = -1, sock5_step, sock_read_timeout, push_wait_timeout;
  int error = false, exit_with_no_error = false;

  sock_read_timeout = 0;
  push_wait_timeout = 0;
  sock5_step = SOCK5_STEP_START;
  sock5_step_length = 1;
  while(true) {
    if(error || exit_with_no_error) break;
    if(sock5_step == SOCK5_STEP_WRITE_AUTH || sock5_step == SOCK5_STEP_WRITE_FINISH || sock5_step == SOCK5_STEP_WRITE_ACCEPT || sock5_step == SOCK5_STEP_WRITE_ACCEPT_ACK || sock5_step == SOCK5_STEP_WRITE_ACK) {
      ptr = write_buffer;
      while(write_buffer_length > 0) {
        if((wbytes = send(memory->sockfd, ptr, write_buffer_length, 0)) < 0) {
          if(errno == EAGAIN || errno == EWOULDBLOCK) {
            continue;
          } else {
            log(LOG_ERR, "[%02d] sock5 write error: %s", memory->sockfd, strerror(errno));
            error = true;
            break;
          }
        } else {
          write_buffer_length = write_buffer_length - wbytes;
          ptr = ptr + wbytes;
        }
      }
      
      if(error) break;
      if(sock5_step == SOCK5_STEP_WRITE_FINISH) break;
      else if(sock5_step == SOCK5_STEP_WRITE_AUTH) {
        sock5_step = SOCK5_STEP_READ_COMMAND;
        sock5_step_length = 4;
      } else if(sock5_step == SOCK5_STEP_WRITE_ACCEPT || sock5_step == SOCK5_STEP_WRITE_ACK) {
        sock5_step = SOCK5_STEP_EXCHANGE_DATA;
        sock5_step_length = 0;
      } else if(sock5_step == SOCK5_STEP_WRITE_ACCEPT_ACK) {
        sock5_step = SOCK5_STEP_READ_ACK;
        sock5_step_length = 1;
      }
    } else if(sock5_step == SOCK5_STEP_EXCHANGE_DATA && file_transfer_details != NULL && file_transfer_details->fd > 0 && file_transfer_details->type == FILE_TRANSFER_TYPE_PULL) {
      read_buffer_length = 0;
      while(true) {
        if((nbytes = read(file_transfer_details->fd, read_buffer, READ_BUFFER_SIZE)) > 0) {
          char *ptr = read_buffer;
          wbytes = nbytes;
          while(wbytes > 0) {
            if((tbytes = send(memory->sockfd, ptr, wbytes, 0)) < 0) {
              if(errno == EAGAIN || errno == EWOULDBLOCK) {
                continue;
              } else {
                log(LOG_ERR, "[%02d] sock5 write error: %s", memory->sockfd, strerror(errno));
                error = true;
                break;
              }
            } else {
              ptr = ptr + tbytes;
              wbytes = wbytes - tbytes;
              file_transfer_details->partial_filesize += tbytes;
            }
          }
          push_wait_timeout = 0;
        } else if(file_transfer_details->partial_filesize < file_transfer_details->filesize) {
          if(check_file_transfer(memory->mysql_conn, memory->sockfd, memory->cfg, file_transfer_details->uniqueid)) {
            if((push_wait_timeout = push_wait_timeout + 3) < memory->cfg->wait_for_push_timeout) {
              log(LOG_INFO, "[%02d] another thread is writing to file, waiting 3 seconds and retrying...", memory->sockfd);
              sleep(3);
            } else {
              log(LOG_ERR, "[%02d] push error, aborting...", memory->sockfd);
              error = true;
              break;
            }
          } else {
            log(LOG_ERR, "[%02d] push error, aborting...", memory->sockfd);
            error = true;
            break;
          }
        } else {
          log(LOG_INFO, "[%02d] file sent", memory->sockfd);
          exit_with_no_error = true;
          break;
        }
        if(error) break;
      }
    } else {
      fd_set socket_set;
      struct timespec sock_timeout;
      memset(&sock_timeout, '\0', sizeof(struct timespec));
      sock_timeout.tv_sec = 1;

      FD_ZERO(&socket_set);
      FD_SET(memory->sockfd, &socket_set);
      if((retval = pselect(memory->sockfd + 1, &socket_set, NULL, NULL, &sock_timeout, NULL)) > 0) {    
        sock_read_timeout = 0;
        
        if(FD_ISSET(memory->sockfd, &socket_set)) {
          if((nbytes = recv(memory->sockfd, buffer, 7, 0)) > 0) {
            
            if(read_buffer_length + nbytes <= READ_BUFFER_SIZE) {
              memcpy((read_buffer + read_buffer_length), buffer, nbytes);
              read_buffer_length += nbytes;
              
              
              if(sock5_step == SOCK5_STEP_EXCHANGE_DATA) {
                if(file_transfer_details != NULL && file_transfer_details->fd > 0 && file_transfer_details->type == FILE_TRANSFER_TYPE_PUSH) {
                
//              log(LOG_INFO, "[%02d] step: %d, read %d bytes, read_buffer_length = %d, sock5_step_length = %d, partial_filesize = %lu", memory->sockfd, sock5_step, nbytes, read_buffer_length, sock5_step_length, file_transfer_details->partial_filesize);

                  if((wbytes = write(file_transfer_details->fd, read_buffer, read_buffer_length)) < 0) {
                    log(LOG_ERR, "[%02d] can not write into %d", memory->sockfd, file_transfer_details->fd);
                    error = true;
                    break;
                  } else if(wbytes == 0) {
                    log(LOG_WARNING, "[%02d] 0 bytes was written into %d", memory->sockfd, file_transfer_details->fd);
                  } else {
                    file_transfer_details->partial_filesize += wbytes;
                    file_transfer_details->read_bytes += wbytes;
                    sock5_step_length = wbytes;
                    ADVANCE_READ_BUFFER;
                  }

//                  if(file_transfer_details->read_bytes > 1024) {
//                    error = true;
//                    break;
//                  }
                  
                } else {
                  log(LOG_ERR, "[%02d] can not read/write from/into file", memory->sockfd);
                  error = true;
                  break;
                }
              } else {
                int retry_read_buffer = true;
                while(retry_read_buffer && read_buffer_length != 0 && read_buffer_length >= sock5_step_length) {
                  switch(sock5_step) {
                    case SOCK5_STEP_START:
                      // SOCK5 version
                      if(read_buffer[0] == 0x05) {
                        ADVANCE_READ_BUFFER;
                        sock5_step = SOCK5_STEP_READ_AUTH_LENGTH;
                        sock5_step_length = 1;
                      } else {
                        log(LOG_ERR, "[%02d] sock5 protocol error: waiting 0x05 received 0x%x", memory->sockfd, read_buffer[0]);
                        error = true;
                      }
                      break;
                      
                    case SOCK5_STEP_READ_AUTH_LENGTH:
                      // SOCK5 number of authentication methods supported
                      if((sock5_new_step_length = read_buffer[0]) > 0) {
                        ADVANCE_READ_BUFFER;
                        sock5_step = SOCK5_STEP_READ_AUTH;
                        sock5_step_length = sock5_new_step_length;
                      } else {
                        log(LOG_ERR, "[%02d] unsuported sock5 authentication method", memory->sockfd);
                        memcpy(write_buffer, "\x05\x07\x00\x00", 4);
                        write_buffer_length = 4;
                        sock5_step = SOCK5_STEP_WRITE_FINISH;
                      }
                      break;
                      
                    case SOCK5_STEP_READ_AUTH:
                      // SOCK5 list of authentication methods. Do nothing
                      ADVANCE_READ_BUFFER;
                      
                      memcpy(write_buffer, "\x05\x00", 2);
                      write_buffer_length = 2;
                      sock5_step = SOCK5_STEP_WRITE_AUTH;
                      sock5_step_length = 0;
                      break;
                      
                    case SOCK5_STEP_READ_COMMAND:
                      // SOCK5 command
                      if(!(((unsigned char)read_buffer[0]) == 0x05 && ((unsigned char)read_buffer[1]) == 0x01)) {
                        log(LOG_ERR, "[%02d] unsuported sock5 version/command: 0x%02x/0x%02x", memory->sockfd, read_buffer[0], read_buffer[1]);
                        memcpy(write_buffer, "\x05\x07\x00\x00", 4);
                        write_buffer_length = 4;
                        sock5_step = SOCK5_STEP_WRITE_FINISH;
                      } else if((((unsigned char)read_buffer[2]) == 0x00 && ((unsigned char)read_buffer[3]) == 0x03)) {
                        ADVANCE_READ_BUFFER;
                        log(LOG_ERR, "[%02d] sock5 command 0x03", memory->sockfd);

                        sock5_step = SOCK5_STEP_READ_3_DOMAIN_LENGTH;
                        sock5_step_length = 1;
                      } else if((((unsigned char)read_buffer[2]) == 0x00 && ((unsigned char)read_buffer[3]) == 0x08)) {
                        ADVANCE_READ_BUFFER;

                        log(LOG_ERR, "[%02d] sock5 command 0x08", memory->sockfd);

                        sock5_step = SOCK5_STEP_READ_8_DOMAIN_LENGTH;
                        sock5_step_length = 1;
                      } else {
                        log(LOG_ERR, "[%02d] unsuported sock5 address: 0x%02x/0x%02x", memory->sockfd, read_buffer[2], read_buffer[3]);
                        memcpy(write_buffer, "\x05\x08\x00\x00", 4);
                        write_buffer_length = 4;
                        sock5_step = SOCK5_STEP_WRITE_FINISH;
                      }
                      break;
                      
                    case SOCK5_STEP_READ_3_DOMAIN_LENGTH:
                      if((sock5_new_step_length = read_buffer[0]) > 0 && sock5_new_step_length < 128) {
                        ADVANCE_READ_BUFFER;
                        sock5_step = SOCK5_STEP_READ_3_DOMAIN;
                        sock5_step_length = sock5_new_step_length + 2;
                      } else {
                        log(LOG_ERR, "[%02d] unsuported sock5 domain with length %d", memory->sockfd, sock5_new_step_length);
                        memcpy(write_buffer, "\x05\x08\x00\x00", 4);
                        write_buffer_length = 4;
                        sock5_step = SOCK5_STEP_WRITE_FINISH;
                      }
                      break;
                      
                    case SOCK5_STEP_READ_3_DOMAIN:
                      memset(sock5_domain_name, '\0', 128);
                      sock5_domain_name_length = sock5_step_length - 2;
                      memcpy(sock5_domain_name, read_buffer, sock5_domain_name_length);
                      memcpy(&destination_port, read_buffer + sock5_domain_name_length, 2);
                      log(LOG_INFO, "[%02d] domain name is \"%s\", on port %d[%d]", memory->sockfd, sock5_domain_name, destination_port, htons(destination_port));
                      
                      ADVANCE_READ_BUFFER;

                      if((file_transfer_details = read_transfer_details(memory->mysql_conn, memory->sockfd, memory->cfg, sock5_domain_name, 0, NULL)) == NULL) {
                        log(LOG_ERR, "[%02d] can not find \"%s\" domain in MySQL", memory->sockfd, sock5_domain_name);
                        memcpy(write_buffer, "\x05\x01\x00\x03", 4);
                        memcpy(write_buffer + 4, &sock5_domain_name_length, 1);
                        memcpy(write_buffer + 5, sock5_domain_name, sock5_domain_name_length);
                        memcpy(write_buffer + 5 + sock5_domain_name_length, &destination_port, 2);
                        write_buffer_length = sock5_domain_name_length + 7;
                        sock5_step = SOCK5_STEP_WRITE_FINISH;
                      } else {
                        memcpy(write_buffer, "\x05\x00\x00\x03", 4);
                        memcpy(write_buffer + 4, &sock5_domain_name_length, 1);
                        memcpy(write_buffer + 5, sock5_domain_name, sock5_domain_name_length);
                        memcpy(write_buffer + 5 + sock5_domain_name_length, &destination_port, 2);
                        write_buffer_length = sock5_domain_name_length + 7;
                        sock5_step = SOCK5_STEP_WRITE_ACCEPT;
                      }
                      break;

                    case SOCK5_STEP_READ_8_DOMAIN_LENGTH:
                      if((sock5_new_step_length = read_buffer[0]) > 0 && sock5_new_step_length < 128) {
                        ADVANCE_READ_BUFFER;
                        sock5_step = SOCK5_STEP_READ_8_DOMAIN;
                        sock5_step_length = sock5_new_step_length + 2;
                      } else {
                        log(LOG_ERR, "[%02d] unsuported sock5 domain with length %d", memory->sockfd, sock5_new_step_length);
                        memcpy(write_buffer, "\x05\x08\x00\x00", 4);
                        write_buffer_length = 4;
                        sock5_step = SOCK5_STEP_WRITE_FINISH;
                      }
                      break;

                    case SOCK5_STEP_READ_8_DOMAIN:
                      memset(sock5_domain_name, '\0', 128);
                      sock5_domain_name_length = sock5_step_length - 2;
                      memcpy(sock5_domain_name, read_buffer, sock5_domain_name_length);
                      memcpy(&destination_port, read_buffer + sock5_domain_name_length, 2);
                      log(LOG_INFO, "[%02d] domain name is \"%s\", on port %d[%d]", memory->sockfd, sock5_domain_name, destination_port, htons(destination_port));
                      
                      ADVANCE_READ_BUFFER;
                      sock5_step = SOCK5_STEP_READ_8_CHUNK_SIZE;
                      sock5_step_length = 4;
                      break;
                      
                    case SOCK5_STEP_READ_8_CHUNK_SIZE:
                      partial_file_size = 0;
                      memcpy(&partial_file_size, read_buffer, 4);
                      ADVANCE_READ_BUFFER;
                      sock5_step = SOCK5_STEP_READ_8_HASH_LENGTH;
                      sock5_step_length = 1;
                      log(LOG_INFO, "[%02d] chunk filesize is %lu", memory->sockfd, partial_file_size);
                      break;
                      
                    case SOCK5_STEP_READ_8_HASH_LENGTH:
                      if((sock5_new_step_length = read_buffer[0]) > 0 && sock5_new_step_length < 128) {
                        ADVANCE_READ_BUFFER;
                        sock5_step = SOCK5_STEP_READ_8_HASH;
                        sock5_step_length = sock5_new_step_length;
                        log(LOG_INFO, "[%02d] hash size is %d", memory->sockfd, sock5_step_length);
                        
                        break;
                      } else {
                        log(LOG_INFO, "[%02d] hash size is 0", memory->sockfd);
                        ADVANCE_READ_BUFFER;
                        sock5_step_length = 0;
                      }
                      // In caz in care hash size = 0, trebuie sarit la pasul urmator !!!
                      
                    case SOCK5_STEP_READ_8_HASH:
                      memset(sock5_file_hash, '\0', 128);
                      if(sock5_step_length > 0) {
                        memcpy(sock5_file_hash, read_buffer, sock5_step_length);
                        log(LOG_INFO, "[%02d] hash is \"%s\"", memory->sockfd, sock5_file_hash);
                      
                        ADVANCE_READ_BUFFER;
                      }

                      if((file_transfer_details = read_transfer_details(memory->mysql_conn, memory->sockfd, memory->cfg, sock5_domain_name, partial_file_size, sock5_file_hash)) == NULL) {
                        log(LOG_ERR, "[%02d] can not find \"%s\" domain in MySQL", memory->sockfd, sock5_domain_name);
                        memcpy(write_buffer, "\x05\x01\x00\x03", 4);
                        memcpy(write_buffer + 4, &sock5_domain_name_length, 1);
                        memcpy(write_buffer + 5, sock5_domain_name, sock5_domain_name_length);
                        memcpy(write_buffer + 5 + sock5_domain_name_length, &destination_port, 2);
                        write_buffer_length = sock5_domain_name_length + 7;
                        sock5_step = SOCK5_STEP_WRITE_FINISH;
                      } else {
                        memcpy(write_buffer, "\x05\x00\x00\x08", 4);
                        memcpy(write_buffer + 4, &sock5_domain_name_length, 1);
                        memcpy(write_buffer + 5, sock5_domain_name, sock5_domain_name_length);
                        memcpy(write_buffer + 5 + sock5_domain_name_length, &destination_port, 2);
                        memcpy(write_buffer + 7 + sock5_domain_name_length, &(file_transfer_details->partial_filesize), 4);
                        memcpy(write_buffer + 11 + sock5_domain_name_length, &(file_transfer_details->hash_length), 1);
                        if(file_transfer_details->hash != NULL) {
                          log(LOG_ERR, "[%02d] sending chunk_size = %lu, hash = '%s'", memory->sockfd, file_transfer_details->partial_filesize, file_transfer_details->hash);
                          memcpy(write_buffer + 12 + sock5_domain_name_length, file_transfer_details->hash, file_transfer_details->hash_length);
                          write_buffer_length = sock5_domain_name_length + file_transfer_details->hash_length + 12;
                        } else {
                          write_buffer_length = sock5_domain_name_length + 12;
                        }
                        sock5_step = SOCK5_STEP_WRITE_ACCEPT_ACK;
                      }
                      break;
                      
                    case SOCK5_STEP_READ_ACK:
                      transfer_client_ack = read_buffer[0];

                      ADVANCE_READ_BUFFER;
                      if(seek_transfer_details(memory->mysql_conn, memory->sockfd, memory->cfg, transfer_client_ack, partial_file_size, sock5_file_hash, file_transfer_details)) {
                        memcpy(write_buffer, "\x01", 1);
                      } else {
                        memcpy(write_buffer, "\x00", 1);
                      }
                      write_buffer_length = 1;
                      sock5_step = SOCK5_STEP_WRITE_ACK;
                      break;
                      
                    case SOCK5_STEP_WRITE_AUTH:
                    case SOCK5_STEP_WRITE_FINISH:
                    case SOCK5_STEP_WRITE_ACCEPT:
                    case SOCK5_STEP_WRITE_ACCEPT_ACK:
                    case SOCK5_STEP_WRITE_ACK:
                      retry_read_buffer = false;
                      break;

                    default:
                      log(LOG_ERR, "[%02d] sock5 protocol error: step = %d (%d/%d)", memory->sockfd, sock5_step, read_buffer_length, sock5_step_length);
                      error = true;
                      break;
                  }
                  
                  if(error) break;
                }
                
                if(error) break;
              }
              
            } else {
              log(LOG_ERR, "[%02d] read buffer full: %d + %d >= %d", memory->sockfd, nbytes, read_buffer_length, READ_BUFFER_SIZE);
              break;
            }
            if(error) break;
          } else if(nbytes == 0) {
            log(LOG_INFO, "[%02d] connection closed by remote host", memory->sockfd);

            if(file_transfer_details != NULL && file_transfer_details->fd > 0 && file_transfer_details->type == FILE_TRANSFER_TYPE_PUSH) {
              while(true) {
                if(read_buffer_length == 0) break;
                if((wbytes = write(file_transfer_details->fd, read_buffer, read_buffer_length)) < 0) {
                  log(LOG_ERR, "[%02d] can not write into %d", memory->sockfd, file_transfer_details->fd);
                  error = true;
                  break;
                } else if(wbytes == 0) {
                  log(LOG_WARNING, "[%02d] 0 bytes was written into %d", memory->sockfd, file_transfer_details->fd);
                } else {
                  file_transfer_details->partial_filesize += wbytes;
                  sock5_step_length = wbytes;
                  ADVANCE_READ_BUFFER;
                }
              }
              
            } else {
              log(LOG_ERR, "[%02d] can not read/write from/into file", memory->sockfd);
              error = true;
            }
            break;
          } else if(errno == EAGAIN || errno == EWOULDBLOCK) {
            continue;
          } else {
            log(LOG_ERR, "[%02d] error occured on connection: %s", memory->sockfd, strerror(errno));
            break;
          }
        } else {
          log(LOG_ERR, "[%02d] socket unknown error", memory->sockfd);
          break;
        }
      } else if(retval == 0) {
        // Timeout occured, go forward with checkings
        if((sock_read_timeout = sock_read_timeout + 1) > memory->cfg->socket_timeout) {
          log(LOG_ERR, "[%02d] socket timeout", memory->sockfd);
          break;
        }
      } else if(errno == EINTR) {
        // Interrupt occured, go beck to pselect
        continue;
      } else {
        log(LOG_ERR, "[%02d] socket pselect error: %s", memory->sockfd, strerror(errno));
        break;
      }
    }
  }
  
  if(file_transfer_details != NULL) {
    if(file_transfer_details->fd > 0) close(file_transfer_details->fd);
    file_transfer_details->fd = -1;
    log(LOG_INFO, "[%02d] clear file transfer", memory->sockfd);
    
    update_transfer_details(memory->mysql_conn, memory->sockfd, memory->cfg, sock5_domain_name, error, file_transfer_details);

    log(LOG_INFO, "[%02d] clear file transfer done", memory->sockfd);

    if(file_transfer_details->filename != NULL) free(file_transfer_details->filename);
    if(file_transfer_details->uniqueid != NULL) free(file_transfer_details->uniqueid);
    if(file_transfer_details->hash != NULL) free(file_transfer_details->hash);
    free(file_transfer_details);
    file_transfer_details = NULL;

    log(LOG_INFO, "[%02d] free file transfer details", memory->sockfd);
  }

  close(memory->sockfd);
  free(memory);
  pthread_exit(0);
}

void sock5_process_server(struct config *cfg, MYSQL *mysql_conn) {
  int sockfd = -1, newfd = -1, error = false;

  if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) > 0) {
    struct sockaddr_in serv_addr, cli_addr;
    socklen_t addrlen = sizeof(cli_addr);
    int keepalive_flag = true, nodelay_flag = true, reuse_flag = true, socket_buffer_size = 1024 * 8, flags = 0;
    struct timeval tv;

    memset(&serv_addr, '\0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    if(cfg->listen_ip_address == 0) serv_addr.sin_addr.s_addr = INADDR_ANY;
    else serv_addr.sin_addr.s_addr = cfg->listen_ip_address;
    serv_addr.sin_port = htons(cfg->listen_port);

    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse_flag, sizeof(reuse_flag));

    if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) == 0) {
      if(listen(sockfd, MAX_SYN_BACKLOG) == 0) {

        log(LOG_INFO, "listening on %s:%d and socket %d", intoa(cfg->listen_ip_address), cfg->listen_port, sockfd);
        
        while(true) {
          memset(&cli_addr, '\0', sizeof(cli_addr));
          if((newfd = accept(sockfd, (struct sockaddr *)&cli_addr, &addrlen)) > 0) {
            struct sock5_client_memory *memory = NULL;
            
            setsockopt(newfd, SOL_SOCKET, SO_KEEPALIVE, (char*)&keepalive_flag, sizeof(keepalive_flag));
            
            tv.tv_sec = cfg->sndrcv_timeout; tv.tv_usec = 0;
//            setsockopt(newfd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(struct timeval));
//            setsockopt(newfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));

//            setsockopt(newfd, SOL_SOCKET, SO_SNDBUF, &socket_buffer_size, sizeof(socket_buffer_size));
//            setsockopt(newfd, SOL_SOCKET, SO_RCVBUF, &socket_buffer_size, sizeof(socket_buffer_size));

#if defined(O_NONBLOCK)
            if((flags = fcntl(newfd, F_GETFL, 0)) == -1) flags = 0;
            fcntl(newfd, F_SETFL, flags | O_NONBLOCK);
#else
            ioctl(newfd, FIOBIO, &flags);
#endif

            if((memory = (struct sock5_client_memory *)malloc(sizeof(struct sock5_client_memory))) != NULL) {
              pthread_t thread_id;
              pthread_attr_t attr;
              
              memory->sockfd = newfd;
              memory->cfg = cfg;
              memory->mysql_conn = mysql_conn;
              memory->thread_ready = false;

              if(pthread_attr_init(&attr) == 0) {
                if(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) == 0) {
                  if(pthread_create(&thread_id, &attr, &accept_sock5_client, memory) == 0) {
                    log(LOG_INFO, "[%02d] accepted connection from %s on thread \"%010lX\"", newfd, inet_ntoa(cli_addr.sin_addr), (unsigned long int)thread_id);
                  } else {
                    log(LOG_ERR, "[%02d] can not create thread: %s", newfd, strerror(errno));
                    error = true;
                  }
                } else {
                  log(LOG_ERR, "[%02d] can not create detached thread: %s", newfd, strerror(errno));
                  error = true;
                }
              } else {
                log(LOG_ERR, "[%02d] can not initialize thread: %s", newfd, strerror(errno));
                error = true;
              }
              
              if(error) {
                free(memory);
                close(newfd);
              }

            } else {
              log(LOG_ERR, "[%02d] memory allocation error: %s", newfd, strerror(errno));
              close(newfd);
              break;
            }
          } else {
            if(errno == EAGAIN || errno == EWOULDBLOCK) {
              log(LOG_INFO, "[%02d] EAGAIN|EWOULDBLOCK on socket", newfd);
            } else {
              log(LOG_ERR, "[%02d] socket accept error: %s", sockfd, strerror(errno));
              break;
            }
          } 
        }
      } else {
        log(LOG_ERR, "listen on socket failed: %s", strerror(errno));
        close(sockfd);
      }
    } else {
      log(LOG_ERR, "bind socket failed: %s", strerror(errno));
      close(sockfd);
    }

  } else {
    log(LOG_ERR, "open socket failed: %s", strerror(errno));
  }

}

