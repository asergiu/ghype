#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <syslog.h>
#include <signal.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <stdarg.h>

#include "sock5.h"

/* globals */
char *filename = NULL;
int process_id = 0;
int var_debug = false;
struct config *cfg;
MYSQL *conn = NULL;

/* Group: Functii */

/*
  Function: usage
  
  Afiseaza informatia cum se foloseste programul
  
*/
void usage() {
  fprintf(stderr, "Usage: sock5 [-d] [-c filename]\n\n\t-d\tSwitch on debugging\n");
  fprintf(stderr, "\t-c\tSpecify alternative configuration file\n");
  fprintf(stderr, "\t-v\tDebug level\n");
}

/*
  Function: process_options
  
  Proceseaza optiunile setata prin  linia de comanda
  
*/
int process_options(int argc, char *argv[]) {
  int c;
  
  filename = strdup(DEF_ETCFILE);

  while ((c = getopt( argc, argv, "c:dfv:l:" )) != EOF) {
    switch(c) {
      case 'c':
        free(filename);
        filename = strdup(optarg);
        break;
      case 'd':
        var_debug = true;
        break;
      case '?':
      default:
        usage();
        return false;
    }
  }
  argc -= optind;
  argv += optind;
  
  if(argc > 1) {
    usage();
    return false;
  }
  
  return true;
}

/*
  Function: do_pid_file
  
  Testeaza unicitatea rurarii procesului pe server
  
*/
int do_pid_file(void) {
  char tmp_pid_filename[256];
  FILE *file = NULL;
  
  sprintf(tmp_pid_filename, "%s.pid", PID_FILE);
  if(access(tmp_pid_filename, F_OK) == 0) {
    char buffer[80];
    int pid;
    
    /* file exists */
    file = fopen(tmp_pid_filename, "r");
    fgets(buffer, sizeof(buffer), file);
    fclose(file);
    
    pid = strtoul(buffer, NULL, 10);
    log(LOG_INFO, "found pid-file with pid %d\n", pid);
    
    if(kill(pid, 0) == -1) {
      log(LOG_INFO, "process %d doesn't exist anymore", pid);
    } else {
      log(LOG_ERR, "process %d is still running", pid);
      return 0;
    }
  }
  if((file = fopen(tmp_pid_filename, "w")) != NULL) {
    fprintf(file, "%d\n", (int) getpid());
    fclose(file);
  }
  return 1;
}

void signal_ignore(int sig) {
}

void sock5_server_stop(int sig) {
  switch(sig) {
    case SIGSEGV:
      log(LOG_ERR, "signal [%d], SEGMENTATION FAULT.", sig);
      exit(1);
      
    default:
      log(LOG_WARNING, "signal [%d], terminate process...", sig);
      pthread_mutex_destroy(&mysql_mutex);
      clean_config(cfg); cfg = NULL;
      exit(0);
      
  }
}

void signal_setup() {
  struct sigaction sa;
  
  SETSIG(SIGINT, sock5_server_stop, 0);
  SETSIG(SIGKILL, sock5_server_stop, 0);  
  SETSIG(SIGTERM, sock5_server_stop, 0);
  SETSIG(SIGSEGV, sock5_server_stop, 0);
  SETSIG(SIGILL, sock5_server_stop, 0);
}

int main(int argc, char* argv[]) {
  int server_pid = -1, retval = 0;
  
  server_pid = getpid();

  /* process user options */
  if(!process_options(argc, argv)) return 1;

  if(var_debug) strcpy(argv[0], "sock5 (debug)                        ");
  else strcpy(argv[0], "sock5                                ");
  argv[0][40] = '\0';

  /* read config file */
  if((cfg = read_config(&cfg, filename)) == NULL) {
    return 1;
  }
  
  /* check and create /var/run/progname.pid */
  if(!do_pid_file()) {
    log(LOG_ERR, "program already running or stale pid-file");
    clean_config(cfg);
    return 1;
  }

  signal_setup();
  
  if(setpriority(PRIO_PROCESS, server_pid, -10) < 0) {
    log(LOG_WARNING, "can not set priority: %s", strerror(errno));
  }
  
  pthread_mutex_init(&mysql_mutex, NULL);
  
  if(cfg->mysql_host != NULL && cfg->mysql_username != NULL && cfg->mysql_password != NULL && cfg->mysql_database != NULL) {
    if((conn = mysql_init(NULL)) != NULL) {
      unsigned int connect_timeout = cfg->mysql_connect_timeout, readwrite_timeout = cfg->mysql_readwrite_timeout;
      my_bool reconnect = true;
      mysql_options(conn, MYSQL_OPT_CONNECT_TIMEOUT, (char*)(&connect_timeout));
      mysql_options(conn, MYSQL_OPT_READ_TIMEOUT, (char*)(&readwrite_timeout)); 
      mysql_options(conn, MYSQL_OPT_WRITE_TIMEOUT, (char*)(&readwrite_timeout));
      mysql_options(conn, MYSQL_OPT_RECONNECT, (my_bool*)(&reconnect));
      mysql_options(conn, MYSQL_OPT_LOCAL_INFILE, NULL);
      mysql_options(conn, MYSQL_OPT_USE_EMBEDDED_CONNECTION, NULL);
      if(mysql_real_connect(conn, cfg->mysql_host, cfg->mysql_username, cfg->mysql_password, cfg->mysql_database, cfg->mysql_port, NULL, CLIENT_MULTI_RESULTS | CLIENT_MULTI_STATEMENTS | CLIENT_REMEMBER_OPTIONS)) {
        if(conn->net.fd != 0) {
          struct timeval tv;
          tv.tv_sec = readwrite_timeout; tv.tv_usec = 0;
          
          setsockopt(conn->net.fd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(struct timeval));
          setsockopt(conn->net.fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));
        }
        if(mysql_autocommit(conn, true) == 0) {
          log(LOG_INFO, "connected to \"%s@%s\" MySQL server with autocommit off", cfg->mysql_host, cfg->mysql_database);
        } else {
          log(LOG_INFO, "connected to \"%s@%s\" MySQL server with autocommit on", cfg->mysql_host, cfg->mysql_database);
        }
        
        clear_transfer_details(conn, -1, cfg);
        
        sock5_process_server(cfg, conn);
        
        mysql_close(conn); conn = NULL;
      } else {
        log(LOG_ERR, "can not connect to MySQL server: %s\n", mysql_error(conn));
        mysql_close(conn); conn = NULL;
      }
    } else {
      log(LOG_ERR, "can not init MySQL: %s", strerror(errno));
    }
  } else {
    log(LOG_ERR, "can not init MySQL, some of the initialization parameters are missing");
  }

  sock5_server_stop(SIGTERM);
  
  return retval;
}
