#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <dirent.h>
#include <syslog.h>
#include <signal.h>
#include <fcntl.h>
#include <stdio.h>   
#include <time.h>  
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <math.h>
#include <stdarg.h>
#include <pthread.h>

#include "sock5.h"

/*
  Function: debug
  
  Daca proramul este in mod de debug, atunci afiseaza informatii pe ecran
  
*/
void _log(const char* file, unsigned int line, const int lvl, const char *format, ...) {
  va_list args;
  
#ifdef DEBUG
  if(var_debug) {
    struct timeval curent;
    struct tm *curent_tm = NULL;
        
    gettimeofday(&curent, NULL);
    curent_tm = localtime(&curent.tv_sec);
    fprintf(stdout, "[%010lX] [%02d:%02d:%02d.%08ld] [%10s][%4d] ", (unsigned long int)pthread_self(), curent_tm->tm_hour, curent_tm->tm_min, curent_tm->tm_sec, curent.tv_usec, file, line);
    va_start(args, format);
    vfprintf(stdout, format, args);
    va_end(args);
    fprintf(stdout, "\n");
  } else if(cfg != NULL && cfg->config_filename != NULL && lvl == LOG_ERR) {
    
  }
#else
  if(cfg != NULL && cfg->config_filename != NULL && lvl == LOG_ERR) {
    
  }
#endif
}

char *intoa(unsigned long addr) {
  static char buffer[18];
  char *p = (char*) &addr;
  
  sprintf(buffer, "%d.%d.%d.%d", (p[0] & 255), (p[1] & 255), (p[2] & 255), (p[3] & 255));
  return buffer;
}

void try_close(int sockfd) {
  if(sockfd != -1) {
    shutdown(sockfd, SHUT_RDWR);
    close(sockfd);
  }
}
                                