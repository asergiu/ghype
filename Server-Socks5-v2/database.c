#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <syslog.h>
#include <signal.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <stdarg.h>
#include <pthread.h>
#include <openssl/md5.h>

#include "sock5.h"

pthread_mutex_t 	mysql_mutex;

#define MD5_BUFFER_LENGTH	1024
char* calculate_md5_hash(int sockfd, int fd, unsigned long int filesize, char* client_hash, unsigned char *hash_length) {
  MD5_CTX c;
  char buf[MD5_BUFFER_LENGTH], *result = NULL;
  int rBytes = -1, remBytes = 0, error = false, i = -1;
  unsigned char out[MD5_DIGEST_LENGTH];
  
  lseek(fd, 0, SEEK_SET);
  
  if(MD5_Init(&c) == 1) {
    remBytes = filesize;
    if(remBytes < MD5_BUFFER_LENGTH) rBytes = read(fd, buf, remBytes);
    else rBytes = read(fd, buf, MD5_BUFFER_LENGTH);
    while(rBytes > 0 && remBytes > 0) {
      remBytes = remBytes - rBytes;
      if(MD5_Update(&c, buf, rBytes) == 1) {
        if(remBytes < MD5_BUFFER_LENGTH) rBytes = read(fd, buf, remBytes);
        else rBytes = read(fd, buf, MD5_BUFFER_LENGTH);
      } else {
        error = true;
        break;
      }
    }
    if(MD5_Final(out, &c) == 0) {
      error = true;
    }
  } else {
    error = true;
  }
  
  if(!error) {
    if((result = (char*)malloc(2 * MD5_DIGEST_LENGTH + 1)) != NULL) {
      memset(result, '\0', 2 * MD5_DIGEST_LENGTH + 1);
      for(i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(result + strlen(result), "%02x", out[i]);
      }
      *hash_length = 2 * MD5_DIGEST_LENGTH;
      log(LOG_INFO, "[%02d] calculated hash on %lu bytes is \"%s\" vs \"%s\"", sockfd, filesize, result, client_hash);
    } else {
      result = NULL;
      *hash_length = 0;
    }
  } else {
    result = NULL;
    *hash_length = 0;
  }
  
  return result;
}

void clear_transfer_details(MYSQL *conn, int sockfd, struct config *cfg) {
  char sql_query[1024];
  MYSQL_RES* mysql_res = NULL;
  int retry_mysql_connect_count = 0, retry_mysql_connect = true;
  
  if(pthread_mutex_lock(&mysql_mutex) == 0) {
    snprintf(sql_query, 1024, "update `file-transfer` set status = 'error', message = 'sock5 server error' where status = 'activate'");
    
    retry_mysql_connect_count = 0;
    retry_mysql_connect = true;   
    while( retry_mysql_connect ) {
      retry_mysql_connect = false;
                
      if(!((mysql_ping(conn) == 0) && (mysql_real_query(conn, sql_query, strlen(sql_query)) == 0))) {
        if((retry_mysql_connect_count++) < 3) {
          retry_mysql_connect = true;
          log(LOG_WARNING, "[%02d] can not execute MySQL query (%d): %s", sockfd, retry_mysql_connect_count, mysql_error(conn));
        } else {
          log(LOG_ERR, "[%02d] can not execute MySQL query: %s", sockfd, mysql_error(conn));
        }
      } else {
        if((mysql_res = mysql_store_result(conn)) != NULL) {
          mysql_free_result(mysql_res); mysql_res = NULL;
        }
        log(LOG_INFO, "MySQL query: \"%s\" executed", sql_query);
      }
    }
    pthread_mutex_unlock(&mysql_mutex);
  } else {
    log(LOG_ERR, "can not execute MySQL query: %s", strerror(errno));
  }
}

struct transfer_details* read_transfer_details(MYSQL *conn, int sockfd, struct config *cfg, char* domain_name, unsigned long int chunk_filesize, char* hash) {
  struct transfer_details *result = NULL;
  char sql_query[1024];
  int i = -1, retry_mysql_connect = true, retry_mysql_connect_count = 0, update_status_in_mysql = false;
  
  if(pthread_mutex_lock(&mysql_mutex) == 0) {
    retry_mysql_connect_count = 0;
    retry_mysql_connect = true;
    while( retry_mysql_connect ) {
      retry_mysql_connect = false;

      snprintf(sql_query, 1024, "SELECT `status`, `type`, `uniqueid`, `filesize` from `file-transfer` WHERE hash = '%s'", domain_name);
      if((mysql_ping(conn) == 0) && (mysql_real_query(conn, sql_query, strlen(sql_query)) == 0)) {
        MYSQL_RES* mysql_res = NULL;
        if((mysql_res = mysql_store_result(conn)) != NULL) {
          MYSQL_ROW mysql_row = NULL;
          
          if(mysql_num_rows(mysql_res) > 0) {
            for(i = 0; i < mysql_num_rows(mysql_res); i++) {
              if((mysql_row = mysql_fetch_row(mysql_res)) != NULL) {
                if(mysql_row[0] != NULL && mysql_row[1] != NULL && mysql_row[2] != NULL && mysql_row[3] != NULL) {

                  if((result = (struct transfer_details *)malloc(sizeof(struct transfer_details))) != NULL) {
                    unsigned long int filename_length = strlen(cfg->upload_directory) + strlen(mysql_row[2]) + 2;
                    
                    result->uniqueid = strdup(mysql_row[2]);
                    result->filename = NULL;
                    result->hash = NULL;
                    result->hash_length = 0;

                    if((result->filename = (char*)malloc(filename_length * sizeof(char))) != NULL) {
                      unsigned short int rwflag = 0;
                      
                      snprintf(result->filename, filename_length * sizeof(char), "%s/%s%c", cfg->upload_directory, mysql_row[2], '\0');
                      result->filesize = strtoul(mysql_row[3], NULL, 10);
                      result->read_bytes = 0;

                      if(strncasecmp(mysql_row[1], "push", 4) == 0) {
                        result->type = FILE_TRANSFER_TYPE_PUSH;
                        log(LOG_INFO, "[%02d] PUSH file", sockfd);
                        rwflag = O_RDWR | O_APPEND;
                      } else {
                        result->type = FILE_TRANSFER_TYPE_PULL;
                        log(LOG_INFO, "[%02d] PULL file", sockfd);
                        rwflag = O_RDONLY;
                      }

                      if((result->fd = open(result->filename, rwflag | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)) != -1) {
                        struct stat fd_stat;
                        
                        if(strncasecmp(mysql_row[0], "activate", 8) == 0) result->status = FILE_TRANSFER_ACTIVATE;
                        else if(strncasecmp(mysql_row[0], "finish", 6) == 0) result->status = FILE_TRANSFER_FINISH;
                        else result->status = FILE_TRANSFER_ERROR;
                        
                        if(fstat(result->fd, &fd_stat) != -1) {
                          if((result->partial_filesize = fd_stat.st_size) > 0 && result->partial_filesize <= result->filesize && chunk_filesize <= result->filesize) {
                            result->status = FILE_TRANSFER_PARTIAL;
                            if(result->type == FILE_TRANSFER_TYPE_PULL && chunk_filesize == 0) {
                              result->hash = calculate_md5_hash(sockfd, result->fd, result->partial_filesize, hash, &(result->hash_length));
                              result->partial_filesize = 0;
                              lseek(result->fd, result->partial_filesize, SEEK_SET);
                            } else if(result->partial_filesize < chunk_filesize) {
                              result->hash = calculate_md5_hash(sockfd, result->fd, result->partial_filesize, hash, &(result->hash_length));
                              lseek(result->fd, result->partial_filesize, SEEK_SET);
                            } else if(result->partial_filesize > chunk_filesize) {
                              result->hash = calculate_md5_hash(sockfd, result->fd, chunk_filesize, hash, &(result->hash_length));
                              result->partial_filesize = chunk_filesize;
                              lseek(result->fd, result->partial_filesize, SEEK_SET);
                            } else {
                              result->hash = calculate_md5_hash(sockfd, result->fd, chunk_filesize, hash, &(result->hash_length));
                              lseek(result->fd, chunk_filesize, SEEK_SET);
                            }
                          } else {
                            result->partial_filesize = 0;
                            result->status = FILE_TRANSFER_ACTIVATE;
                          }
                        } else {
                          result->partial_filesize = 0;
                          result->status = FILE_TRANSFER_ACTIVATE;
                        }
                        
                        if(result->partial_filesize == 0) {
                          truncate_transfer_details(conn, sockfd, cfg, result);
                        }
                        
                        if(result->hash == NULL) log(LOG_INFO, "[%02d] filename = \"%s\", size = %lu/%lu, status = \"%s\"[%d]", sockfd, result->filename, result->partial_filesize, result->filesize, mysql_row[0], result->status);
                        else log(LOG_INFO, "[%02d] filename = \"%s\", size = %lu/%lu, status = \"%s\"[%d], hash = \"%s\"", sockfd, result->filename, result->partial_filesize, result->filesize, mysql_row[0], result->status, result->hash);
                        
                        update_status_in_mysql = true;
                      } else {
                        log(LOG_ERR, "[%02d] can not open/create \"%s\": %s", sockfd, result->filename, strerror(errno));
                        free(result->filename);
                        free(result);
                        result = NULL;
                      }
                    } else {
                      log(LOG_ERR, "[%02d] memory allocation error: %s", sockfd, strerror(errno));
                      free(result);
                      result = NULL;
                    }
                  } else {
                    log(LOG_ERR, "[%02d] memory allocation error: %s", sockfd, strerror(errno));
                  }
                } else {
                  log(LOG_ERR, "[%02d] can not find record for hash \"%s\"", sockfd, domain_name);
                }
              } else {
                log(LOG_ERR, "[%02d] can not find record for hash \"%s\"", sockfd, domain_name);
              }
            }
          } else {
            log(LOG_ERR, "[%02d] can not find record for hash \"%s\"", sockfd, domain_name);
          }
          
          mysql_free_result(mysql_res);
        } else {
          log(LOG_ERR, "[%02d] can not fetch MySQL query: %s", sockfd, mysql_error(conn));
        }
      } else {
        if((retry_mysql_connect_count++) < 3) {
          retry_mysql_connect = true;
          log(LOG_WARNING, "[%02d] can not execute MySQL query (%d): %s", sockfd, retry_mysql_connect_count, mysql_error(conn));
        } else {
          log(LOG_ERR, "[%02d] can not execute MySQL query: %s", sockfd, mysql_error(conn));
        }
      }
    }
    pthread_mutex_unlock(&mysql_mutex);
  } else {
    log(LOG_ERR, "can not execute MySQL query: %s", strerror(errno));
  }
    
  if(result != NULL) {
    if(result->filename == NULL || result->uniqueid == NULL) {
      if(result->filename != NULL) free(result->filename);
      if(result->uniqueid != NULL) free(result->uniqueid);
      if(result->hash != NULL) free(result->hash);
      free(result); result = NULL;
    } else if(result->status == FILE_TRANSFER_FINISH || result->status == FILE_TRANSFER_ERROR) {
      if(result->filename != NULL) free(result->filename);
      if(result->uniqueid != NULL) free(result->uniqueid);
      if(result->hash != NULL) free(result->hash);
      free(result); result = NULL;
    } else if(update_status_in_mysql) {
      update_transfer_status(conn, sockfd, cfg, domain_name, result);
    }
  }

  return result;
}

void update_transfer_status(MYSQL *conn, int sockfd, struct config *cfg, char* domain_name, struct transfer_details* details) {
  char sql_query[1024];
  MYSQL_RES* mysql_res = NULL;
  int retry_mysql_connect = true, retry_mysql_connect_count = 0;

  snprintf(sql_query, 1024, "update `file-transfer` set status = 'partial', message = 'restarting transfer from %lu' where hash = '%s' and status = 'error'", details->partial_filesize, domain_name);

  if(pthread_mutex_lock(&mysql_mutex) == 0) {
    retry_mysql_connect_count = 0;
    retry_mysql_connect = true;
    while( retry_mysql_connect ) {
      retry_mysql_connect = false;
      if(!((mysql_ping(conn) == 0) && (mysql_real_query(conn, sql_query, strlen(sql_query)) == 0))) {
        if((retry_mysql_connect_count++) < 3) {
          retry_mysql_connect = true;
          log(LOG_WARNING, "[%02d] can not execute MySQL query (%d): %s", sockfd, retry_mysql_connect_count, mysql_error(conn));
        } else {
          log(LOG_ERR, "[%02d] can not execute MySQL query: %s", sockfd, mysql_error(conn));
        }
      } else {
        if((mysql_res = mysql_store_result(conn)) != NULL) {
          mysql_free_result(mysql_res); mysql_res = NULL;
        }
        log(LOG_INFO, "[%02d] MySQL query: \"%s\" executed", sockfd, sql_query);
      }
    }
    pthread_mutex_unlock(&mysql_mutex);
  } else {
    log(LOG_ERR, "can not execute MySQL query: %s", strerror(errno));
  }
}

void update_transfer_details(MYSQL *conn, int sockfd, struct config *cfg, char* domain_name, int error, struct transfer_details* details) {
  char sql_query[1024];
  MYSQL_RES* mysql_res = NULL;
  int retry_mysql_connect = true, retry_mysql_connect_count = 0;

  if(pthread_mutex_lock(&mysql_mutex) == 0) {
    if(details->partial_filesize == 0) {
      snprintf(sql_query, 1024, "update `file-transfer` set status = 'error', message = 'sock5 file transfer error' where hash = '%s'", domain_name);
    } else if(details->partial_filesize == details->filesize) {
      snprintf(sql_query, 1024, "update `file-transfer` set status = 'finish', message = NULL where hash = '%s'", domain_name);
    } else {
      snprintf(sql_query, 1024, "update `file-transfer` set status = 'partial', message = 'transfered only %lu bytes' where hash = '%s'", details->partial_filesize, domain_name);
    }
    
    retry_mysql_connect_count = 0;
    retry_mysql_connect = true;
    while( retry_mysql_connect ) {
      retry_mysql_connect = false;
      if(!((mysql_ping(conn) == 0) && (mysql_real_query(conn, sql_query, strlen(sql_query)) == 0))) {
        if((retry_mysql_connect_count++) < 3) {
          retry_mysql_connect = true;
          log(LOG_WARNING, "[%02d] can not execute MySQL query (%d){%d}: %s", sockfd, retry_mysql_connect_count, strlen(sql_query), mysql_error(conn));
        } else {
          log(LOG_ERR, "[%02d] can not execute MySQL query: %s", sockfd, mysql_error(conn));
        }
      } else {
        if((mysql_res = mysql_store_result(conn)) != NULL) {
          mysql_free_result(mysql_res); mysql_res = NULL;
        }
        log(LOG_INFO, "[%02d] MySQL query: \"%s\" executed", sockfd, sql_query);
      }
    }
    pthread_mutex_unlock(&mysql_mutex);
  } else {
    log(LOG_ERR, "can not execute MySQL query: %s", strerror(errno));
  }
}

int check_file_transfer(MYSQL *conn, int sockfd, struct config *cfg, char* uniqueid) {
  char sql_query[1024];
  int result = false, retry_mysql_connect = true, retry_mysql_connect_count = 0;

  if(pthread_mutex_lock(&mysql_mutex) == 0) {
    snprintf(sql_query, 1024, "SELECT `status` from `file-transfer` WHERE uniqueid = '%s' and type='push'", uniqueid);
    retry_mysql_connect = true;
    retry_mysql_connect_count = 0;
    while( retry_mysql_connect ) {
      retry_mysql_connect = false;
      log(LOG_INFO, "[%02d] MySQL try 1", sockfd);
      if((mysql_ping(conn) == 0) && (mysql_real_query(conn, sql_query, strlen(sql_query)) == 0)) {
        MYSQL_RES* mysql_res = NULL;
      log(LOG_INFO, "[%02d] MySQL try 2", sockfd);
        if((mysql_res = mysql_store_result(conn)) != NULL) {
          MYSQL_ROW mysql_row = NULL;
          
          if(mysql_num_rows(mysql_res) == 1) {
            if((mysql_row = mysql_fetch_row(mysql_res)) != NULL) {
              if(mysql_row[0] != NULL) {
                if(strncasecmp(mysql_row[0], "error", 5) != 0) {
                  result = true;
                }
              } else {
                log(LOG_ERR, "[%02d] has no results for uniqueid %s", sockfd, uniqueid);
              }
            } else {
              log(LOG_ERR, "[%02d] has no results for uniqueid %s", sockfd, uniqueid);
            }
          } else {
            log(LOG_ERR, "[%02d] has no results for uniqueid %s", sockfd, uniqueid);
          }
        } else {
          log(LOG_ERR, "[%02d] can not fetch MySQL query: %s", sockfd, mysql_error(conn));
        }
      } else {
        if((retry_mysql_connect_count++) < 3) {
          retry_mysql_connect = true;
          log(LOG_WARNING, "[%02d] can not execute MySQL query (%d): %s", sockfd, retry_mysql_connect_count, mysql_error(conn));
        } else {
          log(LOG_ERR, "[%02d] can not execute MySQL query: %s", sockfd, mysql_error(conn));
        }
      }
    }
    pthread_mutex_unlock(&mysql_mutex);
  } else {
    log(LOG_ERR, "can not execute MySQL query: %s", strerror(errno));
  }
  
  return result;
}

void truncate_transfer_details(MYSQL *conn, int sockfd, struct config *cfg, struct transfer_details *ftransfer) { 
  if(ftransfer->type == FILE_TRANSFER_TYPE_PUSH) {
    if(ftruncate(ftransfer->fd, 0) == -1) {
      log(LOG_INFO, "[%02d] can not truncate file: %s", sockfd, strerror(errno));
      lseek(ftransfer->fd, 0, SEEK_SET);
    } else {
      log(LOG_INFO, "[%02d] restarting file transfer from 0", sockfd);
      lseek(ftransfer->fd, 0, SEEK_SET);
      ftransfer->partial_filesize = 0;
    }
  } else {
    log(LOG_INFO, "[%02d] restarting file transfer from 0", sockfd);
    lseek(ftransfer->fd, 0, SEEK_SET);
    ftransfer->partial_filesize = 0;
  }
}

int seek_transfer_details(MYSQL *conn, int sockfd, struct config *cfg, unsigned char ack, unsigned long int client_filesize, char *client_hash, struct transfer_details *ftransfer) {
  if(ack == 0) {
    log(LOG_INFO, "[%02d] NACK received from client", sockfd);
    truncate_transfer_details(conn, sockfd, cfg, ftransfer);
    return false;
  } else {
    log(LOG_INFO, "[%02d] ACK received from client", sockfd);
    return true;
  }
}
