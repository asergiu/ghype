#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#define __USE_GNU 
#include <pthread.h>
#include <sys/syscall.h>

#include "defs.h"
#include "liblog.h"
#include "libthread.h"

static const UINT8 g_debug_str[8][8] = {
    "TESTS",
    "DEBUG",
    "INFOM",
    "ALARM",
    "WARNS",
    "ERROR",
    "UNDEF",
    "UNDEF",
};

static CS_T g_log_cs = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
static FILE *g_logfile = NULL;
static UINT64 g_dlevel = LEVEL_INFORM;
static UINT32 g_range_srart = 0;
static UINT32 g_range_end = -1;
//static UINT64 g_dlevel = LEVEL_DEBUG;

INT32 liblog_init(char *log_file)
{
    g_logfile = fopen(log_file, "a");
    if (g_logfile == NULL) {
        liblog_log(LEVEL_DEBUG | COLOR_GREEN, "failed to open log file \"%s\", logging to stdout only\n", log_file);
        return R_ERROR;
    } else {
        printf("\r");
        liblog_log(LEVEL_DEBUG | COLOR_GREEN, "open log file \"%s\" ok.\n", log_file);
    }
    return R_OK;
}

INT32 liblog_level(UINT64 level)
{
    g_dlevel =  ((UINT64)level<<LEVEL_POS) & LEVEL_MASK;
}

void liblog_range(UINT32 start, UINT32 end)
{
    g_range_srart = start;
    g_range_end = end;
}

UINT32 liblog_range_start()
{
    return g_range_srart;
}
UINT32 liblog_range_end()
{
    return g_range_end;
}

INT32 liblog_log(UINT64 mode, char *format, ...)
{
    UINT64      dlevel = -1;
    va_list     arg;

    dlevel = mode & LEVEL_MASK;
      
    if (g_dlevel > dlevel)
    {
        return R_OK;
    }
    
    if (LEVEL_ERROR == dlevel)
    {
        mode &= COLOR_MASK;
        mode |= COLOR_RED;
    }

    CS_ENTER(&g_log_cs);

    // show different color for shell console
    if (1 == isatty(STDOUT_FILENO))
    {
        INT8 color = (INT8)((mode & COLOR_MASK) >> COLOR_POS);
        if (0 == color) color = 9;
        fprintf(stdout, "\033[%d;49;%dm", (INT8)((mode & TEXT_MASK) >> TEXT_POS), 30 + color);
    }

    int tid = syscall(SYS_gettid);
    fprintf(stdout, "<%d> ", tid);
    if (g_logfile) fprintf(g_logfile, "<%d> ", tid);

    // show time
    if (TIME_SHOW == (mode & TIME_MASK))
    {
        INT8 cur_time[128];
        struct tm ptm = {0};
        time_t t = time(NULL);

        localtime_r(&t, &ptm);
        strftime(cur_time, 128, "[%H:%M:%S %Y-%m-%d] ",&ptm);
        fprintf(stdout, "%s", cur_time);
        if (g_logfile) fprintf(g_logfile, "%s", cur_time);
    
        // show debug level info
        if (g_logfile) fprintf(g_logfile, "[%s] ", g_debug_str[dlevel >> LEVEL_POS]);
        fprintf(stdout, "[%s] ", g_debug_str[dlevel >> LEVEL_POS]); 
    }

    va_start(arg, format);
    vfprintf(stdout, format, arg);
    va_end(arg);
    va_start(arg, format);
    if (g_logfile) vfprintf(g_logfile, format, arg);
    va_end(arg);
    
    if (1 == isatty(STDOUT_FILENO))
    {
        fprintf(stdout, "\033[0m");
    }
    
    fflush(stdout);
    if (g_logfile) fflush(g_logfile);
    
    CS_LEAVE(&g_log_cs);
    
    return 0;
}
