#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <ctype.h>
#include <signal.h>
#include <time.h>
#include <errno.h>

#include <fcntl.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <dirent.h>
#include <mysql/mysql.h>
#include <mysql/errmsg.h>

#define __USE_GNU
#include <ev.h>
#include <pthread.h>

#include "defs.h"
#include "liblog.h"
#include "socks5.h"
#include "libthread.h"

static INT32 g_state = SOCKS5_STATE_PREPARE;

static int g_sockfd = 0;
static socks5_cfg_t g_cfg = {0};
struct ev_loop *g_loop = NULL;  
struct ev_io g_io_accept;
MYSQL *g_db_conn;
static socks5_file_attrs_t **g_active_transfers = NULL;
static int g_transfers_count = 0;

static void help();
static INT32 check_para(int argc, char **argv);
static void signal_func(int sig);
static void signal_init();

static INT32 socks5_srv_init(UINT16 port, char *ip_addr, INT32 backlog);
static INT32 socks5_srv_exit(int sockfd);

static INT32 socks5_sockset(int sockfd);

static void read_config(socks5_cfg_t *config, char* fname);

static void accept_cb(struct ev_loop *loop, struct ev_io *watcher, int revents);
static void push_cb(struct ev_loop *loop, struct ev_io *watcher, int revents);
static void pull_cb(struct ev_loop *loop, struct ev_io *watcher, int revents);

static socks5_file_attrs_t fetch_file_attrs(char *hash);
static void free_file_attrs(socks5_file_attrs_t attrs);
static void process_client_callback(void *arg);

static void free_config(socks5_cfg_t *config);
static void free_watcher_param(socks5_watcher_param_t *wparam);

static INT32 execute_query(MYSQL *conn, char *query, int query_len);
static int setup_mysql_connection();

// active transfers vector
static CS_T g_transfers_cs = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
static int init_active_transfers();
static void add_transfer(socks5_file_attrs_t *transfer);
static socks5_file_attrs_t* find_transfer(char *unique_id);

int main(int argc, char **argv)
{
    signal_init();

    if (R_ERROR == check_para(argc, argv)) {
        PRINTF(LEVEL_ERROR, "check argument error.\n");
        return R_ERROR;
    }

    if (g_cfg.config_file == NULL) {
        help();
        return R_ERROR;
    }

    liblog_init(g_cfg.log_file);

    PRINTF(LEVEL_INFORM, "connecting to database...\n");
    if (R_ERROR == setup_mysql_connection()) {
        PRINTF(LEVEL_ERROR, "unable to setup database, exiting...\n");
        return R_ERROR;
    }
    PRINTF(LEVEL_INFORM, "database connection succeded\n");

    DIR *files_dir = opendir(g_cfg.files_dir);
    if (files_dir == NULL) {
        PRINTF(LEVEL_WARNING, "files directory does not exist, will create it.\n");
        if (mkdir(g_cfg.files_dir, S_IRWXU) < 0) {
            PRINTF(LEVEL_ERROR, "unable to create files directory %s: %s.\n", g_cfg.files_dir, strerror(errno));
            return R_ERROR;
        }
    } else {
        closedir(files_dir);
    }

    if ((g_sockfd = socks5_srv_init(g_cfg.port, g_cfg.ip_addr, 10)) == R_ERROR)  {
        PRINTF(LEVEL_ERROR, "socks server init error.\n");
        return R_ERROR;
    }

    if (init_active_transfers() == R_ERROR) {
        PRINTF(LEVEL_ERROR, "init active transfers list failed\n");
        return R_ERROR;
    }

    PRINTF(LEVEL_INFORM, "socks5 starting on port %d.\n", g_cfg.port);

    g_state = SOCKS5_STATE_RUNNING;

    g_loop = ev_default_loop(0);
    ev_io_init(&g_io_accept, accept_cb, g_sockfd, EV_READ);
    ev_io_start(g_loop, &g_io_accept);

    ev_loop(g_loop, 0); 

    PRINTF(LEVEL_INFORM, "exiting socks server.\n");
    socks5_srv_exit(g_sockfd);
    PRINTF(LEVEL_DEBUG, "freeing resources...\n");
    CS_DEL(&g_transfers_cs);
    mysql_close(g_db_conn);
    free_config(&g_cfg);
    return 0;
}

static int init_active_transfers() {
    if ((g_active_transfers = malloc(SOCKS5_TRANSFERS_COUNT * sizeof(socks5_file_attrs_t*))) == NULL) {
        return R_ERROR;
    }
    return R_OK;
}

static void add_transfer(socks5_file_attrs_t *transfer) {
    if (g_transfers_count > 0 && (g_transfers_count % SOCKS5_TRANSFERS_COUNT == 0)) {
        socks5_file_attrs_t **new_transfers = realloc(g_active_transfers, SOCKS5_TRANSFERS_COUNT);
        if (new_transfers == NULL) {
            PRINTF(LEVEL_ERROR, "realloc of active transfers failed, size is: %d\n", g_transfers_count);
            return;
        }
        g_active_transfers = new_transfers;
    }
    g_active_transfers[g_transfers_count++] = transfer;
}

static socks5_file_attrs_t* find_transfer(char *unique_id) {
    int i;
    for (i = 0; i < g_transfers_count; ++i) {
        if (strcmp(g_active_transfers[i]->uniqueid, unique_id) == 0) {
            return g_active_transfers[i];
        }
    }
    return NULL;
}

static void remove_transfer(char *unique_id) {
    int i, index = -1;
    socks5_file_attrs_t *transfer = NULL;
    for (i = 0; i < g_transfers_count; ++i) {
        if (strcmp(g_active_transfers[i]->uniqueid, unique_id) == 0) {
            index = i;
            break;
        }
    }
    if (index < 0) {
        PRINTF(LEVEL_WARNING, "transfer to be removed not found\n");
        return;
    }
    transfer = g_active_transfers[index];
    free(transfer->type); 
    // transfer->status does not need to be freed, since it's a string literal assigned in push_cb()
    free(transfer->uniqueid);
    free(transfer);

    for (i = index + 1; i < g_transfers_count; ++i) {
        g_active_transfers[i - 1] = g_active_transfers[i];
    }
    g_transfers_count--;
}

static int setup_mysql_connection() {
    g_db_conn = mysql_init(NULL);
    // set mysql db options
    UINT32 connect_timeout = 10;
    if (mysql_options(g_db_conn, MYSQL_OPT_CONNECT_TIMEOUT, &connect_timeout) != 0) {
        PRINTF(LEVEL_ERROR, "failed to set mysql connect timeout\n");
        return R_ERROR;
    }
    my_bool reconnect = 1;
    if (mysql_options(g_db_conn, MYSQL_OPT_RECONNECT, &reconnect) != 0) {
        PRINTF(LEVEL_ERROR, "failed to set mysql reconnect option\n");
        return R_ERROR;
    }

    if (!mysql_real_connect(g_db_conn, g_cfg.db_host, g_cfg.db_user, g_cfg.db_password, g_cfg.db_schema, g_cfg.db_port, NULL, 0)) {
        PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
        return R_ERROR;
    }
    return R_OK;
}

static socks5_file_attrs_t fetch_file_attrs(char *hash) {
    char query[QUERY_SIZE];
    int query_len = 0;
    socks5_file_attrs_t attrs = {0};
    query_len = snprintf(query, QUERY_SIZE, "SELECT `status`, `type`, `uniqueid`, `filesize` from `file-transfer` WHERE hash = '%s'", hash);
    if (execute_query(g_db_conn, query, query_len) != 0) {
        PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
        return attrs;
    }
    MYSQL_RES *res = mysql_use_result(g_db_conn);
    if (res == NULL) {
        PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
        return attrs;
    }
    MYSQL_ROW row = mysql_fetch_row(res);
    if (row == NULL) {
        PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
        return attrs;
    }
    attrs.status = row[0] == NULL ? NULL : strdup(row[0]);
    attrs.type = row[1] == NULL ? NULL : strdup(row[1]);
    attrs.uniqueid = row == NULL ? NULL : strdup(row[2]);
    attrs.size = row == NULL ? -1 : atoi(row[3]);
    mysql_free_result(res);
    return attrs;
}

static void help()
{
    printf("Usage: socks5 [options]\n");
    printf("Options:\n");
    printf("    -D              enable debugging\n");
    printf("    -c <file>       configuration file\n");
    printf("    -h              print help information\n");
}

static INT32 check_para(int argc, char **argv)
{
    int ch;
    while ((ch = getopt(argc, argv, "Dc:h")) != -1) {
        switch (ch) {
            case 'D':
                liblog_level(0);
                break;
            case 'c':
                read_config(&g_cfg, optarg);
                break;
            case 'h':
                help();
                exit(EXIT_SUCCESS);
                break;
            case '?':
                if (isprint(optopt))
                   printf("unknown option '-%c'.\n", optopt);
                else
                   printf("unknown option character '\\x%x'.\n", optopt);
                break;
            case ':':
                if (isprint(optopt))
                   printf("missing argment for '-%c'.\n", optopt);
                else
                   printf("missing argment for '\\x%x'.\n", optopt);
            default:
                break;
        }
    }

    return R_OK;
}

static void signal_init()
{
    int sig;
    // Ctrl + C
    sig = SIGINT;
    if (SIG_ERR == signal(sig, signal_func)) {
        PRINTF(LEVEL_ERROR, "%s signal[%d] failed.\n", __func__, sig);
    }
    // kill/pkill -15
    sig = SIGTERM;
    if (SIG_ERR == signal(sig, signal_func)) {
        PRINTF(LEVEL_ERROR, "%s signal[%d] failed.\n", __func__, sig);
    }
    // segmentation fault
    sig = SIGSEGV;
    if (SIG_ERR == signal(sig, signal_func)) {
        PRINTF(LEVEL_ERROR, "%s signal[%d] failed.\n", __func__, sig);
    }
}

static void signal_func(int sig)
{
    switch(sig) {
        case SIGINT:
        case SIGTERM:
            ev_io_stop(g_loop, &g_io_accept);
            ev_break(g_loop, EVBREAK_ALL);
            g_state = SOCKS5_STATE_STOP;
            PRINTF(LEVEL_INFORM, "signal [%d], exit.\n", sig);
            break;
        case SIGSEGV:
            PRINTF(LEVEL_ERROR, "signal [%d], SEGMENTATION FAULT.\n", sig);
            exit(1);
            break;
        default:
            PRINTF(LEVEL_INFORM, "signal [%d], not supported.\n", sig);
            break;
    }
}

static INT32 socks5_srv_init(UINT16 port, char* ip_addr, INT32 backlog)
{
    struct sockaddr_in serv;
    int sockfd;
    int opt;
    int flags;
    
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        PRINTF(LEVEL_ERROR, "socket error!\n");
        return R_ERROR;
    }

    bzero((char *)&serv, sizeof(serv));
    serv.sin_family = AF_INET;
    if (ip_addr != NULL) {
        if (inet_pton(AF_INET, ip_addr, &(serv.sin_addr)) != 1) {
            PRINTF(LEVEL_WARNING, "invalid IP address: %s\n", ip_addr);
            serv.sin_addr.s_addr = htonl(INADDR_ANY);
        }
    } else {
        serv.sin_addr.s_addr = htonl(INADDR_ANY);
    }
    serv.sin_port = htons(port);
    
    if (-1 ==(flags = fcntl(sockfd, F_GETFL, 0)))
        flags = 0;
    fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);
    
    opt = 1;
    if (-1 == setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (INT8 *)&opt, sizeof(opt))) {
        PRINTF(LEVEL_ERROR, "setsockopt SO_REUSEADDR fail.\n");
        return R_ERROR;
    }
    #ifdef SO_NOSIGPIPE 
    if (-1 == setsockopt(listen_sock, SOL_SOCKET, SO_NOSIGPIPE, &opt, sizeof(opt))) {
        PRINTF(LEVEL_ERROR, "setsockopt SO_NOSIGPIPE fail.\n");
        return R_ERROR;
    }
    #endif

    if (bind(sockfd, (struct sockaddr *)&serv, sizeof(serv)) < 0) {
        PRINTF(LEVEL_ERROR, "bind error %s\n", strerror(errno));
        return R_ERROR;
    }
    
    if (listen(sockfd, backlog) < 0) {
        PRINTF(LEVEL_ERROR, "listen error: %s\n", strerror(errno));
        return R_ERROR;
    }

    return sockfd;
}

static INT32 socks5_srv_exit(int sockfd)
{
    if (0 != sockfd)
        close(sockfd);

    return R_OK;  
}

static INT32 socks5_sockset(int sockfd)
{
    struct timeval tmo = {0};
    int opt = 1;
    
    tmo.tv_sec = 2;
    tmo.tv_usec = 0;
    if (-1 == setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tmo, sizeof(tmo)) \
        || -1 == setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&tmo, sizeof(tmo))) {
         PRINTF(LEVEL_ERROR, "setsockopt error.\n");
         return R_ERROR;
    }

#ifdef SO_NOSIGPIPE
    setsockopt(sockfd, SOL_SOCKET, SO_NOSIGPIPE, &opt, sizeof(opt));
#endif

    if (-1 == setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (INT8 *)&opt, sizeof(opt))) {
        PRINTF(LEVEL_ERROR, "setsockopt SO_REUSEADDR fail.\n");
        return R_ERROR;
    }
    
    return R_OK;
}

// returns the domain name
static char* socks5_auth(int sockfd)
{
    char buff[BUFFER_SIZE];
    UINT8 domain_len = 0;
    char *domain_name = NULL;
    UINT16 dest_port = 0;
    int ret = 0;

    socks5_sockset(sockfd);

    // VERSION and METHODS
    if (-1 == recv(sockfd, buff, 2, 0))
        GOTO_ERR;
    if (SOCKS5_VERSION != ((socks5_method_req_t *)buff)->ver)
        GOTO_ERR;
    ret = ((socks5_method_req_t *)buff)->nmethods;
    if (-1 == recv(sockfd, buff, ret, 0))
        GOTO_ERR;

    // no auth
    memcpy(buff, "\x05\x00", 2);
    if (-1 == send(sockfd, buff, 2, 0))
        GOTO_ERR;

    // REQUEST and REPLY
    if (-1 == recv(sockfd, buff, 4, 0))
        GOTO_ERR;
    //if (0x05 != buff[0] || 0x01 != buff[1]) //GOTO_ERR;
    if (SOCKS5_VERSION != ((socks5_request_t *)buff)->ver
        || SOCKS5_CMD_CONNECT != ((socks5_request_t *)buff)->cmd)
    {
        PRINTF(LEVEL_DEBUG, "ver : %d\tcmd = %d.\n", \
            ((socks5_request_t *)buff)->ver, ((socks5_request_t *)buff)->cmd);

        ((socks5_response_t *)buff)->ver = SOCKS5_VERSION;
        ((socks5_response_t *)buff)->cmd = SOCKS5_CMD_NOT_SUPPORTED;
        ((socks5_response_t *)buff)->rsv = 0;

        // cmd not supported
        send(sockfd, buff, 4, 0);
        goto _err;
    }

    if (SOCKS5_DOMAIN == ((socks5_request_t *)buff)->atype) {
        if (-1 == recv(sockfd, buff, 1, 0)) GOTO_ERR;
        domain_len = buff[0];
        buff[domain_len] = 0;
        if (-1 == recv(sockfd, buff, domain_len, 0)) GOTO_ERR;
        domain_name = strdup(buff);
        PRINTF(LEVEL_DEBUG, "domain name: [%.*s].\n", domain_len, domain_name);

        if (-1 == recv(sockfd, buff, 2, 0)) GOTO_ERR;
        memcpy(&dest_port, buff, 2);
    } else {
        ((socks5_response_t *)buff)->ver = SOCKS5_VERSION;
        ((socks5_response_t *)buff)->cmd = SOCKS5_ADDR_NOT_SUPPORTED;
        ((socks5_response_t *)buff)->rsv = 0;

        // cmd not supported
        send(sockfd, buff, 4, 0);
        GOTO_ERR;
    }

    // reply remote address info
    memcpy(buff, "\x05\x00\x00\x03", 4);
    memcpy(buff + 4, &domain_len, 1);
    memcpy(buff + 5, domain_name, domain_len);
    memcpy(buff + 5 + domain_len, &dest_port, 2);
    send(sockfd, buff, domain_len + 7, 0);

    return domain_name;

_err:
    return NULL;
}

static void accept_cb(struct ev_loop *loop, struct ev_io *watcher, int revents)
{
    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr);

    int client_fd = accept(watcher->fd, (struct sockaddr *)&client_addr, &client_len);
    if (client_fd < 0) {
        PRINTF(LEVEL_ERROR, "socket accept error: %s\n", strerror(errno));
        return;  
    }

    PRINTF(LEVEL_DEBUG, "accepted new connection\n");

    pthread_t client_thread;
    if (pthread_create(&client_thread, NULL, (void*)&process_client_callback, (void*)(intptr_t)client_fd) != 0) {
        PRINTF(LEVEL_ERROR, "error creating thread: %s\n", strerror(errno));
        return;
    }

    if (pthread_detach(client_thread) != 0) {
        PRINTF(LEVEL_ERROR, "error detaching thread: %s\n", strerror(errno));
    }
}

static void push_cb(struct ev_loop *loop, struct ev_io *watcher, int revents)
{
    if (watcher == NULL || watcher->data == NULL) {
        PRINTF(LEVEL_ERROR, "Watcher or watcher param is invalid, returning...\n");
        return;
    }
    char buffer[BUFFER_SIZE];
    int query_len = 0;
    char query[QUERY_SIZE];
    ssize_t read_count;
    socks5_watcher_param_t *param = (socks5_watcher_param_t*)watcher->data;
    socks5_file_attrs_t *attrs = NULL;

    read_count = recv(param->client_fd, buffer, BUFFER_SIZE, 0);
//    PRINTF(LEVEL_DEBUG, "push_cb, read count: %d\n", read_count);
    if (read_count > 0) {
        if (write(param->file_descriptor, buffer, read_count) < 0) {
            PRINTF(LEVEL_ERROR, "write to file rror: %s\n", strerror(errno));
            query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET status = 'error', message = 'SOCKS5: Push error (write).' WHERE hash = '%s'", param->hash);
            if (execute_query(g_db_conn, query, query_len) != 0) {
                PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
            }
            goto end;
        }
        param->bytes_to_read -= read_count;
    } else if(read_count == 0 && param->bytes_to_read > 0) {
        CS_ENTER(&g_transfers_cs);
        attrs = find_transfer(param->unique_id);
        CS_LEAVE(&g_transfers_cs);
        PRINTF(LEVEL_INFORM, "abort occured in PUSH, remaining bytes to transfer: %d\n", param->bytes_to_read);
        query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET status = 'error', message = 'SOCKS5: Abort on push.' WHERE hash = '%s'", param->hash);
        if (execute_query(g_db_conn, query, query_len) != 0) {
            PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
        }
        CS_ENTER(&g_transfers_cs);
        if (attrs) {
            free(attrs->status);
            attrs->status = strdup("error");
        }
        CS_LEAVE(&g_transfers_cs);
        goto end;
    } else if(read_count == -1 && ( errno == EAGAIN || errno == EWOULDBLOCK )) {
        PRINTF(LEVEL_INFORM, "EAGAIN/EWOULDBLOCK on recv, retry...\n");
    } else {
        CS_ENTER(&g_transfers_cs);
        attrs = find_transfer(param->unique_id);
        CS_LEAVE(&g_transfers_cs);
        if (attrs == NULL) {
            PRINTF(LEVEL_ERROR, "active transfer attributes not available for %s\n", param->unique_id);
        }
        if (param->bytes_to_read > 0) {
            PRINTF(LEVEL_INFORM, "error occured in PUSH, remaining bytes to transfer: %d, read_count = %d, error = %s\n", param->bytes_to_read, read_count, strerror(errno));
            query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET status = 'error', message = 'SOCKS5: Push error.' WHERE hash = '%s'", param->hash);
            if (execute_query(g_db_conn, query, query_len) != 0) {
                PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
            }
            CS_ENTER(&g_transfers_cs);
            if (attrs) {
                free(attrs->status);
                attrs->status = strdup("error");
            }
            CS_LEAVE(&g_transfers_cs);
            goto end;
        } else {
            socks5_file_attrs_t file_attrs = fetch_file_attrs(param->hash);
            PRINTF(LEVEL_INFORM, "PUSH completed successfully for %s, transferred %d bytes.\n", file_attrs.uniqueid, file_attrs.size);
            if (file_attrs.status != NULL && strcmp(file_attrs.status, "activate") == 0) {
                query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET status = 'finish' WHERE hash = '%s'", param->hash);
                if (execute_query(g_db_conn, query, query_len) != 0) {
                    PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
                }
            }
            free_file_attrs(file_attrs);
            CS_ENTER(&g_transfers_cs);
            if (attrs) {
                free(attrs->status);
                attrs->status = strdup("finish");
            }
            CS_LEAVE(&g_transfers_cs);
            goto end;
        }
    }

    return;

end:
    CS_ENTER(&g_transfers_cs);
    if (attrs && attrs->readers_count <= 0) {
        remove_transfer(param->unique_id);
    }
    CS_LEAVE(&g_transfers_cs);

    ev_io_stop(loop, watcher);
    ev_loop_destroy(loop);
}

static void pull_cb(struct ev_loop *loop, struct ev_io *watcher, int revents)
{
    if (watcher == NULL || watcher->data == NULL) {
        PRINTF(LEVEL_ERROR, "Watcher or watcher param is invalid, returning...\n");
        return;
    }
    char buffer[BUFFER_SIZE], query[QUERY_SIZE], *ptr = NULL;
    ssize_t read_count, sent_count, remain_read_count;
    int query_len = 0;
    socks5_watcher_param_t *param = (socks5_watcher_param_t*)watcher->data;
    CS_ENTER(&g_transfers_cs);
    socks5_file_attrs_t *attrs = find_transfer(param->unique_id);
    CS_LEAVE(&g_transfers_cs);

    if (attrs != NULL && strcmp(attrs->status, "error") == 0) {
        query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET status = 'error', message = 'SOCKS5: Push error.' WHERE hash = '%s'", param->hash);
        if (execute_query(g_db_conn, query, query_len) != 0) {
            PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
        }
        goto end;
    }

    read_count = read(param->file_descriptor, buffer, BUFFER_SIZE);
//    PRINTF(LEVEL_DEBUG, "pull_cb, read count: %d\n", read_count);
    if (read_count > 0) {
        remain_read_count = read_count;
        ptr = buffer;
        while(remain_read_count > 0) {
            if ((sent_count = send(param->client_fd, ptr, remain_read_count, 0)) < 0) {
                if(errno == EAGAIN || errno == EWOULDBLOCK) {
                    PRINTF(LEVEL_INFORM, "received EAGAIN/EWOULDBLOCK, retry...\n");
                } else {
                    PRINTF(LEVEL_ERROR, "send error: %s\n", strerror(errno));
                    query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET status = 'error', message = 'SOCKS5: Pull error (send).' WHERE hash = '%s'", param->hash);
                    if (execute_query(g_db_conn, query, query_len) != 0) {
                        PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
                    }
                    goto end;
                }
            } else {
                if(sent_count != read_count) {
                    PRINTF(LEVEL_ERROR, "sent %d from %d\n", sent_count, read_count);
                }
                remain_read_count = remain_read_count - sent_count;
                ptr = ptr + sent_count;
            }
        }
        param->bytes_to_read -= read_count;
    } else {
        if (param->bytes_to_read > 0) {
            query_len = snprintf(query, QUERY_SIZE, "SELECT `status` FROM `file-transfer` WHERE uniqueid = '%s' AND type='push'", param->unique_id);
            if (execute_query(g_db_conn, query, query_len) != 0) {
                PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
            }
            MYSQL_RES *res = mysql_use_result(g_db_conn);
            if (res == NULL) {
                PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
                return;
            }
            MYSQL_ROW row = mysql_fetch_row(res);
            if (row == NULL) {
                PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
                return;
            }
            char *push_status = row[0] == NULL ? NULL : strdup(row[0]);
            mysql_free_result(res);

            if (push_status == NULL || strcmp(push_status, "error") == 0) {
                PRINTF(LEVEL_INFORM, "push error, stopping transfer\n");
                query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET status = 'error', message = 'SOCKS5: Push error.' WHERE hash = '%s'", param->hash);
                if (execute_query(g_db_conn, query, query_len) != 0) {
                    PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
                }
                free(push_status);
                push_status = NULL;
            } else {
                PRINTF(LEVEL_DEBUG, "another thread is writing to file, waiting 3 seconds and retrying...\n");
                free(push_status);
                push_status = NULL;
                sleep(3);
                return;
            }
        } else {
            socks5_file_attrs_t file_attrs = fetch_file_attrs(param->hash);
            PRINTF(LEVEL_INFORM, "PULL completed successfully for %s, transferred %d bytes.\n", file_attrs.uniqueid, file_attrs.size);
            if (file_attrs.status != NULL && strcmp(file_attrs.status, "activate") == 0) {
                query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET status = 'finish' WHERE hash = '%s'", param->hash);
                if (execute_query(g_db_conn, query, query_len) != 0) {
                    PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
                }
            }
            free_file_attrs(file_attrs);
        }
        goto end;
    }

    return;

end:
    CS_ENTER(&g_transfers_cs);
    if (attrs) {
        attrs->readers_count--;
        if (attrs->readers_count <= 0) {
            remove_transfer(param->unique_id);
        }
    }
    CS_LEAVE(&g_transfers_cs);

    ev_io_stop(loop, watcher);
    ev_loop_destroy(loop);
}

static void read_config(socks5_cfg_t *config, char* fname)
{
    FILE *file;
    if (config == NULL) {
      return;
    }
    free(config->config_file);
    config->config_file = strdup(fname);

    if((file = fopen(fname, "r")) != NULL) {
      char buffer[1024];
      int line = 0;
      while(fgets(buffer, sizeof(buffer), file)) {
        char *cmt = strchr(buffer, '\n');
        if(cmt) *cmt = '\0';

        line++;

        cmt = strchr(buffer, '#');
        if(cmt) {
          if(cmt == buffer) *cmt = '\0';
          if(cmt && ( *(cmt-1) == '\n' || *(cmt-1) == '\r' || isspace(*(cmt-1)))) *cmt = '\0';
        }

        while(isspace(*buffer)) memmove(buffer, buffer+1, strlen(buffer));
        if(!*buffer) continue;

        cmt = strchr(buffer, '\0');
        cmt--;
        while(isspace(*cmt)) {
          *cmt = '\0';
          cmt--;
        }

        if(*buffer) {
          char *kwd = buffer;
          char *value = buffer + strcspn(buffer, " =\t");
          *value++ = '\0';
          while(isspace(*value) || *value == '=') value++;

         if(strcasecmp(kwd, "IPAddress") == 0) {
            free(config->ip_addr);
            config->ip_addr = strdup(value);
            PRINTF(LEVEL_DEBUG, "IPAddress is \"%s\"\n", config->ip_addr);
          } else if(strcasecmp(kwd, "Port") == 0) {
            config->port = atoi(value);
            PRINTF(LEVEL_DEBUG, "Port is \"%d\"\n", config->port);
          } else if(strcasecmp(kwd, "FilesDirectory") == 0) {
            free(config->files_dir);
            config->files_dir = strdup(value);
            PRINTF(LEVEL_DEBUG, "FilesDirectory is \"%s\"\n", config->files_dir);
          } else if(strcasecmp(kwd, "JID") == 0) {
             free(config->jid);
             config->jid = strdup(value);
             PRINTF(LEVEL_DEBUG, "JID is \"%s\"\n", config->jid);
          } else if(strcasecmp(kwd, "DBHost") == 0) {
             free(config->db_host);
             config->db_host = strdup(value);
             PRINTF(LEVEL_DEBUG, "DBHost is \"%s\"\n", config->db_host);
         } else if(strcasecmp(kwd, "DBPort") == 0) {
             config->db_port = atoi(value);
             PRINTF(LEVEL_DEBUG, "DBPort is \"%d\"\n", config->db_port);
          } else if(strcasecmp(kwd, "DBUser") == 0) {
              free(config->db_user);
              config->db_user = strdup(value);
              PRINTF(LEVEL_DEBUG, "DBUser is \"%s\"\n", config->db_user);
         } else if(strcasecmp(kwd, "DBPassword") == 0) {
             free(config->db_password);
             config->db_password = strdup(value);
             PRINTF(LEVEL_DEBUG, "DBPassword is \"%s\"\n", config->db_password);
         } else if(strcasecmp(kwd, "DBSchema") == 0) {
             free(config->db_schema);
             config->db_schema = strdup(value);
             PRINTF(LEVEL_DEBUG, "DBSchema is \"%s\"\n", config->db_schema);
         } else if(strcasecmp(kwd, "LogFile") == 0) {
             free(config->log_file);
             config->log_file = strdup(value);
             PRINTF(LEVEL_DEBUG, "LogFile is \"%s\"\n", config->log_file);
          } else {
            PRINTF(LEVEL_DEBUG, "unknown keyword %s in line %d\n", kwd, line);
          }
        }
      }
      fclose(file);

    } else {
      PRINTF(LEVEL_DEBUG, "%s\n", strerror(errno));
    }
}

static void free_config(socks5_cfg_t *config)
{
    free(config->ip_addr);
    free(config->files_dir);
    free(config->config_file);
    free(config->jid);
    free(config->db_host);
    free(config->db_user);
    free(config->db_password);
    free(config->db_schema);
    free(config->log_file);
}

static void free_watcher_param(socks5_watcher_param_t *wparam)
{
    close(wparam->file_descriptor);
    close(wparam->client_fd);
    free(wparam->hash);
    free(wparam->filename);
    free(wparam->unique_id);
    free(wparam);
}

static void process_client_callback(void *arg)
{
    PRINTF(LEVEL_DEBUG, "processing client on a new thread...\n");
    int client_fd = (intptr_t)arg;
    char *hash = socks5_auth(client_fd);
    if (hash == NULL) {
        PRINTF(LEVEL_ERROR, "auth error.\n");
        free(hash);
        close(client_fd);
        return;
    }

    socks5_file_attrs_t file_attrs = fetch_file_attrs(hash);
    if (file_attrs.status == NULL || file_attrs.type == NULL || file_attrs.uniqueid == NULL || file_attrs.size == -1) {
        PRINTF(LEVEL_ERROR, "fetch file attributes failed\n");
        close(client_fd);
        free(hash);
        free_file_attrs(file_attrs);
        return;
    }
    PRINTF(LEVEL_DEBUG, "(status, type, uid, size): (\"%s\", \"%s\", \"%s\", \"%d\")\n",file_attrs.status, file_attrs.type, file_attrs.uniqueid, file_attrs.size);
    char query[QUERY_SIZE];
    int query_len = 0;
    if (strcmp(file_attrs.status, "activate") != 0 || (strcmp(file_attrs.type, "pull") != 0 && strcmp(file_attrs.type, "push") != 0)) {
        query_len = snprintf(query, QUERY_SIZE, "UPDATE `file-transfer` SET `message` = 'SOCKS5: Invalid status or type, file transfer not started.' WHERE hash = '%s'", hash);
        if (execute_query(g_db_conn, query, query_len) != 0) {
            PRINTF(LEVEL_ERROR, "%s\n", mysql_error(g_db_conn));
        }
        PRINTF(LEVEL_ERROR, "invalid status [%s], will not start file transfer\n");
        close(client_fd);
        free_file_attrs(file_attrs);
        free(hash);
        return;
    }
    struct ev_loop *client_loop = ev_loop_new(0);
    struct ev_io *w_client = (struct ev_io*)malloc(sizeof(struct ev_io));
    socks5_watcher_param_t *watcher_param = (socks5_watcher_param_t*)malloc(sizeof(socks5_watcher_param_t));
    w_client->data = watcher_param;
    watcher_param->hash = hash;
    watcher_param->client_fd = client_fd;

    int file_descriptor;
    int fn_len = strlen(g_cfg.files_dir) + strlen(file_attrs.uniqueid) + 2;
    char *filename = (char*)malloc(fn_len);
    snprintf(filename, fn_len, "%s/%s", g_cfg.files_dir, file_attrs.uniqueid);
    filename[fn_len - 1] = '\0';
    PRINTF(LEVEL_INFORM, "transferring file: \"%s\"\n", filename);
    int rwflag = strcmp(file_attrs.type, "pull") == 0 ? O_RDONLY : O_WRONLY;
    if ((file_descriptor = open(filename, rwflag | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)) == -1) {
        PRINTF(LEVEL_ERROR, "unable to open / create output file \"%s\"\n", filename);
        free_file_attrs(file_attrs);
        close(client_fd);
        free_watcher_param(watcher_param);
        free(w_client);
        ev_loop_destroy(client_loop);
        free(filename);
        return;
    }

    CS_ENTER(&g_transfers_cs);
    socks5_file_attrs_t *attrs = find_transfer(file_attrs.uniqueid);
    CS_LEAVE(&g_transfers_cs);
    if (attrs == NULL) {
        attrs = malloc(sizeof(socks5_file_attrs_t));
        attrs->status = strdup(file_attrs.status);
        attrs->type = strdup(file_attrs.type);
        attrs->uniqueid = strdup(file_attrs.uniqueid);
        attrs->size = file_attrs.size;
        attrs->readers_count = 0;
        CS_ENTER(&g_transfers_cs);
        add_transfer(attrs);
        CS_LEAVE(&g_transfers_cs);
    }

    watcher_param->file_descriptor = file_descriptor;
    watcher_param->filename = filename;
    watcher_param->bytes_to_read = file_attrs.size;
    watcher_param->unique_id = strdup(file_attrs.uniqueid);
    if (strcmp(file_attrs.type, "push") == 0) {
        ev_io_init(w_client, push_cb, client_fd, EV_READ);
    } else {
        ev_io_init(w_client, pull_cb, watcher_param->file_descriptor, EV_READ);
        attrs->readers_count++;
    }

    PRINTF(LEVEL_INFORM, "starting %s for file: %s\n", file_attrs.type, file_attrs.uniqueid);
    free_file_attrs(file_attrs);
    ev_io_start(client_loop, w_client);
    ev_loop(client_loop, 0);

    free_watcher_param(watcher_param);
    free(w_client);

    PRINTF(LEVEL_DEBUG, "event loop finished\n");
}

static INT32 execute_query(MYSQL *conn, char *query, int query_len)
{
    PRINTF(LEVEL_DEBUG, "executing query: %s\n", query);
    int max_fails = 3;
    do {
        if (mysql_ping(g_db_conn) == 0) {
            if (mysql_real_query(conn, query, query_len) == 0) {
                return 0;
            } else if (mysql_errno(g_db_conn) != CR_SERVER_LOST) {
                return 1;
            }
        }
        if (--max_fails > 0) {
            PRINTF(LEVEL_DEBUG, "db connection lost, retrying query...\n");
            usleep(10 * 1000);
        }
    } while (max_fails > 0);
    return 1;
}

static void free_file_attrs(socks5_file_attrs_t attrs) {
    free(attrs.type);
    attrs.type = NULL;
    free(attrs.status);
    attrs.status = NULL;
    free(attrs.uniqueid);
    attrs.uniqueid = NULL;
}
